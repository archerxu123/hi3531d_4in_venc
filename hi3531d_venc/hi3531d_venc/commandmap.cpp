#define LOG_TAG    "COMMANDMAP"

#include <fstream>

#include "commandmap.h"

char gClientIP[20] = "";
int  gClientPort = 0;

int repeat_time[8];
video_type v_type[8];
unsigned int u32DstFrameRate[8];
char msg[5] = "ok";
bool    gb_master = false;


int diff_index = 0;



/*
 * 功能： 注册各种指令（关键字），对应的处理函数
 * 输入： 无
 * 返回： 无
 * 日期： 2018.1.25
 * 作者： zh.sun
 */
CommandMap::CommandMap()
{
    //创建回复socket
    echoudp_ = new UDPSocket();
    echoudp_->CreateUDPClient(nullptr, 0); //设为非阻塞

    //创建一级关键字指令处理队列
    pfuncmdmap_.insert(make_pair("Type",            &CommandMap::TypeHandler));
    pfuncmdmap_.insert(make_pair("Function",        &CommandMap::FuncHandler));
    pfuncmdmap_.insert(make_pair("Value",           &CommandMap::ValueHandler));

    //创建媒体配置相关指令处理队列
    mediafuncmdmap_.insert(make_pair("setNet",           &CommandMap::SetNetHandler));
    mediafuncmdmap_.insert(make_pair("setVenc",          &CommandMap::SetVencHandler));
    mediafuncmdmap_.insert(make_pair("setAudio",         &CommandMap::SetAudioHandler));
    mediafuncmdmap_.insert(make_pair("setMulticast",     &CommandMap::SetMulticastHandler));
    mediafuncmdmap_.insert(make_pair("setSnap",          &CommandMap::SetSnapHandler));
    mediafuncmdmap_.insert(make_pair("setSnapMulticast", &CommandMap::SetSnapMulticastHandler));
    mediafuncmdmap_.insert(make_pair("setBasicConfig",   &CommandMap::SetBasicConfigHandler));
    mediafuncmdmap_.insert(make_pair("setViCrop",        &CommandMap::SetViCropHandler));
    mediafuncmdmap_.insert(make_pair("setOsd",           &CommandMap::SetOsdHandler));
    mediafuncmdmap_.insert(make_pair("getSystemMsg",     &CommandMap::GetMsgHandler));
    mediafuncmdmap_.insert(make_pair("getLog",           &CommandMap::GetLogHandler));
    mediafuncmdmap_.insert(make_pair("deleteConfig",     &CommandMap::DeleteConfigHandler));
    mediafuncmdmap_.insert(make_pair("setAuMulticast",   &CommandMap::SetAuMulticastHandler));
    mediafuncmdmap_.insert(make_pair("getLog",           &CommandMap::GetLogHandler));
    mediafuncmdmap_.insert(make_pair("getShellMsg",      &CommandMap::GetShellHandler));
    mediafuncmdmap_.insert(make_pair("setTickMaster",    &CommandMap::SetTickMasterHandler));


}

CommandMap::~CommandMap()
{
    if(echoudp_ != NULL)
        delete echoudp_;
}

/*
 * 功能： 执行指令对应的处理函数
 * 输入： string key： 指令关键词
 *       cJSON *value： json字符串
 * 返回： bool
 * 日期： 2018.1.25
 * 作者： zh.sun
 */
bool CommandMap::ProcessCmd(string key, cJSON *value)
{
    auto iter = pfuncmdmap_.find(key);
    if(iter != pfuncmdmap_.end()){
        return (this->*(iter->second))(value); //调用对应的函数指针
    }
    else
        return false;
}

bool CommandMap::TypeHandler(cJSON *value)
{
    if( !strcmp(value->string, "Type") ){
        if (cJSON_IsString(value) && (value->valuestring != NULL))
            cmdtype_ = value->valuestring;
        else
            cmdtype_ = "null";
    }

    return true;
}



bool CommandMap::FuncHandler(cJSON *value)
{
    if( !strcmp(value->string, "Function") ){
        if (cJSON_IsString(value) && (value->valuestring != NULL))
            cmdfunc_ = value->valuestring;
        else
            cmdfunc_ = "null";
    }
    COMMON_PRT("FuncHandler %s\n",value->valuestring);
    return true;
}

bool CommandMap::ValueHandler(cJSON *value)
{
    auto iter = mediafuncmdmap_.find(cmdfunc_);
    if(iter != mediafuncmdmap_.end()){
        COMMON_PRT("%s: %s", cmdfunc_.c_str(), cJSON_Print(value));
        return (this->*(iter->second))(value); //调用对应的函数指针
    }
    else
        return false;

    return true;
}

bool CommandMap::SetNetHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();


    cJSON *net;
    cJSON_ArrayForEach(net, value){
        cJSON *name = cJSON_GetObjectItemCaseSensitive(net, "name");
        if (name && cJSON_IsString(name)) {
            if(!strcmp(name->valuestring, "eth0")){
                cJSON *ip = cJSON_GetObjectItemCaseSensitive(net, "ip");
                if(ip && cJSON_IsString(ip) ){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Ip, ip->valuestring) ){
                        COMMON_PRT("set net->ip set failed");
                    }
                }

                cJSON *mask = cJSON_GetObjectItemCaseSensitive(net, "mask");
                if(mask && cJSON_IsString(mask)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Mask, mask->valuestring) ){
                        COMMON_PRT("set net->mask set failed");
                    }
                }

                cJSON *gateway = cJSON_GetObjectItemCaseSensitive(net, "gateway");
                if(gateway && cJSON_IsString(gateway)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Gateway, gateway->valuestring) ){
                        COMMON_PRT("set net->gateway set failed");
                    }
                }

                cJSON *dns = cJSON_GetObjectItemCaseSensitive(net, "dns");
                if(dns && cJSON_IsString(dns)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Dns, dns->valuestring) ){
                        COMMON_PRT("set net->dns set failed");
                    }
                }

                cJSON *mac = cJSON_GetObjectItemCaseSensitive(net, "mac");
                if(mac && cJSON_IsString(mac)){
                    if( !softwareconfig->SetConfig(SoftwareConfig::kEth0Mac, mac->valuestring) ){
                        COMMON_PRT("set net->mac set failed");
                    }
                }
            }
            else{
                COMMON_PRT("set net->name Invalid format");
                continue;
            }
        }
        else{
            COMMON_PRT("set net->name Invalid format");
            continue;
        }
    }

#ifdef __DEBUG__ // 在*.pro中定义,即 Makefile中指定
    softwareconfig->PrintConfig();
#endif // __DEBUG__

    softwareconfig->SaveNetConfig();
    this->LetMeTellYou(msg);
    return true;
}


void* input_ai(void *arg)
{
    // uint in = *(uint *)arg;
    HI_S32 in = *(HI_S32*)arg;
    printf(" input number:%d\n",in);
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    //himppmaster->StartLoop(VencStreamCnt*in, VencStreamCnt*(in+1), in);    //1-VENC
    // himppmaster->StartAudioEnc_1(0,in);
    himppmaster->StartAudioEnc(in);


    return NULL;
}


void* input1proc_(void *arg)
{
    //  uint in = *(uint *)arg;
    HI_S32 in = *(HI_S32*)arg;

    printf(" input number:%d\n",in);
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    himppmaster->StartLoop(VencStreamCnt*in, VencStreamCnt*(in+1), in);    //1-VENC
    printf("***********************loop*input1 exit\n");
    return NULL;
}

void CommandMap::ACK(const char *code, const char *msg)
{
    char sendmsg[128] = "";
    sprintf(sendmsg, code, msg);
    this->LetMeTellYou(sendmsg);
}

void CommandMap::ACK(const char *code, int errcode, const char *msg)
{
    char sendmsg[128] = "";
    sprintf(sendmsg, code, msg, errcode);
    this->LetMeTellYou(sendmsg);
}


/*
 * 统一UMP版本适用
 * */
bool swAudioFormat(int port)
{
    switch (port) {
        case 0:
            if(coderFormat[0] == AUDIO_NULL)
                viParam[0].AI_type = AUDIO_AAC;
            else if(coderFormat[0] == AUDIO_AAC)
                viParam[0].AI_type = AUDIO_G711A;
            else
                viParam[0].AI_type = AUDIO_NULL;
            break;
        case 1:
            if(coderFormat[1] == AUDIO_NULL)
                viParam[1].AI_type = AUDIO_AAC;
            else if(coderFormat[1] == AUDIO_AAC)
                viParam[1].AI_type = AUDIO_G711A;
            else
                viParam[1].AI_type = AUDIO_NULL;
            break;
        case 2:
            if(coderFormat[2] == AUDIO_NULL)
                viParam[2].AI_type = AUDIO_AAC;
            else if(coderFormat[2] == AUDIO_AAC)
                viParam[2].AI_type = AUDIO_G711A;
            else
                viParam[2].AI_type = AUDIO_NULL;
            break;
        case 3:
            if(coderFormat[3] == AUDIO_NULL)
                viParam[3].AI_type = AUDIO_AAC;
            else if(coderFormat[3] == AUDIO_AAC)
                viParam[3].AI_type = AUDIO_G711A;
            else
                viParam[3].AI_type = AUDIO_NULL;
            break;

        default:
            break;
    }

    printf("get vi %d rtsp param-->%d\n",port,viParam[port].AI_type);
    return true;
}

bool CommandMap::SetVencHandler(cJSON *value)
{
    this->LoadEchoUdp();
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    bool vencChg[4] = {false,false,false,false};
    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            vencChg[0] = true;
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");  //防止UMP端设置错误的编码分辨率，做矫正处理。优先处理编码宽X高 DST帧率

                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_W, min((HI_U32)venc_w->valueint,viSet[0].viStSize.u32Width)) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[0].stVencSize.u32Width = min((HI_U32)venc_w->valueint,viSet[0].viStSize.u32Width);
                        }
                    }

                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");


                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_H, min((HI_U32)venc_h->valueint,viSet[0].viStSize.u32Height)) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[0].stVencSize.u32Height = min((HI_U32)venc_h->valueint,viSet[0].viStSize.u32Height);
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_DstFrmRate, min((HI_U32)venc_dstframerate->valueint, viSet[0].viStRate))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[0].u32DstFrameRate[0] = min((HI_U32)venc_dstframerate->valueint, viSet[0].viStRate);
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");   //检测到同输入分辨率一致，上面设置的编码参数无效，则直接替换VI设置的宽X高 DST帧率和SRC帧率一致。
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_W,viSet[0].viStSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stVencSize.u32Width = viSet[0].viStSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_H, viSet[0].viStSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stVencSize.u32Height = viSet[0].viStSize.u32Height;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_DstFrmRate,viSet[0].viStRate)) {
                                COMMON_PRT("set venc destFrameRate failed");
                            }
                            else  {
                                viParam[0].u32DstFrameRate[0] = viSet[0].viStRate;
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[0].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[0].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[0].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[0].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[0].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint ;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[0].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[0].u32MinQp[0] = min_qp->valueint;
                        }

                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[0].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[0].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[0].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32BQp[0] = b_qp->valueint;
                        }
                    }

                }else if(!strcmp(name->valuestring, "sub0")){         //次码流
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");  //防止UMP端设置错误的编码分辨率，做矫正处理。优先处理编码宽X高 DST帧率

                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_W, min((HI_U32)venc_w->valueint,viSet[0].viStSize.u32Width)) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[0].stMinorSize.u32Width = min((HI_U32)venc_w->valueint,viSet[0].viStSize.u32Width);
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");

                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_H, min((HI_U32)venc_h->valueint,viSet[0].viStSize.u32Height)) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[0].stMinorSize.u32Height = min((HI_U32)venc_h->valueint,viSet[0].viStSize.u32Height);
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_DstFrmRate, min((HI_U32)venc_dstframerate->valueint, viSet[0].viStRate))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[0].u32DstFrameRate[1] = min((HI_U32)venc_dstframerate->valueint, viSet[0].viStRate);
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");   //检测到同输入分辨率一致，上面设置的编码参数无效，则直接替换VI设置的宽X高 DST帧率和SRC帧率一致。
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_W,viSet[0].viStSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stMinorSize.u32Width = viSet[0].viStSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_H, viSet[0].viStSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[0].stMinorSize.u32Height = viSet[0].viStSize.u32Height;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_DstFrmRate,viSet[0].viStRate)) {
                                COMMON_PRT("set venc destFrameRate failed");
                            }
                            else  {
                                viParam[0].u32DstFrameRate[1] = viSet[0].viStRate;
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[0].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[0].enType[1]  = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[0].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[0].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[0].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[0].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[0].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[0].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[0].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[0].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[0].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[0].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn1")){
            vencChg[1] = true;
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流1
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");  //防止UMP端设置错误的编码分辨率，做矫正处理。优先处理编码宽X高 DST帧率

                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_W, min((HI_U32)venc_w->valueint,viSet[1].viStSize.u32Width)) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[1].stVencSize.u32Width = min((HI_U32)venc_w->valueint,viSet[1].viStSize.u32Width);
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");

                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_H, min((HI_U32)venc_h->valueint,viSet[1].viStSize.u32Height)) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[1].stVencSize.u32Height = min((HI_U32)venc_h->valueint,viSet[1].viStSize.u32Height);
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_DstFrmRate, min((HI_U32)venc_dstframerate->valueint, viSet[1].viStRate))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[1].u32DstFrameRate[0] = min((HI_U32)venc_dstframerate->valueint, viSet[1].viStRate);
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");   //检测到同输入分辨率一致，上面设置的编码参数无效，则直接替换VI设置的宽X高 DST帧率和SRC帧率一致。
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_W,viSet[1].viStSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stVencSize.u32Width = viSet[1].viStSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_H, viSet[1].viStSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stVencSize.u32Height = viSet[1].viStSize.u32Height;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_DstFrmRate,viSet[1].viStRate)) {
                                COMMON_PRT("set venc destFrameRate failed");
                            }
                            else  {
                                viParam[1].u32DstFrameRate[0] = viSet[0].viStRate;
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[1].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[1].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[1].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[1].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[1].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[1].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[1].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[1].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[1].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[1].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流1
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");  //防止UMP端设置错误的编码分辨率，做矫正处理。优先处理编码宽X高 DST帧
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_W, min((HI_U32)venc_w->valueint,viSet[1].viStSize.u32Width)) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[1].stMinorSize.u32Width = min((HI_U32)venc_w->valueint,viSet[1].viStSize.u32Width);
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_H, min((HI_U32)venc_h->valueint,viSet[1].viStSize.u32Height)) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[1].stMinorSize.u32Height = min((HI_U32)venc_h->valueint,viSet[1].viStSize.u32Height);
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_DstFrmRate, min((HI_U32)venc_dstframerate->valueint, viSet[1].viStRate))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[1].u32DstFrameRate[1] = min((HI_U32)venc_dstframerate->valueint, viSet[1].viStRate);
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");   //检测到同输入分辨率一致，上面设置的编码参数无效，则直接替换VI设置的宽X高 DST帧率和SRC帧率一致。
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_W,viSet[1].viStSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stMinorSize.u32Width = viSet[1].viStSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_H, viSet[1].viStSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[1].stMinorSize.u32Height = viSet[1].viStSize.u32Height;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_DstFrmRate,viSet[1].viStRate)) {
                                COMMON_PRT("set venc destFrameRate failed");
                            }
                            else  {
                                viParam[1].u32DstFrameRate[1] = viSet[1].viStRate;
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[1].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[1].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[1].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[1].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[1].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[1].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[1].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[1].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[1].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[1].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[1].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[1].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn2")){
            vencChg[2] = true;
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流2
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");  //防止UMP端设置错误的编码分辨率，做矫正处理。优先处理编码宽X高 DST帧
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_W, min((HI_U32)venc_w->valueint,viSet[2].viStSize.u32Width)) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[2].stVencSize.u32Width = min((HI_U32)venc_w->valueint,viSet[2].viStSize.u32Width);
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_H, min((HI_U32)venc_h->valueint,viSet[2].viStSize.u32Height)) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[2].stVencSize.u32Height = min((HI_U32)venc_h->valueint,viSet[2].viStSize.u32Height);
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_DstFrmRate, min((HI_U32)venc_dstframerate->valueint, viSet[2].viStRate))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[0].u32DstFrameRate[2] = min((HI_U32)venc_dstframerate->valueint, viSet[2].viStRate);
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");   //检测到同输入分辨率一致，上面设置的编码参数无效，则直接替换VI设置的宽X高 DST帧率和SRC帧率一致。
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_W,viSet[2].viStSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stVencSize.u32Width = viSet[2].viStSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_H, viSet[2].viStSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stVencSize.u32Height = viSet[2].viStSize.u32Height;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_DstFrmRate,viSet[2].viStRate)) {
                                COMMON_PRT("set venc destFrameRate failed");
                            }
                            else  {
                                viParam[2].u32DstFrameRate[0] = viSet[2].viStRate;
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[2].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[2].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[2].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[2].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[2].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[2].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[2].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[2].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[2].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[2].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流2
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");  //防止UMP端设置错误的编码分辨率，做矫正处理。优先处理编码宽X高 DST帧率
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_W, min((HI_U32)venc_w->valueint,viSet[2].viStSize.u32Width)) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[2].stMinorSize.u32Width = min((HI_U32)venc_w->valueint,viSet[2].viStSize.u32Width);
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_H, min((HI_U32)venc_h->valueint,viSet[2].viStSize.u32Height)) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[2].stMinorSize.u32Height = min((HI_U32)venc_h->valueint,viSet[2].viStSize.u32Height);
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_DstFrmRate, min((HI_U32)venc_dstframerate->valueint, viSet[2].viStRate))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[2].u32DstFrameRate[1] = min((HI_U32)venc_dstframerate->valueint, viSet[2].viStRate);
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");   //检测到同输入分辨率一致，上面设置的编码参数无效，则直接替换VI设置的宽X高 DST帧率和SRC帧率一致。
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_W,viSet[2].viStSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stMinorSize.u32Width = viSet[2].viStSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_H, viSet[0].viStSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[2].stMinorSize.u32Height = viSet[2].viStSize.u32Height;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_DstFrmRate,viSet[2].viStRate)) {
                                COMMON_PRT("set venc destFrameRate failed");
                            }
                            else  {
                                viParam[2].u32DstFrameRate[1] = viSet[2].viStRate;
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[2].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[2].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[2].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[2].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[2].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[2].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[2].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[2].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[2].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[2].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[2].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[2].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn3")){
            vencChg[3] = true;
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){            //主码流3
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");  //防止UMP端设置错误的编码分辨率，做矫正处理。优先处理编码宽X高 DST帧率
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_W, min((HI_U32)venc_w->valueint,viSet[3].viStSize.u32Width)) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[3].stVencSize.u32Width = min((HI_U32)venc_w->valueint,viSet[3].viStSize.u32Width);
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_H, min((HI_U32)venc_h->valueint,viSet[3].viStSize.u32Height)) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[3].stVencSize.u32Height = min((HI_U32)venc_h->valueint,viSet[3].viStSize.u32Height);
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_DstFrmRate, min((HI_U32)venc_dstframerate->valueint, viSet[3].viStRate))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[3].u32DstFrameRate[0] = min((HI_U32)venc_dstframerate->valueint, viSet[3].viStRate);
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");   //检测到同输入分辨率一致，上面设置的编码参数无效，则直接替换VI设置的宽X高 DST帧率和SRC帧率一致。
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_W,viSet[3].viStSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stVencSize.u32Width = viSet[3].viStSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_H, viSet[3].viStSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stVencSize.u32Height = viSet[3].viStSize.u32Height;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_DstFrmRate,viSet[3].viStRate)) {
                                COMMON_PRT("set venc destFrameRate failed");
                            }
                            else  {
                                viParam[3].u32DstFrameRate[0] = viSet[3].viStRate;
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[3].venc_SAME_INPUT[0] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        viParam[3].enType[0] = (PAYLOAD_TYPE_E)venc_type->valueint;
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[3].u32Gop[0] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[3].u32BitRate[0] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[3].enRcMode[0] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[3].u32Profile[0] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[3].u32MinQp[0] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[3].u32MaxQp[0] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MinIProp[0] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MaxIProp[0] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32MinIQp[0] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[3].u32IQp[0] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[3].u32PQp[0] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32BQp[0] = b_qp->valueint;
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){         //次码流3
                    cJSON *venc_w = cJSON_GetObjectItemCaseSensitive(value, "venc_w");  //防止UMP端设置错误的编码分辨率，做矫正处理。优先处理编码宽X高 DST帧率
                    if (venc_w && cJSON_IsNumber(venc_w)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_W, min((HI_U32)venc_w->valueint,viSet[3].viStSize.u32Width)) ){
                            COMMON_PRT("set venc width set failed");
                        }
                        else {
                            viParam[3].stMinorSize.u32Width = min((HI_U32)venc_w->valueint,viSet[3].viStSize.u32Width);
                        }
                    }
                    cJSON *venc_h = cJSON_GetObjectItemCaseSensitive(value, "venc_h");
                    if (venc_h && cJSON_IsNumber(venc_h)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_H, min((HI_U32)venc_h->valueint,viSet[3].viStSize.u32Height)) ){
                            COMMON_PRT("set venc height set failed");
                        }
                        else {
                            viParam[3].stMinorSize.u32Height = min((HI_U32)venc_h->valueint,viSet[3].viStSize.u32Height);
                        }
                    }
                    cJSON *venc_dstframerate = cJSON_GetObjectItemCaseSensitive(value, "venc_dstframerate");
                    if (venc_dstframerate && cJSON_IsNumber(venc_dstframerate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_DstFrmRate, min((HI_U32)venc_dstframerate->valueint, viSet[3].viStRate))){
                            COMMON_PRT("set venc_dstframerate set failed");
                        }
                        else {
                            viParam[3].u32DstFrameRate[1] = min((HI_U32)venc_dstframerate->valueint, viSet[3].viStRate);
                        }
                    }
                    cJSON *venc_SAME_INPUT = cJSON_GetObjectItemCaseSensitive(value, "venc_same_as_input");   //检测到同输入分辨率一致，上面设置的编码参数无效，则直接替换VI设置的宽X高 DST帧率和SRC帧率一致。
                    if (venc_SAME_INPUT && cJSON_IsNumber(venc_SAME_INPUT)) {
                        if(venc_SAME_INPUT->valueint == 1){
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_W,viSet[3].viStSize.u32Width )){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stMinorSize.u32Width = viSet[3].viStSize.u32Width;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_H, viSet[3].viStSize.u32Height)){
                                COMMON_PRT("set venc height set failed");
                            }
                            else {
                                viParam[3].stMinorSize.u32Height = viSet[3].viStSize.u32Height;
                            }
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_DstFrmRate,viSet[3].viStRate)) {
                                COMMON_PRT("set venc destFrameRate failed");
                            }
                            else  {
                                viParam[3].u32DstFrameRate[1] = viSet[3].viStRate;
                            }
                        }
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_SAME_INPUT, venc_SAME_INPUT->valueint) ){
                            COMMON_PRT("set venc_SAME_INPUT set failed");
                        }
                        else {
                            printf("venc same as vi enable-->%d\n",venc_SAME_INPUT->valueint);

                            viParam[3].venc_SAME_INPUT[1] = (HI_BOOL)venc_SAME_INPUT->valueint;
                        }
                    }
                    cJSON *venc_type = cJSON_GetObjectItemCaseSensitive(value, "venc_type");
                    if (venc_type && cJSON_IsNumber(venc_type)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_VENC_Type, venc_type->valueint) ){
                            COMMON_PRT("set venc_type set failed");
                        }
                        else {
                            viParam[3].enType[1] = (PAYLOAD_TYPE_E)venc_type->valueint;
                        }
                    }
                    cJSON *gop = cJSON_GetObjectItemCaseSensitive(value, "gop");
                    if (gop && cJSON_IsNumber(gop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_Gop, gop->valueint) ){
                            COMMON_PRT("set gop set failed");
                        }
                        else {
                            viParam[3].u32Gop[1] = gop->valueint;
                        }
                    }
                    cJSON *bit_rate = cJSON_GetObjectItemCaseSensitive(value, "bit_rate");
                    if (bit_rate && cJSON_IsNumber(bit_rate)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_BitRate, bit_rate->valueint) ){
                            COMMON_PRT("set bit_rate set failed");
                        }
                        else {
                            viParam[3].u32BitRate[1] = bit_rate->valueint;
                        }
                    }
                    cJSON *rcmode = cJSON_GetObjectItemCaseSensitive(value, "rcmode");
                    if (rcmode && cJSON_IsNumber(rcmode)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_RcMode, rcmode->valueint) ){
                            COMMON_PRT("set rcmode set failed");
                        }
                        else {
                            viParam[3].enRcMode[1] = (SAMPLE_RC_E)rcmode->valueint;
                        }
                    }
                    cJSON *profile = cJSON_GetObjectItemCaseSensitive(value, "profile");
                    if (profile && cJSON_IsNumber(profile)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_Profile, profile->valueint) ){
                            COMMON_PRT("set profile set failed");
                        }
                        else {
                            viParam[3].u32Profile[1] = profile->valueint;
                        }
                    }
                    cJSON *min_qp = cJSON_GetObjectItemCaseSensitive(value, "min_qp");
                    if (min_qp && cJSON_IsNumber(min_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MIN_QP, min_qp->valueint) ){
                            COMMON_PRT("set min_qp set failed");
                        }
                        else {
                            viParam[3].u32MinQp[1] = min_qp->valueint;
                        }
                    }
                    cJSON *max_qp = cJSON_GetObjectItemCaseSensitive(value, "max_qp");
                    if (max_qp && cJSON_IsNumber(max_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MAX_QP, max_qp->valueint) ){
                            COMMON_PRT("set max_qp set failed");
                        }
                        else {
                            viParam[3].u32MaxQp[1] = max_qp->valueint;
                        }
                    }
                    cJSON *min_i_prop = cJSON_GetObjectItemCaseSensitive(value, "min_i_prop");
                    if (min_i_prop && cJSON_IsNumber(min_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MINI_PROP, min_i_prop->valueint) ){
                            COMMON_PRT("set min_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MinIProp[1] = min_i_prop->valueint;
                        }
                    }
                    cJSON *max_i_prop = cJSON_GetObjectItemCaseSensitive(value, "max_i_prop");
                    if (max_i_prop && cJSON_IsNumber(max_i_prop)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MAXI_PROP, max_i_prop->valueint) ){
                            COMMON_PRT("set max_i_prop set failed");
                        }
                        else {
                            viParam[3].u32MaxIProp[1] = max_i_prop->valueint;
                        }
                    }
                    cJSON *min_i_qp = cJSON_GetObjectItemCaseSensitive(value, "min_i_qp");
                    if (min_i_qp && cJSON_IsNumber(min_i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_MinI_Qp, min_i_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32MinIQp[1] = min_i_qp->valueint;
                        }
                    }
                    cJSON *i_qp = cJSON_GetObjectItemCaseSensitive(value, "i_qp");
                    if (i_qp && cJSON_IsNumber(i_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_I_QP, i_qp->valueint) ){
                            COMMON_PRT("set i_qp set failed");
                        }
                        else {
                            viParam[3].u32IQp[1] = i_qp->valueint;
                        }
                    }
                    cJSON *p_qp = cJSON_GetObjectItemCaseSensitive(value, "p_qp");
                    if (p_qp && cJSON_IsNumber(p_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_P_QP, p_qp->valueint) ){
                            COMMON_PRT("set p_qp set failed");
                        }
                        else {
                            viParam[3].u32PQp[1] = p_qp->valueint;
                        }
                    }
                    cJSON *b_qp = cJSON_GetObjectItemCaseSensitive(value, "b_qp");
                    if (b_qp && cJSON_IsNumber(b_qp)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_B_QP, b_qp->valueint) ){
                            COMMON_PRT("set min_i_qp set failed");
                        }
                        else {
                            viParam[3].u32BQp[1] = b_qp->valueint;
                        }
                    }
                }
            }
        }
    }
    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);
    for(int i = 0; i < g_vi_en_number; i++) {
        if(vencChg[i]) {
            vi_src_chang[i] = true;
            usleep(20000);
            himppmaster->retartVIVPSSVENC(i,HI_TRUE,HI_FALSE);  //只重启VENC
            usleep(20000);
            vi_src_chang[i] = false;
        }
    }
    return true;
}


/*
 *重启生效
 * 设置RTSP音频
 * chnx---  4路VI RTSP视频分别和HDMI0 HDIM1 3.5MM 音频MIX
 * ai_mode--- HDMI0 HDMI1 3.5MM 音频
 * ai_codeformat--- 音频编码格式 0 ---AAC  1--- G711A  3 ---NULL（无音频）
*/

bool CommandMap::SetAudioHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    this->LoadEchoUdp();

    bool rtsp_ai[VI_PORT_NUMBER] = {false,false,false,false};


    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *ai_codformat = cJSON_GetObjectItemCaseSensitive(value, "ai_codformat");
            if (ai_codformat && cJSON_IsNumber(ai_codformat)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_AiCoderFormat, ai_codformat->valueint) ){
                    COMMON_PRT("set audio->codformat set failed");
                }
                if(coderFormat[0] != (enum audio_type)ai_codformat->valueint) {
                    rtsp_ai[0] = true;
                }

                coderFormat[0] = (enum audio_type)ai_codformat->valueint;
                swAudioFormat(0);
            }
            cJSON *aimode = cJSON_GetObjectItemCaseSensitive(value, "ai_mode");
            if (aimode && cJSON_IsString(aimode)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_AiDev, aimode->valuestring) ){
                    COMMON_PRT("set audio->mode set failed");
                }
                if(!strcmp(aimode->valuestring,"HDMI")) {
                    if (versionFlag == MIX_4) {
                        viParam[0].AI_enable = HI_TRUE;
                        viParam[0].aiDev = 0;
                    }
                }
                else if(!strcmp(aimode->valuestring,"3.5mm")) {
                    if(versionFlag == HDMI_4) {
                        viParam[0].AI_enable = HI_TRUE;
                        viParam[0].aiDev = 0;
                    }
                    else if(versionFlag == MIX_4) {
                        viParam[0].AI_enable = HI_TRUE;
                        viParam[0].aiDev = 1;
                    }
                }
                else if(!strcmp(aimode->valuestring,"NULL")) {
                    viParam[0].AI_enable = HI_FALSE;
                    viParam[0].aiDev = -1;
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *ai_codformat = cJSON_GetObjectItemCaseSensitive(value, "ai_codformat");
            if (ai_codformat && cJSON_IsNumber(ai_codformat)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_AiCoderFormat, ai_codformat->valueint) ){
                    COMMON_PRT("set audio->codformat set failed");
                }

                if(coderFormat[1] != (enum audio_type)ai_codformat->valueint) {
                    rtsp_ai[1] = true;
                }

                coderFormat[1] = (enum audio_type)ai_codformat->valueint;
                swAudioFormat(1);
            }

            cJSON *aimode = cJSON_GetObjectItemCaseSensitive(value, "ai_mode");
            if (aimode && cJSON_IsString(aimode)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_AiDev, aimode->valuestring) ){
                    COMMON_PRT("set audio->mode set failed");
                }
                if(!strcmp(aimode->valuestring,"HDMI")) {
                    if (versionFlag == HDMI_4) {
                        viParam[1].AI_enable = HI_TRUE;
                        viParam[1].aiDev = 1;
                    }
                }
                else if(!strcmp(aimode->valuestring,"3.5mm")) {
                    if(versionFlag == HDMI_4) {
                        viParam[1].AI_enable = HI_TRUE;
                        viParam[1].aiDev = 0;
                    }
                    else if(versionFlag == MIX_4) {
                        viParam[1].AI_enable = HI_TRUE;
                        viParam[1].aiDev = 1;
                    }
                }
                else if(!strcmp(aimode->valuestring,"NULL")) {
                    viParam[1].AI_enable = HI_FALSE;
                    viParam[1].aiDev = -1;
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *ai_codformat = cJSON_GetObjectItemCaseSensitive(value, "ai_codformat");
            if (ai_codformat && cJSON_IsNumber(ai_codformat)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_AiCoderFormat, ai_codformat->valueint) ){
                    COMMON_PRT("set audio->codformat set failed");
                }
                if(coderFormat[2] != (enum audio_type)ai_codformat->valueint) {
                    rtsp_ai[2] = true;
                }

                coderFormat[2] = (enum audio_type)ai_codformat->valueint;
                swAudioFormat(2);
            }

            cJSON *aimode = cJSON_GetObjectItemCaseSensitive(value, "ai_mode");
            if (aimode && cJSON_IsString(aimode)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_AiDev, aimode->valuestring) ){
                    COMMON_PRT("set audio->mode set failed");
                }
                if(!strcmp(aimode->valuestring,"HDMI")) {
                }
                else if(!strcmp(aimode->valuestring,"3.5mm")) {
                    if(versionFlag == HDMI_4) {
                        viParam[2].AI_enable = HI_TRUE;
                        viParam[2].aiDev = 0;
                    }
                    else if(versionFlag == MIX_4) {
                        viParam[2].AI_enable = HI_TRUE;
                        viParam[2].aiDev = 1;
                    }
                }
                else if(!strcmp(aimode->valuestring,"NULL")) {
                    viParam[2].AI_enable = HI_FALSE;
                    viParam[2].aiDev = -1;
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *ai_codformat = cJSON_GetObjectItemCaseSensitive(value, "ai_codformat");
            if (ai_codformat && cJSON_IsNumber(ai_codformat)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_AiCoderFormat, ai_codformat->valueint) ){
                    COMMON_PRT("set audio->codformat set failed");
                }
                if(coderFormat[3] != (enum audio_type)ai_codformat->valueint) {
                    rtsp_ai[3] = true;
                }

                coderFormat[3] = (enum audio_type)ai_codformat->valueint;
                swAudioFormat(3);
            }

            cJSON *aimode = cJSON_GetObjectItemCaseSensitive(value, "ai_mode");
            if (aimode && cJSON_IsString(aimode)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_AiDev, aimode->valuestring) ){
                    COMMON_PRT("set audio->mode set failed");
                }
                if(!strcmp(aimode->valuestring,"HDMI")) {
                    if (versionFlag == HDMI_4) {
                        viParam[3].AI_enable = HI_TRUE;
                        viParam[3].aiDev = 2;
                    }
                    else if(versionFlag == MIX_4) {
                        viParam[3].AI_enable = HI_TRUE;
                        viParam[3].aiDev = 2;
                    }
                }
                else if(!strcmp(aimode->valuestring,"3.5mm")) {
                    if(versionFlag == HDMI_4) {
                        viParam[3].AI_enable = HI_TRUE;
                        viParam[3].aiDev = 0;
                    }
                    else if(versionFlag == MIX_4) {
                        viParam[3].AI_enable = HI_TRUE;
                        viParam[3].aiDev = 1;
                    }
                }
                else if(!strcmp(aimode->valuestring,"NULL")) {
                    viParam[3].AI_enable = HI_FALSE;
                    viParam[3].aiDev = -1;
                }
            }
        }

    }

    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);

    return HI_TRUE;
}

bool CommandMap::SetAuMulticastHandler(cJSON *value)
{
    bool nspc = false;

    cJSON *cmd = cJSON_GetObjectItemCaseSensitive(value, "cmd_frome");
    if(cmd && cJSON_IsString(cmd)) {
        if(!strcmp(cmd->valuestring,"nspc")) {
            nspc = true;
        }
    }
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "name");
    if(channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring,"hdmi0")) {
            cJSON *aumulticast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (aumulticast_enable && cJSON_IsNumber(aumulticast_enable)) {
                if(nspc) {
                    if(versionFlag == HDMI_4) {
                        if(aumulticast_enable->valueint) {
                            aiParam[1].Amcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
                        }
                        else {
                            aiParam[1].Amcast_enable = HI_FALSE;
                        }
                    }
                    else if(versionFlag == MIX_4) {
                        if(aumulticast_enable->valueint) {
                            aiParam[0].Amcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
                        }
                        else {
                            aiParam[0].Amcast_enable = HI_FALSE;
                        }
                    }
                    else {
                        if(aumulticast_enable->valueint) {
                            aiParam[0].Amcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi0_Au_McastEnable));
                        }
                        else {
                            aiParam[0].Amcast_enable = HI_FALSE;
                        }
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastEnable, aumulticast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    if(versionFlag == HDMI_4) {
                        aiParam[1].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }
                    else if(versionFlag == MIX_4) {
                        aiParam[0].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }
                    else {
                        aiParam[0].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }

                }
            }
            cJSON *aumulticast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if (aumulticast_ip && cJSON_IsString(aumulticast_ip)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastIp, aumulticast_ip->valuestring) ){
                    COMMON_PRT("set aumcast_ip set failed");
                }
                if(versionFlag == HDMI_4) {
                    memset(aiParam[1].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[1].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }
                else if(versionFlag == MIX_4) {
                    memset(aiParam[0].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[0].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }
                else {
                    memset(aiParam[0].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[0].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }
            }
            cJSON *aumulticast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (aumulticast_port && cJSON_IsNumber(aumulticast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi0_Au_McastPort, aumulticast_port->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                if(versionFlag == HDMI_4) {
                    aiParam[1].Amcast_port = aumulticast_port->valueint;
                }
                else if(versionFlag == MIX_4) {
                    aiParam[0].Amcast_port = aumulticast_port->valueint;
                }
                else {
                    aiParam[0].Amcast_port = aumulticast_port->valueint;
                }
            }
        }
        else if(!strcmp(channel->valuestring,"hdmi1")) {
            cJSON *aumulticast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (aumulticast_enable && cJSON_IsNumber(aumulticast_enable)) {
                if(nspc) {
                    if(versionFlag == HDMI_4) {
                        if(aumulticast_enable->valueint) {
                            aiParam[2].Amcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
                        }
                        else {
                            aiParam[2].Amcast_enable = HI_FALSE;
                        }
                    }
                    else if(versionFlag == MIX_4) {
                        if(aumulticast_enable->valueint) {
                            aiParam[2].Amcast_enable =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
                        }
                        else {
                            aiParam[2].Amcast_enable = HI_FALSE;
                        }
                    }
                    else {
                        if(aumulticast_enable->valueint) {
                            aiParam[1].Amcast_enable =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kHdmi1_Au_McastEnable));
                        }
                        else {
                            aiParam[1].Amcast_enable = HI_FALSE;
                        }
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastEnable, aumulticast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    if(versionFlag == HDMI_4) {
                        aiParam[2].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }
                    else if(versionFlag == MIX_4) {
                        aiParam[2].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }
                    else {
                        aiParam[1].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }

                }
            }
            cJSON *aumulticast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if (aumulticast_ip && cJSON_IsString(aumulticast_ip)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastIp, aumulticast_ip->valuestring) ){
                    COMMON_PRT("set aumcast_ip set failed");
                }
                if(versionFlag == HDMI_4) {
                    memset(aiParam[2].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[2].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }
                else if(versionFlag == MIX_4) {
                    memset(aiParam[2].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[2].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }
                else {
                    memset(aiParam[1].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[1].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }
            }
            cJSON *aumulticast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (aumulticast_port && cJSON_IsNumber(aumulticast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kHdmi1_Au_McastPort, aumulticast_port->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                if(versionFlag == HDMI_4) {
                    aiParam[2].Amcast_port = aumulticast_port->valueint;
                }
                else if(versionFlag == MIX_4) {
                    aiParam[2].Amcast_port = aumulticast_port->valueint;
                }
                else {
                    aiParam[1].Amcast_port = aumulticast_port->valueint;
                }

            }
        }
        else if(!strcmp(channel->valuestring,"3.5mm")) { //3.5mm组播设置
            cJSON *aumulticast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (aumulticast_enable && cJSON_IsNumber(aumulticast_enable)) {
                if(nspc) {
                    if(versionFlag == HDMI_4) {
                        if(aumulticast_enable->valueint) {
                            aiParam[0].Amcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastEnable));
                        }
                        else {
                            aiParam[0].Amcast_enable = HI_FALSE;
                        }
                    }
                    else if(versionFlag == MIX_4) {
                        if(aumulticast_enable->valueint) {
                            aiParam[1].Amcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastEnable));
                        }
                        else {
                            aiParam[1].Amcast_enable = HI_FALSE;
                        }
                    }
                    else {
                        if(aumulticast_enable->valueint) {
                            aiParam[2].Amcast_enable = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kAnalog_Au_McastEnable));
                        }
                        else {
                            aiParam[2].Amcast_enable = HI_FALSE;
                        }
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastEnable, aumulticast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    if(versionFlag == HDMI_4) {
                        aiParam[0].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }
                    else if(versionFlag == MIX_4) {
                        aiParam[1].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }
                    else {
                        aiParam[2].Amcast_enable = (HI_BOOL)aumulticast_enable->valueint;
                    }
                }
            }
            cJSON *aumulticast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if (aumulticast_ip && cJSON_IsString(aumulticast_ip)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastIp, aumulticast_ip->valuestring) ){
                    COMMON_PRT("set aumcast_ip set failed");
                }
                if(versionFlag == HDMI_4) {
                    memset(aiParam[0].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[0].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }
                else if(versionFlag == MIX_4) {
                    memset(aiParam[1].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[1].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }
                else {
                    memset(aiParam[2].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[2].Amcast_ip,"%s",aumulticast_ip->valuestring);
                }

            }
            cJSON *aumulticast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (aumulticast_port && cJSON_IsNumber(aumulticast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kAnalog_Au_McastPort, aumulticast_port->valueint) ){
                    COMMON_PRT("set aumcast_enable set failed");
                }
                if(versionFlag == HDMI_4) {
                    aiParam[0].Amcast_port = aumulticast_port->valueint;
                }
                else if(versionFlag == MIX_4) {
                    aiParam[1].Amcast_port = aumulticast_port->valueint;
                }
                else {
                    aiParam[2].Amcast_port = aumulticast_port->valueint;
                }
            }
        }
    }
    softwareconfig->SaveConfig();
    return true;
}

bool CommandMap::SetMulticastHandler(cJSON *value)
{

    bool nspc = false;
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    cJSON *cmd = cJSON_GetObjectItemCaseSensitive(value, "cmd_frome");
    if(cmd && cJSON_IsString(cmd)) {
        if(!strcmp(cmd->valuestring,"nspc")) {
            nspc = true;
        }
    }
    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {

                if(!strcmp(name->valuestring, "main")){

                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                viParam[0].mcast_enable[0] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                viParam[0].mcast_enable[0] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                            viParam[0].mcast_enable[0] =  (HI_BOOL)multicast_enable->valueint;
                        }
                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set multicast_ip set failed");
                        }
                        memset(viParam[0].mcast_ip[0],0,IPSIZE);
                        sprintf(viParam[0].mcast_ip[0],"%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set multicast_port len set failed");
                        }
                        viParam[0].mcast_port[0] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_M_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                viParam[0].mcast_enable[1] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                viParam[0].mcast_enable[1] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                            viParam[0].mcast_enable[1] =  (HI_BOOL)multicast_enable->valueint;
                        }
                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set mcast_ip set failed");
                        }
                        memset(viParam[0].mcast_ip[1],0,IPSIZE);
                        sprintf(viParam[0].mcast_ip[1], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set mcast_port len set failed");
                        }
                        viParam[0].mcast_port[1] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn0_S0_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_S0_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                viParam[1].mcast_enable[0] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                viParam[1].mcast_enable[0] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                            viParam[1].mcast_enable[0] =  (HI_BOOL)multicast_enable->valueint; // str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                            //  viParam[0].mcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastEnable));
                        }
                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set multicast_ip set failed");
                        }
                        memset(viParam[1].mcast_ip[0],0,IPSIZE);
                        sprintf(viParam[1].mcast_ip[0],"%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set multicast_port len set failed");
                        }
                        viParam[1].mcast_port[0] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn1_M_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_M_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                viParam[1].mcast_enable[1] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                viParam[1].mcast_enable[1] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                            viParam[1].mcast_enable[1] =  (HI_BOOL)multicast_enable->valueint; // str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                            // viParam[0].mcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
                        }
                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set mcast_ip set failed");
                        }
                        memset(viParam[1].mcast_ip[1],0,IPSIZE);
                        sprintf(viParam[1].mcast_ip[1], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set mcast_port len set failed");
                        }
                        viParam[1].mcast_port[1] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_S0_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                viParam[2].mcast_enable[0] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                viParam[2].mcast_enable[0] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                             viParam[2].mcast_enable[0] =  (HI_BOOL)multicast_enable->valueint; // str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                            // viParam[0].mcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
                        }
                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set multicast_ip set failed");
                        }
                        memset(viParam[2].mcast_ip[0],0,IPSIZE);
                        sprintf(viParam[2].mcast_ip[0], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set multicast_port len set failed");
                        }
                        viParam[2].mcast_port[0] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn2_M_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_M_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }

                }else if(!strcmp(name->valuestring, "sub0")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                viParam[2].mcast_enable[1] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                viParam[2].mcast_enable[1] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                            viParam[2].mcast_enable[1] =  (HI_BOOL)multicast_enable->valueint; // str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                            // viParam[0].mcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
                        }
                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set mcast_ip set failed");
                        }
                        memset(viParam[2].mcast_ip[1],0,IPSIZE);
                        sprintf(viParam[2].mcast_ip[1], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set mcast_port len set failed");
                        }
                        viParam[2].mcast_port[1] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn2_S0_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_S0_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                }
            }
        }else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *name = cJSON_GetObjectItemCaseSensitive(value, "name");
            if (name && cJSON_IsString(name)) {
                if(!strcmp(name->valuestring, "main")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {
                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                viParam[3].mcast_enable[0]= HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                viParam[3].mcast_enable[0] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                           viParam[3].mcast_enable[0] =  (HI_BOOL)multicast_enable->valueint; // str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                            // viParam[0].mcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
                        }
                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set multicast_ip set failed");
                        }
                        memset(viParam[3].mcast_ip[0],0,IPSIZE);
                        sprintf(viParam[3].mcast_ip[0], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set multicast_port len set failed");
                        }
                        viParam[3].mcast_port[0] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn3_M_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_M_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                }else if(!strcmp(name->valuestring, "sub0")){
                    cJSON *multicast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
                    if (multicast_enable && cJSON_IsNumber(multicast_enable)) {

                        if(nspc) {      //来自NSPC命令
                            printf("i am is SetMulticastHandler an NSPC\n");
                            if(multicast_enable->valueint == 0) {       //NSPC关闭编码组播,编码线程关闭组播，ini不保存状态
                                viParam[3].mcast_enable[1] = HI_FALSE;
                            }
                            else {                                      //NSPC打开编码组播，读取UMP设置的状态
                                viParam[3].mcast_enable[1] =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_McastEnable));
                            }
                        }
                        else {
                            if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastEnable, multicast_enable->valueint) ){
                                COMMON_PRT("set mcast_enable set failed");
                            }
                            viParam[3].mcast_enable[1] =  (HI_BOOL)multicast_enable->valueint; // str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_M_McastEnable));
                            // viParam[0].mcast_enable[0] = (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_S0_McastEnable));
                        }
                    }

                    cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
                    if(multicast_ip && cJSON_IsString(multicast_ip)){
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastIp, multicast_ip->valuestring) ){
                            COMMON_PRT("set mcast_ip set failed");
                        }
                        memset(viParam[3].mcast_ip[1],0,IPSIZE);
                        sprintf(viParam[3].mcast_ip[1], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_McastIp)).c_str());
                    }

                    cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
                    if (multicast_port && cJSON_IsNumber(multicast_port)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastPort, multicast_port->valueint) ){
                            COMMON_PRT("set mcast_port len set failed");
                        }
                        viParam[3].mcast_port[1] = atoi(softwareconfig->GetConfig(SoftwareConfig::kChn3_S0_McastPort).c_str());
                    }

                    cJSON *mcast_len = cJSON_GetObjectItemCaseSensitive(value, "mcast_len");
                    if (mcast_len && cJSON_IsNumber(mcast_len)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastLen, mcast_len->valueint) ){
                            COMMON_PRT("set mcast_len set failed");
                        }
                    }

                    cJSON *mcast_delay = cJSON_GetObjectItemCaseSensitive(value, "mcast_delay");
                    if (mcast_delay && cJSON_IsNumber(mcast_delay)) {
                        if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_S0_McastDelay, mcast_delay->valueint) ){
                            COMMON_PRT("set mcast_delay set failed");
                        }
                    }
                }
            }
        }
    }

    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);

    return true;
}

/*
 * 重启生效抓图设置
 * */



bool CommandMap::SetSnapHandler(cJSON *value)
{
    this->LoadEchoUdp();
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();

    bool snap_wh_chang[4] = {false,false,false,false};      //需要设置venc通道属性

    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *snap_w = cJSON_GetObjectItemCaseSensitive(value, "snap_w");
            if (snap_w && cJSON_IsNumber(snap_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_W, snap_w->valueint) ){
                    COMMON_PRT("set snap_w set failed");
                }
                else {
                    if(viParam[0].stSnapSize.u32Width != snap_w->valueint) {
                        snap_wh_chang[0] = true;
                    }
                    viParam[0].stSnapSize.u32Width = snap_w->valueint;
                }
            }

            cJSON *snap_h = cJSON_GetObjectItemCaseSensitive(value, "snap_h");
            if (snap_h && cJSON_IsNumber(snap_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_H, snap_h->valueint) ){
                    COMMON_PRT("set snap_h len set failed");
                }
                else {
                    if(viParam[0].stSnapSize.u32Height != snap_h->valueint) {
                        snap_wh_chang[0] = true;
                    }
                    viParam[0].stSnapSize.u32Height = snap_h->valueint;
                }
            }

            cJSON *snap_port = cJSON_GetObjectItemCaseSensitive(value, "snap_port");            //修改抓图端口需要重启
            if (snap_port && cJSON_IsNumber(snap_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_SnapPort, snap_port->valueint) ){
                    COMMON_PRT("set snap_port set failed");
                }
            }

            cJSON *snap_len = cJSON_GetObjectItemCaseSensitive(value, "snap_len");
            if (snap_len && cJSON_IsNumber(snap_len)) {
                if(SNAP_LEN_MAX >= snap_len->valueint ) {           //防止有些人手贱
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_SnapLen, snap_len->valueint) ){
                        COMMON_PRT("set snap_len set failed");
                    }
                    else {
                        viParam[0].SnapLen = snap_len->valueint;
                    }
                }
            }

            cJSON *snap_delay = cJSON_GetObjectItemCaseSensitive(value, "snap_delay");
            if (snap_delay && cJSON_IsNumber(snap_delay)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_Delay, snap_delay->valueint) ){
                    COMMON_PRT("set snap_delay set failed");
                }
                else {
                    viParam[0].SnapDelay = snap_delay->valueint;
                }
            }

            cJSON *snap_quality = cJSON_GetObjectItemCaseSensitive(value, "snap_quality");
            if (snap_quality && cJSON_IsNumber(snap_quality)) {
                if(snap_quality->valueint < 1)
                    snap_quality->valueint = 1;
                else if(snap_quality->valueint > 99)
                    snap_quality->valueint = 99;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_Quality, snap_quality->valueint) ){
                    COMMON_PRT("set snap_quality set failed");
                }
                else {
                    if(viParam[0].SnapQuality != snap_quality->valueint) {
                        snap_wh_chang[0] = true;
                        viParam[0].SnapQuality = snap_quality->valueint;
                        printf("ump set snapquality %d\n",snap_quality->valueint);
                    }

                }
            }

        }

        if(!strcmp(channel->valuestring, "chn1")){
            cJSON *snap_w = cJSON_GetObjectItemCaseSensitive(value, "snap_w");
            if (snap_w && cJSON_IsNumber(snap_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_W, snap_w->valueint) ){
                    COMMON_PRT("set snap_w set failed");
                }
                else {
                    if(viParam[1].stSnapSize.u32Width != snap_w->valueint) {
                        snap_wh_chang[1] = true;
                    }
                    viParam[1].stSnapSize.u32Width = snap_w->valueint;
                }
            }


            cJSON *snap_h = cJSON_GetObjectItemCaseSensitive(value, "snap_h");
            if (snap_h && cJSON_IsNumber(snap_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_H, snap_h->valueint) ){
                    COMMON_PRT("set snap_h len set failed");
                }
                else {
                    if(viParam[1].stSnapSize.u32Height != snap_h->valueint) {
                        snap_wh_chang[1] = true;
                    }
                    viParam[1].stSnapSize.u32Height = snap_h->valueint;
                }
            }

            cJSON *snap_port = cJSON_GetObjectItemCaseSensitive(value, "snap_port");
            if (snap_port && cJSON_IsNumber(snap_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_SnapPort, snap_port->valueint) ){
                    COMMON_PRT("set snap_port set failed");
                }
            }

            cJSON *snap_len = cJSON_GetObjectItemCaseSensitive(value, "snap_len");
            if (snap_len && cJSON_IsNumber(snap_len)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_SnapLen, snap_len->valueint) ){
                    COMMON_PRT("set snap_len set failed");
                }
                else {
                    viParam[1].SnapLen = snap_len->valueint;
                }
            }

            cJSON *snap_delay = cJSON_GetObjectItemCaseSensitive(value, "snap_delay");
            if (snap_delay && cJSON_IsNumber(snap_delay)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_Delay, snap_delay->valueint) ){
                    COMMON_PRT("set snap_delay set failed");
                }
                else {
                    viParam[1].SnapDelay = snap_delay->valueint;
                }
            }

            cJSON *snap_quality = cJSON_GetObjectItemCaseSensitive(value, "snap_quality");
            if (snap_quality && cJSON_IsNumber(snap_quality)) {
                if(snap_quality->valueint < 1)
                    snap_quality->valueint = 1;
                else if(snap_quality->valueint > 99)
                    snap_quality->valueint = 99;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_Quality, snap_quality->valueint) ){
                    COMMON_PRT("set snap_quality set failed");
                }
                else {
                    if(viParam[1].SnapQuality != snap_quality->valueint) {
                        snap_wh_chang[1] = true;
                        viParam[1].SnapQuality = snap_quality->valueint;
                    }
                }
            }
        }

        if(!strcmp(channel->valuestring, "chn2")){
            cJSON *snap_w = cJSON_GetObjectItemCaseSensitive(value, "snap_w");
            if (snap_w && cJSON_IsNumber(snap_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_W, snap_w->valueint) ){
                    COMMON_PRT("set snap_w set failed");
                }
                else {
                    if(viParam[2].stSnapSize.u32Width != snap_w->valueint) {
                        snap_wh_chang[2] = true;
                    }
                    viParam[2].stSnapSize.u32Width = snap_w->valueint;
                }
            }

            cJSON *snap_h = cJSON_GetObjectItemCaseSensitive(value, "snap_h");
            if (snap_h && cJSON_IsNumber(snap_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_H, snap_h->valueint) ){
                    COMMON_PRT("set snap_h len set failed");
                }
                else {
                    if(viParam[2].stSnapSize.u32Height != snap_h->valueint) {
                        snap_wh_chang[2] = true;
                    }
                    viParam[2].stSnapSize.u32Height = snap_h->valueint;
                }
            }

            cJSON *snap_port = cJSON_GetObjectItemCaseSensitive(value, "snap_port");
            if (snap_port && cJSON_IsNumber(snap_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_SnapPort, snap_port->valueint) ){
                    COMMON_PRT("set snap_port set failed");
                }
            }

            cJSON *snap_len = cJSON_GetObjectItemCaseSensitive(value, "snap_len");
            if (snap_len && cJSON_IsNumber(snap_len)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_SnapLen, snap_len->valueint) ){
                    COMMON_PRT("set snap_len set failed");
                }
                else {
                    viParam[2].SnapLen = snap_len->valueint;
                }
            }

            cJSON *snap_delay = cJSON_GetObjectItemCaseSensitive(value, "snap_delay");
            if (snap_delay && cJSON_IsNumber(snap_delay)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_Delay, snap_delay->valueint) ){
                    COMMON_PRT("set snap_delay set failed");
                }
                else {
                    viParam[2].SnapDelay = snap_delay->valueint;
                }
            }

            cJSON *snap_quality = cJSON_GetObjectItemCaseSensitive(value, "snap_quality");
            if (snap_quality && cJSON_IsNumber(snap_quality)) {
                if(snap_quality->valueint < 1)
                    snap_quality->valueint = 1;
                else if(snap_quality->valueint > 99)
                    snap_quality->valueint = 99;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_Quality, snap_quality->valueint) ){
                    COMMON_PRT("set snap_quality set failed");
                }
                else {
                    if(viParam[2].SnapQuality != snap_quality->valueint) {
                        snap_wh_chang[2] = true;
                        viParam[2].SnapQuality = snap_quality->valueint;
                    }
                }
            }
        }

        if(!strcmp(channel->valuestring, "chn3")){
            cJSON *snap_w = cJSON_GetObjectItemCaseSensitive(value, "snap_w");

            if (snap_w && cJSON_IsNumber(snap_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_W, snap_w->valueint) ){
                    COMMON_PRT("set snap_w set failed");
                }
                else {
                    if(viParam[3].stSnapSize.u32Width != snap_w->valueint) {
                        snap_wh_chang[3] = true;
                    }
                    viParam[3].stSnapSize.u32Width = snap_w->valueint;
                }
            }

            cJSON *snap_h = cJSON_GetObjectItemCaseSensitive(value, "snap_h");
            if (snap_h && cJSON_IsNumber(snap_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_H, snap_h->valueint) ){
                    COMMON_PRT("set snap_h len set failed");
                }
                else {
                    if(viParam[3].stSnapSize.u32Height != snap_h->valueint) {
                        snap_wh_chang[3] = true;
                    }
                    viParam[3].stSnapSize.u32Height = snap_h->valueint;
                }
            }

            cJSON *snap_port = cJSON_GetObjectItemCaseSensitive(value, "snap_port");
            if (snap_port && cJSON_IsNumber(snap_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_SnapPort, snap_port->valueint) ){
                    COMMON_PRT("set snap_port set failed");
                }
            }

            cJSON *snap_len = cJSON_GetObjectItemCaseSensitive(value, "snap_len");
            if (snap_len && cJSON_IsNumber(snap_len)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_SnapLen, snap_len->valueint) ){
                    COMMON_PRT("set snap_len set failed");
                }
                else {
                    viParam[3].SnapLen = snap_len->valueint;
                }
            }

            cJSON *snap_delay = cJSON_GetObjectItemCaseSensitive(value, "snap_delay");
            if (snap_delay && cJSON_IsNumber(snap_delay)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_Delay, snap_delay->valueint) ){
                    COMMON_PRT("set snap_delay set failed");
                }
                else {
                     viParam[3].SnapDelay = snap_delay->valueint;
                }
            }

            cJSON *snap_quality = cJSON_GetObjectItemCaseSensitive(value, "snap_quality");
            if (snap_quality && cJSON_IsNumber(snap_quality)) {
                if(snap_quality->valueint < 1)
                    snap_quality->valueint = 1;
                else if(snap_quality->valueint > 99)
                    snap_quality->valueint = 99;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_Quality, snap_quality->valueint) ){
                    COMMON_PRT("set snap_quality set failed");
                }
                else {
                    if(viParam[3].SnapQuality != snap_quality->valueint) {
                        snap_wh_chang[3] = true;
                        viParam[3].SnapQuality = snap_quality->valueint;
                    }
                }
            }
        }
    }

    VENC_CHN  veChn = 0;
    VPSS_GRP  vpssGrp = 0;

    for(int i = 0; i < g_vi_en_number; i++) {
        if(snap_wh_chang[i]) {                      //修改了对应的宽高和抓图质量
            snap_vh_chang[i] = true;
            usleep(100000);
            veChn = i + 8;
            vpssGrp = i + 4;
            himppmaster->VencUnbindVpss(veChn,vpssGrp);
            himppmaster->VencDestroy(veChn);
            himppmaster->vpssStop(vpssGrp);
            himppmaster->VpssModuleInit(vpssGrp);
            himppmaster->VencSnapInit(veChn,viParam[i].stSnapSize);
            himppmaster->VencBindVpss(veChn,vpssGrp);
            snap_vh_chang[i] = false;
        }
    }
    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);
    return true;
}

bool CommandMap::SetSnapMulticastHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    bool nspc = false;
    bool snap_freq[4] = {false,false,false,false};
    cJSON *cmd = cJSON_GetObjectItemCaseSensitive(value, "cmd_frome");
    if(cmd && cJSON_IsString(cmd)) {
        if(!strcmp(cmd->valuestring,"nspc")) {
            nspc = true;
        }
    }

    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        viParam[0].Snapmcast_enable = HI_FALSE;
                    }
                    else {
                        viParam[0].Snapmcast_enable =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastEnable).c_str());
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    viParam[0].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                }
            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
                else {
                    memset(viParam[0].Snapmcast_ip,0,IPSIZE);
                    sprintf(viParam[0].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastIp)).c_str());
                }
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
                else {
                    viParam[0].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastPort));
                }
            }

            cJSON *multicast_freq = cJSON_GetObjectItemCaseSensitive(value, "mcast_freq");
            if (multicast_freq && cJSON_IsNumber(multicast_freq)) {
                if(multicast_freq->valueint < 1)
                    multicast_freq->valueint = 1;
                else if(multicast_freq->valueint > viParam[0].u32SrcFrmRate)
                    multicast_freq->valueint = viParam[0].u32SrcFrmRate;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Snap_McastFreq, multicast_freq->valueint) ){
                    COMMON_PRT("set multicast_freq len set failed");
                }
                else {
                    if(viParam[0].Snapmcast_freq != multicast_freq->valueint) {
                        snap_freq[0] = true;
                    }
                    viParam[0].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Snap_McastFreq));
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        viParam[1].Snapmcast_enable = HI_FALSE;
                    }
                    else {
                        viParam[1].Snapmcast_enable =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastEnable).c_str());
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                     viParam[0].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                }
            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
                else {
                    memset(viParam[1].Snapmcast_ip,0,IPSIZE);
                    sprintf(viParam[1].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastIp)).c_str());
                }
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
                else {
                    viParam[1].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastPort));
                }
            }

            cJSON *multicast_freq = cJSON_GetObjectItemCaseSensitive(value, "mcast_freq");
            if (multicast_freq && cJSON_IsNumber(multicast_freq)) {
                if(multicast_freq->valueint < 1)
                    multicast_freq->valueint = 1;
                else if(multicast_freq->valueint > viParam[1].u32SrcFrmRate)
                    multicast_freq->valueint = viParam[1].u32SrcFrmRate;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Snap_McastFreq, multicast_freq->valueint) ){
                    COMMON_PRT("set multicast_freq len set failed");
                }
                else {
                    if(viParam[1].Snapmcast_freq != multicast_freq->valueint) {
                        snap_freq[1] = true;
                    }
                    viParam[1].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Snap_McastFreq));
                }
            }

        }
        else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {
                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        viParam[2].Snapmcast_enable = HI_FALSE;
                    }
                    else {
                        viParam[2].Snapmcast_enable =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastEnable).c_str());
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    viParam[2].Snapmcast_enable =  (HI_BOOL)mcast_enable->valueint;
                }
            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
                else {
                    memset(viParam[2].Snapmcast_ip,0,IPSIZE);
                    sprintf(viParam[2].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastIp)).c_str());
                }
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
                else {
                    viParam[2].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastPort));
                }
            }

            cJSON *multicast_freq = cJSON_GetObjectItemCaseSensitive(value, "mcast_freq");
            if (multicast_freq && cJSON_IsNumber(multicast_freq)) {
                if(multicast_freq->valueint < 1)
                    multicast_freq->valueint = 1;
                else if(multicast_freq->valueint > viParam[2].u32SrcFrmRate)
                    multicast_freq->valueint = viParam[2].u32SrcFrmRate;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Snap_McastFreq, multicast_freq->valueint) ){
                    COMMON_PRT("set multicast_freq len set failed");
                }
                else {
                    if(viParam[2].Snapmcast_freq != multicast_freq->valueint) {
                        snap_freq[2] = true;
                    }
                    viParam[2].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Snap_McastFreq));
                }
            }
        }
        else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *mcast_enable = cJSON_GetObjectItemCaseSensitive(value, "mcast_enable");
            if (mcast_enable && cJSON_IsNumber(mcast_enable)) {

                if(nspc) {
                    if(0 == mcast_enable->valueint) {
                        viParam[3].Snapmcast_enable = HI_FALSE;
                    }
                    else {
                        viParam[3].Snapmcast_enable =  (HI_BOOL)str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastEnable).c_str());
                    }
                }
                else {
                    if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastEnable, mcast_enable->valueint) ){
                        COMMON_PRT("set mcast_enable set failed");
                    }
                    viParam[3].Snapmcast_enable = (HI_BOOL)mcast_enable->valueint;
                }
            }

            cJSON *multicast_ip = cJSON_GetObjectItemCaseSensitive(value, "mcast_ip");
            if(multicast_ip && cJSON_IsString(multicast_ip)){
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastIp, multicast_ip->valuestring) ){
                    COMMON_PRT("set multicast_ip set failed");
                }
                else {
                    memset(viParam[3].Snapmcast_ip,0,IPSIZE);
                    sprintf(viParam[3].Snapmcast_ip,"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastIp)).c_str());
                }
            }

            cJSON *multicast_port = cJSON_GetObjectItemCaseSensitive(value, "mcast_port");
            if (multicast_port && cJSON_IsNumber(multicast_port)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastPort, multicast_port->valueint) ){
                    COMMON_PRT("set multicast_port len set failed");
                }
                else {
                    viParam[3].Snapmcast_port = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastPort));
                }
            }

            cJSON *multicast_freq = cJSON_GetObjectItemCaseSensitive(value, "mcast_freq");
            if (multicast_freq && cJSON_IsNumber(multicast_freq)) {
                if(multicast_freq->valueint < 1)
                    multicast_freq->valueint = 1;
                else if(multicast_freq->valueint > viParam[3].u32SrcFrmRate)
                    multicast_freq->valueint = viParam[3].u32SrcFrmRate;
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Snap_McastFreq, multicast_freq->valueint) ){
                    COMMON_PRT("set multicast_freq len set failed");
                }
                else {
                    if(viParam[3].Snapmcast_freq != multicast_freq->valueint) {
                        snap_freq[3] = true;
                    }
                    viParam[3].Snapmcast_freq = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Snap_McastFreq));
                }
            }
        }
    }

    VENC_CHN  veChn = 0;
    VPSS_GRP  vpssGrp = 0;
    for(uint i = 0; i < g_vi_en_number; i++) {
        if(snap_freq[i]) {
            snap_vh_chang[i] = true;
            usleep(100000);
            veChn = i + 8;
            vpssGrp = i + 4;
            himppmaster->VencUnbindVpss(veChn,vpssGrp);
            himppmaster->VencDestroy(veChn);
            himppmaster->vpssStop(vpssGrp);
            himppmaster->VpssModuleInit(vpssGrp);
            himppmaster->VencSnapInit(veChn,viParam[i].stSnapSize);
            himppmaster->VencBindVpss(veChn,vpssGrp);
            snap_vh_chang[i] = false;
        }
    }

    softwareconfig->SaveConfig();
    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());
    return true;
}



bool CommandMap::SetBasicConfigHandler(cJSON *value)
{

    //节点名称
    cJSON *node_name = cJSON_GetObjectItemCaseSensitive(value, "node_name");
    if (node_name && cJSON_IsString(node_name)) {
        char namefile[30] = "";
        sprintf(namefile, "%s/%s", DATA_FILE, NODE_NAME_FILE);
        FILE *f = fopen( namefile, "wb" );
        if(f){
            fprintf(f, "%s\n", node_name->valuestring);
            fflush(f);
            fclose(f);
            f = NULL;
        }
    }

    //组播PTS  UMP指定值
    cJSON *mcast_diff = cJSON_GetObjectItemCaseSensitive(value, "mcast_diff");
    if (mcast_diff && cJSON_IsNumber(mcast_diff)) {
        diff_index = mcast_diff->valueint;
    }

    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();

    cJSON * edid4K  = cJSON_GetObjectItemCaseSensitive(value, "edid");

    if(edid4K && cJSON_IsNumber(edid4K)) {

        if( !softwareconfig->SetConfig(SoftwareConfig::kEdid, edid4K->valueint) ){
            COMMON_PRT("set edid_4k set failed");
        }
    }

    cJSON *vi_module = cJSON_GetObjectItemCaseSensitive(value, "vi_module");
    if (vi_module && cJSON_IsNumber(vi_module)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Module, vi_module->valueint) ){
            COMMON_PRT("set vi_module set failed");
        }
    }


    softwareconfig->SaveConfig();
    usleep(1000);
    this->LetMeTellYou(msg);

    return true;
}

bool CommandMap::SetViCropHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    bool vi_crop_wh_chg[4] = {false,false,false,false};
    bool vi_crop_en_chg[4] = {false,false,false,false};

    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *vi_crop_enable = cJSON_GetObjectItemCaseSensitive(value, "vi_crop_enable");
            if (vi_crop_enable && cJSON_IsNumber(vi_crop_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Crop_Enable, vi_crop_enable->valueint) ){
                    COMMON_PRT("set vi_crop_enable set failed");
                }
                if(viParam[0].vi_crop_enable != (HI_BOOL)vi_crop_enable->valueint) {
                    vi_crop_en_chg[0] = true;
                }
                viParam[0].vi_crop_enable = (HI_BOOL)vi_crop_enable->valueint;
            }

            cJSON *crop_x = cJSON_GetObjectItemCaseSensitive(value, "crop_x");
            if (crop_x && cJSON_IsNumber(crop_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_X, crop_x->valueint) ){
                    COMMON_PRT("set crop_x set failed");
                }
                if(viParam[0].u32X != crop_x->valueint) {
                    vi_crop_wh_chg[0] = true;
                }
                viParam[0].u32X = crop_x->valueint;
            }

            cJSON *crop_y = cJSON_GetObjectItemCaseSensitive(value, "crop_y");
            if (crop_y && cJSON_IsNumber(crop_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Y, crop_y->valueint) ){
                    COMMON_PRT("set crop_y set failed");
                }
                if(viParam[0].u32Y != crop_y->valueint) {
                    vi_crop_wh_chg[0] = true;
                }
                viParam[0].u32Y = crop_y->valueint;
            }

            cJSON *crop_w = cJSON_GetObjectItemCaseSensitive(value, "crop_w");
            if (crop_w && cJSON_IsNumber(crop_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Width, crop_w->valueint) ){
                    COMMON_PRT("set crop_w set failed");
                }
                if(viParam[0].u32W != crop_w->valueint) {
                    vi_crop_wh_chg[0] = true;
                }
                viParam[0].u32W = crop_w->valueint;
            }

            cJSON *crop_h = cJSON_GetObjectItemCaseSensitive(value, "crop_h");
            if (crop_h && cJSON_IsNumber(crop_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_VI_Height, crop_h->valueint) ){
                    COMMON_PRT("set crop_h set failed");
                }
                if(viParam[0].u32H != crop_h->valueint) {
                    vi_crop_wh_chg[0] = true;
                }
                viParam[0].u32H = crop_h->valueint;
            }
        }else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *vi_crop_enable = cJSON_GetObjectItemCaseSensitive(value, "vi_crop_enable");
            if (vi_crop_enable && cJSON_IsNumber(vi_crop_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_Crop_Enable, vi_crop_enable->valueint) ){
                    COMMON_PRT("set vi_crop_enable set failed");
                }
                if(viParam[1].vi_crop_enable != (HI_BOOL)vi_crop_enable->valueint) {
                    vi_crop_en_chg[1] = true;
                }
                viParam[1].vi_crop_enable = (HI_BOOL)vi_crop_enable->valueint;
            }

            cJSON *crop_x = cJSON_GetObjectItemCaseSensitive(value, "crop_x");
            if (crop_x && cJSON_IsNumber(crop_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_X, crop_x->valueint) ){
                    COMMON_PRT("set crop_x set failed");
                }
                if(viParam[1].u32X != crop_x->valueint) {
                    vi_crop_wh_chg[1] = true;
                }
                viParam[1].u32X = crop_x->valueint;
            }

            cJSON *crop_y = cJSON_GetObjectItemCaseSensitive(value, "crop_y");
            if (crop_y && cJSON_IsNumber(crop_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_Y, crop_y->valueint) ){
                    COMMON_PRT("set crop_y set failed");
                }
                if(viParam[1].u32Y != crop_y->valueint) {
                    vi_crop_wh_chg[1] = true;
                }
                viParam[1].u32Y = crop_y->valueint;
            }

            cJSON *crop_w = cJSON_GetObjectItemCaseSensitive(value, "crop_w");
            if (crop_w && cJSON_IsNumber(crop_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_Width, crop_w->valueint) ){
                    COMMON_PRT("set crop_w set failed");
                }
                if(viParam[1].u32W != crop_w->valueint) {
                    vi_crop_wh_chg[1] = true;
                }
                viParam[1].u32W = crop_w->valueint;
            }

            cJSON *crop_h = cJSON_GetObjectItemCaseSensitive(value, "crop_h");
            if (crop_h && cJSON_IsNumber(crop_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_VI_Height, crop_h->valueint) ){
                    COMMON_PRT("set crop_h set failed");
                }
                if(viParam[1].u32H != crop_h->valueint) {
                    vi_crop_wh_chg[1] = true;
                }
                viParam[1].u32H = crop_h->valueint;
            }
        }else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *vi_crop_enable = cJSON_GetObjectItemCaseSensitive(value, "vi_crop_enable");
            if (vi_crop_enable && cJSON_IsNumber(vi_crop_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_Crop_Enable, vi_crop_enable->valueint) ){
                    COMMON_PRT("set vi_crop_enable set failed");
                }
                if(viParam[2].vi_crop_enable != (HI_BOOL)vi_crop_enable->valueint) {
                    vi_crop_en_chg[2] = true;
                }
                viParam[2].vi_crop_enable = (HI_BOOL)vi_crop_enable->valueint;
            }

            cJSON *crop_x = cJSON_GetObjectItemCaseSensitive(value, "crop_x");
            if (crop_x && cJSON_IsNumber(crop_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_X, crop_x->valueint) ){
                    COMMON_PRT("set crop_x set failed");
                }
                if(viParam[2].u32X != crop_x->valueint) {
                    vi_crop_wh_chg[2] = true;
                }
                viParam[2].u32X = crop_x->valueint;
            }

            cJSON *crop_y = cJSON_GetObjectItemCaseSensitive(value, "crop_y");
            if (crop_y && cJSON_IsNumber(crop_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_Y, crop_y->valueint) ){
                    COMMON_PRT("set crop_y set failed");
                }
                if(viParam[2].u32Y != crop_y->valueint) {
                    vi_crop_wh_chg[2] = true;
                }
                viParam[2].u32Y = crop_y->valueint;
            }

            cJSON *crop_w = cJSON_GetObjectItemCaseSensitive(value, "crop_w");
            if (crop_w && cJSON_IsNumber(crop_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_Width, crop_w->valueint) ){
                    COMMON_PRT("set crop_w set failed");
                }
                if(viParam[2].u32W != crop_w->valueint) {
                    vi_crop_wh_chg[2] = true;
                }
                viParam[2].u32W = crop_w->valueint;
            }

            cJSON *crop_h = cJSON_GetObjectItemCaseSensitive(value, "crop_h");
            if (crop_h && cJSON_IsNumber(crop_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_VI_Height, crop_h->valueint) ){
                    COMMON_PRT("set crop_h set failed");
                }
                if(viParam[2].u32H != crop_h->valueint) {
                    vi_crop_wh_chg[2] = true;
                }
                viParam[2].u32H = crop_h->valueint;
            }
        }else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *vi_crop_enable = cJSON_GetObjectItemCaseSensitive(value, "vi_crop_enable");
            if (vi_crop_enable && cJSON_IsNumber(vi_crop_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_Crop_Enable, vi_crop_enable->valueint) ){
                    COMMON_PRT("set vi_crop_enable set failed");
                }
                if(viParam[3].vi_crop_enable != (HI_BOOL)vi_crop_enable->valueint) {
                    vi_crop_en_chg[3] = true;
                }
                viParam[3].vi_crop_enable = (HI_BOOL)vi_crop_enable->valueint;
            }

            cJSON *crop_x = cJSON_GetObjectItemCaseSensitive(value, "crop_x");
            if (crop_x && cJSON_IsNumber(crop_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_X, crop_x->valueint) ){
                    COMMON_PRT("set crop_x set failed");
                }
                if(viParam[3].u32X != crop_x->valueint) {
                    vi_crop_wh_chg[3] = true;
                }
                viParam[3].u32X = crop_x->valueint;
            }

            cJSON *crop_y = cJSON_GetObjectItemCaseSensitive(value, "crop_y");
            if (crop_y && cJSON_IsNumber(crop_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_Y, crop_y->valueint) ){
                    COMMON_PRT("set crop_y set failed");
                }
                if(viParam[3].u32Y != crop_y->valueint) {
                    vi_crop_wh_chg[3] = true;
                }
                viParam[3].u32Y = crop_y->valueint;
            }

            cJSON *crop_w = cJSON_GetObjectItemCaseSensitive(value, "crop_w");
            if (crop_w && cJSON_IsNumber(crop_w)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_Width, crop_w->valueint) ){
                    COMMON_PRT("set crop_w set failed");
                }
                if(viParam[3].u32W != crop_w->valueint) {
                    vi_crop_wh_chg[3] = true;
                }
                viParam[3].u32W = crop_w->valueint;
            }

            cJSON *crop_h = cJSON_GetObjectItemCaseSensitive(value, "crop_h");
            if (crop_h && cJSON_IsNumber(crop_h)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_VI_Height, crop_h->valueint) ){
                    COMMON_PRT("set crop_h set failed");
                }
            }
            if(viParam[3].u32H != crop_h->valueint) {
                vi_crop_wh_chg[3] = true;
            }
            viParam[3].u32H = crop_h->valueint;
        }
    }

    softwareconfig->SaveConfig();

    this->LetMeTellYou(msg);
    for(uint i = 0; i < g_vi_en_number; i++) {
        if((viParam[i].vi_crop_enable && vi_crop_wh_chg[i]) || vi_crop_en_chg[i]) {        //VI裁剪使能，且宽高变化或者使能状态改变
            vi_src_chang[i] = true;             //暂停VIGET  VENCPUSH SNAP
            usleep(20000);
            if(vi_flag[i] == false) {
                himppmaster->retartVIVPSSVENC(i,HI_FALSE,HI_TRUE);
            }
            else {
                himppmaster->retartVIVPSSVENC(i,HI_TRUE,HI_TRUE);
            }
            usleep(20000);
            vi_src_chang[i] = false;
        }
    }
    return true;
}

bool CommandMap::SetOsdHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();


    cJSON *channel = cJSON_GetObjectItemCaseSensitive(value, "channel");
    if (channel && cJSON_IsString(channel)) {
        if(!strcmp(channel->valuestring, "chn0")){
            cJSON *osd_enable = cJSON_GetObjectItemCaseSensitive(value, "osd_enable");
            if (osd_enable && cJSON_IsNumber(osd_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Enable, osd_enable->valueint) ){
                    COMMON_PRT("set osd_enable set failed");
                }
                viParam[0].osd_enable = (HI_BOOL)osd_enable->valueint;
            }

            cJSON *osd_name = cJSON_GetObjectItemCaseSensitive(value, "osd_name");
            if (osd_name && cJSON_IsString(osd_name)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Name, osd_name->valuestring) ){
                    COMMON_PRT("set osd_name set failed");
                }
            }

            cJSON *osd_color = cJSON_GetObjectItemCaseSensitive(value, "osd_color");
            if (osd_color && cJSON_IsString(osd_color)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Color, osd_color->valuestring) ){
                    COMMON_PRT("set osd_color set failed");
                }
            }

            cJSON *osd_x = cJSON_GetObjectItemCaseSensitive(value, "osd_x");
            if (osd_x && cJSON_IsNumber(osd_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_X, osd_x->valueint) ){
                    COMMON_PRT("set osd_x set failed");
                }
            }

            cJSON *osd_y = cJSON_GetObjectItemCaseSensitive(value, "osd_y");
            if (osd_y && cJSON_IsNumber(osd_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Y, osd_y->valueint) ){
                    COMMON_PRT("set osd_y set failed");
                }
            }

            cJSON *osd_size = cJSON_GetObjectItemCaseSensitive(value, "osd_size");
            if (osd_size && cJSON_IsNumber(osd_size)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Size, osd_size->valueint) ){
                    COMMON_PRT("set osd_size set failed");
                }
            }

            cJSON *osd_transparent = cJSON_GetObjectItemCaseSensitive(value, "osd_transparent");
            if (osd_transparent && cJSON_IsNumber(osd_transparent)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_Transparent, osd_transparent->valueint) ){
                    COMMON_PRT("set osd_transparent set failed");
                }
            }

            cJSON *osd_bgcolor = cJSON_GetObjectItemCaseSensitive(value, "osd_bgcolor");
            if (osd_bgcolor && cJSON_IsString(osd_bgcolor)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn0_Osd_BGColor, osd_bgcolor->valuestring) ){
                    COMMON_PRT("set osd_bgcolor set failed");
                }
            }
            osd_chng[0] = true;
        }else if(!strcmp(channel->valuestring, "chn1")){
            cJSON *osd_enable = cJSON_GetObjectItemCaseSensitive(value, "osd_enable");
            if (osd_enable && cJSON_IsNumber(osd_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Enable, osd_enable->valueint) ){
                    COMMON_PRT("set osd_enable set failed");
                }
                viParam[1].osd_enable = (HI_BOOL)osd_enable->valueint;
            }

            cJSON *osd_name = cJSON_GetObjectItemCaseSensitive(value, "osd_name");
            if (osd_name && cJSON_IsString(osd_name)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Name, osd_name->valuestring) ){
                    COMMON_PRT("set osd_name set failed");
                }
            }

            cJSON *osd_color = cJSON_GetObjectItemCaseSensitive(value, "osd_color");
            if (osd_color && cJSON_IsString(osd_color)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Color, osd_color->valuestring) ){
                    COMMON_PRT("set osd_color set failed");
                }
            }

            cJSON *osd_x = cJSON_GetObjectItemCaseSensitive(value, "osd_x");
            if (osd_x && cJSON_IsNumber(osd_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_X, osd_x->valueint) ){
                    COMMON_PRT("set osd_x set failed");
                }
            }

            cJSON *osd_y = cJSON_GetObjectItemCaseSensitive(value, "osd_y");
            if (osd_y && cJSON_IsNumber(osd_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Y, osd_y->valueint) ){
                    COMMON_PRT("set osd_y set failed");
                }
            }

            cJSON *osd_size = cJSON_GetObjectItemCaseSensitive(value, "osd_size");
            if (osd_size && cJSON_IsNumber(osd_size)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Size, osd_size->valueint) ){
                    COMMON_PRT("set osd_size set failed");
                }
            }

            cJSON *osd_transparent = cJSON_GetObjectItemCaseSensitive(value, "osd_transparent");
            if (osd_transparent && cJSON_IsNumber(osd_transparent)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_Transparent, osd_transparent->valueint) ){
                    COMMON_PRT("set osd_transparent set failed");
                }
            }

            cJSON *osd_bgcolor = cJSON_GetObjectItemCaseSensitive(value, "osd_bgcolor");
            if (osd_bgcolor && cJSON_IsString(osd_bgcolor)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn1_Osd_BGColor, osd_bgcolor->valuestring) ){
                    COMMON_PRT("set osd_bgcolor set failed");
                }
            }
            osd_chng[1] = true;
        }else if(!strcmp(channel->valuestring, "chn2")){
            cJSON *osd_enable = cJSON_GetObjectItemCaseSensitive(value, "osd_enable");
            if (osd_enable && cJSON_IsNumber(osd_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Enable, osd_enable->valueint) ){
                    COMMON_PRT("set osd_enable set failed");
                }
                viParam[2].osd_enable = (HI_BOOL)osd_enable->valueint;
            }

            cJSON *osd_name = cJSON_GetObjectItemCaseSensitive(value, "osd_name");
            if (osd_name && cJSON_IsString(osd_name)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Name, osd_name->valuestring) ){
                    COMMON_PRT("set osd_name set failed");
                }
            }

            cJSON *osd_color = cJSON_GetObjectItemCaseSensitive(value, "osd_color");
            if (osd_color && cJSON_IsString(osd_color)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Color, osd_color->valuestring) ){
                    COMMON_PRT("set osd_color set failed");
                }
            }

            cJSON *osd_x = cJSON_GetObjectItemCaseSensitive(value, "osd_x");
            if (osd_x && cJSON_IsNumber(osd_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_X, osd_x->valueint) ){
                    COMMON_PRT("set osd_x set failed");
                }
            }

            cJSON *osd_y = cJSON_GetObjectItemCaseSensitive(value, "osd_y");
            if (osd_y && cJSON_IsNumber(osd_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Y, osd_y->valueint) ){
                    COMMON_PRT("set osd_y set failed");
                }
            }

            cJSON *osd_size = cJSON_GetObjectItemCaseSensitive(value, "osd_size");
            if (osd_size && cJSON_IsNumber(osd_size)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Size, osd_size->valueint) ){
                    COMMON_PRT("set osd_size set failed");
                }
            }

            cJSON *osd_transparent = cJSON_GetObjectItemCaseSensitive(value, "osd_transparent");
            if (osd_transparent && cJSON_IsNumber(osd_transparent)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_Transparent, osd_transparent->valueint) ){
                    COMMON_PRT("set osd_transparent set failed");
                }
            }

            cJSON *osd_bgcolor = cJSON_GetObjectItemCaseSensitive(value, "osd_bgcolor");
            if (osd_bgcolor && cJSON_IsString(osd_bgcolor)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn2_Osd_BGColor, osd_bgcolor->valuestring) ){
                    COMMON_PRT("set osd_bgcolor set failed");
                }
            }
            osd_chng[2] = true;
        }else if(!strcmp(channel->valuestring, "chn3")){
            cJSON *osd_enable = cJSON_GetObjectItemCaseSensitive(value, "osd_enable");
            if (osd_enable && cJSON_IsNumber(osd_enable)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Enable, osd_enable->valueint) ){
                    COMMON_PRT("set osd_enable set failed");
                }
                viParam[3].osd_enable = (HI_BOOL)osd_enable->valueint;
            }

            cJSON *osd_name = cJSON_GetObjectItemCaseSensitive(value, "osd_name");
            if (osd_name && cJSON_IsString(osd_name)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Name, osd_name->valuestring) ){
                    COMMON_PRT("set osd_name set failed");
                }
            }

            cJSON *osd_color = cJSON_GetObjectItemCaseSensitive(value, "osd_color");
            if (osd_color && cJSON_IsString(osd_color)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Color, osd_color->valuestring) ){
                    COMMON_PRT("set osd_color set failed");
                }
            }

            cJSON *osd_x = cJSON_GetObjectItemCaseSensitive(value, "osd_x");
            if (osd_x && cJSON_IsNumber(osd_x)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_X, osd_x->valueint) ){
                    COMMON_PRT("set osd_x set failed");
                }
            }

            cJSON *osd_y = cJSON_GetObjectItemCaseSensitive(value, "osd_y");
            if (osd_y && cJSON_IsNumber(osd_y)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Y, osd_y->valueint) ){
                    COMMON_PRT("set osd_y set failed");
                }
            }

            cJSON *osd_size = cJSON_GetObjectItemCaseSensitive(value, "osd_size");
            if (osd_size && cJSON_IsNumber(osd_size)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Size, osd_size->valueint) ){
                    COMMON_PRT("set osd_size set failed");
                }
            }

            cJSON *osd_transparent = cJSON_GetObjectItemCaseSensitive(value, "osd_transparent");
            if (osd_transparent && cJSON_IsNumber(osd_transparent)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_Transparent, osd_transparent->valueint) ){
                    COMMON_PRT("set osd_transparent set failed");
                }
            }

            cJSON *osd_bgcolor = cJSON_GetObjectItemCaseSensitive(value, "osd_bgcolor");
            if (osd_bgcolor && cJSON_IsString(osd_bgcolor)) {
                if( !softwareconfig->SetConfig(SoftwareConfig::kChn3_Osd_BGColor, osd_bgcolor->valuestring) ){
                    COMMON_PRT("set osd_bgcolor set failed");
                }
            }
            osd_chng[3] = true;
        }
    }

    softwareconfig->SaveConfig();
    this->LetMeTellYou(msg);


    return true;
}

//执行一个shell命令，输出结果逐行存储在resvec中，并返回字数
int LinuxShell(const char *cmd, std::string &str)
{
    str.clear();
    FILE *pp = popen(cmd, "r"); //建立管道,读取模式,”w“为写入模式
    if (!pp) {
        return -1;
    }
    char tmp[1024]; //设置一个合适的长度，以存储每一行输出
    while (fgets(tmp, sizeof(tmp), pp) != NULL) {
        str += tmp;
    }
    if(str[str.size() - 1] == '\n')
        str = str.substr(0, str.size() - 1); //去掉字符串最后的 '/n'
    pclose(pp); //关闭管道
    return str.size();
}


bool CommandMap::GetMsgHandler(cJSON *value)
{
    bool ret = false;

    cJSON *version = cJSON_GetObjectItemCaseSensitive(value, "get_version");
    if (version) {
        char filepath[24];
        sprintf(filepath, "%s/%s", VERSION_DIR, VERSION_FILE);
        ret = this->SendFileMsg(version, filepath, "[version]");
        if(!ret){
            COMMON_PRT("get version failed!");
        }
    }

    cJSON *uuid = cJSON_GetObjectItemCaseSensitive(value, "get_uuid");
    if (uuid) {
        char filepath[24];
        sprintf(filepath, "%s/%s", UUID_DIR, UUID_FILE);
        ret = this->SendFileMsg(uuid, filepath, "[uuid]");
        if(!ret){
            COMMON_PRT("get uuid failed!");
        }
    }

    cJSON *venc_config = cJSON_GetObjectItemCaseSensitive(value, "get_venc_config");
    if (venc_config) {
        ret = this->SendFileMsg(venc_config, filepath_, "[venc]");
        if(!ret){
            COMMON_PRT("get hi3531d_venc.ini failed!");
        }
    }

    cJSON *net_config = cJSON_GetObjectItemCaseSensitive(value, "get_net_config");
    if (net_config) {
        ret = this->SendFileMsg(net_config, netfilepath_, "[net]");
        if(!ret){
            COMMON_PRT("get net.ini failed!");
        }
    }

    cJSON * js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_sys_info");
    if (js_ret) {
        string content;
        string cmd;
        string c;

        /** NOTE: CPU空闲率说明
        master@jay-intel:~$ cat /proc/uptime
        6447032.12 48185264.69
        master@jay-intel:~$ cat /proc/cpuinfo  | grep processor | wc -l
        8

        第一列输出的是，系统启动到现在的时间（以秒为单位），这里简记为num1；
        第二列输出的是，系统空闲的时间（以秒为单位）,这里简记为num2。

        注意，很多很多人都知道第二个是系统空闲的时间，但是可能你不知道是，在SMP系统里，系统空闲的时间有时会是系统运行时间的几倍，这是怎么回事呢？
        因为系统空闲时间的计算，是把SMP算进去的，就是所你有几个逻辑的CPU（包括超线程）。

        系统的空闲率(%) = num2/(num1*N) 其中N是SMP系统中的CPU个数。
        **/
        /*NOTE: CPU + runingtime + idleness(空闲率)
        */
        string str;
        c = "iostat -c | awk '{printf $1}' | awk -F: '{print $2}'"; LinuxShell(c.c_str(), str);
        cmd += "CPU: " + str + "%\n";
        c = "cat /proc/uptime | awk -F. '{run_days=$1 / 86400;run_hour=($1 % 86400)/3600;run_minute=($1 % 3600)/60;run_second=$1 % 60;printf(\"%dd.%dh:%dm:%ds\",run_days,run_hour,run_minute,run_second)}'"; LinuxShell(c.c_str(), str);
        cmd += "Runing time: " + str + '\n';
        c = "cat /proc/cpuinfo  | grep processor | wc -l"; LinuxShell(c.c_str(), str);
        c = "cat /proc/uptime | awk '{print $2/$1/" + str + "*100}'"; LinuxShell(c.c_str(), str);
        cmd += "Iidleness: " + str + "%\n";

        /*NOTE:  sys cmdline
         * 样式
         * mem=128M console=ttyAMA0,115200 root=/dev/mtdblock2 rw rootfstype=yaffs2 mtdparts=hinand:1M(boot),4M(kernel),-(rootfs) ver=20191223
        */
        content = ReadSthFile("/proc/cmdline");
        cmd += "OS Mem: " + content.substr(content.find("mem=") + strlen("mem="), content.find(' ') - content.find("mem=") - strlen("mem=")) + '\n';

        c = "free -m | grep 'Mem' | awk '{print \"total \"$2\"M | used \"$3\"M | free \"$4\"M\"}'"; LinuxShell(c.c_str(), str);
        cmd += "Memory Status: " + str + '\n';

        c = "df -h | grep 'root' | awk '{print \"total \"$2\" | used \"$5}'"; LinuxShell(c.c_str(), str);
        cmd += "Flash Size: rootfs " + content.substr(content.rfind(",", content.rfind("(rootfs)")) + 1, content.rfind("(rootfs)") - content.rfind(",", content.find("(rootfs)")) - 1) + " | " + str + '\n';

        if(content.find("ver=") != string::npos)
           cmd += "U-Boot Version: " + content.substr(content.find("ver=") + strlen("ver=")) + '\n';
        else
           cmd += "U-Boot Version: <null>\n";

        /*NOTE: kernel version
         * 样式
         * Linux version 3.18.20 (lwx@lwx-VirtualBox) (gcc version 4.9.4 20150629 (prerelease) (Hisilicon_v500_20180120) ) #5 SMP Wed Feb 20 21:20:31 CST 2019
        */
        content = ReadSthFile("/proc/version");
        cmd += "Kernel Version: " + content.substr(content.find("#")) + '\n';
        cmd += "Compiler Version: " + content.substr(content.find("Hisilicon") + strlen("Hisilicon") + 1, content.find(")", content.find("Hisilicon")) - content.find("Hisilicon") - strlen("Hisilicon") - 1) + '\n';

        /*NOTE: software version
         * 样式
         * {
         *  "software_version": "0.14.5.20191203133232_alpha",
         *  "hardware_version": "892457216_v500_20180120"
         *  }
        */
        content = ReadSthFile("/version/version");
        cmd += "Software Version: " + content.substr(content.find("\"software_version\": ") + strlen("\"software_version\": ") + 1, content.find(",") - (content.find("\"software_version\": ") + strlen("\"software_version\": ") + 1) - 1) + '\n';

        /*NOTE: uuid
         * 样式
         * 38-BB-CD-22-A6-B5-49-80-80-AB-B8-9F-05-80-2B-93
        */
        content = ReadSthFile("/uuid/uuid");
        cmd += "UUID: " + content + '\n';

        /**NOTE:
         * [sys_info]
         * CPU: 8.04%
         * Runing time: 0d.1h:18m:16s
         * OS Mem: 128M
         * Memory Status: total 119M | used 89M | free 30M
         * Flash Size: rootfs - | total 507.0M | used 20%
         * U-Boot Version: 20191223
         * Kernel Version: #61 SMP Thu Dec 19 15:19:18 CST 2019
         * Compiler Version: v500_20180120
         * Software Version: 1.0.1.20191224140647_beta
         * UUID: 38-BB-CD-22-A6-B5-49-80-80-AB-B8-9F-05-80-2B-93
         **/
        ret = this->SendStringMsg(js_ret, cmd, "[sys_info]");
        if(!ret){
            printf("get media.json failed!");
        }
    }

    return true;
}

bool CommandMap::SendStringMsg(cJSON *value, string str, const char *addhead)
{
    if (value && cJSON_IsString(value)) {
        UDPSocket *udpsocket = new UDPSocket();
        if(!strcmp(value->valuestring, "echo")){
            udpsocket->CreateUDPClient(gClientIP, gClientPort);
        }
        else{
            vector<string> infolist;
            SplitString(value->valuestring, infolist, "@");

            udpsocket->CreateUDPClient(infolist[0].c_str(), atoi(infolist[1].c_str()));
        }

        ostringstream content;
        content << addhead << endl;
        if(!str.empty()){
            content << str;
            if(content.str().empty())
                content << "null" << endl;
        }
        else{
            content << "null" << endl;
        }

        if( udpsocket->SendTo(content.str().c_str(), content.str().length()) <= 0){
            printf("udp sendto failed!");
            delete udpsocket;
            return false;
        }

        delete udpsocket;
    }
    else{
        printf("get json Invalid format");
        return false;
    }

    return true;
}

bool CommandMap::DeleteConfigHandler(cJSON *value)
{
    char command[50] = "";
    sprintf(command, "rm %s\n", filepath_);
    system(command);

    return true;
}

bool CommandMap::LoadEchoUdp()
{
    if(echoudp_ != NULL)
        delete echoudp_;

    echoudp_ = new UDPSocket();
    if(gClientPort != 0)
        echoudp_->CreateUDPClient(gClientIP, gClientPort); //设为非阻塞
    else
        return false;

    return true;
}

bool CommandMap::SendFileMsg(cJSON *value, const char *filepath, const char *key)
{
    if (value && cJSON_IsString(value)) {
        UDPSocket *udpsocket = new UDPSocket();
        if(!strcmp(value->valuestring, "echo")){
            udpsocket->CreateUDPClient(gClientIP, gClientPort);
        }
        else{
            vector<string> infolist;
            SplitString(value->valuestring, infolist, "@");

            udpsocket->CreateUDPClient(infolist[0].c_str(), atoi(infolist[1].c_str()));
        }

        ifstream in(filepath);
        ostringstream content;
        content << key << endl << in.rdbuf();


        if( udpsocket->SendTo(content.str().c_str(), content.str().length()) <= 0){
            COMMON_PRT("udp sendto failed!");
            delete udpsocket;
            return false;
        }

        delete udpsocket;
    }
    else{
        COMMON_PRT("get json Invalid format");
        return false;
    }

    return true;
}

bool CommandMap::LetMeTellYou(const char *msg)
{
    if((gClientPort != 0) &&
       (strlen(gClientIP) != 0)){
        COMMON_PRT("echo data: %s", msg);
        if( echoudp_->SendTo(gClientIP, gClientPort, msg, strlen(msg)) < 0 ){
            COMMON_PRT("echoudp sendto error: %s", strerror(errno));
            return false;
        }
    }

    return true;
}

void CommandMap::ClearCmdTypeAndFunc()
{
    cmdfunc_.clear();
    cmdtype_.clear();
}

bool CommandMap::GetLogHandler(cJSON *value)
{
    bool ret = false;
    cJSON* js_ret = NULL;

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_elog");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "./log/*", "[elog]");
        if(!ret){
            printf("GetElogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_sys_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/sys", "[sys_log]");
        if(!ret){
            printf("GetSysLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_vdec_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/vdec", "[vdec_log]");
        if(!ret){
            printf("GetVdecLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_vo_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/vo", "[vo_log]");
        if(!ret){
            printf("GetVoLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_venc_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/venc", "[venc_log]");
        if(!ret){
            printf("GetVencLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_vi_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/vi", "[vi_log]");
        if(!ret){
            printf("GetViLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_vpss_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/vpss", "[vpss_log]");
        if(!ret){
            printf("GetVpssLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_hdmi_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/hdmi0", "[hdmi_log]");
        if(!ret){
            printf("GetHdmiLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_ao_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/ao", "[ao_log]");
        if(!ret){
            printf("GetAoLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_ai_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/umap/ai", "[ai_log]");
        if(!ret){
            printf("GetAiLogHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_bus_input");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/bus/input/devices", "[bus_input]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_upgrade_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/tmp/upgrade.tmp", "[upgrade_log]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_bin_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/data/log/bin_up.log", "[bin_log]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_patch_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/data/log/patch_up.log", "[patch_log]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_debug_log");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/data/log/abnormal_long.log", "[debug_log]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_media-mem");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/proc/media-mem", "[media-mem]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_hardware_info");
    if (js_ret) {
        ret = this->SendFileMsg(js_ret, "/version/hardware_info", "[hardware_info]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_sys_msg");
    if (js_ret) {
        string cmd = "sh /home/public/sysmonitor.sh";
        string str;
        LinuxShell(cmd.c_str(), str);
        ret = this->SendStringMsg(js_ret, str, "[sys_msg]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_home_file");
    if (js_ret) {
        string cmd = "sh /home/public/readFileInfo.sh /home";
        string str;
        LinuxShell(cmd.c_str(), str);
        ret = this->SendStringMsg(js_ret, str, "[file_info]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_curr_file");
    if (js_ret) {
        string cmd = "sh /home/public/readFileInfo.sh $PWD";
        string str;
        LinuxShell(cmd.c_str(), str);
        ret = this->SendStringMsg(js_ret, str, "[file_info]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "get_public_file");
    if (js_ret) {
        string cmd = "sh /home/public/readFileInfo.sh /home/public";
        string str;
        LinuxShell(cmd.c_str(), str);
        ret = this->SendStringMsg(js_ret, str, "[file_info]");
        if(!ret){
            printf("GetSysMsgHandler() failed!");
        }
    }

    return true;
}

bool CommandMap::GetShellHandler(cJSON *value)
{
    bool ret = false;
    cJSON* js_ret = NULL;

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "cmd");
    if (js_ret && cJSON_IsString(js_ret)) {
        char cmd[1024] = "";

        if(strstr(js_ret->valuestring, "rm")       ||
           strstr(js_ret->valuestring, "reboot")   ||
           strstr(js_ret->valuestring, "poweroff") ||
           strstr(js_ret->valuestring, "halt")     ||
           strstr(js_ret->valuestring, "kill")){
            sprintf(cmd, "echo %s > /tmp/cmd.tmp", "\"rm/reboot/poweroff/halt/kill are not supported in this API\"");
        }
        else
            sprintf(cmd, "%s > /tmp/cmd.tmp", js_ret->valuestring);
        system(cmd);

        js_ret = cJSON_GetObjectItemCaseSensitive(value, "reply_way");
        if (js_ret && cJSON_IsString(js_ret)) {
            ret = this->SendFileMsg(js_ret, "/tmp/cmd.tmp", "[shell_cmd]");
            if(!ret){
                COMMON_PRT("get /tmp/cmd.tmp failed!");
            }
        }
    }

    return true;
}

bool CommandMap::RemoveFileHandler(cJSON *value)
{
    cJSON* js_ret = NULL;
    js_ret = cJSON_GetObjectItemCaseSensitive(value, "filename");
    if(js_ret && cJSON_IsString(js_ret)){
        char cmd[128] = "";
        if(!strcmp(js_ret->valuestring, "media.json") ||
           !strcmp(js_ret->valuestring, "software.ini") ||
           !strcmp(js_ret->valuestring, "net.ini") ||
           !strcmp(js_ret->valuestring, "node_name") ||
           !strcmp(js_ret->valuestring, "remserial.ini") ||
           !strcmp(js_ret->valuestring, "venc.ini")){

            sprintf(cmd, "rm /data/%s", js_ret->valuestring);
        }
        else{
            if(!strcmp(js_ret->valuestring, "load")      ||
               !strcmp(js_ret->valuestring, "dog.sh")    ||
               !strcmp(js_ret->valuestring, "netserver") ||
               !strcmp(js_ret->valuestring, "processor_III")){

                COMMON_PRT("Host want to remove [%s] was prevented.", js_ret->valuestring);

                this->ACK(CODE_SYS_ERR, 2, cmdfunc_.c_str());
                return true;
            }
            else
                sprintf(cmd, "rm %s", js_ret->valuestring);
        }

        COMMON_PRT("Host want to remove [%s]", cmd + 3);
        system(cmd);
    }

    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());

    return true;
}


bool CommandMap::SetTickMasterHandler(cJSON *value)
{
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();

    cJSON *js_ret = NULL;

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "auto");
    if (js_ret) {
        if(cJSON_IsTrue(js_ret)){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickAuto, "1") ){
                COMMON_PRT("set tickmaster->auto set failed");
            }
        }
        else if(cJSON_IsFalse(js_ret)){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickAuto, "0") ){
                COMMON_PRT("set tickmaster->auto set failed");
            }
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "module");
    if (js_ret && cJSON_IsString(js_ret)) {
        if ( !strcmp(js_ret->valuestring, "master") ||
             !strcmp(js_ret->valuestring, "slave")){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickModule, js_ret->valuestring) ){
                COMMON_PRT("set tickmaster->module set failed");
            }
        }
        else
            COMMON_PRT("tickmaster->module Invalid format");
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "rate");
    if (js_ret && cJSON_IsNumber(js_ret)) {
        if(js_ret->valueint > 0 && js_ret->valueint < 1024){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickRate, js_ret->valueint) ){
                COMMON_PRT("set tickmaster->rate set failed");
            }
        }
        else
            COMMON_PRT("tickmaster->rate Invalid format");
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "offset");
    if (js_ret && cJSON_IsNumber(js_ret)) {
        if(js_ret->valueint > -900 && js_ret->valueint < 900){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickOffset, js_ret->valueint) ){
                COMMON_PRT("set tickmaster->offset set failed");
            }
        }
        else
            COMMON_PRT("tickmaster->offset Invalid format");
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "master_delay");
    if (js_ret && cJSON_IsNumber(js_ret)) {
        if(js_ret->valueint >= -1 && js_ret->valueint < 16700){
            if( !softwareconfig->SetConfig(SoftwareConfig::kTickMDelay, js_ret->valueint) ){
                COMMON_PRT("set tickmaster->master_delay set failed");
            }
        }
        else
            COMMON_PRT("tickmaster->offset Invalid format");
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "master_ip");
    if (js_ret && cJSON_IsString(js_ret)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kTickServerIp, js_ret->valueint) ){
            COMMON_PRT("set tickmaster->master ip set failed");
        }
    }

    js_ret = cJSON_GetObjectItemCaseSensitive(value, "master_port");
    if (js_ret && cJSON_IsNumber(js_ret)) {
        if( !softwareconfig->SetConfig(SoftwareConfig::kTickServerPort, js_ret->valueint) ){
            COMMON_PRT("set tickmaster->master port set failed");
        }
    }



    //NOTE: 重启tickMaster,完成主从节点设置的实时生效
    if (softwareconfig->GetConfig(SoftwareConfig::kTickModule) == "master"){
        if(gb_master == false){
            system("killall tickMaster");
            gb_master = true;
        }
    }
    else{
        if(gb_master == true){
            system("killall tickMaster");
            gb_master = false;
        }
    }

    softwareconfig->SaveConfig();
    this->ACK(CODE_SUCCEED, cmdfunc_.c_str());

    return true;
}
