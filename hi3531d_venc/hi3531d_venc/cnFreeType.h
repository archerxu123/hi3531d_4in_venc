#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <wchar.h>
#include <ft2build.h>
#include FT_FREETYPE_H
#include FT_GLYPH_H
#include FT_OUTLINE_H
#include "software_config.h"
#include "bmptoyuv.h"

typedef struct fbdev
{
    int fdfd; //open "dev/fb0"
    struct fb_var_screeninfo vinfo;
    struct fb_fix_screeninfo finfo;
    long int screensize;
    char *map_fb;
}FBDEV;

typedef struct slangBmp_ {
    int             rows;
    int             width;
    int             horiBearingY;
    unsigned char*  buffer;
}slangBmp;

typedef struct tagIMAGEDATA
{
    unsigned char blue;
    unsigned char green;
    unsigned char red;
}IMAGEDATA;

extern slangBmp *chnBmp[VI_PORT_NUMBER];
extern unsigned short testData[VI_PORT_NUMBER][30];
extern IMAGEDATA charColor[VI_PORT_NUMBER];
extern POINT_S startPoint[VI_PORT_NUMBER];

void init_dev(FBDEV *dev);
void clean_lcd();
void draw_dot(FBDEV *dev,int x,int y);
void freetype_draw(unsigned long ch, FT_Face face, FT_Bitmap *bitmap,int *offY);
void freetype_exit(int vi_port);
int font_init(int vi_port);
void putTextFrame(slangBmp bitmap, POINT_S chPoint, VIDEO_FRAME_INFO_S * srcFrame, IMAGEDATA charColor,bool crossFlag, int max_height);
void setTypeSize(int vi_port, uint w_px, uint h_px);


FBDEV fr_dev;

void init_dev(FBDEV *dev)
{
    /*FBDEV *fr_dev=dev;
    fr_dev->fdfd=open("/dev/fb0",O_RDWR);
    printf("the framebuffer device was opended successfully.\n");
    ioctl(fr_dev->fdfd,FBIOGET_FSCREENINFO,&(fr_dev->finfo)); //»ñÈ¡ ¹Ì¶¨²ÎÊý
    ioctl(fr_dev->fdfd,FBIOGET_VSCREENINFO,&(fr_dev->vinfo)); //»ñÈ¡¿É±ä²ÎÊý
    fr_dev->screensize=fr_dev->vinfo.xres*fr_dev->vinfo.yres*fr_dev->vinfo.bits_per_pixel/8;
    fr_dev->map_fb=(char *)mmap(NULL,fr_dev->screensize,PROT_READ|PROT_WRITE,MAP_SHARED,fr_dev->fdfd,0);
    printf("init_dev successfully.\n");*/
}

void clean_lcd()
{
    int i;
    for(i=0;i<fr_dev.screensize;i++)
        *((unsigned short int*)(fr_dev.map_fb+i)) = 255<<11|255<<5|255;
}

void draw_dot(FBDEV *dev,int x,int y) //(x.y) ÊÇ×ø±ê
{
    FBDEV *fr_dev=dev;
    int *xx=&x;
    int *yy=&y;
    long int location=0;
    location=location=(*xx+fr_dev->vinfo.xoffset)*(fr_dev->vinfo.bits_per_pixel/8)+(*yy+fr_dev->vinfo.yoffset)*fr_dev->finfo.line_length;
    int b=0;
    int g=0;
    int r=0;
    unsigned short int t=r<<11|g<<5|b;
    *((unsigned short int *)(fr_dev->map_fb+location))=t;
}

FT_Face  face[4];
FT_Library    library[4];
int size;
FT_UInt       now_index;
FT_UInt       old_index;


int font_init(int vi_port)
{

    int err = FT_Init_FreeType(&library[vi_port]);
    if(err)
    {
        printf("Init library failed\n");
        return 0;
    }

    err = FT_New_Face(library[vi_port],"/font/font.ttc",0,&face[vi_port]);
    if(err == FT_Err_Unknown_File_Format)
    {
        printf("the font is not supported\n");
        return 0;
    }
    else if(err)
    {
        printf("the font file can't open\n");
        return 0;
    }
    return 1;
}

void setTypeSize(int vi_port, uint w_px, uint h_px)
{
    FT_Set_Pixel_Sizes(face[vi_port],w_px,h_px);
}


void freetype_draw(unsigned short ch,FT_Face face, FT_Bitmap *bitmap,int *offY)
{
    FT_UInt       now_index;

    now_index = FT_Get_Char_Index(face,ch);
    int err = FT_Load_Glyph(face,FT_Get_Char_Index(face,ch),FT_LOAD_DEFAULT|FT_LOAD_NO_BITMAP);
    if(err)
    {
        printf("FT_Load_Glyph failed\n");
        return;
    }

    FT_Glyph glyph;
    err = FT_Get_Glyph(face->glyph,&glyph);
    if(err)
    {
        printf("FT_Get_Glyph failed\n");
        return;
    }
    FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);
    FT_Glyph_To_Bitmap(&glyph,FT_RENDER_MODE_NORMAL,0,1);
    FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph)glyph;
    *bitmap=bitmap_glyph->bitmap;
    *offY = face->glyph->metrics.horiBearingY / 64;

}

unsigned char clipValue(unsigned char tmpValue,unsigned char minValue,unsigned char maxValue)
{
    if(tmpValue >= maxValue)
        return maxValue;
    else if(tmpValue <= minValue)
        return minValue;
    else
        return tmpValue;
}

void putTextFrame(slangBmp bitmap,POINT_S chPoint,VIDEO_FRAME_INFO_S * srcFrame,IMAGEDATA charColor,bool crossFlag, int max_height)
{
    HI_U8 y = 0, u = 0, v = 0;
    HI_U8 bg_y = 0, bg_u = 0, bg_v = 0;
    HI_S32 pointX = chPoint.s32X;
    HI_S32 pointY = chPoint.s32Y;
    HI_U32 tmpStride = srcFrame->stVFrame.u32Stride[0];
    HI_S32 indexY = 0,indexU = 0,indexV = 0;
    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    int osd_touming = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_Transparent));

    /*if(osd_touming == 0){
        IMAGEDATA bgColor;
        char *end;
        string basemap_bgcolor  = softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_BGColor);
        int color = static_cast<int>(strtol(basemap_bgcolor.c_str(), &end, 16));
        bgColor.red = color>>16;
        bgColor.green = color>>8&0x00ff;
        bgColor.blue = color&0x0000ff;

        bg_y = (HI_U8)((66*bgColor.red + 129*bgColor.green + 25*bgColor.blue + 128)>>8) +16;
        bg_y = clipValue(bg_y,0,255);
        bg_u = (HI_U8)((112*bgColor.blue - 38*bgColor.red -74*bgColor.green + 128)>>8) +128;
        bg_u = clipValue(bg_u,0,255);
        bg_v = (HI_U8)((112*bgColor.red - 94*bgColor.green - 18*bgColor.blue +128)>>8) +128;
        bg_v = clipValue(bg_v,0,255);

        for (int i = 0; i < max_height; ++i)
        {
            for (int j = 0; j < bitmap.width; ++j)
            {
                if(crossFlag) {
                    if((pointX + j) <= tmpStride) {
                        indexY = (pointY + i)*tmpStride + pointX + j;
                        *(unsigned char*)(srcFrame->stVFrame.pVirAddr[0] + indexY) = bg_y;

                        indexU = ((pointY + i)/2)*(tmpStride) + pointX + j - (pointX + j)%2 + 1;
                        indexV = ((pointY + i)/2)*(tmpStride) + pointX + j - (pointX + j)%2;
                        *(unsigned char*)(srcFrame->stVFrame.pVirAddr[1] + indexU) = bg_u;
                        *(unsigned char*)(srcFrame->stVFrame.pVirAddr[1] + indexV) = bg_v;
                    }
                }
                else {
                    indexY = (pointY + i)*tmpStride + pointX + j;
                    *(unsigned char*)(srcFrame->stVFrame.pVirAddr[0] + indexY) = bg_y;

                    indexU = ((pointY + i)/2)*(tmpStride) + pointX + j - (pointX + j)%2 + 1;
                    indexV = ((pointY + i)/2)*(tmpStride) + pointX + j - (pointX + j)%2;
                    *(unsigned char*)(srcFrame->stVFrame.pVirAddr[1] + indexU) = bg_u;
                    *(unsigned char*)(srcFrame->stVFrame.pVirAddr[1] + indexV) = bg_v;
                }
            }
        }
    }*/

    //RGB转YUV
    y = (HI_U8)((66*charColor.red + 129*charColor.green + 25*charColor.blue + 128)>>8) +16;
    y = clipValue(y,0,255);
    u = (HI_U8)((112*charColor.blue - 38*charColor.red -74*charColor.green + 128)>>8) +128;
    u = clipValue(u,0,255);
    v = (HI_U8)((112*charColor.red - 94*charColor.green - 18*charColor.blue +128)>>8) +128;
    v = clipValue(v,0,255);
    //printf("%d,%d,%d\n",y,u,v);

    for (int i = 0; i < bitmap.rows; ++i)
    {
        for (int j = 0; j < bitmap.width; ++j)
        {

            if (bitmap.buffer[i * bitmap.width + j] != 0)//像素点有内容
            {
                if(crossFlag) {
                    if((pointX + j) <= tmpStride) {
                        indexY = (pointY + i)*tmpStride + pointX + j;
                        *(unsigned char*)(srcFrame->stVFrame.pVirAddr[0] + indexY) = y;

                        indexU = ((pointY + i)/2)*(tmpStride) + pointX + j - (pointX + j)%2 + 1;
                        indexV = ((pointY + i)/2)*(tmpStride) + pointX + j - (pointX + j)%2;
                        *(unsigned char*)(srcFrame->stVFrame.pVirAddr[1] + indexU) = u;
                        *(unsigned char*)(srcFrame->stVFrame.pVirAddr[1] + indexV) = v;
                    }
                }
                else {
                    indexY = (pointY + i)*tmpStride + pointX + j;
                    *(unsigned char*)(srcFrame->stVFrame.pVirAddr[0] + indexY) = y;

                    indexU = ((pointY + i)/2)*(tmpStride) + pointX + j - (pointX + j)%2 + 1;
                    indexV = ((pointY + i)/2)*(tmpStride) + pointX + j - (pointX + j)%2;
                    *(unsigned char*)(srcFrame->stVFrame.pVirAddr[1] + indexU) = u;
                    *(unsigned char*)(srcFrame->stVFrame.pVirAddr[1] + indexV) = v;
                }
            }
        }
    }
}

bool checkLetter(char srcChar)
{
    char diffChar[6] = "gjpqy";
    int i = 0;
    while(diffChar[i]) {
        if(srcChar == diffChar[i++])
            return true;
    }
    return false;
}

unsigned short Get_Unicode(char *utf8)
{

    unsigned short unicode;
    unicode = utf8[0];
    //printf("unicode before:0x%x\n",unicode);
    if (unicode >= 0xF0)
    {
        unicode = (unsigned short) (utf8[0] & 0x07) << 18;
        unicode |= (unsigned short) (utf8[1] & 0x3F) << 12;
        unicode |= (unsigned short) (utf8[2] & 0x3F) << 6;
        unicode |= (unsigned short) (utf8[3] & 0x3F);
    }
    else if (unicode >= 0xE0)
    {
        unicode = (unsigned short) (utf8[0] & 0x0F) << 12;
        unicode |= (unsigned short) (utf8[1] & 0x3F) << 6;
        unicode |= (unsigned short) (utf8[2] & 0x3F);
    }
    else if (unicode >= 0xC0)
    {
        unicode = (unsigned short) (utf8[0] & 0x1F) << 6;
        unicode |= (unsigned short) (utf8[1] & 0x3F);
    }
    return unicode;
}

int utf_to_unicode( unsigned long utf, unsigned char *unicode )
{
    int size = 0;
    if ( utf <= 0x7F )
    {
        *( unicode + size++ ) = utf & 0x7F;
    }
    else if ( utf >= 0xC080 && utf <= 0xCFBF )
    {
        *( unicode + size++ ) = ( ( utf >> 10 ) & 0x07 );
        *( unicode + size++ ) = ( utf & 0x3F ) | ( ( ( utf >> 8 ) & 0x03 ) << 6);
    }
    else if ( utf >= 0xE08080 && utf <= 0xEFBFBF )
    {
        *( unicode + size++ ) = ( ( utf >> 10 ) & 0x0F ) | (( utf >> 16 ) & 0x0F ) << 4;
        *( unicode + size++ ) = ( utf & 0x3F ) | ((( utf >> 8 ) & 0x03 ) << 6 );
    }
    else if ( utf >= 0xF0808080 && utf <= 0xF7BFBFBF )
    {
        *( unicode + size++ ) = ( (utf >> 20 ) & 0x03 ) | ((( utf >> 24 ) & 0x07 ) << 2 );
        *( unicode + size++ ) = (( utf >> 10 ) & 0x0F ) | ( ( ( utf >> 16 ) & 0x0F ) << 4 );
        *( unicode + size++ ) = ( utf & 0x3F ) | ( ( utf >> 8 ) & 0x03 ) << 6;
    }
    else if ( utf >= 0xF880808080 && utf <= 0xFBBFBFBFBF )
    {
        *( unicode + size++ ) = ( utf >> 32 ) & 0x03;
        *( unicode + size++ ) = ( ( utf >> 20 ) & 0x03 ) | ( ( ( utf >> 24 ) & 0x3F ) << 2 );
        *( unicode + size++ ) = ( ( utf >> 10 ) & 0x0F ) | ( ( ( utf >> 16 ) & 0x0F ) << 4 );
        *( unicode + size++ ) = ( utf & 0x3F ) | ( ( (utf >> 8) & 0x03 ) << 6 );
    }
    else if ( utf >= 0xFC8080808080 && utf <= 0xFDBFBFBFBFBF )
    {
        *( unicode + size++ ) = ( ( utf >> 32 ) & 0x3F ) | ( ( ( utf >> 40 ) & 0x01 ) << 6 );
        *( unicode + size++ ) = ( ( utf >> 20 ) & 0x03 ) | ( ( ( utf >> 24 ) & 0x3F ) << 2 );
        *( unicode + size++ ) = ( ( utf >> 10 ) & 0x0F ) | ( ( ( utf >> 16 ) & 0x0F ) << 4 );
        *( unicode + size++ ) = ( utf & 0x3F ) | ( ( ( utf >> 8 ) & 0x03 ) << 6 );
    }
    else
    {
        printf( "Error : unknow scope\n" );
        return -1;
    }

    *( unicode + size ) = '\0';
    return size;
}

void unicode_print( unsigned char *unicode, int size )
{
    if ( size == -1 )
    {
        printf("Error : unknow scope\n");
        return;
    }

    int index = 0;
    for ( ; index < size; index += 1 )
    {
        printf( "%02X", *( unicode + index ) );
    }

    printf("\n");
}

bool init_flag[4] = {false,false,false,false};

int set_font(HI_U32 vi_port, HI_U32 *bmp_max_height)
{

    FT_Bitmap tmpMap;
    int osdname_len =0;
    char *end;
    char osd_name[VI_PORT_NUMBER][100];
    char basemap_bgcolor[VI_PORT_NUMBER][15];
    HI_U32 osd_size[VI_PORT_NUMBER];
    int size_n[VI_PORT_NUMBER];
    int horiBearingY = 0;

    auto softwareconfig = Singleton<SoftwareConfig>::getInstance();
    sprintf(basemap_bgcolor[0], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_Color)).c_str());
    sprintf(basemap_bgcolor[1], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn1_Osd_Color)).c_str());
    sprintf(basemap_bgcolor[2], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn2_Osd_Color)).c_str());
    sprintf(basemap_bgcolor[3], "%s", string(softwareconfig->GetConfig(SoftwareConfig::kChn3_Osd_Color)).c_str());
    int bgcolor = static_cast<int>(strtol(basemap_bgcolor[vi_port]+1, &end, 16));
    charColor[vi_port].red = bgcolor>>16;
    charColor[vi_port].green = bgcolor>>8&0x00ff;
    charColor[vi_port].blue = bgcolor&0x0000ff;
    //printf("%x,%x,%x,%x\n",bgcolor,charColor.red,charColor.green,charColor.blue);
    osd_size[0] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_Size));
    startPoint[0].s32X = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_X));
    startPoint[0].s32Y = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_Y));
    osd_size[1] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Osd_Size));
    startPoint[1].s32X = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Osd_X));
    startPoint[1].s32Y = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn1_Osd_Y));
    osd_size[2] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Osd_Size));
    startPoint[2].s32X = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Osd_X));
    startPoint[2].s32Y = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn2_Osd_Y));
    osd_size[3] = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Osd_Size));
    startPoint[3].s32X = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Osd_X));
    startPoint[3].s32Y = str2int(softwareconfig->GetConfig(SoftwareConfig::kChn3_Osd_Y));

    size_n[0] = string(softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_Name)).length();
    if(size_n[0] > 90)
        size_n[0] = 90;
    sprintf(osd_name[0],"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn0_Osd_Name)).c_str());
    size_n[1] = string(softwareconfig->GetConfig(SoftwareConfig::kChn1_Osd_Name)).length();
    if(size_n[1] > 90)
        size_n[1] = 90;
    sprintf(osd_name[1],"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn1_Osd_Name)).c_str());
    size_n[2] = string(softwareconfig->GetConfig(SoftwareConfig::kChn2_Osd_Name)).length();
    if(size_n[2] > 90)
        size_n[2] = 90;
    sprintf(osd_name[2],"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn2_Osd_Name)).c_str());
    size_n[3] = string(softwareconfig->GetConfig(SoftwareConfig::kChn3_Osd_Name)).length();
    if(size_n[3] > 90)
        size_n[3] = 90;
    sprintf(osd_name[3],"%s",string(softwareconfig->GetConfig(SoftwareConfig::kChn3_Osd_Name)).c_str());

    int font_flag  = 1;
    if(!init_flag[vi_port]) {
        chnBmp[vi_port] = (slangBmp*)malloc(30 * sizeof(slangBmp));
        font_flag = font_init(vi_port);
        init_flag[vi_port] = true;
    }
    setTypeSize(vi_port,osd_size[vi_port],osd_size[vi_port]);

    int m = 0;
    if(font_flag > 0){
        while(size_n[vi_port])
        {
            testData[vi_port][osdname_len] = Get_Unicode(osd_name[vi_port] + m);
            //printf("%d,code:0x%x \n",osdname_len,testData[osdname_len]);
            if(testData[vi_port][osdname_len]&0xff00)
            {
                size_n[vi_port] -= 3;//如果是中文字符，则需要-3
                m += 3;
            }
            else
            {
                size_n[vi_port]--;
                m ++;
            }
            freetype_draw(testData[vi_port][osdname_len],face[vi_port],&tmpMap,&horiBearingY);
            chnBmp[vi_port][osdname_len].buffer = (unsigned char*)malloc(tmpMap.rows *tmpMap.width);
            chnBmp[vi_port][osdname_len].rows = tmpMap.rows;
            chnBmp[vi_port][osdname_len].width = tmpMap.width;
            chnBmp[vi_port][osdname_len].horiBearingY = face[vi_port]->glyph->metrics.horiBearingY / 64;
            if(tmpMap.rows > *bmp_max_height)
                *bmp_max_height = tmpMap.rows;
            memcpy(chnBmp[vi_port][osdname_len].buffer,tmpMap.buffer,tmpMap.rows * tmpMap.width);
            osdname_len++;
        }
    }
    return osdname_len;
}


void freetype_exit(int vi_port)
{
    FT_Done_Face(face[vi_port]);
    FT_Done_FreeType(library[vi_port]);
}

