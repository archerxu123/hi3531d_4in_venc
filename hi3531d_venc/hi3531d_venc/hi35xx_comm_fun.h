#ifndef HI35xx_COMM_FUN_H
#define HI35xx_COMM_FUN_H

#include "hi3531d_common/sample_comm.h"
#include "hi3531d_common/sample_comm_ivs.h"
#include <hifb.h>

#include "global.h"
#include "mytimer.h"
#include "audio_g711.h"
#include "../common/version.h"

#ifdef __cplusplus
#if __cplusplus
extern "C"{
#endif
#endif /* Begin of #ifdef __cplusplus */

#define CUT_NUMBER   1
#define SAMPLE_VI_MODE      SAMPLE_VI_MODE_8_1080P
#define DMA_MIN_ROW     1080
#define HI35xx_AUDIO_PTNUMPERFRM 1024
#define _CEILING_2_POWER(x,a)   ( ((x) + ((a) - 1) ) & ( ~((a) - 1) ) )
#define _FLOOR_2_POWER(x,a)     (  (x) & (~((a) - 1) ) )
#define DMA_MIN_WIDTH   32
#define DMA_MIN_HEIGHT  270

#define MIN(x,y)  x>=y?y:x
extern HI_S32 SnapQuality[4];
extern PARAMETER viParam[4];
extern enum audio_type coderFormat[4];

//RGB色彩
typedef struct COLOR_RGB
{
    HI_U8 R;
    HI_U8 G;
    HI_U8 B;
}COLOR_RGB;
//YUV色彩
typedef struct COLOR_YUV
{
    HI_U8 Y;
    HI_U8 U;
    HI_U8 V;
}COLOR_YUV;

/*******************************************************
    function announce
*******************************************************/
HI_S32 HI35xx_COMM_SYS_CalcPicVbBlkSize(SIZE_S *stSize, PIXEL_FORMAT_E enPixFmt, HI_U32 u32AlignWidth,COMPRESS_MODE_E enCompFmt);

HI_S32 HI35xx_COMM_VI_Start(VI_DEV ViDev, VI_CHN ViChn, SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type);
HI_S32 HI35xx_COMM_VI_Stop(VI_DEV ViDev, VI_CHN ViChn);
HI_S32 HI35xx_COMM_VI_BindVpss(VI_DEV ViDev, VPSS_GRP VpssGrp,VI_CHN ViChn);
HI_S32 HI35xx_COMM_VI_StartDev(VI_DEV ViDev, SAMPLE_VI_MODE_E enViMode);
HI_S32 HI35xx_COMM_VI_SetMask(VI_DEV ViDev, VI_DEV_ATTR_S *pstDevAttr);
HI_S32 HI35xx_COMM_VI_StartChn(VI_CHN ViChn, RECT_S *pstCapRect, SIZE_S *pstTarSize, HI_S32 input_type, SAMPLE_VI_CHN_SET_E enViChnSet);
HI_S32 HI35xx_COMM_VI_BindVenc(VI_DEV ViDev, VENC_CHN VeChn,VI_CHN ViChn);
HI_S32 HI35xx_COMM_VI_UnBindVenc(VI_DEV ViDev, VENC_CHN VeChn,VI_CHN ViChn);
HI_S32 HI35xx_COMM_VI_BindVo(VI_DEV ViDev, VI_CHN ViChn, VO_LAYER VoLayer, VO_CHN VoChn);

HI_S32 HI35xx_COMM_VENC_Start(VI_CHN vi_snap,VENC_CHN VencChn, PAYLOAD_TYPE_E enType, SIZE_S stMaxPicSize, SIZE_S stPicSize, SAMPLE_RC_E enRcMode, HI_U32  u32Profile,  HI_U32 u32Gop, HI_U32 u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 u32DstFrameRate, HI_U32 u32MinQp, HI_U32 u32MaxQp, HI_U32 u32MinIQp, HI_U32 u32IQp, HI_U32 u32PQp, HI_U32 u32BQp, HI_U32 u32MinIProp, HI_U32 u32MaxIProp, HI_BOOL setFlag);
HI_S32 SAMPLE_COMM_VENC_SaveStream(PAYLOAD_TYPE_E enType, FILE* pFd, VENC_STREAM_S* pstStream);
HI_S32 HI35xx_COMM_VENC_SnapStart(VENC_CHN VencChn, SIZE_S *pstSize);
HI_S32 HI35xx_COMM_VENC_SnapStop(VENC_CHN VencChn,
                                 HI_BOOL bBindVpss = HI_FALSE, VPSS_GRP VpssGrp = 0, VPSS_CHN VpssChn = 0);
HI_S32 RSAMPLE_COMM_VENC_BindVpss(VENC_CHN VeChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn = 0);
HI_S32 RSAMPLE_COMM_VENC_UnBindVpss(VENC_CHN VeChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn);
HI_S32 HI35xx_VENC_Snap_size_vicop(void );

HI_S32 HI35xx_COMM_VO_StartChn(VO_LAYER VoLayer, VO_CHN VoChn);
HI_S32 HI35xx_COMM_VO_StopChn(VO_LAYER VoLayer, VO_CHN VoChn);
HI_S32 HI35xx_COMM_VO_BindVpss(VPSS_GRP VpssGrp, VPSS_CHN VpssChn, VO_LAYER VoLayer, VO_CHN VoChn);
HI_S32 HI35xx_COMM_VO_GetWH(VO_INTF_SYNC_E *enIntfSync, SIZE_S stVoSize, HI_U32 pu32Frm);

HI_S32 HI35xx_COMM_VPSS_Start(HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr);
HI_S32 Hi35xx_VPSS_Destory(VPSS_GRP vpssGrp_, VPSS_CHN vpssChn_);
HI_S32 HI35xx_VPSS_Start(VPSS_GRP vpssGrp_, VPSS_CHN vpssChn_, SIZE_S vpssSzie_, SIZE_S maxSize_);
HI_S32 HI35xx_COMM_SYS_MemConfig(HI_S32 s32VdChnCnt, HI_S32 s32VpssGrpCnt, HI_S32 s32VeChnCnt, VO_DEV VoDevId);

HI_S32 HI35xx_COMM_AUDIO_StartAi(AUDIO_DEV AiDevId, HI_S32 s32AiChn, AIO_ATTR_S* pstAioAttr,
                                 AUDIO_SAMPLE_RATE_E enOutSampleRate, HI_BOOL bResampleEn, HI_VOID* pstAiVqeAttr, HI_U32 u32AiVqeType);
HI_S32 HI35xx_COMM_AUDIO_StopAi(AUDIO_DEV AiDevId, HI_S32 s32AiChn, HI_BOOL bResampleEn, HI_BOOL bVqeEn);
HI_S32 HI35xx_COMM_VI_UnBindVpss(VI_DEV ViDev, VENC_CHN VpssGrp,VI_CHN ViChn);
char *getFileAll(char *pBuf, char *fname,int *length);
HI_BOOL HI35xx_IVE_MAP(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *stSrcFrame, POINT_S stDstPoint, RECT_S stCropRect, HI_BOOL bTimeOut, HI_BOOL uv_IVE);

HI_S32 Hi_SetReg(HI_U32 u32Addr, HI_U32 u32Value);
HI_S32 Hi_GetReg(HI_U32 u32Addr, HI_U32 *pu32Value);
int tim_subtract(struct timeval *result, struct timeval *begin, struct timeval *end );

HI_S32 HI35xx_VB_PoolInit(SIZE_S stPoolSize, HI_U32 u32BlkCnt);
HI_S32 HI35xx_VB_PoolDestroy();
HI_BOOL HI35xx_StructureVFrmInfo(VIDEO_FRAME_INFO_S *pstFrmInfo, HI_U32 u32Block, SIZE_S stSize);
HI_BOOL Hi35xx_IVE_DmaCopy(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *pstSrcFrame,
                           POINT_S stDstPoint, RECT_S stCropRect,
                           HI_BOOL isQuery = HI_FALSE, HI_BOOL bTimeOut = HI_FALSE);
HI_BOOL Hi35xx_IVE_And(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *pstSrcFrame1,
                       VIDEO_FRAME_INFO_S *pstSrcFrame2,
                       POINT_S stDstPoint, POINT_S stSrc1Point, POINT_S stSrc2Point,
                       SIZE_S stCropSize, HI_BOOL isQuery = HI_FALSE, HI_BOOL bTimeOut = HI_FALSE);
HI_BOOL Hi35xx_IVE_Or(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *pstSrcFrame1,
                      VIDEO_FRAME_INFO_S *pstSrcFrame2,
                      POINT_S stDstPoint, POINT_S stSrc1Point, POINT_S stSrc2Point,
                      SIZE_S stCropSize, HI_BOOL isQuery = HI_FALSE, HI_BOOL bTimeOut = HI_FALSE);
HI_BOOL Hi35xx_IVE_DmaFill(VIDEO_FRAME_INFO_S *pstDstFrame, RECT_S stCropRect, COLOR_YUV stColor,
                           HI_BOOL isQuery = HI_FALSE, HI_BOOL bTimeOut = HI_FALSE);
HI_BOOL HI35xx_PaddingYUV420FrameRGB(VIDEO_FRAME_INFO_S *stVFrmInfo, RECT_S stFrmRect, COLOR_RGB stColor);
HI_BOOL HI35xx_PaddingYUV420FrameYUV(VIDEO_FRAME_INFO_S *stVFrmInfo, RECT_S stFrmRect, COLOR_YUV stColor);
HI_BOOL Hi35xx_VGS_Line(VIDEO_FRAME_INFO_S *stImgOut, POINT_S startPoint, POINT_S endPoint, int w, int clr_rgb);
HI_BOOL Hi35xx_VGS_Frame(VIDEO_FRAME_INFO_S *stImgOut, RECT_S rect, int w, int clr_rgb);

#ifdef __cplusplus
#if __cplusplus
}
#endif
#endif /* End of #ifdef __cplusplus */

#endif // HI35xx_COMM_FUN_H

