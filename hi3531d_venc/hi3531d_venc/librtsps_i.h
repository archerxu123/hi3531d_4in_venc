﻿#ifndef _librtsps_i__
#define _librtsps_i__

/* 
*实现RTSP服务器完成与VLC数据通信
*/

#ifdef WIN32
#ifdef LIBRTSPS_EXPORTS
#define rtsps_extern extern "C" __declspec(dllexport)
#else
#define rtsps_extern extern "C" __declspec(dllimport)
#endif
#else
#define rtsps_extern extern "C" 
#endif

enum video_type
{
	VIDEO_NULL = 0,
	VIDEO_H264,
	VIDEO_H265,
};

enum audio_type
{
	AUDIO_NULL = 0,
	AUDIO_AAC,
	AUDIO_G711A,  // param of crtsp_openstream is unsigned int param[2] = {channel, sample}
	AUDIO_G711U,  // param of crtsp_openstream is unsigned int param[2] = {channel, sample}
	AUDIO_ADPCM, // param of crtsp_openstream is unsigned int param[4] = {channel, sample, bitrate, 0} 注：bitrate码率(kbit/s)取值为 16 24 32 40
};

/*
 * 获取版本信息
 * [in] [out] char v[64]
 * [ret] 成功：>=0 失败：<0
 */
rtsps_extern int crtsps_version( char* v );

/*
 * 打开一个rtsp服务器
 * [in] [port] 服务器监听端口(554)
 * [ret] 成功：>=0 失败：<0
 */
rtsps_extern int crtsps_start( short port );

/*
 * 关闭一个rtsp服务器
 * [ret] 成功：>=0 失败：<0
 */
rtsps_extern int crtsps_stop(void);

/*
 * 在rtsp服务器中开启一路媒体流
 * [in] [vt] 视频编码类型
 * [in] [at] 音频编码类型
 * [in] [path] 流资源地址(/live01)
 * [in] [param] g711或AUDIO_ADPCM时指定音频参数
 * [ret] 成功：stream_id 失败：<0
 */
rtsps_extern int crtsps_openstream( video_type vt, audio_type at, const char* path, void* param = 0 );

/*
 * 在rtsp服务器中关闭一路媒体流
 * [ret] 成功：>=0 失败：<0
 */
rtsps_extern int crtsps_closestream( int stream_id );

/*
 * 向rtsp服务器发布视频数据
 * [in] [stream_id]
 * [in] [data] 视频数据
 * [in] [len] 视频长度
 * [out] [itype] 数据帧类型
 * [in] [ms_pts] 视频时间戳毫秒
 * [ret] 成功：>=0 失败：<0
 */
rtsps_extern int crtsps_pushvideo( int stream_id, void* data, int len, bool itype, unsigned int ms_pts );

/*
 * 向rtsp服务器发布音频数据
 * [in] [stream_id]
 * [in] [data] 音频数据
 * [in] [len] 音频长度
 * [in] [ms_pts] 音频时间戳毫秒
 * [ret] 成功：>=0 失败：<0
 */
rtsps_extern int crtsps_pushaudio( int stream_id, void* data, int len, unsigned int ms_pts );

/*
 * 获取rtsp服务器转发状态
 * [in] [stream_id]
 * [out] [s] 1转发中 0转发停止
 * [ret] 成功：>=0 失败：<0
 */
rtsps_extern int crtsps_status( int stream_id, int* s );


#endif
