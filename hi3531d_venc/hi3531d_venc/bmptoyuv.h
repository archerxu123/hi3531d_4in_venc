#ifndef BMPTOYUV_H
#define BMPTOYUV_H

#include "hi35xx_comm_fun.h"
#include <math.h>



typedef struct BITMAPFILEHEADER
{
    HI_U16 bfType;
    HI_U32 bfSize;
    HI_U16 bfReserved1;
    HI_U16 bfReserved2;
    HI_U32 bfOffBits;
}BITMAPFILEHEADER;

typedef struct BITMAPINFOHEADER
{
    HI_U32 biSize;
    HI_U32 biWidth;
    HI_U32 biHeight;
    HI_U16 biPlanes;
    HI_U16 biBitCount;
    HI_U32 biCompression;
    HI_U32 biSizeImage;
    HI_U32 biXPelsPerMeter;
    HI_U32 biYPelsPerMeter;
    HI_U32 biClrUsed;
    HI_U32 biClrImportant;
}BITMAPINFODEADER;

typedef struct
{
    BITMAPFILEHEADER file_header;
    BITMAPINFOHEADER info_header;
    unsigned long width,height;
    unsigned char *rgbData;
    FILE* (* BMPopen)(const char *,const char *);
    bool (* BMPread)(FILE *);
    bool (* Convert2Yuv)(unsigned long,unsigned long,unsigned char*,unsigned char*,unsigned char*,unsigned char *);
    bool (* BMPclose)(FILE *);
}BMPSTRUCT;

typedef struct
{
    unsigned char *Y,*U,*V;
    FILE* (* YUVopen)(const char *,const char *);
    bool (* YUVwrite)(FILE *);
    bool (* YUVclose)(FILE *);
}YUVSTRUCT;

typedef struct {
    char rgbBlue;
    char rgbGreen;
    char rgbRed;
    char rgbReserved;
}RGB;

typedef struct {
    char yuvY;
    char yuvU;
    char yuvV;
}YUV;

BMPSTRUCT bmp;
YUVSTRUCT yuv;

void initLookupTable();

bool MakePalette(FILE * pFile,BITMAPFILEHEADER &file_h,BITMAPINFOHEADER & info_h,RGB *pRGB_out)
{
    if ((file_h.bfOffBits - sizeof(BITMAPFILEHEADER) - info_h.biSize) == sizeof(RGB)*pow(float(2),info_h.biBitCount))
    {
        fseek(pFile,sizeof(BITMAPFILEHEADER)+info_h.biSize,0);
        fread(pRGB_out,sizeof(RGB),(unsigned int)pow(float(2),info_h.biBitCount),pFile);
        return true;
    }
    else
        return false;
}

void ReadRGB(FILE * pFile,BITMAPFILEHEADER & file_h,BITMAPINFOHEADER & info_h,unsigned char * rgbDataOut)
{
    unsigned long Loop,iLoop,jLoop,width,height,w,h;
    unsigned char mask,*Index_Data,* Data;

    if (((info_h.biWidth/8*info_h.biBitCount)%4) == 0)
        w = info_h.biWidth;
    else
        w = (info_h.biWidth*info_h.biBitCount+31)/32*4;
    if ((info_h.biHeight%2) == 0)
        h = info_h.biHeight;
    else
        h = info_h.biHeight + 1;

    width = w/8*info_h.biBitCount;
    height = h;

    Index_Data = (unsigned char *)malloc(height*width);
    Data = (unsigned char *)malloc(height*width);

    fseek(pFile,file_h.bfOffBits,0);
    if(fread(Index_Data,height*width,1,pFile) != 1)
    {
        printf("read file error!\n\n");
        exit(0);
    }


    for ( iLoop = 0;iLoop < height;iLoop ++)
        for (jLoop = 0;jLoop < width;jLoop++)
        {
            Data[iLoop*width+jLoop] = Index_Data[(height-iLoop-1)*width+jLoop];
        }

    switch(info_h.biBitCount)
    {
    case 24:
        memcpy(rgbDataOut,Data,height*width);
        if(Index_Data)
            free(Index_Data);
        if(Data)
            free(Data);
        return;
    case 16:
        if(info_h.biCompression == 0)
        {
            for (Loop = 0;Loop < height * width;Loop +=2)
            {
                *rgbDataOut = (Data[Loop]&0x1F)<<3;
                *(rgbDataOut + 1) = ((Data[Loop]&0xE0)>>2) + ((Data[Loop+1]&0x03)<<6);
                *(rgbDataOut + 2) = (Data[Loop+1]&0x7C)<<1;

                rgbDataOut +=3;
            }
        }
        if(Index_Data)
            free(Index_Data);
        if(Data)
            free(Data);
        return;
    default:
        RGB *pRGB = (RGB *)malloc(sizeof(RGB)*(unsigned char)pow(float(2),info_h.biBitCount));
        int temp = sizeof(pRGB);
        if(!MakePalette(pFile,file_h,info_h,pRGB))
            printf("No palette!\n\n");

        for (Loop=0;Loop<height*width;Loop++)
        {

            switch(info_h.biBitCount)
            {
            case 1:
                mask = 0x80;
                break;
            case 2:
                mask = 0xC0;
                break;
            case 4:
                mask = 0xF0;
                break;
            case 8:
                mask = 0xFF;
            }

            int shiftCnt = 1;

            while (mask)
            {
                unsigned char index = mask == 0xFF ? Data[Loop] : ((Data[Loop] & mask)>>(8 - shiftCnt * info_h.biBitCount));
                * rgbDataOut = pRGB[index].rgbBlue;
                * (rgbDataOut+1) = pRGB[index].rgbGreen;
                * (rgbDataOut+2) = pRGB[index].rgbRed;

                if(info_h.biBitCount == 8)
                    mask =0;
                else
                    mask >>= info_h.biBitCount;
                rgbDataOut+=3;
                shiftCnt ++;
            }
        }
        if(Index_Data)
            free(Index_Data);
        if(Data)
            free(Data);
// 		if(pRGB)
// 			free(pRGB);
    }

}




float RGBYUV02990[256],RGBYUV05870[256],RGBYUV01140[256];
float RGBYUV01684[256],RGBYUV03316[256];
float RGBYUV04187[256],RGBYUV00813[256];


bool RGB2YUV(unsigned long w,unsigned long h,unsigned char * rgbData,unsigned char * y,unsigned char * u,unsigned char *v)
{
    initLookupTable();//³õÊ¼»¯²éÕÒ±í
    unsigned char *ytemp = NULL;
    unsigned char *utemp = NULL;
    unsigned char *vtemp = NULL;
    utemp = (unsigned char *)malloc(w*h);
    vtemp = (unsigned char *)malloc(w*h);

    unsigned long i,nr,ng,nb,nSize;
    //¶ÔÃ¿¸öÏñËØ½øÐÐ rgb -> yuvµÄ×ª»»
    for (i=0,nSize=0;nSize<w*h*3;nSize+=3)
    {
        nb = rgbData[nSize];
        ng = rgbData[nSize+1];
        nr = rgbData[nSize+2];
        y[i] = (unsigned char)(RGBYUV02990[nr]+RGBYUV05870[ng]+RGBYUV01140[nb]);
        utemp[i] = (unsigned char)(-RGBYUV01684[nr]-RGBYUV03316[ng]+nb/2+128);
        vtemp[i] = (unsigned char)(nr/2-RGBYUV04187[ng]-RGBYUV00813[nb]+128);
        i++;
    }
    //¶ÔuÐÅºÅ¼°vÐÅºÅ½øÐÐ²ÉÑù
    int k = 0;
    for (i=0;i<h;i+=2)
        for(unsigned long j=0;j<w;j+=2)
        {
            u[k]=(utemp[i*w+j]+utemp[(i+1)*w+j]+utemp[i*w+j+1]+utemp[(i+1)*w+j+1])/4;
            v[k]=(vtemp[i*w+j]+vtemp[(i+1)*w+j]+vtemp[i*w+j+1]+vtemp[(i+1)*w+j+1])/4;
            k++;
        }
    //¶Ôy¡¢u¡¢v ÐÅºÅ½øÐÐ¿¹Ôë´¦Àí
    for (i=0;i<w*h;i++)
    {
        if(y[i]<16)
            y[i] = 16;
        if(y[i]>235)
            y[i] = 235;
    }
    for(i=0;i<h*w/4;i++)
    {
        if(u[i]<16)
            u[i] = 16;
        if(v[i]<16)
            v[i] = 16;
        if(u[i]>240)
            u[i] = 240;
        if(v[i]>240)
            v[i] = 240;
    }
    if(utemp)
        free(utemp);
    if(vtemp)
        free(vtemp);
    return true;
}

void initLookupTable()
{
    for (int i=0;i<256;i++)
    {
        RGBYUV02990[i] = (float)0.2990 * i;
        RGBYUV05870[i] = (float)0.5870 * i;
        RGBYUV01140[i] = (float)0.1140 * i;
        RGBYUV01684[i] = (float)0.1684 * i;
        RGBYUV03316[i] = (float)0.3316 * i;
        RGBYUV04187[i] = (float)0.4187 * i;
        RGBYUV00813[i] = (float)0.0813 * i;
    }
}

FILE *OpenBmp(const char *strFileName,const char *strMode)
{
    return fopen(strFileName,strMode);
}

FILE *OpenYuv(const char *strFileName,const char *strMode)
{
    return fopen(strFileName,strMode);
}

bool ReadBmp(FILE *bmpFile)
{
    if(fread(&bmp.file_header,14,1,bmpFile) != 1)
    {
        printf("read file header error!\n\n");
        return false;
    }

    if (bmp.file_header.bfType != 0x4D42)
    {
        printf("Not bmp file!\n\n");
        return false;
    }
    if(fread(&bmp.info_header,40,1,bmpFile) != 1)
    {
        printf("read info header error!\n\n");
        return false;
    }

    if (((bmp.info_header.biWidth/8*bmp.info_header.biBitCount)%4) == 0)
        bmp.width = bmp.info_header.biWidth;
    else
        bmp.width = (bmp.info_header.biWidth*bmp.info_header.biBitCount+31)/32*4;
    if ((bmp.info_header.biHeight%2) == 0)
        bmp.height = bmp.info_header.biHeight;
    else
        bmp.height = bmp.info_header.biHeight + 1;

    bmp.rgbData = (unsigned char *)malloc(bmp.height*bmp.width*3);
    memset(bmp.rgbData,0,bmp.height*bmp.width*3);
    yuv.Y = (unsigned char * )malloc(bmp.height*bmp.width);
    yuv.U = (unsigned char * )malloc((bmp.height*bmp.width)/4);
    yuv.V = (unsigned char * )malloc((bmp.height*bmp.width)/4);

    printf("This is a %d bits image!\n",bmp.info_header.biBitCount);
    printf("\nbmp size: \t%d X %d\n\n",bmp.info_header.biWidth,bmp.info_header.biHeight);

    ReadRGB(bmpFile,bmp.file_header,bmp.info_header,bmp.rgbData);

    return true;
}
bool WriteYuv(FILE *outFile)
{
    unsigned int size = bmp.width*bmp.height;

    if(fwrite(yuv.Y,1,size,outFile) != size)
        return false;
    if (fwrite(yuv.U,1,size/4,outFile) != size/4)
        return false;
    if(fwrite(yuv.V,1,size/4,outFile) != size/4)
        return false;
    return true;
}
bool CloseYuv(FILE * file)
{
    if(file)
    {
        fclose(file);
        return true;
    }
    return false;
}
bool CloseBmp(FILE *file)
{
    if(file)
    {
        fclose(file);
        return true;
    }
    return false;
}





void init();
void release();

void change_yuv()
{
    FILE *bmpFile = NULL;
    FILE *yuvFile = NULL;

    init();//³õÊ¼»¯

    //	open bmp & yuv file
    if((bmpFile = bmp.BMPopen("down24.bmp","rb")) == NULL)
    {
        printf("bmp file open failed!\n\n");
        exit(0);
    }
    if ((yuvFile = yuv.YUVopen("test_yuv.yuv","wb")) == NULL)
    {
        printf("yuv file failed!\n\n");
        exit(0);
    }
    //	end open bmp & yuv file

    if(!bmp.BMPread(bmpFile))//	read file & info header & rgb data
        exit(0);

    //rgb2yuv
    if(bmp.Convert2Yuv(bmp.width,bmp.height,bmp.rgbData,yuv.Y,yuv.U,yuv.V))
        printf("rgb2yuv successful!\n\n");
    else
        exit(0);

    //write yuv file
    if(yuv.YUVwrite(yuvFile))
        printf("write YUV file successful!\n\n");
    else
        printf("write YUV file failed!\n\n");

    //close yuv & bmp file
    yuv.YUVclose(yuvFile);
    bmp.BMPclose(bmpFile);

    //release buffer
    release();
}

void init()
{
    bmp.rgbData = NULL;

    bmp.BMPopen = OpenBmp;
    bmp.BMPread = ReadBmp;
    bmp.Convert2Yuv = RGB2YUV;
    bmp.BMPclose = CloseBmp;

    yuv.Y = NULL;
    yuv.U = NULL;
    yuv.V = NULL;
    yuv.YUVopen = OpenYuv;
    yuv.YUVwrite = WriteYuv;
    yuv.YUVclose = CloseYuv;
}

void release()
{
    if(bmp.rgbData)
        free(bmp.rgbData);
    if(yuv.Y)
        free(yuv.Y);
    if(yuv.U)
        free(yuv.U);
    if(yuv.V)
        free(yuv.V);
}



#endif // BMPTOYUV_H
