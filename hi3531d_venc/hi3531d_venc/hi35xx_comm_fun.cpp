#define LOG_TAG    "HI35xx_COMM_FUN"

#include <stdlib.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "hi35xx_comm_fun.h"

#define HI35xx_SYS_ALIGN_WIDTH 16
enum audio_type coderFormat[4];
HI_S32 SnapQuality[4];

HI_S32 HI35xx_COMM_SYS_CalcPicVbBlkSize(SIZE_S *stSize, PIXEL_FORMAT_E enPixFmt, HI_U32 u32AlignWidth,COMPRESS_MODE_E enCompFmt)
{

    HI_U32 u32Width 		= 0;
    HI_U32 u32Height 		= 0;
    HI_U32 u32BlkSize 		= 0;
    HI_U32 u32HeaderSize 	= 0;

    if (PIXEL_FORMAT_YUV_SEMIPLANAR_422 != enPixFmt && PIXEL_FORMAT_YUV_SEMIPLANAR_420 != enPixFmt)
    {
        SAMPLE_PRT("pixel format[%d] input failed!\n", enPixFmt);
        return HI_FAILURE;
    }

    if (16!=u32AlignWidth && 32!=u32AlignWidth && 64!=u32AlignWidth)
    {
        SAMPLE_PRT("system align width[%d] input failed!\n",\
                   u32AlignWidth);
        return HI_FAILURE;
    }
    if (704 == stSize->u32Width)
    {
        stSize->u32Width = 720;
    }
    //SAMPLE_PRT("w:%d, u32AlignWidth:%d\n", CEILING_2_POWER(stSize.u32Width,u32AlignWidth), u32AlignWidth);

    u32Width  = CEILING_2_POWER(stSize->u32Width, u32AlignWidth);
    u32Height = CEILING_2_POWER(stSize->u32Height,u32AlignWidth);

    if (PIXEL_FORMAT_YUV_SEMIPLANAR_422 == enPixFmt)
    {
        u32BlkSize = u32Width * u32Height * 2;
    }
    else
    {
        u32BlkSize = u32Width * u32Height * 3 / 2;
    }


    if(COMPRESS_MODE_SEG == enCompFmt)
    {
        VB_PIC_HEADER_SIZE(u32Width,u32Height,enPixFmt,u32HeaderSize);
    }

    u32BlkSize += u32HeaderSize;

    return u32BlkSize;
}


HI_S32 HI35xx_COMM_VI_Start(VI_DEV ViDev, VI_CHN ViChn, SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type)
{

    HI_S32 i;
    HI_S32 s32Ret;
    SAMPLE_VI_PARAM_S stViParam;
    SIZE_S stTargetSize;

    /*** get parameter from Sample_Vi_Mode ***/
    /*s32Ret = SAMPLE_COMM_VI_Mode2Param(enViMode, &stViParam);
    if (HI_SUCCESS !=s32Ret)
    {
        SAMPLE_PRT("vi get param failed!\n");
        return HI_FAILURE;
    }
    SAMPLE_PRT("chris ViDevCnt=%d,ViDevInterval=%d,ViChnCnt=%d,ViChnInterval=%d\n",stViParam.s32ViDevCnt,stViParam.s32ViDevInterval,stViParam.s32ViChnCnt,stViParam.s32ViChnInterval);*/
    stTargetSize.u32Width  = stCapRect.u32Width;
    stTargetSize.u32Height = stCapRect.u32Height;

    SAMPLE_PRT("setvi stCapRect x=%d,y=%d,width=%d,height=%d\n",stCapRect.s32X,stCapRect.s32Y,stCapRect.u32Width,stCapRect.u32Height);
    SAMPLE_PRT("setvi stTargetSize width=%d,height=%d\n",stTargetSize.u32Width,stTargetSize.u32Height);

    /*** Start VI Dev ***/
    s32Ret = HI35xx_COMM_VI_StartDev(ViDev, enViMode);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_VI_StartDev failed with %#x\n", s32Ret);
        return HI_FAILURE;
    }

    /*** Start VI Chn ***/
    s32Ret = HI35xx_COMM_VI_StartChn(ViChn, &stCapRect, &stTargetSize, input_type, VI_CHN_SET_NORMAL);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("call SAMPLE_COMM_VI_StarChn failed with %#x\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_StartChn(VI_CHN ViChn, RECT_S *pstCapRect, SIZE_S *pstTarSize,
                               HI_S32 input_type, SAMPLE_VI_CHN_SET_E enViChnSet)
{
    HI_S32 s32Ret;
    VI_CHN_ATTR_S stChnAttr;

    /* step  5: config & start vicap dev */
    memcpy(&stChnAttr.stCapRect, pstCapRect, sizeof(RECT_S));
    /* to show scale. this is a sample only, we want to show dist_size = D1 only */
    stChnAttr.stDestSize.u32Width = pstTarSize->u32Width;
    stChnAttr.stDestSize.u32Height = pstTarSize->u32Height;
    stChnAttr.enCapSel = VI_CAPSEL_BOTH;
    stChnAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;   /* sp420 or sp422 */
    stChnAttr.bMirror = (VI_CHN_SET_MIRROR == enViChnSet)?HI_TRUE:HI_FALSE;
    stChnAttr.bFlip = (VI_CHN_SET_FILP == enViChnSet)?HI_TRUE:HI_FALSE;
    stChnAttr.s32SrcFrameRate = -1;
    stChnAttr.s32DstFrameRate = -1;
    if(input_type == 1)
        stChnAttr.enScanMode =  VI_SCAN_INTERLACED;
    else if(input_type == 0)
        stChnAttr.enScanMode =  VI_SCAN_PROGRESSIVE;

    printf("enScanMode %d\n",(VI_SCAN_MODE_E)stChnAttr.enScanMode);

    s32Ret = HI_MPI_VI_SetChnAttr(ViChn, &stChnAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VI_EnableChn(ViChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_Stop(VI_DEV ViDev, VI_CHN ViChn)
{
    HI_S32 s32Ret;
    /*** Stop VI Chn ***/
    s32Ret = HI_MPI_VI_DisableChn(ViChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_VI_StopChn failed with %#x\n",s32Ret);
        return HI_FAILURE;
    }

    /*** Stop VI Dev ***/
    s32Ret = HI_MPI_VI_DisableDev(ViDev);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("SAMPLE_COMM_VI_StopDev %d failed with %#x\n",ViDev, s32Ret);
        return HI_FAILURE;
    }
    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_BindVpss(VI_DEV ViDev, VPSS_GRP VpssGrp,VI_CHN ViChn)
{
    HI_S32 s32Ret;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VPSS;
    stDestChn.s32DevId = VpssGrp;
    stDestChn.s32ChnId = 0;

    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

char *getFileAll(char *pBuf, char *fname,int *length)
{
    int  fileLight = 0;
    FILE *pFile;

    pFile = fopen(fname,"r");
    if(pFile == NULL)
    {
        printf("Open file %s fail\n",pFile);
        sprintf(pBuf, "%s", "null");
        return pBuf;
    }

    fseek(pFile,0,SEEK_END);
    fileLight = ftell(pFile);
    rewind(pFile);
    fread(pBuf,1,fileLight,pFile);
    pBuf[fileLight]=0;
    fclose(pFile);
    *length = fileLight;
    return pBuf;
}

HI_BOOL HI35xx_IVE_MAP(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *stSrcFrame, POINT_S stDstPoint, RECT_S stCropRect, HI_BOOL bTimeOut, HI_BOOL uv_IVE)
{
    IVE_HANDLE IveHandle;
    IVE_SRC_IMAGE_S stSrc;
    IVE_SRC_IMAGE_S stDst;
    HI_BOOL bInstant = HI_TRUE;
    HI_BOOL bFinish;
    HI_U32 s32Ret;

    // NOTE: 保证拷贝源的图像和目标图像有交集
    if(stDstPoint.s32X + (HI_S32)stCropRect.u32Width <= 0 ||
       stDstPoint.s32X >= (HI_S32)pstDstFrame->stVFrame.u32Width)
        return HI_TRUE;
    if(stDstPoint.s32Y + (HI_S32)stCropRect.u32Height <= 0 ||
       stDstPoint.s32Y >= (HI_S32)pstDstFrame->stVFrame.u32Height)
        return HI_TRUE;
    // NOTE: 计算落在目标图像中的拷贝源区域,并重新计算拷贝目标左上角坐标
    if(stDstPoint.s32X < 0){
        stCropRect.s32X = 0 - stDstPoint.s32X;
        stCropRect.u32Width -= stCropRect.s32X;
        stDstPoint.s32X = 0;
    }
    if(stDstPoint.s32Y < 0){
        stCropRect.s32Y = 0 - stDstPoint.s32Y;
        stCropRect.u32Height -= stCropRect.s32Y;
        stDstPoint.s32Y = 0;
    }
    if( stCropRect.s32X + stCropRect.u32Width >= pstDstFrame->stVFrame.u32Width)
        stCropRect.u32Width = pstDstFrame->stVFrame.u32Width - stDstPoint.s32X;
    if( stCropRect.s32Y + stCropRect.u32Height >= pstDstFrame->stVFrame.u32Height)
        stCropRect.u32Height = pstDstFrame->stVFrame.u32Height - stDstPoint.s32Y;

    // COPY ----- Y
    //增加支持4K
    stSrc.u16Stride[0]  = stSrcFrame->stVFrame.u32Stride[0];
    //stSrc.u16Stride  = stSrcFrame->stVFrame.u32Stride[1];
    stSrc.enType = IVE_IMAGE_TYPE_U8C1;

    stDst.enType = IVE_IMAGE_TYPE_U8C1;
    stDst.u16Stride[0]  = pstDstFrame->stVFrame.u32Stride[0];
    // stDst.u16Stride  = stDstFrame->stVFrame.u32Stride[1];

    //    int area = 0;
    POINT_S bk_dest_pt = stDstPoint;
    RECT_S bk_crop_rect = stCropRect;

    while((int)bk_crop_rect.u32Height != 0){
        bk_crop_rect.u32Width = stCropRect.u32Width;
        bk_crop_rect.s32X = stCropRect.s32X;
        bk_dest_pt.s32X = stDstPoint.s32X;

        while((int)bk_crop_rect.u32Width != 0){
            stSrc.u16Width = MIN((int)bk_crop_rect.u32Width, 1920);
            stSrc.u16Height = MIN((int)bk_crop_rect.u32Height, DMA_MIN_ROW);
            stSrc.pu8VirAddr[0] = (HI_U8*)stSrcFrame->stVFrame.pVirAddr[0] + (int)stSrc.u16Stride[0] * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stSrc.u32PhyAddr[0] = stSrcFrame->stVFrame.u32PhyAddr[0] + (int)stSrc.u16Stride[0] * bk_crop_rect.s32Y + bk_crop_rect.s32X;;

            stDst.pu8VirAddr[0] = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[0] + (int)stDst.u16Stride[0] * bk_dest_pt.s32Y + bk_dest_pt.s32X;;
            stDst.u32PhyAddr[0] = pstDstFrame->stVFrame.u32PhyAddr[0] + (int)stDst.u16Stride[0] * bk_dest_pt.s32Y + bk_dest_pt.s32X;;
            stDst.u16Width =  stSrc.u16Width;
            stDst.u16Height  =  stSrc.u16Height;
            s32Ret=HI_MPI_IVE_Map(&IveHandle, &stSrc, &pstMap, &stDst, bInstant);
            if(HI_SUCCESS!=s32Ret){
                SAMPLE_PRT("HI_MPI_IVE_Map failed with %x", s32Ret);
                HI_MPI_SYS_MmzFree(stSrc.u32PhyAddr[0], stSrc.pu8VirAddr[0]);
                HI_MPI_SYS_MmzFree(stDst.u32PhyAddr[0], stDst.pu8VirAddr[0]);
                HI_MPI_SYS_MmzFree(pstMap.u32PhyAddr,pstMap.pu8VirAddr);
                return HI_FALSE;
            }

            // COPY ----- UV
            if(uv_IVE == HI_TRUE){
                stSrc.pu8VirAddr[0] = (HI_U8*)stSrcFrame->stVFrame.pVirAddr[1] + (int)stSrc.u16Stride[1] * bk_crop_rect.s32Y / 2 + bk_crop_rect.s32X;
                stSrc.u32PhyAddr[0] = stSrcFrame->stVFrame.u32PhyAddr[1] + (int)stSrc.u16Stride[1] * bk_crop_rect.s32Y / 2 + bk_crop_rect.s32X;
                stSrc.u16Width  =  MIN((int)bk_crop_rect.u32Width, 1920);
                stSrc.u16Height =  MIN((int)bk_crop_rect.u32Height, DMA_MIN_ROW) / 2;

                stDst.pu8VirAddr[0] = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[1];
                stDst.u32PhyAddr[0] = pstDstFrame->stVFrame.u32PhyAddr[1] + (int)stDst.u16Stride[1] * bk_dest_pt.s32Y / 2 + bk_dest_pt.s32X;
                stDst.u16Width =  stSrc.u16Width;
                stDst.u16Height  =  stSrc.u16Height;

                s32Ret=HI_MPI_IVE_Map(&IveHandle, &stSrc, &pstMap, &stDst, bInstant);
                if(HI_SUCCESS!=s32Ret){
                    SAMPLE_PRT("HI_MPI_IVE_Map failed with %x", s32Ret);
                    HI_MPI_SYS_MmzFree(stSrc.u32PhyAddr[0], stSrc.pu8VirAddr[0]);
                    HI_MPI_SYS_MmzFree(stDst.u32PhyAddr[0], stDst.pu8VirAddr[0]);
                    HI_MPI_SYS_MmzFree(pstMap.u32PhyAddr,pstMap.pu8VirAddr);
                    return HI_FALSE;
                }
            }
            int act_w = MIN((int)bk_crop_rect.u32Width, 1920);
            bk_crop_rect.u32Width -= act_w;
            bk_crop_rect.s32X += act_w;
            bk_dest_pt.s32X += act_w;
        }
        int act_h = MIN((int)bk_crop_rect.u32Height, DMA_MIN_ROW);
        bk_crop_rect.u32Height -= act_h;
        bk_crop_rect.s32Y += act_h;
        bk_dest_pt.s32Y += act_h;
    }

    s32Ret = HI_MPI_IVE_Query(IveHandle, &bFinish, bTimeOut);
    if (HI_SUCCESS != s32Ret){
        return HI_FALSE;
    }

    return HI_TRUE;
}

HI_S32 HI35xx_COMM_VI_BindVenc(VI_DEV ViDev, VENC_CHN VeChn,VI_CHN ViChn)
{
    HI_S32 s32Ret;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = VeChn;

    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_UnBindVenc(VI_DEV ViDev, VENC_CHN VeChn,VI_CHN ViChn)
{
    HI_S32 s32Ret;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = VeChn;

    s32Ret = HI_MPI_SYS_UnBind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_UnBindVpss(VI_DEV ViDev, VENC_CHN VpssGrp,VI_CHN ViChn)
{
    HI_S32 s32Ret;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VPSS;
    stDestChn.s32DevId = VpssGrp;
    stDestChn.s32ChnId = 0;

    s32Ret = HI_MPI_SYS_UnBind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}



HI_S32 HI35xx_COMM_VI_BindVo(VI_DEV ViDev, VI_CHN ViChn, VO_LAYER VoLayer, VO_CHN VoChn)
{
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VIU;
    stSrcChn.s32DevId = ViDev;
    stSrcChn.s32ChnId = ViChn;

    stDestChn.enModId = HI_ID_VOU;
    stDestChn.s32DevId = VoLayer;
    stDestChn.s32ChnId = VoChn;

    return HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
}

HI_S32 Hi35xx_VPSS_Destory(VPSS_GRP vpssGrp_, VPSS_CHN vpssChn_)
{
    HI_S32 s32Ret = HI_SUCCESS;
    s32Ret = HI_MPI_VPSS_StopGrp( vpssGrp_);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    s32Ret = HI_MPI_VPSS_DisableChn( vpssGrp_, vpssChn_);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    s32Ret = HI_MPI_VPSS_DestroyGrp( vpssGrp_);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_VPSS_Start(VPSS_GRP vpssGrp_, VPSS_CHN vpssChn_, SIZE_S vpssSzie_, SIZE_S maxSize_)
{
    VPSS_CHN_MODE_S stVpssMode;
    VPSS_GRP_ATTR_S stGrpAttr;
    VPSS_CHN_ATTR_S stChnAttr;
    VPSS_GRP_PARAM_S stVpssParam;
    HI_S32 s32Ret;
    memset(&stGrpAttr,0,sizeof(VPSS_GRP_ATTR_S));
    stGrpAttr.u32MaxW = maxSize_.u32Width;
    stGrpAttr.u32MaxH = maxSize_.u32Height;
    stGrpAttr.enPixFmt = SAMPLE_PIXEL_FORMAT;
    stGrpAttr.bIeEn = HI_FALSE;
    stGrpAttr.bNrEn = HI_TRUE;
    stGrpAttr.bDciEn = HI_FALSE;
    stGrpAttr.bHistEn = HI_FALSE;
    stGrpAttr.bEsEn = HI_FALSE;
    stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;

    s32Ret = HI_MPI_VPSS_CreateGrp(vpssGrp_, &stGrpAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VPSS_CreateGrp [%d] failed with %#x!\n", vpssGrp_,s32Ret);
        return HI_FAILURE;
    }

    /*** set vpss param ***/
    s32Ret = HI_MPI_VPSS_GetGrpParam(vpssGrp_, &stVpssParam);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    stVpssParam.u32IeStrength = 0;
    s32Ret = HI_MPI_VPSS_SetGrpParam(vpssGrp_, &stVpssParam);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VPSS_GetChnMode(vpssGrp_,vpssChn_,&stVpssMode);
    if(s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VPSS_GetChnMode failed with %#x\n", s32Ret);
        return s32Ret;
    }
    stVpssMode.enChnMode = VPSS_CHN_MODE_USER;
    stVpssMode.enPixelFormat = PIXEL_FORMAT_YUV_SEMIPLANAR_420;
    stVpssMode.u32Width = vpssSzie_.u32Width;
    stVpssMode.u32Height = vpssSzie_.u32Height;

    s32Ret = HI_MPI_VPSS_SetChnMode(vpssGrp_,vpssChn_,&stVpssMode);
    if(s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VPSS_SetChnMode %d--%d failed with %#x\n",vpssGrp_,vpssChn_, s32Ret);
        return s32Ret;
    }

    /* Set Vpss Chn attr */
    stChnAttr.bSpEn = HI_FALSE;
    stChnAttr.bUVInvert = HI_FALSE;
    stChnAttr.bBorderEn = HI_FALSE;  //去边框
    stChnAttr.stBorder.u32Color = 0xffffff;
    stChnAttr.stBorder.u32LeftWidth = 2;
    stChnAttr.stBorder.u32RightWidth = 2;
    stChnAttr.stBorder.u32TopWidth = 2;
    stChnAttr.stBorder.u32BottomWidth = 2;


    s32Ret = HI_MPI_VPSS_SetChnAttr(vpssGrp_, vpssChn_, &stChnAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VPSS_SetChnAttr failed with %#x\n", s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VPSS_EnableChn(vpssGrp_, vpssChn_);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VPSS_EnableChn failed with %#x\n", s32Ret);
        return HI_FAILURE;
    }

    /*** start vpss group ***/
    s32Ret = HI_MPI_VPSS_StartGrp(vpssGrp_);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VPSS_StartGrp failed with %#x\n", s32Ret);
        return HI_FAILURE;
    }


    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VPSS_Start(HI_S32 s32GrpCnt, SIZE_S *pstSize, HI_S32 s32ChnCnt,VPSS_GRP_ATTR_S *pstVpssGrpAttr)
{
    VPSS_GRP VpssGrp;
    VPSS_CHN VpssChn;
    VPSS_CHN_MODE_S stVpssMode;
    HI_U32 u32Depth = 3;
    VPSS_GRP_ATTR_S stGrpAttr;
    VPSS_CHN_ATTR_S stChnAttr;
    VPSS_GRP_PARAM_S stVpssParam;
    HI_S32 s32Ret;
    HI_S32 i, j;

    /*** Set Vpss Grp Attr ***/

    if(NULL == pstVpssGrpAttr)
    {
        stGrpAttr.u32MaxW = pstSize->u32Width;
        stGrpAttr.u32MaxH = pstSize->u32Height;
        stGrpAttr.enPixFmt = SAMPLE_PIXEL_FORMAT;

        stGrpAttr.bIeEn = HI_FALSE;
        stGrpAttr.bNrEn = HI_TRUE;
        stGrpAttr.bDciEn = HI_FALSE;
        stGrpAttr.bHistEn = HI_FALSE;
        stGrpAttr.bEsEn = HI_FALSE;
        stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
    }
    else
    {
        memcpy(&stGrpAttr,pstVpssGrpAttr,sizeof(VPSS_GRP_ATTR_S));
    }


    for(i = 0; i < s32GrpCnt; i++)
    {
        VpssGrp = i;
        /*** create vpss group ***/
        s32Ret = HI_MPI_VPSS_CreateGrp(VpssGrp, &stGrpAttr);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_CreateGrp failed [%d]with %#x!\n", VpssGrp,s32Ret);
            return HI_FAILURE;
        }

        /*** set vpss param ***/
        s32Ret = HI_MPI_VPSS_GetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        stVpssParam.u32IeStrength = 0;
        s32Ret = HI_MPI_VPSS_SetGrpParam(VpssGrp, &stVpssParam);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("failed with %#x!\n", s32Ret);
            return HI_FAILURE;
        }

        /*** enable vpss chn, with frame ***/
        for(j=0; j<s32ChnCnt; j++)
        {
            VpssChn = j;

            s32Ret = HI_MPI_VPSS_GetChnMode(VpssGrp,VpssChn,&stVpssMode);
            if(s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_GetChnMode failed with %#x\n", s32Ret);
                return s32Ret;
            }
            stVpssMode.enChnMode = VPSS_CHN_MODE_USER;
            stVpssMode.enPixelFormat = PIXEL_FORMAT_YUV_SEMIPLANAR_420;
            if(i < VI_PORT_NUMBER) {
                stVpssMode.u32Width = viParam[i].stMinorSize.u32Width;
                stVpssMode.u32Height = viParam[i].stMinorSize.u32Height;
            }
            else {
                stVpssMode.u32Width = viParam[i - VI_PORT_NUMBER].stSnapSize.u32Width;
                stVpssMode.u32Height = viParam[i - VI_PORT_NUMBER].stSnapSize.u32Height;
            }

            s32Ret = HI_MPI_VPSS_SetChnMode(VpssGrp,VpssChn,&stVpssMode);
            if(s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_SetChnMode %d failed with %#x\n", VpssGrp,s32Ret);
                return s32Ret;
            }
            s32Ret = HI_MPI_VPSS_SetDepth(VpssGrp,VpssChn,u32Depth);
            if(s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_SetDepth failed with %#x\n", s32Ret);
                return s32Ret;
            }

            /* Set Vpss Chn attr */
            stChnAttr.bSpEn = HI_FALSE;
            stChnAttr.bUVInvert = HI_FALSE;
            stChnAttr.bBorderEn = HI_FALSE;  //去边框
            stChnAttr.stBorder.u32Color = 0xffffff;
            stChnAttr.stBorder.u32LeftWidth = 2;
            stChnAttr.stBorder.u32RightWidth = 2;
            stChnAttr.stBorder.u32TopWidth = 2;
            stChnAttr.stBorder.u32BottomWidth = 2;

            s32Ret = HI_MPI_VPSS_SetChnAttr(VpssGrp, VpssChn, &stChnAttr);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_SetChnAttr failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }

            s32Ret = HI_MPI_VPSS_EnableChn(VpssGrp, VpssChn);
            if (s32Ret != HI_SUCCESS)
            {
                SAMPLE_PRT("HI_MPI_VPSS_EnableChn failed with %#x\n", s32Ret);
                return HI_FAILURE;
            }
        }

        /*** start vpss group ***/
        s32Ret = HI_MPI_VPSS_StartGrp(VpssGrp);
        if (s32Ret != HI_SUCCESS)
        {
            SAMPLE_PRT("HI_MPI_VPSS_StartGrp failed with %#x\n", s32Ret);
            return HI_FAILURE;
        }

    }
    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VO_BindVpss(VPSS_GRP VpssGrp, VPSS_CHN VpssChn, VO_LAYER VoLayer, VO_CHN VoChn)
{
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;

    stSrcChn.enModId = HI_ID_VPSS;
    stSrcChn.s32DevId = VpssGrp;
    stSrcChn.s32ChnId = VpssChn;

    stDestChn.enModId = HI_ID_VOU;
    stDestChn.s32DevId = VoLayer;
    stDestChn.s32ChnId = VoChn;

    return HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
}

VI_DEV_ATTR_S DEV_ATTR_chris_BT1120_STANDARD_BASE =
{
    /*interface mode*/
    VI_MODE_BT1120_STANDARD,
    /*work mode, 1/2/4 multiplex*/
    VI_WORK_MODE_1Multiplex,
    /* r_mask    g_mask    b_mask*/
    {0xFF000000,    0xFF0000},//{0xFF000000,    0xFF0000},

    /* for single/double edge, must be set when double edge*/
    VI_CLK_EDGE_DOUBLE,

    /*AdChnId*/
    {-1, -1, -1, -1},
    /*enDataSeq, just support yuv*/
    VI_INPUT_DATA_UVUV,//VI_INPUT_DATA_UVUV,

    /*sync info*/
    {
        /*port_vsync   port_vsync_neg     port_hsync        port_hsync_neg        */
        VI_VSYNC_PULSE, VI_VSYNC_NEG_HIGH, VI_HSYNC_VALID_SINGNAL,VI_HSYNC_NEG_HIGH,VI_VSYNC_NORM_PULSE,VI_VSYNC_VALID_NEG_HIGH,

        /*timing info*/
        /*hsync_hfb    hsync_act    hsync_hhb*/
        {0,            1920,        0,
         /*vsync0_vhb vsync0_act vsync0_hhb*/
         0,            1080,        0,
         /*vsync1_vhb vsync1_act vsync1_hhb*/
         0,            0,            0}
    },
    /*whether use isp*/
    VI_PATH_BYPASS,
    /*data type*/
    VI_DATA_TYPE_YUV,

    HI_FALSE

};

HI_S32 HI35xx_COMM_VI_StartDev(VI_DEV ViDev, SAMPLE_VI_MODE_E enViMode)
{
    HI_S32 s32Ret;
    VI_DEV_ATTR_S stViDevAttr;
    memset(&stViDevAttr,0,sizeof(stViDevAttr));

    switch (enViMode)
    {
        case SAMPLE_VI_MODE_8_1080P:
            if(versionFlag == MIX_4)  //4混合输入还是启用上升沿采样，4HDMI采用双边沿采样
                DEV_ATTR_chris_BT1120_STANDARD_BASE.enClkEdge = VI_CLK_EDGE_SINGLE_UP;
#ifdef  __EDC_VENC__
                DEV_ATTR_chris_BT1120_STANDARD_BASE.enClkEdge = VI_CLK_EDGE_SINGLE_UP;
#endif
            memcpy(&stViDevAttr,&DEV_ATTR_chris_BT1120_STANDARD_BASE,sizeof(stViDevAttr));
            HI35xx_COMM_VI_SetMask(ViDev,&stViDevAttr);
            break;
        default:
            SAMPLE_PRT("vi input type[%d] is invalid!\n", enViMode);
            return HI_FAILURE;
    }

    s32Ret = HI_MPI_VI_SetDevAttr(ViDev, &stViDevAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VI_SetDevAttr [%d] failed with %#x!\n", ViDev,s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VI_EnableDev(ViDev);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("HI_MPI_VI_EnableDev failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VI_SetMask(VI_DEV ViDev, VI_DEV_ATTR_S *pstDevAttr)
{
    switch (ViDev % 2)
    {
        case 0:
            pstDevAttr->au32CompMask[0] = 0xFF000000;
            if (VI_MODE_BT1120_STANDARD == pstDevAttr->enIntfMode)
            {
                pstDevAttr->au32CompMask[0] = 0xFF000000;
                pstDevAttr->au32CompMask[1] = 0xFF000000>>8;
            }
            else
            {
                pstDevAttr->au32CompMask[1] = 0x0;
            }
            break;

        case 1:
#ifdef __EDC_VENC__
            pstDevAttr->au32CompMask[0] = 0xFF000000;
            if (VI_MODE_BT1120_STANDARD == pstDevAttr->enIntfMode)
            {
                pstDevAttr->au32CompMask[0] = 0xFF000000;
                pstDevAttr->au32CompMask[1] = 0xFF000000>>8;
            }
            else
            {
                pstDevAttr->au32CompMask[1] = 0x0;
            }
            break;
#else
            pstDevAttr->au32CompMask[0] = 0xFF000000>>8;
            pstDevAttr->au32CompMask[1] = 0x0;
            break;
#endif
        default:
            SAMPLE_PRT("VISetMask failed!\n");
            return HI_FAILURE;
    }
}

HI_S32 HI35xx_COMM_VENC_SnapStart(VENC_CHN VencChn, SIZE_S *pstSize)
{
    HI_S32 s32Ret;
    VENC_CHN_ATTR_S stVencChnAttr;
    VENC_ATTR_JPEG_S stJpegAttr;
    HI_S32 vi_port = VencChn - 8;
    /******************************************
     step 1:  Create Venc Channel
    ******************************************/
    SIZE_S  max_snap = { pstSize->u32Width,pstSize->u32Height};

    int snaph_4 = max_snap.u32Height % 4;
    int snapw_4 = max_snap.u32Width  % 4;

    if(snaph_4 != 0)
        pstSize->u32Height = pstSize->u32Height + snaph_4;
    if(snapw_4 != 0)
        pstSize->u32Width = pstSize->u32Width + snapw_4 ;

    stVencChnAttr.stVeAttr.enType = PT_JPEG;
    stJpegAttr.u32MaxPicWidth  = VI_MAX_W;
    stJpegAttr.u32MaxPicHeight = VI_MAX_H;
    stJpegAttr.u32BufSize   = VI_MAX_W*VI_MAX_H * 3 ;
    stJpegAttr.u32PicWidth  = pstSize->u32Width;
    stJpegAttr.u32PicHeight = pstSize->u32Height;

    stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
    stJpegAttr.bSupportDCF  = HI_FALSE; //是否支持 Jpeg 的缩略图

    memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));
    stVencChnAttr.stGopAttr.enGopMode  = VENC_GOPMODE_NORMALP;
    stVencChnAttr.stGopAttr.stNormalP.s32IPQpDelta = 0;
    s32Ret = HI_MPI_VENC_CreateChn(VencChn, &stVencChnAttr);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_CreateChn [%d] faild with %#x!\n",\
                   VencChn, s32Ret);
        return s32Ret;
    }



    VENC_FRAME_RATE_S venc_jpeg_rate;
    s32Ret = HI_MPI_VENC_GetFrameRate(VencChn,&venc_jpeg_rate);
    if (HI_SUCCESS != s32Ret)
    {
        printf("HI_MPI_VENC_GetFrameRate err 0x%x\n",s32Ret);
        return HI_FAILURE;
    }

    venc_jpeg_rate.s32SrcFrmRate = viParam[vi_port].u32SrcFrmRate;
    venc_jpeg_rate.s32DstFrmRate = viParam[vi_port].Snapmcast_freq;
    s32Ret = HI_MPI_VENC_SetFrameRate(VencChn,&venc_jpeg_rate);
    if (HI_SUCCESS != s32Ret)
    {
        printf("HI_MPI_VENC_SetFrameRate err 0x%x\n",s32Ret);
        return HI_FAILURE;
    }

    VENC_PARAM_JPEG_S stParamJpeg;
    s32Ret = HI_MPI_VENC_GetJpegParam(VencChn, &stParamJpeg);
    if (HI_SUCCESS != s32Ret)
    {
        printf("HI_MPI_VENC_GetJpegParam err 0x%x\n",s32Ret);
        return HI_FAILURE;
    }

    stParamJpeg.u32Qfactor = viParam[vi_port].SnapQuality;
    printf("SnapQuality %d\n",stParamJpeg.u32Qfactor);

    s32Ret = HI_MPI_VENC_SetJpegParam(VencChn, &stParamJpeg);
    if (HI_SUCCESS != s32Ret)
    {
        printf("HI_MPI_VENC_SetJpegParam err 0x%x\n",s32Ret);
        return HI_FAILURE;
    }

    /******************************************
     step 3:  Start Recv Venc Pictures
    ******************************************/
    s32Ret = HI_MPI_VENC_StartRecvPic(VencChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_StartRecvPic %d faild with%#x!\n", VencChn,s32Ret);
        return s32Ret;
    }


    printf("create snap venc %d success\n",VencChn);



    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VENC_SnapStop(VENC_CHN VencChn, HI_BOOL bBindVpss, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret;

    /******************************************
     step 1:  Stop Recv Pictures
    ******************************************/
    s32Ret = HI_MPI_VENC_StopRecvPic(VencChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_StopRecvPic vechn[%d] failed with %#x!\n", VencChn, s32Ret);
        return s32Ret;
    }

    /******************************************
     step 2:  unbind
    ******************************************/
    if(bBindVpss){
        s32Ret = RSAMPLE_COMM_VENC_UnBindVpss(VencChn, VpssGrp, VpssChn);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("SAMPLE_COMM_VENC_UnBindVpss failed!\n");
            return s32Ret;
        }
    }

    /******************************************
     step 3:  Distroy Venc Channel
    ******************************************/
    s32Ret = HI_MPI_VENC_DestroyChn(VencChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", VencChn, s32Ret);
        return s32Ret;
    }

    return HI_SUCCESS;
}

/******************************************************************************
* function : venc bind vpss
******************************************************************************/
HI_S32 RSAMPLE_COMM_VENC_BindVpss(VENC_CHN VeChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret = HI_SUCCESS;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;
    stSrcChn.enModId = HI_ID_VPSS;
    stSrcChn.s32DevId = VpssGrp;
    stSrcChn.s32ChnId = VpssChn;
    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = VeChn;
    // printf("+++++++++++++++++++++++bind++%d\n",VeChn);
    s32Ret = HI_MPI_SYS_Bind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    return s32Ret;
}

/******************************************************************************
* function : venc unbind vpss
******************************************************************************/
HI_S32 RSAMPLE_COMM_VENC_UnBindVpss(VENC_CHN VeChn, VPSS_GRP VpssGrp, VPSS_CHN VpssChn)
{
    HI_S32 s32Ret = HI_SUCCESS;
    MPP_CHN_S stSrcChn;
    MPP_CHN_S stDestChn;
    stSrcChn.enModId = HI_ID_VPSS;
    stSrcChn.s32DevId = VpssGrp;
    stSrcChn.s32ChnId = VpssChn;
    stDestChn.enModId = HI_ID_VENC;
    stDestChn.s32DevId = 0;
    stDestChn.s32ChnId = VeChn;
    s32Ret = HI_MPI_SYS_UnBind(&stSrcChn, &stDestChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    return s32Ret;
}

HI_S32 HI35xx_COMM_VENC_Start(VI_CHN vi_snap, VENC_CHN VencChn, PAYLOAD_TYPE_E enType, SIZE_S stMaxPicSize, SIZE_S stPicSize, SAMPLE_RC_E enRcMode, HI_U32  u32Profile,  HI_U32 u32Gop, HI_U32 u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 u32DstFrameRate, HI_U32 u32MinQp, HI_U32 u32MaxQp, HI_U32 u32MinIQp, HI_U32 u32IQp, HI_U32 u32PQp, HI_U32 u32BQp, HI_U32 u32MinIProp, HI_U32 u32MaxIProp,HI_BOOL setFlag)
{

    //    printf("set vi frameRate %d  venc frameRate %d\n",u32SrcFrmRate,u32DstFrameRate);
    HI_S32 s32Ret;
    VENC_CHN_ATTR_S stVencChnAttr;
    VENC_ATTR_H264_S stH264Attr;
    VENC_ATTR_H264_CBR_S    stH264Cbr;
    VENC_ATTR_H264_VBR_S    stH264Vbr;
    VENC_ATTR_H264_AVBR_S    stH264AVbr;
    VENC_ATTR_H264_FIXQP_S  stH264FixQp;
    VENC_ATTR_H265_S        stH265Attr;
    VENC_ATTR_H265_CBR_S    stH265Cbr;
    VENC_ATTR_H265_VBR_S    stH265Vbr;
    VENC_ATTR_H265_AVBR_S    stH265AVbr;
    VENC_ATTR_H265_FIXQP_S  stH265FixQp;
    VENC_ATTR_MJPEG_S stMjpegAttr;
    VENC_ATTR_MJPEG_FIXQP_S stMjpegeFixQp;
    VENC_ATTR_JPEG_S stJpegAttr;

    int diff_ip_qp = 0;

    if((stPicSize.u32Width > 1920) || (stPicSize.u32Height > 1080))
        diff_ip_qp = 5;

    SIZE_S max_snap = {stPicSize.u32Width,stPicSize.u32Height};
    int snaph_4 = max_snap.u32Height % 2;
    int snapw_4 = max_snap.u32Width  % 2;



    if(snaph_4 != 0)
        stPicSize.u32Height =  stPicSize.u32Height + snaph_4;
    else if(snapw_4 != 0)
        stPicSize.u32Width = stPicSize.u32Width + snapw_4 ;

    printf("entype ==%d-----h=%d---w--%d\n",enType,stPicSize.u32Height,stPicSize.u32Width );


    if(setFlag) {
        // s32Ret = HI_MPI_VENC_GetChnAttr(VencChn, &stVencChnAttr);
        stVencChnAttr.stVeAttr.enType = enType;
        switch (enType)
        {
            case PT_H264:
            {
                stH264Attr.u32MaxPicWidth = stMaxPicSize.u32Width;
                stH264Attr.u32MaxPicHeight = stMaxPicSize.u32Height;
                stH264Attr.u32PicWidth = stPicSize.u32Width;/*the picture width*/
                stH264Attr.u32PicHeight = stPicSize.u32Height;/*the picture height*/
                stH264Attr.u32BufSize  = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3/2;/*stream buffer size*/
                stH264Attr.u32Profile  = u32Profile;/*0: baseline; 1:MP; 2:HP;  3:svc_t */
                stH264Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode ?*/
                //stH264Attr.u32BFrameNum = 0;/* 0: not support B frame; >=1: number of B frames */
                //stH264Attr.u32RefNum = 1;/* 0: default; number of refrence frame*/
                memcpy(&stVencChnAttr.stVeAttr.stAttrH264e, &stH264Attr, sizeof(VENC_ATTR_H264_S));
                if (SAMPLE_RC_CBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264CBR;
                    stH264Cbr.u32Gop            = u32Gop;
                    stH264Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                    stH264Cbr.u32SrcFrmRate      = u32SrcFrmRate; /* input (vi) frame rate */
                    stH264Cbr.fr32DstFrmRate = u32DstFrameRate; /* target frame rate */
                    stH264Cbr.u32BitRate        = u32BitRate;
                    stH264Cbr.u32FluctuateLevel = 1; /* average bit rate */
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264Cbr, &stH264Cbr, sizeof(VENC_ATTR_H264_CBR_S));
                }
                else if (SAMPLE_RC_FIXQP == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264FIXQP;
                    stH264FixQp.u32Gop = u32Gop;
                    stH264FixQp.u32SrcFrmRate = u32SrcFrmRate;
                    stH264FixQp.fr32DstFrmRate = u32DstFrameRate;
                    stH264FixQp.u32IQp = u32IQp + diff_ip_qp;
                    stH264FixQp.u32PQp = u32PQp + diff_ip_qp;
                    stH264FixQp.u32BQp = u32BQp + diff_ip_qp;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264FixQp, &stH264FixQp, sizeof(VENC_ATTR_H264_FIXQP_S));
                }
                else if (SAMPLE_RC_VBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264VBR;
                    stH264Vbr.u32Gop = u32Gop;
                    stH264Vbr.u32StatTime = 1;
                    stH264Vbr.u32SrcFrmRate = u32SrcFrmRate;
                    stH264Vbr.fr32DstFrmRate = u32DstFrameRate;
                    stH264Vbr.u32MinQp = u32MinQp;
                    stH264Vbr.u32MinIQp = u32MinIQp;
                    stH264Vbr.u32MaxQp = u32MaxQp;
                    stH264Vbr.u32MaxBitRate = u32BitRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264Vbr, &stH264Vbr, sizeof(VENC_ATTR_H264_VBR_S));
                }
                else if (SAMPLE_RC_AVBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264AVBR;
                    stH264AVbr.u32Gop = u32Gop;
                    stH264AVbr.u32StatTime = 1;
                    stH264AVbr.u32SrcFrmRate = u32SrcFrmRate;
                    stH264AVbr.fr32DstFrmRate = u32DstFrameRate;
                    stH264Vbr.u32MaxBitRate = u32BitRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH264AVbr, sizeof(VENC_ATTR_H264_AVBR_S));
                }
                else
                {
                    SAMPLE_PRT("error enRcMode\n");
                    return HI_FAILURE;
                }
            }
                break;
            case PT_MJPEG:
            {
                stMjpegAttr.u32MaxPicWidth = stMaxPicSize.u32Width;
                stMjpegAttr.u32MaxPicHeight = stMaxPicSize.u32Height;
                stMjpegAttr.u32PicWidth = stPicSize.u32Width;
                stMjpegAttr.u32PicHeight = stPicSize.u32Height;
                stMjpegAttr.u32BufSize = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3;
                stMjpegAttr.bByFrame = HI_TRUE;  /*get stream mode is field mode  or frame mode*/
                memcpy(&stVencChnAttr.stVeAttr.stAttrMjpege, &stMjpegAttr, sizeof(VENC_ATTR_MJPEG_S));
                if (SAMPLE_RC_FIXQP == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGFIXQP;
                    stMjpegeFixQp.u32Qfactor        = 90;
                    stMjpegeFixQp.u32SrcFrmRate      = u32SrcFrmRate;
                    stMjpegeFixQp.fr32DstFrmRate = u32DstFrameRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrMjpegeFixQp, &stMjpegeFixQp,
                           sizeof(VENC_ATTR_MJPEG_FIXQP_S));
                }
                else if (SAMPLE_RC_CBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGCBR;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32StatTime       = 1;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32SrcFrmRate      = u32SrcFrmRate;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.fr32DstFrmRate = u32DstFrameRate;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32FluctuateLevel = 1;
                    stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32BitRate = u32BitRate;
                }
                else if (SAMPLE_RC_VBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGVBR;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32StatTime = 1;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32SrcFrmRate = u32SrcFrmRate;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.fr32DstFrmRate = 5;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MinQfactor = 50;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxQfactor = 95;
                    stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxBitRate = u32BitRate;
                }
                else
                {
                    SAMPLE_PRT("cann't support other mode in this version!\n");
                    return HI_FAILURE;
                }
            }
                break;
            case PT_JPEG:
                stJpegAttr.u32PicWidth  = stPicSize.u32Width;
                stJpegAttr.u32PicHeight = stPicSize.u32Height;
                stJpegAttr.u32MaxPicWidth  = stMaxPicSize.u32Width;
                stJpegAttr.u32MaxPicHeight = stMaxPicSize.u32Height;
                stJpegAttr.u32BufSize   = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3;
                stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
                stJpegAttr.bSupportDCF  = HI_FALSE;
                memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));
                break;
            case PT_H265:
            {
                stH265Attr.u32MaxPicWidth = stMaxPicSize.u32Width;
                stH265Attr.u32MaxPicHeight = stMaxPicSize.u32Height;
                stH265Attr.u32PicWidth = stPicSize.u32Width;/*the picture width*/
                stH265Attr.u32PicHeight = stPicSize.u32Height;/*the picture height*/
                stH265Attr.u32BufSize  = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 2;/*stream buffer size*/
                if (u32Profile >= 1)
                {
                    stH265Attr.u32Profile = 0;
                }/*0:MP; */
                else
                {
                    stH265Attr.u32Profile  = u32Profile;
                }/*0:MP*/
                stH265Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode?*/
                memcpy(&stVencChnAttr.stVeAttr.stAttrH265e, &stH265Attr, sizeof(VENC_ATTR_H265_S));
                if (SAMPLE_RC_CBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265CBR;
                    stH265Cbr.u32Gop            = u32Gop;
                    stH265Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                    stH265Cbr.u32SrcFrmRate      = u32SrcFrmRate; /* input (vi) frame rate */
                    stH265Cbr.fr32DstFrmRate = u32DstFrameRate; /* target frame rate */
                    stH265Cbr.u32BitRate        =  u32BitRate;
                    stH265Cbr.u32FluctuateLevel = 1; /* average bit rate */
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH265Cbr, &stH265Cbr, sizeof(VENC_ATTR_H265_CBR_S));
                }
                else if (SAMPLE_RC_FIXQP == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265FIXQP;
                    stH265FixQp.u32Gop = u32Gop;
                    stH265FixQp.u32SrcFrmRate = u32SrcFrmRate;
                    stH265FixQp.fr32DstFrmRate = u32DstFrameRate;
                    stH265FixQp.u32IQp = u32IQp + diff_ip_qp;
                    stH265FixQp.u32PQp = u32PQp + diff_ip_qp;
                    stH265FixQp.u32BQp = u32BQp + diff_ip_qp;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH265FixQp, &stH265FixQp, sizeof(VENC_ATTR_H265_FIXQP_S));
                }
                else if (SAMPLE_RC_VBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265VBR;
                    stH265Vbr.u32Gop = u32Gop;
                    stH265Vbr.u32StatTime = 1;
                    stH265Vbr.u32SrcFrmRate = u32SrcFrmRate;
                    stH265Vbr.fr32DstFrmRate = u32DstFrameRate;
                    stH265Vbr.u32MinQp  = u32MinQp;
                    stH265Vbr.u32MinIQp = u32MinIQp;
                    stH265Vbr.u32MaxQp  = u32MaxQp;
                    stH265Vbr.u32MaxBitRate = u32BitRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH265Vbr, &stH265Vbr, sizeof(VENC_ATTR_H265_VBR_S));
                }
                else if (SAMPLE_RC_AVBR == enRcMode)
                {
                    stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265AVBR;
                    stH265AVbr.u32Gop = u32Gop;
                    stH265AVbr.u32StatTime = 1;
                    stH265AVbr.u32SrcFrmRate = u32SrcFrmRate;
                    stH265AVbr.fr32DstFrmRate = u32DstFrameRate;
                    stH265AVbr.u32MaxBitRate = u32BitRate;
                    memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH265AVbr, sizeof(VENC_ATTR_H265_AVBR_S));
                }
                else
                {
                    printf("h265------\n");
                    return HI_FAILURE;
                }
            }
                break;
            default:
                return HI_ERR_VENC_NOT_SUPPORT;
        }

        if((enType == PT_JPEG) &&  viParam[vi_snap].Snapmcast_enable) {
            VENC_FRAME_RATE_S venc_jpeg_rate;
            s32Ret = HI_MPI_VENC_GetFrameRate(VencChn,&venc_jpeg_rate);
            if (HI_SUCCESS != s32Ret)
            {
                printf("HI_MPI_VENC_GetFrameRate err 0x%x\n",s32Ret);
                return HI_FAILURE;
            }

            venc_jpeg_rate.s32SrcFrmRate = viParam[vi_snap].u32SrcFrmRate;
            venc_jpeg_rate.s32DstFrmRate = viParam[vi_snap].Snapmcast_freq;
            printf("jpeg venc srcRate=%d, dstRate=%d\n",venc_jpeg_rate.s32SrcFrmRate,venc_jpeg_rate.s32DstFrmRate);
            s32Ret = HI_MPI_VENC_SetFrameRate(VencChn,&venc_jpeg_rate);
            if (HI_SUCCESS != s32Ret)
            {
                printf("HI_MPI_VENC_SetFrameRate err 0x%x\n",s32Ret);
                return HI_FAILURE;
            }
        }


        stVencChnAttr.stGopAttr.enGopMode  = VENC_GOPMODE_NORMALP;
        stVencChnAttr.stGopAttr.stNormalP.s32IPQpDelta = 0;
        s32Ret = HI_MPI_VENC_SetChnAttr(VencChn, &stVencChnAttr);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("HI_MPI_VENC_SetChnAttr [%d] faild with %#x! ===\n", \
                       VencChn, s32Ret);
            return s32Ret;
        }
        s32Ret = HI_MPI_VENC_StartRecvPic(VencChn);
        if (HI_SUCCESS != s32Ret)
        {
            SAMPLE_PRT("HI_MPI_VENC_StartRecvPic faild with%#x! \n", s32Ret);
            return HI_FAILURE;
        }
        printf("----venc %d set ok------",VencChn);
        return HI_SUCCESS;

    }

    /******************************************
     step 1:  Create Venc Channel
     ******************************************/
    stVencChnAttr.stVeAttr.enType = enType;
    switch (enType)
    {
        case PT_H264:
        {
            stH264Attr.u32MaxPicWidth = stMaxPicSize.u32Width;
            stH264Attr.u32MaxPicHeight = stMaxPicSize.u32Height;
            stH264Attr.u32PicWidth = stPicSize.u32Width;/*the picture width*/
            stH264Attr.u32PicHeight = stPicSize.u32Height;/*the picture height*/
            stH264Attr.u32BufSize  = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 2;/*stream buffer size*/
            stH264Attr.u32Profile  = u32Profile;/*0: baseline; 1:MP; 2:HP;  3:svc_t */
            stH264Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode ?*/
            //stH264Attr.u32BFrameNum = 0;/* 0: not support B frame; >=1: number of B frames */
            //stH264Attr.u32RefNum = 1;/* 0: default; number of refrence frame*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrH264e, &stH264Attr, sizeof(VENC_ATTR_H264_S));
            if (SAMPLE_RC_CBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264CBR;
                stH264Cbr.u32Gop            = u32Gop;
                stH264Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                stH264Cbr.u32SrcFrmRate      = u32SrcFrmRate; /* input (vi) frame rate */
                stH264Cbr.fr32DstFrmRate = u32DstFrameRate; /* target frame rate */
                stH264Cbr.u32BitRate        = u32BitRate;
                stH264Cbr.u32FluctuateLevel = 1; /* average bit rate */
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264Cbr, &stH264Cbr, sizeof(VENC_ATTR_H264_CBR_S));
            }
            else if (SAMPLE_RC_FIXQP == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264FIXQP;
                stH264FixQp.u32Gop = u32Gop;
                stH264FixQp.u32SrcFrmRate = u32SrcFrmRate;
                stH264FixQp.fr32DstFrmRate = u32DstFrameRate;
                stH264FixQp.u32IQp = u32IQp;
                stH264FixQp.u32PQp = u32PQp;
                stH264FixQp.u32BQp = u32BQp;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264FixQp, &stH264FixQp, sizeof(VENC_ATTR_H264_FIXQP_S));
            }
            else if (SAMPLE_RC_VBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264VBR;
                stH264Vbr.u32Gop = u32Gop;
                stH264Vbr.u32StatTime = 1;
                stH264Vbr.u32SrcFrmRate = u32SrcFrmRate;
                stH264Vbr.fr32DstFrmRate = u32DstFrameRate;
                stH264Vbr.u32MinQp = u32MinQp;
                stH264Vbr.u32MinIQp = u32MinIQp;
                stH264Vbr.u32MaxQp = u32MaxQp;
                stH264Vbr.u32MaxBitRate = u32BitRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264Vbr, &stH264Vbr, sizeof(VENC_ATTR_H264_VBR_S));
            }
            else if (SAMPLE_RC_AVBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H264AVBR;
                stH264AVbr.u32Gop = u32Gop;
                stH264AVbr.u32StatTime = 1;
                stH264AVbr.u32SrcFrmRate = u32SrcFrmRate;
                stH264AVbr.fr32DstFrmRate = u32DstFrameRate;
                stH264Vbr.u32MaxBitRate = u32BitRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH264AVbr, sizeof(VENC_ATTR_H264_AVBR_S));
            }
            else
            {
                SAMPLE_PRT("error enRcMode\n");
                return HI_FAILURE;
            }
        }
            break;
        case PT_MJPEG:
        {
            stMjpegAttr.u32MaxPicWidth = stMaxPicSize.u32Width;
            stMjpegAttr.u32MaxPicHeight = stMaxPicSize.u32Height;
            stMjpegAttr.u32PicWidth = stPicSize.u32Width;
            stMjpegAttr.u32PicHeight = stPicSize.u32Height;
            stMjpegAttr.u32BufSize = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3;
            stMjpegAttr.bByFrame = HI_TRUE;  /*get stream mode is field mode  or frame mode*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrMjpege, &stMjpegAttr, sizeof(VENC_ATTR_MJPEG_S));
            if (SAMPLE_RC_FIXQP == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGFIXQP;
                stMjpegeFixQp.u32Qfactor        = 90;
                stMjpegeFixQp.u32SrcFrmRate      = u32SrcFrmRate;
                stMjpegeFixQp.fr32DstFrmRate = u32DstFrameRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrMjpegeFixQp, &stMjpegeFixQp,
                       sizeof(VENC_ATTR_MJPEG_FIXQP_S));
            }
            else if (SAMPLE_RC_CBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGCBR;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32StatTime       = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32SrcFrmRate      = u32SrcFrmRate;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.fr32DstFrmRate = u32DstFrameRate;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32FluctuateLevel = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeCbr.u32BitRate = u32BitRate;
            }
            else if (SAMPLE_RC_VBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_MJPEGVBR;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32StatTime = 1;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32SrcFrmRate = u32SrcFrmRate;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.fr32DstFrmRate = 5;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MinQfactor = 50;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxQfactor = 95;
                stVencChnAttr.stRcAttr.stAttrMjpegeVbr.u32MaxBitRate = u32BitRate;
            }
            else
            {
                SAMPLE_PRT("cann't support other mode in this version!\n");
                return HI_FAILURE;
            }
        }
            break;
        case PT_JPEG:
            stJpegAttr.u32PicWidth  = stPicSize.u32Width;
            stJpegAttr.u32PicHeight = stPicSize.u32Height;
            stJpegAttr.u32MaxPicWidth  = stMaxPicSize.u32Width;
            stJpegAttr.u32MaxPicHeight = stMaxPicSize.u32Height;
            stJpegAttr.u32BufSize   = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 3;
            stJpegAttr.bByFrame     = HI_TRUE;/*get stream mode is field mode  or frame mode*/
            stJpegAttr.bSupportDCF  = HI_FALSE;
            memcpy(&stVencChnAttr.stVeAttr.stAttrJpege, &stJpegAttr, sizeof(VENC_ATTR_JPEG_S));
            break;
        case PT_H265:
        {
            stH265Attr.u32MaxPicWidth = stMaxPicSize.u32Width;
            stH265Attr.u32MaxPicHeight = stMaxPicSize.u32Height;
            stH265Attr.u32PicWidth = stPicSize.u32Width;/*the picture width*/
            stH265Attr.u32PicHeight = stPicSize.u32Height;/*the picture height*/
            stH265Attr.u32BufSize  = stMaxPicSize.u32Width * stMaxPicSize.u32Height * 2;/*stream buffer size*/
            if (u32Profile >= 1)
            {
                stH265Attr.u32Profile = 0;
            }/*0:MP; */
            else
            {
                stH265Attr.u32Profile  = u32Profile;
            }/*0:MP*/
            stH265Attr.bByFrame = HI_TRUE;/*get stream mode is slice mode or frame mode?*/
            memcpy(&stVencChnAttr.stVeAttr.stAttrH265e, &stH265Attr, sizeof(VENC_ATTR_H265_S));
            if (SAMPLE_RC_CBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265CBR;
                stH265Cbr.u32Gop            = u32Gop;
                stH265Cbr.u32StatTime       = 1; /* stream rate statics time(s) */
                stH265Cbr.u32SrcFrmRate      = u32SrcFrmRate; /* input (vi) frame rate */
                stH265Cbr.fr32DstFrmRate = u32DstFrameRate; /* target frame rate */
                stH265Cbr.u32BitRate        =  u32BitRate;
                stH265Cbr.u32FluctuateLevel = 1; /* average bit rate */
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265Cbr, &stH265Cbr, sizeof(VENC_ATTR_H265_CBR_S));
            }
            else if (SAMPLE_RC_FIXQP == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265FIXQP;
                stH265FixQp.u32Gop = u32Gop;
                stH265FixQp.u32SrcFrmRate = u32SrcFrmRate;
                stH265FixQp.fr32DstFrmRate = u32DstFrameRate;
                stH265FixQp.u32IQp = u32IQp;
                stH265FixQp.u32PQp = u32PQp;
                stH265FixQp.u32BQp = u32BQp;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265FixQp, &stH265FixQp, sizeof(VENC_ATTR_H265_FIXQP_S));
            }
            else if (SAMPLE_RC_VBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265VBR;
                stH265Vbr.u32Gop = u32Gop;
                stH265Vbr.u32StatTime = 1;
                stH265Vbr.u32SrcFrmRate = u32SrcFrmRate;
                stH265Vbr.fr32DstFrmRate = u32DstFrameRate;
                stH265Vbr.u32MinQp  = u32MinQp;
                stH265Vbr.u32MinIQp = u32MinIQp;
                stH265Vbr.u32MaxQp  = u32MaxQp;
                stH265Vbr.u32MaxBitRate = u32BitRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH265Vbr, &stH265Vbr, sizeof(VENC_ATTR_H265_VBR_S));
            }
            else if (SAMPLE_RC_AVBR == enRcMode)
            {
                stVencChnAttr.stRcAttr.enRcMode = VENC_RC_MODE_H265AVBR;
                stH265AVbr.u32Gop = u32Gop;
                stH265AVbr.u32StatTime = 1;
                stH265AVbr.u32SrcFrmRate = u32SrcFrmRate;
                stH265AVbr.fr32DstFrmRate = u32DstFrameRate;
                stH265AVbr.u32MaxBitRate = u32BitRate;
                memcpy(&stVencChnAttr.stRcAttr.stAttrH264AVbr, &stH265AVbr, sizeof(VENC_ATTR_H265_AVBR_S));
            }
            else
            {
                printf("------h265\n");
                return HI_FAILURE;
            }
        }
            break;
        default:
            return HI_ERR_VENC_NOT_SUPPORT;
    }
    stVencChnAttr.stGopAttr.enGopMode  = VENC_GOPMODE_NORMALP;
    stVencChnAttr.stGopAttr.stNormalP.s32IPQpDelta = 0;
    s32Ret = HI_MPI_VENC_CreateChn(VencChn, &stVencChnAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_CreateChn [%d] faild with %#x! ===\n", \
                   VencChn, s32Ret);
        return s32Ret;
    }


    if(enType == PT_JPEG){
        VENC_PARAM_JPEG_S stParamJpeg;
        s32Ret = HI_MPI_VENC_GetJpegParam(VencChn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }

        stParamJpeg.u32Qfactor = viParam[VencChn-8].SnapQuality;
        printf("SnapQuality %d\n",stParamJpeg.u32Qfactor);

        s32Ret = HI_MPI_VENC_SetJpegParam(VencChn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetJpegParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
    }
    if((enType == PT_H264)&&(SAMPLE_RC_CBR == enRcMode)){
        VENC_RC_PARAM_S stVencRcPara;

        s32Ret = HI_MPI_VENC_GetRcParam(VencChn, &stVencRcPara);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetRcParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
        //printf("MinIprop=%d,MaxIprop=%d\n",stVencRcPara.stParamH264Cbr.u32MinIprop,stVencRcPara.stParamH264Cbr.u32MaxIprop);
        //printf("MaxQp=%d,MinQp=%d\n",stVencRcPara.stParamH264Cbr.u32MaxQp,stVencRcPara.stParamH264Cbr.u32MinQp);

        stVencRcPara.stParamH264Cbr.u32MaxQp = u32MaxQp;
        stVencRcPara.stParamH264Cbr.u32MinQp = u32MinQp;
        stVencRcPara.stParamH264Cbr.u32MinIprop = u32MinIProp;
        stVencRcPara.stParamH264Cbr.u32MaxIprop = u32MaxIProp;

        s32Ret = HI_MPI_VENC_SetRcParam(VencChn, &stVencRcPara);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetRcParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }

        s32Ret = HI_MPI_VENC_GetRcParam(VencChn, &stVencRcPara);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetRcParam err 0x%x\n",s32Ret);
            return HI_FAILURE;
        }
        //printf("MinIprop=%d,MaxIprop=%d\n",stVencRcPara.stParamH264Cbr.u32MinIprop,stVencRcPara.stParamH264Cbr.u32MaxIprop);
        //printf("MaxQp=%d,MinQp=%d\n",stVencRcPara.stParamH264Cbr.u32MaxQp,stVencRcPara.stParamH264Cbr.u32MinQp);


    }

    s32Ret = HI_MPI_VENC_StartRecvPic(VencChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_StartRecvPic faild with%#x! \n", s32Ret);
        return HI_FAILURE;
    }

    //    VENC_FRAME_RATE_S pstFrameRate;
    //    pstFrameRate.s32SrcFrmRate = u32SrcFrmRate;//输入帧率src必须大于或等于输出帧率dst,可减帧，不可增帧
    //    pstFrameRate.s32DstFrmRate = u32DstFrameRate;//同时为-1，表示不进行帧率控制
    //    printf("vi frameRate:%d  venc frameRate:%d\n",pstFrameRate.s32SrcFrmRate,pstFrameRate.s32DstFrmRate);
    //    s32Ret = HI_MPI_VENC_SetFrameRate(VencChn, &pstFrameRate);
    //    if(s32Ret != HI_SUCCESS )
    //    {
    //        SAMPLE_PRT("HI_MPI_VENC_SetFrameRate faild with%#x! \n", s32Ret);

    //    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VO_StartChn(VO_LAYER VoLayer, VO_CHN VoChn)
{
    HI_S32 s32Ret = HI_SUCCESS;
    HI_U32 u32Width = 0;
    HI_U32 u32Height = 0;
    VO_CHN_ATTR_S stChnAttr;
    VO_VIDEO_LAYER_ATTR_S stLayerAttr;

    s32Ret = HI_MPI_VO_GetVideoLayerAttr(VoLayer, &stLayerAttr);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }
    u32Width = stLayerAttr.stImageSize.u32Width;
    u32Height = stLayerAttr.stImageSize.u32Height;

    stChnAttr.stRect.s32X       = 0;
    stChnAttr.stRect.s32Y       = 0;
    stChnAttr.stRect.u32Width   = u32Width;
    stChnAttr.stRect.u32Height  = u32Height;
    stChnAttr.u32Priority       = 0;
    stChnAttr.bDeflicker        = (SAMPLE_VO_LAYER_VSD0 == VoLayer) ? HI_TRUE : HI_FALSE;

    s32Ret = HI_MPI_VO_SetChnAttr(VoLayer, VoChn, &stChnAttr);
    if (s32Ret != HI_SUCCESS)
    {
        printf("%s(%d):failed with %#x!\n",\
               __FUNCTION__,__LINE__,  s32Ret);
        return HI_FAILURE;
    }

    s32Ret = HI_MPI_VO_EnableChn(VoLayer, VoChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_VO_StopChn(VO_LAYER VoLayer, VO_CHN VoChn)
{
    HI_S32 s32Ret = HI_SUCCESS;

    s32Ret = HI_MPI_VO_DisableChn(VoLayer, VoChn);
    if (s32Ret != HI_SUCCESS)
    {
        SAMPLE_PRT("failed with %#x!\n", s32Ret);
        return HI_FAILURE;
    }

    return s32Ret;
}

HI_S32 HI35xx_COMM_VO_GetWH(VO_INTF_SYNC_E *enIntfSync, SIZE_S stVoSize, HI_U32 pu32Frm){
    if((stVoSize.u32Width == 3840)&&(stVoSize.u32Height == 2160)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_3840x2160_60;
    else if((stVoSize.u32Width == 3840)&&(stVoSize.u32Height == 2160)&&(pu32Frm == 30))
        *enIntfSync = VO_OUTPUT_3840x2160_30;
    else if((stVoSize.u32Width == 1920)&&(stVoSize.u32Height == 1080)&&(pu32Frm == 30))
        *enIntfSync = VO_OUTPUT_1080P30;
    else if((stVoSize.u32Width == 1920)&&(stVoSize.u32Height == 1080)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1080P60;
    else if((stVoSize.u32Width == 1600)&&(stVoSize.u32Height == 1200)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1600x1200_60;
    else if((stVoSize.u32Width == 1680)&&(stVoSize.u32Height == 1050)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1680x1050_60;
    else if((stVoSize.u32Width == 1440)&&(stVoSize.u32Height == 900)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1440x900_60;
    else if((stVoSize.u32Width == 1280)&&(stVoSize.u32Height == 1024)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1280x1024_60;
    else if((stVoSize.u32Width == 1280)&&(stVoSize.u32Height == 720)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_720P60;
    else if((stVoSize.u32Width == 1024)&&(stVoSize.u32Height == 768)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1024x768_60;
    else if((stVoSize.u32Width == 800)&&(stVoSize.u32Height == 600)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_800x600_60;
    else if((stVoSize.u32Width == 720)&&(stVoSize.u32Height == 480)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_480P60;
    else if((stVoSize.u32Width == 1366)&&(stVoSize.u32Height == 768)&&(pu32Frm == 60))
        *enIntfSync = VO_OUTPUT_1366x768_60;
    else if((stVoSize.u32Width == 720)&&(stVoSize.u32Height == 480)&&(pu32Frm == 30))
        *enIntfSync = VO_OUTPUT_NTSC;
    else
        *enIntfSync = VO_OUTPUT_1080P60;

}

HI_S32 HI35xx_COMM_SYS_MemConfig(HI_S32 s32VdChnCnt, HI_S32 s32VpssGrpCnt, HI_S32 s32VeChnCnt, VO_DEV VoDevId)
{
    HI_S32 i = 0;
    HI_S32 s32Ret = HI_SUCCESS;

    HI_CHAR * pcMmzName = NULL;
    MPP_CHN_S stMppChnVO;
    MPP_CHN_S stMppChnVPSS;
    MPP_CHN_S stMppChnVENC;
    MPP_CHN_S stMppChnVDEC;

    /* vdec chn config to mmz 'null' */
    /* vdec max chn is VDEC_MAX_CHN_NUM = 128*/
    for(i=0; i<s32VdChnCnt; i++)
    {
        stMppChnVDEC.enModId = HI_ID_VDEC;
        stMppChnVDEC.s32DevId = 0;
        stMppChnVDEC.s32ChnId = i;

        s32Ret = HI_MPI_SYS_SetMemConf(&stMppChnVDEC, pcMmzName);
        if (s32Ret)
        {
            SAMPLE_PRT("HI_MPI_SYS_SetMemConf ERR !\n");
            return HI_FAILURE;
        }
    }

    /* vpss group config to mmz 'null' */
    /* vpss max group is VPSS_MAX_GRP_NUM = 256*/
    for(i=0;i<s32VpssGrpCnt;i++)
    {
        stMppChnVPSS.enModId  = HI_ID_VPSS;
        stMppChnVPSS.s32DevId = i;
        stMppChnVPSS.s32ChnId = 0;

        s32Ret = HI_MPI_SYS_SetMemConf(&stMppChnVPSS, pcMmzName);
        if (s32Ret)
        {
            SAMPLE_PRT("HI_MPI_SYS_SetMemConf ERR !\n");
            return HI_FAILURE;
        }
    }

    /* venc chn config to mmz 'null' */
    /* venc max chn is VENC_MAX_CHN_NUM = 128*/
    for (i = 0;i < s32VeChnCnt; i++)
    {
        stMppChnVENC.enModId = HI_ID_VENC;
        stMppChnVENC.s32DevId = 0;
        stMppChnVENC.s32ChnId = i;

        s32Ret = HI_MPI_SYS_SetMemConf(&stMppChnVENC, pcMmzName);
        //printf("######## %d\n",i);
        if (s32Ret)
        {
            SAMPLE_PRT("HI_MPI_SYS_SetMemConf failed 0x%x!\n",s32Ret);
            return HI_FAILURE;
        }

    }

    /* vo config to mmz 'null' */
    stMppChnVO.enModId  = HI_ID_VOU;
    stMppChnVO.s32DevId = VoDevId;
    stMppChnVO.s32ChnId = 0;
    s32Ret = HI_MPI_SYS_SetMemConf(&stMppChnVO, pcMmzName);
    if (s32Ret)
    {
        SAMPLE_PRT("HI_MPI_SYS_SetMemConf ERR !\n");
        return HI_FAILURE;
    }

    return s32Ret;
}

/******************************************************************************
* function : Start Ai
******************************************************************************/
HI_S32 HI35xx_COMM_AUDIO_StartAi(AUDIO_DEV AiDevId, HI_S32 s32AiChn, AIO_ATTR_S* pstAioAttr,
                                 AUDIO_SAMPLE_RATE_E enOutSampleRate, HI_BOOL bResampleEn, HI_VOID* pstAiVqeAttr, HI_U32 u32AiVqeType)
{
    HI_S32 s32Ret;
    int i = 0;
    if (pstAioAttr->u32ClkChnCnt == 0)
    {
        pstAioAttr->u32ClkChnCnt = 16;
    }

    s32Ret = HI_MPI_AI_SetPubAttr(AiDevId, pstAioAttr);
    if (s32Ret)
    {
        printf("%s: HI_MPI_AI_SetPubAttr(%d) failed with %#x\n", __FUNCTION__, AiDevId, s32Ret);
        return s32Ret;
    }

    s32Ret = HI_MPI_AI_Enable(AiDevId);
    if (s32Ret)
    {
        printf("%s: HI_MPI_AI_Enable(%d) failed with %#x\n", __FUNCTION__, AiDevId, s32Ret);
        return s32Ret;
    }
#ifdef __EDC_VENC__

    if(2 == AiDevId)
        s32AiChn = 1;
#endif
    for(i = 0; i < s32AiChn; i++) {
        s32Ret = HI_MPI_AI_EnableChn(AiDevId, i);
        if (s32Ret)
        {
            printf("%s: HI_MPI_AI_EnableChn(%d,%d) failed with %#x\n", __FUNCTION__, AiDevId, i, s32Ret);
            return s32Ret;
        }

    }


    if (HI_TRUE == bResampleEn)
    {
        for(i = 0; i < s32AiChn; i++) {
            //            if((i == 1) && (AiDevId ==2) && (HDMI_4 == versionFlag))
            //                continue;
            s32Ret = HI_MPI_AI_EnableReSmp(AiDevId, i, enOutSampleRate);
            if (s32Ret)
            {
                printf("%s: HI_MPI_AI_EnableReSmp(%d,%d) failed with %#x\n", __FUNCTION__, AiDevId, i, s32Ret);
                return s32Ret;
            }


        }
    }

    //    if(AiDevId == 1)
    //    {

    //    s32Ret = HI_MPI_AI_SetVqeAttr(AiDevId, s32AiChn, SAMPLE_AUDIO_AO_DEV, i, (AI_VQE_CONFIG_S *)pstAiVqeAttr);

    // }

    if (NULL != pstAiVqeAttr)
    {
        HI_BOOL bAiVqe = HI_TRUE;
        switch (u32AiVqeType)
        {
            case 0:
                s32Ret = HI_SUCCESS;
                bAiVqe = HI_FALSE;
                break;
            case 1:
                for(i = 0; i < s32AiChn; i++) {
                    //                    if((i == 1) && (AiDevId ==2)&& (HDMI_4 == versionFlag))
                    //                        continue;
                    s32Ret = HI_MPI_AI_SetVqeAttr(AiDevId, s32AiChn, SAMPLE_AUDIO_AO_DEV, i, (AI_VQE_CONFIG_S *)pstAiVqeAttr);
                }
                break;
            default:
                s32Ret = HI_FAILURE;
                break;
        }
        if (s32Ret)
        {
            printf("%s: HI_MPI_AI_SetVqeAttr(%d,%d) failed with %#x\n", __FUNCTION__, AiDevId, i, s32Ret);
            return s32Ret;
        }

        if (bAiVqe)
        {
            for(i = 0; i < s32AiChn; i++) {
                //                if((i == 1) && (AiDevId ==2)&& (HDMI_4 == versionFlag))
                //                    continue;
                s32Ret = HI_MPI_AI_EnableVqe(AiDevId, i);
                if (s32Ret)
                {
                    printf("%s: HI_MPI_AI_EnableVqe(%d,%d) failed with %#x\n", __FUNCTION__, AiDevId, i, s32Ret);
                    return s32Ret;
                }
            }
        }
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_COMM_AUDIO_StopAi(AUDIO_DEV AiDevId, HI_S32 s32AiChn,
                                HI_BOOL bResampleEn, HI_BOOL bVqeEn)
{
    HI_S32 s32Ret;
    int i = 0 ;

    if (HI_TRUE == bResampleEn)
    {
        for(i = 0; i < s32AiChn; i++){
            //            if((1 == i) && (2 == AiDevId) && (HDMI_4 ==  versionFlag))
            //                continue;
            s32Ret = HI_MPI_AI_DisableReSmp(AiDevId, i);
            if (HI_SUCCESS != s32Ret)
            {
                printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
                return s32Ret;
            }
        }

    }

    if (HI_TRUE == bVqeEn)
    {
        for(i = 0; i < s32AiChn; i++){
            //            if((1 == i) && (2 == AiDevId) && (HDMI_4 ==  versionFlag))
            //                continue;
            s32Ret = HI_MPI_AI_DisableVqe(AiDevId, i);
            if (HI_SUCCESS != s32Ret)
            {
                printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
                return s32Ret;
            }
        }
    }

    for(i = 0; i < s32AiChn; i++){
        //        if((1 == i) && (2 == AiDevId) && (HDMI_4 ==  versionFlag))
        //            continue;
        s32Ret = HI_MPI_AI_DisableChn(AiDevId, i);
        if (HI_SUCCESS != s32Ret)
        {
            printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
            return s32Ret;
        }
    }


    s32Ret = HI_MPI_AI_Disable(AiDevId);
    if (HI_SUCCESS != s32Ret)
    {
        printf("[Func]:%s [Line]:%d [Info]:%s\n", __FUNCTION__, __LINE__, "failed");
        return s32Ret;
    }

    return HI_SUCCESS;
}

HI_S32 Hi_SetReg(HI_U32 u32Addr, HI_U32 u32Value)
{
    HI_U32 *pu32Addr = NULL;
    HI_U32 u32MapLen = sizeof(u32Value);
    //  printf("hi_setreg %x,  ----------------------------------------------\n",u32Value);
    pu32Addr = (HI_U32 *)HI_MPI_SYS_Mmap(u32Addr, u32MapLen);
    if(NULL == pu32Addr)
    {
        return HI_FAILURE;
    }

    *pu32Addr = u32Value;

    return HI_MPI_SYS_Munmap(pu32Addr, u32MapLen);
}

HI_S32 Hi_GetReg(HI_U32 u32Addr, HI_U32 *pu32Value)
{
    HI_U32 *pu32Addr = NULL;
    HI_U32 u32MapLen;

    if (NULL == pu32Value)
    {
        return HI_ERR_SYS_NULL_PTR;
    }

    u32MapLen = sizeof(*pu32Value);
    pu32Addr = (HI_U32 *)HI_MPI_SYS_Mmap(u32Addr, u32MapLen);
    if(NULL == pu32Addr)
    {
        return HI_FAILURE;
    }

    *pu32Value = *pu32Addr;

    return HI_MPI_SYS_Munmap(pu32Addr, u32MapLen);
}

int tim_subtract(struct timeval *result, struct timeval *begin, struct timeval *end )
{
    if(begin->tv_sec > end->tv_sec)    return -1;

    if((begin->tv_sec == end->tv_sec) && (begin->tv_usec > end->tv_usec))    return -2;

    result->tv_sec  = (end->tv_sec - begin->tv_sec);
    result->tv_usec = (end->tv_usec - begin->tv_usec);

    if (result->tv_usec<0){
        result->tv_sec--;
        result->tv_usec += 1000000;
    }

    //if ( outPutFlag )
    //printf("diff time %ds%dms%dus\n", result->tv_sec, result->tv_usec/1000, result->tv_usec%1000);

    return 0;
}




SAMPLE_MEMBUF_S l_stVbMem[BLK_MAX + 1]; //用于存储固定内存区域的指针
HI_S32 HI35xx_VB_PoolInit(SIZE_S stPoolSize, HI_U32 u32BlkCnt)
{
    VB_POOL hPool  = VB_INVALID_POOLID;
#if YUV444
    HI_U32  u32BlkSize = (stPoolSize.u32Width * stPoolSize.u32Height * 3) * 3;
#elif YUV422
    HI_U32  u32BlkSize = (stPoolSize.u32Width * stPoolSize.u32Height * 3) * 2;
#else
    HI_U32  u32BlkSize = (stPoolSize.u32Width * stPoolSize.u32Height * 3) >> 1;
#endif
    u32BlkSize = _CEILING_2_POWER(u32BlkSize, HI35xx_SYS_ALIGN_WIDTH);

    hPool = HI_MPI_VB_CreatePool( u32BlkSize, u32BlkCnt, NULL);
    if (hPool == VB_INVALID_POOLID){
        printf("HI_MPI_VB_CreatePool failed!");
        return HI_FAILURE;
    }

    for(HI_U32 i = 0; i < BLK_MAX; i++){
        l_stVbMem[i].hPool = hPool;
        while((l_stVbMem[i].hBlock = HI_MPI_VB_GetBlock(l_stVbMem[i].hPool, u32BlkSize, NULL)) == VB_INVALID_HANDLE){
            ;
        }

        l_stVbMem[i].u32PhyAddr = HI_MPI_VB_Handle2PhysAddr(l_stVbMem[i].hBlock);
        l_stVbMem[i].pVirAddr = (HI_U8 *)HI_MPI_SYS_Mmap( l_stVbMem[i].u32PhyAddr, u32BlkSize );
        if(l_stVbMem[i].pVirAddr == NULL){
            printf("Mem dev may not open");
            HI_MPI_VB_ReleaseBlock(l_stVbMem[i].hBlock);
            return HI_FAILURE;
        }
    }

    return HI_SUCCESS;
}

HI_S32 HI35xx_VB_PoolDestroy()
{
    HI_S32 s32Ret ;
    for(HI_U32 i = 0; i < BLK_MAX + 1 ; i++){
        s32Ret = HI_MPI_VB_ReleaseBlock(l_stVbMem[i].hBlock);
        if( HI_SUCCESS != s32Ret ){
            printf("HI_MPI_VB_ReleaseBlock failed errno 0x%x", s32Ret);
        }
    }

    for(int i = 0; i < VB_MAX_POOLS; i ++){
        s32Ret = HI_MPI_VB_DestroyPool( i );
    }

    return HI_SUCCESS;
}

HI_BOOL HI35xx_StructureVFrmInfo(VIDEO_FRAME_INFO_S *pstFrmInfo, HI_U32 u32Block, SIZE_S stSize)
{
    //向上置为16的倍数
    HI_U32 u32PicLStride    = _CEILING_2_POWER(stSize.u32Width, HI35xx_SYS_ALIGN_WIDTH);
    HI_U32 u32PicCStride    = _CEILING_2_POWER(stSize.u32Width, HI35xx_SYS_ALIGN_WIDTH);
    HI_U32 u32LumaSize      = (u32PicLStride * stSize.u32Height);
#if (YUV422 | YUV444)
    HI_U32 u32ChrmSize      = (u32PicCStride * stSize.u32Height);
#else
    HI_U32 u32ChrmSize      = (u32PicCStride * stSize.u32Height) >> 2;
#endif
    memset(&pstFrmInfo->stVFrame, 0, sizeof(VIDEO_FRAME_S));
    pstFrmInfo->u32PoolId = l_stVbMem[u32Block].hPool;
    pstFrmInfo->stVFrame.u32PhyAddr[0] = l_stVbMem[u32Block].u32PhyAddr;
    pstFrmInfo->stVFrame.u32PhyAddr[1] = pstFrmInfo->stVFrame.u32PhyAddr[0] + u32LumaSize;
    pstFrmInfo->stVFrame.u32PhyAddr[2] = pstFrmInfo->stVFrame.u32PhyAddr[1] + u32ChrmSize;

    pstFrmInfo->stVFrame.pVirAddr[0] = l_stVbMem[u32Block].pVirAddr;
    pstFrmInfo->stVFrame.pVirAddr[1] = (HI_U8*)pstFrmInfo->stVFrame.pVirAddr[0] + u32LumaSize;
    pstFrmInfo->stVFrame.pVirAddr[2] = (HI_U8*)pstFrmInfo->stVFrame.pVirAddr[1] + u32ChrmSize;

    pstFrmInfo->stVFrame.u32Width     = stSize.u32Width;
    pstFrmInfo->stVFrame.u32Height    = stSize.u32Height;
    pstFrmInfo->stVFrame.u32Stride[0] = u32PicLStride;
    pstFrmInfo->stVFrame.u32Stride[1] = u32PicLStride;
    pstFrmInfo->stVFrame.u32Stride[2] = u32PicLStride;
    pstFrmInfo->stVFrame.enPixelFormat  = SAMPLE_PIXEL_FORMAT;
    pstFrmInfo->stVFrame.enVideoFormat  = VIDEO_FORMAT_LINEAR;
    pstFrmInfo->stVFrame.enCompressMode = COMPRESS_MODE_NONE;
    pstFrmInfo->stVFrame.u32Field       = VIDEO_FIELD_FRAME;
    pstFrmInfo->stVFrame.u64pts     = 0;
    pstFrmInfo->stVFrame.u32TimeRef = 0;

    return HI_TRUE;
}

HI_BOOL Hi35xx_IVE_DmaCopy(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *pstSrcFrame,
                           POINT_S stDstPoint, RECT_S stCropRect,
                           HI_BOOL isQuery, HI_BOOL bTimeOut)
{
    if(stCropRect.u32Width < 32 || pstSrcFrame->stVFrame.u32Stride[0] < stCropRect.u32Width)
        return HI_FALSE;

    IVE_HANDLE IveHandle;
    IVE_SRC_DATA_S stSrc;
    IVE_DST_DATA_S stDst;
    IVE_DMA_CTRL_S stDmaCtrl = { IVE_DMA_MODE_DIRECT_COPY, 0, 2, 1, 1};
    HI_BOOL bInstant = HI_TRUE;
    HI_BOOL bFinish;
    HI_U32 s32Ret;

    //cout << "{" << stDstPoint.s32X << ", " << stDstPoint.s32Y << "} | " << stCropRect << endl;

    // NOTE: 保证拷贝源的图像和目标图像有交集
    if(stDstPoint.s32X + (HI_S32)stCropRect.u32Width <= 0 ||
       stDstPoint.s32X >= (HI_S32)pstDstFrame->stVFrame.u32Width)
        return HI_TRUE;
    if(stDstPoint.s32Y + (HI_S32)stCropRect.u32Height <= 0 ||
       stDstPoint.s32Y >= (HI_S32)pstDstFrame->stVFrame.u32Height)
        return HI_TRUE;
    // NOTE: 计算落在目标图像中的拷贝源区域,并重新计算拷贝目标左上角坐标
    if(stDstPoint.s32X < 0){
        stCropRect.s32X = 0 - stDstPoint.s32X;
        stCropRect.u32Width -= stCropRect.s32X;
        stDstPoint.s32X = 0;
    }
    if(stDstPoint.s32Y < 0){
        stCropRect.s32Y = 0 - stDstPoint.s32Y;
        stCropRect.u32Height -= stCropRect.s32Y;
        stDstPoint.s32Y = 0;
    }
    if( stCropRect.s32X + stCropRect.u32Width >= pstDstFrame->stVFrame.u32Width)
        stCropRect.u32Width = pstDstFrame->stVFrame.u32Width - stDstPoint.s32X;
    if( stCropRect.s32Y + stCropRect.u32Height >= pstDstFrame->stVFrame.u32Height)
        stCropRect.u32Height = pstDstFrame->stVFrame.u32Height - stDstPoint.s32Y;

    //cout << "=======================================" << endl;
    //cout << "{" << stDstPoint.s32X << ", " << stDstPoint.s32Y << "} | " << stCropRect << endl;

    // COPY ----- Y
    //增加支持4K
    stSrc.u16Stride  = pstSrcFrame->stVFrame.u32Stride[0];
    //stSrc.u16Stride  = stSrcFrame->stVFrame.u32Stride[1];

    stDst.u16Stride  = pstDstFrame->stVFrame.u32Stride[0];
   // stDst.u16Stride  = stDstFrame->stVFrame.u32Stride[1];

    POINT_S bk_dest_pt = stDstPoint;
    RECT_S bk_crop_rect = stCropRect;

    while((int)bk_crop_rect.u32Height != 0){
        /** NOTE: 考虑到执行效率和 DMA运算对分辨率的要求为 32x1～1920x1080, 对拷贝要求的 W 方向进行处理:
         * 如果 W 大于3840, 则进行如下处理:
         *      - 第一拆分 取 W/3 并向下二对齐;
         *      - 第二拆分 取剩余的 W 的 W'/2 并向下二对齐;
         *      - 第三拆分 取剩余所有的 W''
         * 如果 W 大于1920,且小于3840, 则进行如下处理:
         *      - 第一拆分 取 W/2 并向下二对齐;
         *      - 第二拆分 取剩余所有的 W'
         * 如果 W 小于1920, 则不拆分处理.
         * 即 3944,拆分为1314,1314,1316 ; 3940, 拆分为1312,1314,1314;
         *    3840, 拆分为1920,1920 ; 1952, 拆分为976,976
        **/
        if(stCropRect.u32Width > 3840){
            bk_crop_rect.u32Width = _FLOOR_2_POWER(stCropRect.u32Width / 3, 2);
        }
        else if(stCropRect.u32Width > 1920){
            bk_crop_rect.u32Width = _FLOOR_2_POWER(stCropRect.u32Width / 2, 2);
        }
        else{
            bk_crop_rect.u32Width = stCropRect.u32Width;
        }
        bk_crop_rect.s32X = stCropRect.s32X;
        bk_dest_pt.s32X = stDstPoint.s32X;

        while(bk_crop_rect.s32X < stCropRect.s32X + (int)stCropRect.u32Width){
            stSrc.u32PhyAddr = pstSrcFrame->stVFrame.u32PhyAddr[0] + (int)stSrc.u16Stride * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stSrc.pu8VirAddr = (HI_U8*)pstSrcFrame->stVFrame.pVirAddr[0] + (int)stSrc.u16Stride * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stSrc.u16Width  =  bk_crop_rect.u32Width;
            stSrc.u16Height =  MIN((int)bk_crop_rect.u32Height, DMA_MIN_HEIGHT);

            stDst.u32PhyAddr = pstDstFrame->stVFrame.u32PhyAddr[0] + (int)stDst.u16Stride * bk_dest_pt.s32Y + bk_dest_pt.s32X;
            stDst.pu8VirAddr = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[0] + (int)stDst.u16Stride * bk_dest_pt.s32Y + bk_dest_pt.s32X;
            stDst.u16Width =  stSrc.u16Width;
            stDst.u16Height  =  stSrc.u16Height;

            s32Ret = HI_MPI_IVE_DMA(&IveHandle, &stSrc, &stDst, &stDmaCtrl, bInstant);
            if(HI_SUCCESS!=s32Ret){
                printf("HI_MPI_IVE_DMA failed with %x!", s32Ret);
                printf("stSrc.u16Stride: %d(%dx%d), stDst.u16Stride: %d(%dx%d)",
                      stSrc.u16Stride, stSrc.u16Width, stSrc.u16Height,
                      stDst.u16Stride, stDst.u16Width, stDst.u16Height);
                return HI_FALSE;
            }

            // COPY ----- UV
#if defined (YUV422) || defined (YUV444)
            stSrc.u32PhyAddr = stSrcFrame->stVFrame.u32PhyAddr[1] + (int)stSrc.u16Stride * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stSrc.pu8VirAddr = (HI_U8*)stSrcFrame->stVFrame.pVirAddr[1] + (int)stSrc.u16Stride * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stSrc.u16Width  =  min((int)bk_crop_rect.u32Width, 1920);
            stSrc.u16Height =  min((int)bk_crop_rect.u32Height, DMA_MIN_HEIGHT);

            stDst.u32PhyAddr = pstDstFrame->stVFrame.u32PhyAddr[1] + (int)stDst.u16Stride * bk_dest_pt.s32Y + bk_dest_pt.s32X;
            stDst.pu8VirAddr = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[1] + (int)stDst.u16Stride * bk_dest_pt.s32Y + bk_dest_pt.s32X;
            stDst.u16Width =  stSrc.u16Width;
            stDst.u16Height  =  stSrc.u16Height;
#else
            stSrc.u32PhyAddr = pstSrcFrame->stVFrame.u32PhyAddr[1] + (int)stSrc.u16Stride * bk_crop_rect.s32Y / 2 + bk_crop_rect.s32X;
            stSrc.pu8VirAddr = (HI_U8*)pstSrcFrame->stVFrame.pVirAddr[1] + (int)stSrc.u16Stride * bk_crop_rect.s32Y / 2 + bk_crop_rect.s32X;
            stSrc.u16Width  =  bk_crop_rect.u32Width;
            stSrc.u16Height =  MIN((int)bk_crop_rect.u32Height, DMA_MIN_HEIGHT) / 2;

            stDst.u32PhyAddr = pstDstFrame->stVFrame.u32PhyAddr[1] + (int)stDst.u16Stride * bk_dest_pt.s32Y / 2 + bk_dest_pt.s32X;
            stDst.pu8VirAddr = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[1] + (int)stDst.u16Stride * bk_dest_pt.s32Y / 2 + bk_dest_pt.s32X;
            stDst.u16Width =  stSrc.u16Width;
            stDst.u16Height  =  stSrc.u16Height;
#endif
            s32Ret = HI_MPI_IVE_DMA(&IveHandle, &stSrc, &stDst, &stDmaCtrl, bInstant);
            if(HI_SUCCESS!=s32Ret){
                printf("HI_MPI_IVE_DMA failed with %x", s32Ret);
                return HI_FALSE;
            }

            // COPY ----- V
#if YUV444
            stSrc.u32PhyAddr = stSrcFrame->stVFrame.u32PhyAddr[2] + (int)stSrc.u16Stride * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stSrc.pu8VirAddr = (HI_U8*)stSrcFrame->stVFrame.pVirAddr[2] + (int)stSrc.u16Stride * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stSrc.u16Width  =  min((int)bk_crop_rect.u32Width, 1920);
            stSrc.u16Height =  min((int)bk_crop_rect.u32Height, DMA_MIN_HEIGHT);

            stDst.u32PhyAddr = pstDstFrame->stVFrame.u32PhyAddr[2] + (int)stDst.u16Stride * bk_dest_pt.s32Y + bk_dest_pt.s32X;
            stDst.pu8VirAddr = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[2] + (int)stDst.u16Stride * bk_dest_pt.s32Y + bk_dest_pt.s32X;
            stDst.u16Width =  stSrc.u16Width;
            stDst.u16Height  =  stSrc.u16Height;

            s32Ret = HI_MPI_IVE_DMA(&IveHandle, &stSrc, &stDst, &stDmaCtrl, bInstant);
            if(HI_SUCCESS!=s32Ret){
                log_e("HI_MPI_IVE_DMA failed with %x", s32Ret);
                return HI_FALSE;
            }
#endif
            bk_dest_pt.s32X += bk_crop_rect.u32Width;
            bk_crop_rect.s32X += bk_crop_rect.u32Width;
            bk_crop_rect.u32Width = stCropRect.u32Width + stCropRect.s32X - bk_crop_rect.s32X;
            if(bk_crop_rect.u32Width > 1920){
                bk_crop_rect.u32Width = _FLOOR_2_POWER(bk_crop_rect.u32Width / 2, 2);
            }
        }
        int act_h = MIN((int)bk_crop_rect.u32Height, DMA_MIN_HEIGHT);
        bk_crop_rect.u32Height -= act_h;
        bk_crop_rect.s32Y += act_h;
        bk_dest_pt.s32Y += act_h;
    }

    if(isQuery){
        s32Ret = HI_MPI_IVE_Query(IveHandle, &bFinish, bTimeOut);
        if (HI_SUCCESS != s32Ret){
            return HI_FALSE;
        }
        printf("HI_MPI_IVE_Query: bFinish = %d", bFinish);
    }

    return HI_TRUE;
}

HI_BOOL Hi35xx_IVE_And(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *pstSrcFrame1,
                       VIDEO_FRAME_INFO_S *pstSrcFrame2,
                       POINT_S stDstPoint, POINT_S stSrc1Point, POINT_S stSrc2Point,
                       SIZE_S stCropSize, HI_BOOL isQuery, HI_BOOL bTimeOut)
{
    IVE_HANDLE IveHandle;
    IVE_SRC_IMAGE_S stSrc1, stSrc2;
    IVE_DST_IMAGE_S stDst;
    HI_BOOL bInstant = HI_TRUE;
    HI_BOOL bFinish;
    HI_S32 s32Ret;

    // NOTE: 全部都向下2对齐
    stDstPoint.s32X  = _FLOOR_2_POWER(stDstPoint.s32X, 2);
    stDstPoint.s32Y  = _FLOOR_2_POWER(stDstPoint.s32Y, 2);
    stSrc1Point.s32X = _FLOOR_2_POWER(stSrc1Point.s32X, 2);
    stSrc1Point.s32Y = _FLOOR_2_POWER(stSrc1Point.s32Y, 2);
    stSrc2Point.s32X = _FLOOR_2_POWER(stSrc2Point.s32X, 2);
    stSrc2Point.s32Y = _FLOOR_2_POWER(stSrc2Point.s32Y, 2);
    stCropSize.u32Width  = _FLOOR_2_POWER(stCropSize.u32Width, 2);
    stCropSize.u32Height = _FLOOR_2_POWER(stCropSize.u32Height, 2);

    stSrc1.enType = IVE_IMAGE_TYPE_U8C1;
    stSrc2.enType = IVE_IMAGE_TYPE_U8C1;
    stDst.enType  = IVE_IMAGE_TYPE_U8C1;

    stSrc1.u16Stride[0] = pstSrcFrame1->stVFrame.u32Stride[0];
    stSrc2.u16Stride[0] = pstSrcFrame2->stVFrame.u32Stride[0];
    stDst.u16Stride[0]  = pstDstFrame->stVFrame.u32Stride[0];

    stSrc1.u16Width   =  MIN((HI_S32)stCropSize.u32Width , 1920);
    stSrc1.u16Height  =  MIN((HI_S32)stCropSize.u32Height, 1080);
    stSrc2.u16Width   =  stSrc1.u16Width;
    stSrc2.u16Height  =  stSrc1.u16Height;

    stDst.u16Width   =  stSrc1.u16Width;
    stDst.u16Height  =  stSrc1.u16Height;

    // Y
    stSrc1.u32PhyAddr[0] = pstSrcFrame1->stVFrame.u32PhyAddr[0] + (int)stSrc1.u16Stride[0] * stSrc1Point.s32Y + stSrc1Point.s32X;
    stSrc1.pu8VirAddr[0] = (HI_U8*)pstSrcFrame1->stVFrame.pVirAddr[0] + (int)stSrc1.u16Stride[0] * stSrc1Point.s32Y + stSrc1Point.s32X;

    stSrc2.u32PhyAddr[0] = pstSrcFrame2->stVFrame.u32PhyAddr[0] + (int)stSrc2.u16Stride[0] * stSrc2Point.s32Y + stSrc2Point.s32X;
    stSrc2.pu8VirAddr[0] = (HI_U8*)pstSrcFrame2->stVFrame.pVirAddr[0] + (int)stSrc2.u16Stride[0] * stSrc2Point.s32Y + stSrc2Point.s32X;

    stDst.u32PhyAddr[0] = pstDstFrame->stVFrame.u32PhyAddr[0] + (int)stDst.u16Stride[0] * stDstPoint.s32Y + stDstPoint.s32X;
    stDst.pu8VirAddr[0] = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[0] + (int)stDst.u16Stride[0] * stDstPoint.s32Y + stDstPoint.s32X;

    s32Ret = HI_MPI_IVE_And(&IveHandle, &stSrc1, &stSrc2, &stDst, bInstant);
    if(HI_SUCCESS!=s32Ret){
        printf("HI_MPI_IVE_And failed with %x", s32Ret);
        return HI_FALSE;
    }

    // UV
    stSrc1.u16Height = _FLOOR_2_POWER(stSrc1.u16Height / 2, 2);
    stSrc2.u16Height = _FLOOR_2_POWER(stSrc2.u16Height / 2, 2);
    stDst.u16Height   = stSrc1.u16Height;
    stSrc1.u32PhyAddr[0] = pstSrcFrame1->stVFrame.u32PhyAddr[1] + (int)stSrc1.u16Stride[0] * stSrc1Point.s32Y / 2 + stSrc1Point.s32X;
    stSrc1.pu8VirAddr[0] = (HI_U8*)pstSrcFrame1->stVFrame.pVirAddr[1] + (int)stSrc1.u16Stride[0] * stSrc1Point.s32Y / 2 + stSrc1Point.s32X;

    stSrc2.u32PhyAddr[0] = pstSrcFrame2->stVFrame.u32PhyAddr[1] + (int)stSrc2.u16Stride[0] * stSrc2Point.s32Y / 2 + stSrc2Point.s32X;
    stSrc2.pu8VirAddr[0] = (HI_U8*)pstSrcFrame2->stVFrame.pVirAddr[1] + (int)stSrc2.u16Stride[0] * stSrc2Point.s32Y / 2 + stSrc2Point.s32X;

    stDst.u32PhyAddr[0] = pstDstFrame->stVFrame.u32PhyAddr[1] + (int)stDst.u16Stride[0] * stDstPoint.s32Y / 2 + stDstPoint.s32X;
    stDst.pu8VirAddr[0] = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[1] + (int)stDst.u16Stride[0] * stDstPoint.s32Y / 2 + stDstPoint.s32X;

    s32Ret = HI_MPI_IVE_And(&IveHandle, &stSrc1, &stSrc2, &stDst, bInstant);
    if(HI_SUCCESS!=s32Ret){
        printf("HI_MPI_IVE_And failed with %x", s32Ret);
        return HI_FALSE;
    }

    if(isQuery){
        s32Ret = HI_MPI_IVE_Query(IveHandle, &bFinish, bTimeOut);
        if (HI_SUCCESS != s32Ret){
            return HI_FALSE;
        }
        printf("HI_MPI_IVE_Query: bFinish = %d", bFinish);
    }

    return HI_TRUE;
}

HI_BOOL Hi35xx_IVE_Or(VIDEO_FRAME_INFO_S *pstDstFrame, VIDEO_FRAME_INFO_S *pstSrcFrame1,
                      VIDEO_FRAME_INFO_S *pstSrcFrame2,
                      POINT_S stDstPoint, POINT_S stSrc1Point, POINT_S stSrc2Point,
                      SIZE_S stCropSize, HI_BOOL isQuery, HI_BOOL bTimeOut)
{
    IVE_HANDLE IveHandle;
    IVE_SRC_IMAGE_S stSrc1, stSrc2;
    IVE_DST_IMAGE_S stDst;
    HI_BOOL bInstant = HI_TRUE;
    HI_BOOL bFinish;
    HI_S32 s32Ret;

    // NOTE: 全部都向下2对齐
    stDstPoint.s32X  = _FLOOR_2_POWER(stDstPoint.s32X, 2);
    stDstPoint.s32Y  = _FLOOR_2_POWER(stDstPoint.s32Y, 2);
    stSrc1Point.s32X = _FLOOR_2_POWER(stSrc1Point.s32X, 2);
    stSrc1Point.s32Y = _FLOOR_2_POWER(stSrc1Point.s32Y, 2);
    stSrc2Point.s32X = _FLOOR_2_POWER(stSrc2Point.s32X, 2);
    stSrc2Point.s32Y = _FLOOR_2_POWER(stSrc2Point.s32Y, 2);
    stCropSize.u32Width  = _FLOOR_2_POWER(stCropSize.u32Width, 2);
    stCropSize.u32Height = _FLOOR_2_POWER(stCropSize.u32Height, 2);

    stSrc1.enType = IVE_IMAGE_TYPE_U8C1;
    stSrc2.enType = IVE_IMAGE_TYPE_U8C1;
    stDst.enType  = IVE_IMAGE_TYPE_U8C1;

    stSrc1.u16Stride[0] = pstSrcFrame1->stVFrame.u32Stride[0];
    stSrc2.u16Stride[0] = pstSrcFrame2->stVFrame.u32Stride[0];
    stDst.u16Stride[0]  = pstDstFrame->stVFrame.u32Stride[0];

    stSrc1.u16Width   =  MIN((HI_S32)stCropSize.u32Width , 1920);
    stSrc1.u16Height  =  MIN((HI_S32)stCropSize.u32Height, 1080);
    stSrc2.u16Width   =  stSrc1.u16Width;
    stSrc2.u16Height  =  stSrc1.u16Height;

    stDst.u16Width   =  stSrc1.u16Width;
    stDst.u16Height  =  stSrc1.u16Height;

    // Y
    stSrc1.u32PhyAddr[0] = pstSrcFrame1->stVFrame.u32PhyAddr[0] + (int)stSrc1.u16Stride[0] * stSrc1Point.s32Y + stSrc1Point.s32X;
    stSrc1.pu8VirAddr[0] = (HI_U8*)pstSrcFrame1->stVFrame.pVirAddr[0] + (int)stSrc1.u16Stride[0] * stSrc1Point.s32Y + stSrc1Point.s32X;

    stSrc2.u32PhyAddr[0] = pstSrcFrame2->stVFrame.u32PhyAddr[0] + (int)stSrc2.u16Stride[0] * stSrc2Point.s32Y + stSrc2Point.s32X;
    stSrc2.pu8VirAddr[0] = (HI_U8*)pstSrcFrame2->stVFrame.pVirAddr[0] + (int)stSrc2.u16Stride[0] * stSrc2Point.s32Y + stSrc2Point.s32X;

    stDst.u32PhyAddr[0] = pstDstFrame->stVFrame.u32PhyAddr[0] + (int)stDst.u16Stride[0] * stDstPoint.s32Y + stDstPoint.s32X;
    stDst.pu8VirAddr[0] = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[0] + (int)stDst.u16Stride[0] * stDstPoint.s32Y + stDstPoint.s32X;

    s32Ret = HI_MPI_IVE_Or(&IveHandle, &stSrc1, &stSrc2, &stDst, bInstant);
    if(HI_SUCCESS!=s32Ret){
        printf("HI_MPI_IVE_Or failed with %x", s32Ret);
        return HI_FALSE;
    }

    // UV
    stSrc1.u16Height = _FLOOR_2_POWER(stSrc1.u16Height / 2, 2);
    stSrc2.u16Height = _FLOOR_2_POWER(stSrc2.u16Height / 2, 2);
    stDst.u16Height   = stSrc1.u16Height;
    stSrc1.u32PhyAddr[0] = pstSrcFrame1->stVFrame.u32PhyAddr[1] + (int)stSrc1.u16Stride[0] * stSrc1Point.s32Y / 2 + stSrc1Point.s32X;
    stSrc1.pu8VirAddr[0] = (HI_U8*)pstSrcFrame1->stVFrame.pVirAddr[1] + (int)stSrc1.u16Stride[0] * stSrc1Point.s32Y / 2 + stSrc1Point.s32X;

    stSrc2.u32PhyAddr[0] = pstSrcFrame2->stVFrame.u32PhyAddr[1] + (int)stSrc2.u16Stride[0] * stSrc2Point.s32Y / 2 + stSrc2Point.s32X;
    stSrc2.pu8VirAddr[0] = (HI_U8*)pstSrcFrame2->stVFrame.pVirAddr[1] + (int)stSrc2.u16Stride[0] * stSrc2Point.s32Y / 2 + stSrc2Point.s32X;

    stDst.u32PhyAddr[0] = pstDstFrame->stVFrame.u32PhyAddr[1] + (int)stDst.u16Stride[0] * stDstPoint.s32Y / 2 + stDstPoint.s32X;
    stDst.pu8VirAddr[0] = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[1] + (int)stDst.u16Stride[0] * stDstPoint.s32Y / 2 + stDstPoint.s32X;

    s32Ret = HI_MPI_IVE_Or(&IveHandle, &stSrc1, &stSrc2, &stDst, bInstant);
    if(HI_SUCCESS!=s32Ret){
        printf("HI_MPI_IVE_Or failed with %x", s32Ret);
        return HI_FALSE;
    }

    if(isQuery){
        s32Ret = HI_MPI_IVE_Query(IveHandle, &bFinish, bTimeOut);
        if (HI_SUCCESS != s32Ret){
            return HI_FALSE;
        }
        printf("HI_MPI_IVE_Query: bFinish = %d", bFinish);
    }

    return HI_TRUE;
}

HI_BOOL Hi35xx_IVE_DmaFill(VIDEO_FRAME_INFO_S *pstDstFrame, RECT_S stCropRect, COLOR_YUV stColor,
                           HI_BOOL isQuery, HI_BOOL bTimeOut)
{
    HI_U16 data_temp = stColor.Y;
    data_temp <<= 8;
    data_temp |= stColor.Y;
    HI_U64 data_Y = 0;
    for(int i = 0; i < 4; i ++){
        data_Y <<= 16;
        data_Y |= data_temp;
    }

    data_temp = stColor.U;
    data_temp <<= 8;
    data_temp |= stColor.V;
    HI_U64 data_VU = 0;
    for(int i = 0; i < 4; i ++){
        data_VU <<= 16;
        data_VU |= data_temp;
    }

    IVE_HANDLE IveHandle;
    IVE_SRC_DATA_S stDst;
    IVE_DMA_CTRL_S stDmaCtrl = { IVE_DMA_MODE_SET_8BYTE, 0, 2, 1, 1};
    HI_BOOL bInstant = HI_TRUE;
    HI_BOOL bFinish;
    HI_S32 s32Ret;

    // COPY ----- Y
    //增加支持4K
    stDst.u16Stride  = pstDstFrame->stVFrame.u32Stride[0];

//    int area = 0;
    RECT_S bk_crop_rect = stCropRect;

    while((int)bk_crop_rect.u32Height != 0){
        /** NOTE: 考虑到执行效率和 DMA运算对分辨率的要求为 32x1～1920x1080, 对拷贝要求的 W 方向进行处理:
         * 如果 W 大于3840, 则进行如下处理:
         *      - 第一拆分 取 W/3 并向下二对齐;
         *      - 第二拆分 取剩余的 W 的 W'/2 并向下二对齐;
         *      - 第三拆分 取剩余所有的 W''
         * 如果 W 大于1920,且小于3840, 则进行如下处理:
         *      - 第一拆分 取 W/2 并向下二对齐;
         *      - 第二拆分 取剩余所有的 W'
         * 如果 W 小于1920, 则不拆分处理.
         * 即 3944,拆分为1314,1314,1316 ; 3940, 拆分为1312,1314,1314;
         *    3840, 拆分为1920,1920 ; 1952, 拆分为976,976
        **/
        if(stCropRect.u32Width > 3840){
            bk_crop_rect.u32Width = _FLOOR_2_POWER(stCropRect.u32Width / 3, 2);
        }
        else if(stCropRect.u32Width > 1920){
            bk_crop_rect.u32Width = _FLOOR_2_POWER(stCropRect.u32Width / 2, 2);
        }
        else{
            bk_crop_rect.u32Width = stCropRect.u32Width;
        }
        bk_crop_rect.s32X = stCropRect.s32X;

        while(bk_crop_rect.s32X < stCropRect.s32X + (int)stCropRect.u32Width){
            stDst.u32PhyAddr = pstDstFrame->stVFrame.u32PhyAddr[0] + (int)stDst.u16Stride * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stDst.pu8VirAddr = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[0] + (int)stDst.u16Stride * bk_crop_rect.s32Y + bk_crop_rect.s32X;
            stDst.u16Width  =  bk_crop_rect.u32Width;
            stDst.u16Height =  MIN((int)bk_crop_rect.u32Height, DMA_MIN_HEIGHT);

            stDmaCtrl.u64Val = data_Y;
            s32Ret = HI_MPI_IVE_DMA(&IveHandle, &stDst, &stDst, &stDmaCtrl, bInstant);
            if(HI_SUCCESS!=s32Ret){
                printf("HI_MPI_IVE_DMA failed with %x!", s32Ret);
                printf("stDst.u16Stride: %d(%dx%d)",
                      stDst.u16Stride, stDst.u16Width, stDst.u16Height);
                return HI_FALSE;
            }

            // COPY ----- UV
            stDst.u32PhyAddr = pstDstFrame->stVFrame.u32PhyAddr[1] + (int)stDst.u16Stride * bk_crop_rect.s32Y / 2 + bk_crop_rect.s32X;
            stDst.pu8VirAddr = (HI_U8*)pstDstFrame->stVFrame.pVirAddr[1] + (int)stDst.u16Stride * bk_crop_rect.s32Y / 2 + bk_crop_rect.s32X;
            stDst.u16Width  =  bk_crop_rect.u32Width;
            stDst.u16Height =  MIN((int)bk_crop_rect.u32Height, DMA_MIN_HEIGHT) / 2;

            stDmaCtrl.u64Val = data_VU;
            s32Ret = HI_MPI_IVE_DMA(&IveHandle, &stDst, &stDst, &stDmaCtrl, bInstant);
            if(HI_SUCCESS!=s32Ret){
                printf("HI_MPI_IVE_DMA failed with %x", s32Ret);
                return HI_FALSE;
            }
            bk_crop_rect.s32X += bk_crop_rect.u32Width;
            bk_crop_rect.u32Width = stCropRect.u32Width + stCropRect.s32X - bk_crop_rect.s32X;
            if(bk_crop_rect.u32Width > 1920){
                bk_crop_rect.u32Width = _FLOOR_2_POWER(bk_crop_rect.u32Width / 2, 2);
            }
        }
        int act_h = MIN((int)bk_crop_rect.u32Height, DMA_MIN_HEIGHT);
        bk_crop_rect.u32Height -= act_h;
        bk_crop_rect.s32Y += act_h;
    }

    if(isQuery){
        s32Ret = HI_MPI_IVE_Query(IveHandle, &bFinish, bTimeOut);
        if (HI_SUCCESS != s32Ret){
            return HI_FALSE;
        }
        printf("HI_MPI_IVE_Query: bFinish = %d", bFinish);
    }

    return HI_TRUE;
}

HI_BOOL HI35xx_PaddingYUV420FrameRGB(VIDEO_FRAME_INFO_S *stVFrmInfo, RECT_S stFrmRect, COLOR_RGB stColor)
{
    COLOR_YUV color_YUV;
//    color_YUV.Y = (( 66  * stColor.R + 129 * stColor.G +  25 * stColor.B + 128) >> 8) + 16;
//    color_YUV.U = (( -38 * stColor.R -  74 * stColor.G + 112 * stColor.B + 128) >> 8) + 128;
//    color_YUV.V = (( 112 * stColor.R -  94 * stColor.G -  18 * stColor.B + 128) >> 8) + 128;

    color_YUV.Y = MIN(((int)( 77  * (int)stColor.R + 150 * (int)stColor.G +  29 * (int)stColor.B + 128) >> 8), 255);
    color_YUV.U = MAX2(MIN((int)((( -44 ) * (int)stColor.R -  78 * (int)stColor.G + 131 * (int)stColor.B + 128) >> 8) + 128, 255), 0);
    color_YUV.V = MAX2(MIN((int)(( 131 * (int)stColor.R - 110 * (int)stColor.G -  21 * (int)stColor.B + 128) >> 8) + 128, 255), 0);

#if 0
    memset(stVFrmInfo->stVFrame.pVirAddr[0], color_YUV.Y,
            stFrmRect.u32Width * stFrmRect.u32Height);

#if YUV444
    memset(stVFrmInfo->stVFrame.pVirAddr[1], U,
            stFrmRect.u32Width * stFrmRect.u32Height);
    memset(stVFrmInfo->stVFrame.pVirAddr[2], V,
            stFrmRect.u32Width * stFrmRect.u32Height);
#else
#if YUV422
    HI_U32 u32LengthUV = stFrmRect.u32Width * stFrmRect.u32Height;
    for(HI_U32 i = 0; i < stFrmRect.u32Width * stFrmRect.u32Height; i++){
        *((HI_U8*)(stVFrmInfo->stVFrame.pVirAddr[1]) + i) =  V;
        i++;
        *((HI_U8*)(stVFrmInfo->stVFrame.pVirAddr[1]) + i) =  U;
    }
#else
    HI_U32 u32LengthUV = stFrmRect.u32Width * stFrmRect.u32Height / 2;
    for(HI_U32 i = 0; i < u32LengthUV; ++i){
        *((HI_U8*)(stVFrmInfo->stVFrame.pVirAddr[1]) + i) =  color_YUV.V;
        ++i;
        *((HI_U8*)(stVFrmInfo->stVFrame.pVirAddr[1]) + i) =  color_YUV.U;
    }
#endif
#endif
    return HI_TRUE;
#else
    return Hi35xx_IVE_DmaFill(stVFrmInfo, stFrmRect, color_YUV, HI_TRUE, HI_TRUE);
#endif
}

HI_BOOL HI35xx_PaddingYUV420FrameYUV(VIDEO_FRAME_INFO_S *stVFrmInfo, RECT_S stFrmRect, COLOR_YUV stColor)
{
#if 0
    memset(stVFrmInfo->stVFrame.pVirAddr[0], stColor.Y,
            stFrmRect.u32Width * stFrmRect.u32Height);

    HI_U32 u32LengthUV = stFrmRect.u32Width * stFrmRect.u32Height / 2;
    for(HI_U32 i = 0; i < u32LengthUV; ++i){
        *((HI_U8*)(stVFrmInfo->stVFrame.pVirAddr[1]) + i) =  stColor.V;
        ++i;
        *((HI_U8*)(stVFrmInfo->stVFrame.pVirAddr[1]) + i) =  stColor.U;
    }
    return HI_TRUE;
#else
    return Hi35xx_IVE_DmaFill(stVFrmInfo, stFrmRect, stColor, HI_TRUE);
#endif
}

//画线
HI_BOOL Hi35xx_VGS_Line(VIDEO_FRAME_INFO_S *stImgOut, POINT_S startPoint, POINT_S endPoint, int w, int clr_rgb)
{
    HI_S32 s32Ret = HI_FAILURE;
    VGS_HANDLE hHandle;
    VGS_TASK_ATTR_S stTask;
    VGS_LINE_S astVgsDrawLine[4];

//    int clr = clr_rgb.R;
//    clr <<= 8;
//    clr += clr_rgb.G;
//    clr <<= 8;
//    clr += clr_rgb.B;

//    printf("start %d %d, %d %d\n",startPoint.s32X,startPoint.s32Y,endPoint.s32X,endPoint.s32Y);
    astVgsDrawLine[0].u32Color = clr_rgb;
    astVgsDrawLine[0].u32Thick = w;
    astVgsDrawLine[0].stStartPoint.s32X = startPoint.s32X;
    astVgsDrawLine[0].stStartPoint.s32Y = startPoint.s32Y;
    astVgsDrawLine[0].stEndPoint.s32X = endPoint.s32X;
    astVgsDrawLine[0].stEndPoint.s32Y = endPoint.s32Y;

    s32Ret = HI_MPI_VGS_BeginJob(&hHandle);
    if(s32Ret != HI_SUCCESS){
        printf("HI_MPI_VGS_BeginJob failed %#x", s32Ret);
        return HI_FALSE;
    }

    memcpy(&stTask.stImgOut, stImgOut, sizeof(VIDEO_FRAME_INFO_S));
    memcpy(&stTask.stImgIn, stImgOut, sizeof(VIDEO_FRAME_INFO_S));

    s32Ret = HI_MPI_VGS_AddDrawLineTask(hHandle, &stTask, astVgsDrawLine, 1);
    if(s32Ret != HI_SUCCESS){
        printf("HI_MPI_VGS_AddScaleTask failed %#x",s32Ret);
        HI_MPI_VGS_CancelJob(hHandle);
        return HI_FALSE;
    }

    s32Ret = HI_MPI_VGS_EndJob(hHandle);    //执行JOB函数
    if(s32Ret != HI_SUCCESS){
        printf("HI_MPI_VGS_EndJob failed %#x", s32Ret);
        HI_MPI_VGS_CancelJob(hHandle);
        return HI_FALSE;
    }

    return HI_TRUE;
}

//画框
HI_BOOL Hi35xx_VGS_Frame(VIDEO_FRAME_INFO_S *stImgOut, RECT_S rect, int w, int clr_rgb)
{
    HI_S32 s32Ret = HI_FAILURE;
    VGS_HANDLE hHandle;
    VGS_TASK_ATTR_S stTask;
    VGS_LINE_S astVgsDrawLine[4];

//    int clr = clr_rgb.R;
//    clr <<= 8;
//    clr += clr_rgb.G;
//    clr <<= 8;
//    clr += clr_rgb.B;

    astVgsDrawLine[0].u32Color = clr_rgb;
    astVgsDrawLine[0].u32Thick = w;
    astVgsDrawLine[0].stStartPoint.s32X = rect.s32X;
    astVgsDrawLine[0].stStartPoint.s32Y = rect.s32Y;
    astVgsDrawLine[0].stEndPoint = astVgsDrawLine[0].stStartPoint;
    astVgsDrawLine[0].stEndPoint.s32X += rect.u32Width;

    astVgsDrawLine[1].u32Color = clr_rgb;
    astVgsDrawLine[1].u32Thick = w;
    astVgsDrawLine[1].stStartPoint = astVgsDrawLine[0].stEndPoint;
    astVgsDrawLine[1].stEndPoint = astVgsDrawLine[1].stStartPoint;
    astVgsDrawLine[1].stEndPoint.s32Y += rect.u32Height;

    astVgsDrawLine[2].u32Color = clr_rgb;
    astVgsDrawLine[2].u32Thick = w;
    astVgsDrawLine[2].stStartPoint = astVgsDrawLine[1].stEndPoint;
    astVgsDrawLine[2].stEndPoint = astVgsDrawLine[2].stStartPoint;
    astVgsDrawLine[2].stEndPoint.s32X -= rect.u32Width;

    astVgsDrawLine[3].u32Color = clr_rgb;
    astVgsDrawLine[3].u32Thick = w;
    astVgsDrawLine[3].stStartPoint = astVgsDrawLine[2].stEndPoint;
    astVgsDrawLine[3].stEndPoint = astVgsDrawLine[0].stStartPoint;

    s32Ret = HI_MPI_VGS_BeginJob(&hHandle);
    if(s32Ret != HI_SUCCESS){
        printf("HI_MPI_VGS_BeginJob failed %#x", s32Ret);
        return HI_FALSE;
    }

    memcpy(&stTask.stImgOut, stImgOut, sizeof(VIDEO_FRAME_INFO_S));
    memcpy(&stTask.stImgIn, stImgOut, sizeof(VIDEO_FRAME_INFO_S));

    s32Ret = HI_MPI_VGS_AddDrawLineTask(hHandle, &stTask, astVgsDrawLine, 4);
    if(s32Ret != HI_SUCCESS){
        printf("HI_MPI_VGS_AddScaleTask failed %#x",s32Ret);
        HI_MPI_VGS_CancelJob(hHandle);
        return HI_FALSE;
    }

    s32Ret = HI_MPI_VGS_EndJob(hHandle);    //执行JOB函数
    if(s32Ret != HI_SUCCESS){
        printf("HI_MPI_VGS_EndJob failed %#x", s32Ret);
        HI_MPI_VGS_CancelJob(hHandle);
        return HI_FALSE;
    }

    return HI_TRUE;
}

