﻿#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <linux/socket.h>
#include <linux/netlink.h>
#include <fstream>
#include <sstream>
#include <sys/shm.h>






#include "himpp_master.h"
#include "http_handler.h"
#include "software_config.h"
#include "commandmap.h"
#include <semaphore.h>
#include <common.h>

#define MAXDATA BUFF_SIZE

HI_U32 viNumber = 4;
HI_U32 aiNumber = 0;
PARAMETER viParam[VI_PORT_NUMBER];
AIPARAM   aiParam[AI_PORT_NUMBER];
pthread_t input_tid[VI_PORT_NUMBER];
pthread_t venc_tid[VI_PORT_NUMBER];

//SAMPLE_VI_MODE_E enViMode = SAMPLE_VI_MODE_8_1080P;

struct KoData
{
    unsigned int frame_num;
    long sec;	/* Seconds.  */
    long usec;	/* Microseconds.  */
};

typedef struct master_data
{
    int     master_rate; //主节点调节中值
    KoData  ko_date;
}MASTER_DATA;

bool l_it66021fn_running = true;

bool l_gs2971a_running = true;

bool audio_running[3] =  {true,true,true};

AENC_CHN Aechn0Chn;
AENC_CHN Aechn1Chn;
AENC_CHN Aechn2Chn;
AENC_CHN Aechn3Chn;
int ttl = 64;
AENC_CHN Aechn[4];
VISETPARAM viSet[4];
HI_U32 AI_value_35[4] = {0,0,0,0};
HI_U32 AI_value_HD[4] = {0,0,0,0};
HI_BOOL audio_HDMI0 = HI_FALSE;
HI_BOOL audio_HDMI1 = HI_FALSE;
HI_BOOL audio_I2S = HI_FALSE;

HI_BOOL audio_HDMI0_Mcast = HI_FALSE;
HI_BOOL audio_HDMI1_Mcast = HI_FALSE;
HI_BOOL audio_I2S_Mcast = HI_FALSE;

HI_BOOL g_vi0_hdmi = HI_TRUE;
HI_BOOL g_vi1_hdmi = HI_TRUE;
HI_BOOL g_vi2_hdmi = HI_TRUE;
HI_BOOL g_vi3_hdmi = HI_TRUE;


bool l_recvrunning = true;
pthread_t audio_tid[4];
int errFrame[4] = {0,0,0,0};

sem_t sem[4];
bool ex_flag[4] = {false,false,false,false};
HI_BOOL audioFlag[3]  = {HI_FALSE,HI_FALSE,HI_FALSE};

char mcast_ip[8][IPSIZE];
int mcast_port[8];
int aumcast_sv_port[3] = {60123,60124,60125};
char aumcast_ip[3][IPSIZE];
int aumcast_port[4];



int edid_4k = 0;
HI_BOOL i2s_enable = HI_FALSE;
bool vi_src_chang[4] = {false,false,false,false};
bool pth_rcv_jpeg[4] = {false,false,false,false};

int g_vi_en_number = 4;             //VI启用数量，1080P 4个VI 4k30 2个VI
bool g_4hdmi_ai_flag = false;       //强制3.5mm HDMI2  HDMI3有音频 支持UMP动态开启rtsp音频搭配 后期有需求再说

#define NETLINK_IT66021FNBASE 21
#define GROUP_IT66021FNBASE 5

#define MAX_PAYLOAD 1024
#define NETLINK_IT66021FN 21
#define GROUP_IT66021FN   5
#define NETLINK_IT66021FN1 22
#define GROUP_IT66021FN1   6
#define NETLINK_IT66021FN2 23
#define GROUP_IT66021FN2   7
#define NETLINK_IT66021FN3 24
#define GROUP_IT66021FN3   8
HI_S32 VencStreamCnt;
HI_S32 Venc1StreamCnt;
HI_S32 Venc2StreamCnt;
HI_S32 Venc3StreamCnt;

bool pth_it6801_running = true;

#define NODE_MODEL  "input_4hdmi"
struct SW_VERSION sw_version =
{
    "1.1.2.0",     //<主版本号>.<次版本号>.<修订版本号>
    //<阶段版本>
#ifdef __DEBUG__
    //    STAGE_BASE
    STAGE_ALPHA
    //    STAGE_BETA
#else
    //    STAGE_RC
    STAGE_RELEASE
#endif
};


struct msg_struct
{
    unsigned video_width;
    unsigned video_height;
    unsigned video_freq;
    unsigned video_interlaced;   //1-全I帧，识别为无信号
    unsigned video_hdcp;
    unsigned audio_freq;
};

void* input1proc(void *arg);
void Audio_reg_init();
void* pth_vi_venc(void *arg);
void* pth_venc_net(void *arg);
void* pth_snap_net(void * vi_port);
void* pth_rcv_snap_jpeg(void* vi_port);

unsigned int netlink_group_mask(unsigned int group){
    return group ? 1<<(group-1):0;
}

typedef SoftwareConfig Config; //把Class类SoftwareConfig的名称缩写以下
void HandleSig(HI_S32 signo)
{
    int i;
    printf("stop--->0x%x\n",signo);
    l_it66021fn_running = false;
    l_gs2971a_running = false;
    pth_it6801_running = false;
    sleep(1);
    for(i = 0; i < g_vi_en_number; i++) {
        vi_src_chang[i] = true;
        pth_vi_net[i] = false;
        pth_vi_net[i] = false;
        pth_snap[i] = false;
        pth_rcv_jpeg[i] = false;
        usleep(20000);
    }

    for(i = 0; i < AI_PORT_NUMBER; i++) {

        pth_ai_push[i] = false;

        usleep(60000);
        delete g_audio_aac[i];
    }

    usleep(20000);
    for( i = 0; i < g_vi_en_number*2; i++){
        crtsps_closestream(g_stream[i]);
        printf("crtsps_closestream g_strea=%d\n",i);
    }

    crtsps_stop();
    printf("start destory mpp\n");
    Singleton<HimppMaster>::getInstance()->~HimppMaster();

    SAMPLE_PRT("error:program termination abnormally!\n");

    exit(-1);
}

/*****************************************
 * add function : separation information form client
 * 1-- s_str: information from clinet
 * 2-- c_str: command from clinet
 * 3-- d_str: data from client
 * **************************************/
int GetInfo(const char *s_str,char *c_str,char *d_str)
{
    char str1 = '(';
    char str2 = ')';
    char str3 = '[';
    char str4 = ']';
    while( *s_str && (*s_str++ != str1));
    while(*s_str && (*s_str != str2))
        *c_str++ = *s_str++;
    *c_str = '\0';
    while(*s_str && (*s_str++ !=str3));
    while(*s_str && (*s_str !=str4))
        *d_str++ = *s_str++;
    *d_str ='\0';
    return 0;
}



/*
 * 通过获取IT66021驱动信息，同步到INI保存
 * */


RECT_S synViParam(HI_S32 viWidth, HI_S32 viHeight, HI_S32 srcFramRate,HI_S32 viNum,HI_S32  viInte)
{
    auto thisini = Singleton<Config>::getInstance();
    RECT_S setViRect;

    //由驱动获取的信息，初始化新的VI W H SRC
    if((128 >=  viWidth) || (128 >= viHeight) || ((1 == viInte) && (viParam[viNum].vi_model == 0))) {    //无信号，1080P30FPS
        viParam[viNum].u32ViWidth = 1920;
        viParam[viNum].u32ViHeigth = 1080;
        viParam[viNum].u32SrcFrmRate = 30;
        printf("---check no signal---\n");
    }
    else {
        viParam[viNum].u32ViWidth = viWidth;
        viParam[viNum].u32ViHeigth = viHeight;
        viParam[viNum].u32SrcFrmRate = srcFramRate;
    }

    //获取VI RECT_S
    if(viParam[viNum].vi_crop_enable) {
        if((viParam[viNum].u32X + viParam[viNum].u32W) > viParam[viNum].u32ViWidth) {       //如果裁剪的X+W 大于输入源的 W，则自动修正 W = src_w - x;
                viParam[viNum].u32W = viParam[viNum].u32ViWidth - viParam[viNum].u32X;
        }
        if((viParam[viNum].u32Y + viParam[viNum].u32H) > viParam[viNum].u32ViHeigth) {      //如果裁剪的Y+H 大于输入源的 H，则自动修正 H = src_y - y;
            viParam[viNum].u32H = viParam[viNum].u32ViHeigth - viParam[viNum].u32Y;
        }
        setViRect.s32X = viParam[viNum].u32X;
        setViRect.s32Y = viParam[viNum].u32Y;
        setViRect.u32Width = viParam[viNum].u32W;
        setViRect.u32Height = viParam[viNum].u32H;
    }
    else {
        setViRect.s32X = 0;
        setViRect.s32Y = 0;
        setViRect.u32Width = viParam[viNum].u32ViWidth;
        setViRect.u32Height = viParam[viNum].u32ViHeigth;
    }

    //    获取主次编码W H DEST
    if(viParam[viNum].venc_SAME_INPUT[0]) {         //主码流和输入源一致
        if(viParam[viNum].vi_crop_enable) {         //VI裁剪使能，则编码大小和裁剪后一致
            viParam[viNum].stVencSize.u32Width = viParam[viNum].u32W;
            viParam[viNum].stVencSize.u32Height = viParam[viNum].u32H;
        }
        else {
            viParam[viNum].stVencSize.u32Width = viParam[viNum].u32ViWidth;
            viParam[viNum].stVencSize.u32Height = viParam[viNum].u32ViHeigth;
        }
        viParam[viNum].u32DstFrameRate[0] = viParam[viNum].u32SrcFrmRate;
    }
    else {
        if(viParam[viNum].vi_crop_enable) {
            viParam[viNum].stVencSize.u32Width = MIN(viParam[viNum].stVencSize.u32Width,viParam[viNum].u32W);
            viParam[viNum].stVencSize.u32Height = MIN(viParam[viNum].stVencSize.u32Height,viParam[viNum].u32H);
        }
        else {
            viParam[viNum].stVencSize.u32Width = MIN(viParam[viNum].stVencSize.u32Width,viParam[viNum].u32ViWidth);
            viParam[viNum].stVencSize.u32Height = MIN(viParam[viNum].stVencSize.u32Height,viParam[viNum].u32ViHeigth);
        }
        viParam[viNum].u32DstFrameRate[0] = MIN(viParam[viNum].u32DstFrameRate[0],viParam[viNum].u32SrcFrmRate);
    }

    if(viParam[viNum].venc_SAME_INPUT[1]) {         //次码流和输入源一致
        if(viParam[viNum].vi_crop_enable) {         //VI裁剪使能，则编码大小和裁剪后一致
            viParam[viNum].stMinorSize.u32Width = viParam[viNum].u32W;
            viParam[viNum].stMinorSize.u32Height = viParam[viNum].u32H;
        }
        else {
            viParam[viNum].stMinorSize.u32Width = viParam[viNum].u32ViWidth;
            viParam[viNum].stMinorSize.u32Height = viParam[viNum].u32ViHeigth;
        }
        viParam[viNum].u32DstFrameRate[1] = viParam[viNum].u32SrcFrmRate;
    }
    else {
        if(viParam[viNum].vi_crop_enable) {
            viParam[viNum].stMinorSize.u32Width = MIN(viParam[viNum].stMinorSize.u32Width,viParam[viNum].u32W);
            viParam[viNum].stMinorSize.u32Height = MIN(viParam[viNum].stMinorSize.u32Height,viParam[viNum].u32H);
        }
        else {
            viParam[viNum].stMinorSize.u32Width = MIN(viParam[viNum].stMinorSize.u32Width,viParam[viNum].u32ViWidth);
            viParam[viNum].stMinorSize.u32Height = MIN(viParam[viNum].stMinorSize.u32Height,viParam[viNum].u32ViHeigth);
        }
        viParam[viNum].u32DstFrameRate[1] = MIN(viParam[viNum].u32DstFrameRate[1],viParam[viNum].u32SrcFrmRate);
    }

    //保存输入源的参数
    switch (viNum) {
        case 0:
            thisini->SetConfig(Config::kChn0_VI_W, viWidth);
            thisini->SetConfig(Config::kChn0_VI_H, viHeight);
            thisini->SetConfig(Config::kChn0_SrcFrmRate,srcFramRate);

            thisini->SetConfig(Config::kChn0_M_VENC_W,viParam[0].stVencSize.u32Width);
            thisini->SetConfig(Config::kChn0_M_VENC_H,viParam[0].stVencSize.u32Height);
            thisini->SetConfig(Config::kChn0_S0_VENC_W,viParam[0].stMinorSize.u32Width);
            thisini->SetConfig(Config::kChn0_S0_VENC_H,viParam[0].stMinorSize.u32Height);
            thisini->SetConfig(Config::kChn0_M_DstFrmRate, viParam[viNum].u32DstFrameRate[0]);
            thisini->SetConfig(Config::kChn0_S0_DstFrmRate, viParam[viNum].u32DstFrameRate[1]);
            thisini->SetConfig(Config::kChn0_VI_X,viParam[viNum].u32X);
            thisini->SetConfig(Config::kChn0_VI_Y,viParam[viNum].u32Y);
            thisini->SetConfig(Config::kChn0_VI_Width,viParam[viNum].u32W);
            thisini->SetConfig(Config::kChn0_VI_Height,viParam[viNum].u32H);
            thisini->SaveConfig();
            break;
        case 1:
            thisini->SetConfig(Config::kChn1_VI_W, viWidth);
            thisini->SetConfig(Config::kChn1_VI_H, viHeight);
            thisini->SetConfig(Config::kChn1_SrcFrmRate,srcFramRate);
            thisini->SetConfig(Config::kChn1_M_VENC_W,viParam[1].stVencSize.u32Width);
            thisini->SetConfig(Config::kChn1_M_VENC_H,viParam[1].stVencSize.u32Height);
            thisini->SetConfig(Config::kChn1_S0_VENC_W,viParam[1].stMinorSize.u32Width);
            thisini->SetConfig(Config::kChn1_S0_VENC_H,viParam[1].stMinorSize.u32Height);
            thisini->SetConfig(Config::kChn1_M_DstFrmRate, viParam[viNum].u32DstFrameRate[0]);
            thisini->SetConfig(Config::kChn1_VI_X,viParam[viNum].u32X);
            thisini->SetConfig(Config::kChn1_VI_Y,viParam[viNum].u32Y);
            thisini->SetConfig(Config::kChn1_VI_Width,viParam[viNum].u32W);
            thisini->SetConfig(Config::kChn1_VI_Height,viParam[viNum].u32H);

            thisini->SaveConfig();
            break;
        case 2:
            thisini->SetConfig(Config::kChn2_VI_W, viWidth);
            thisini->SetConfig(Config::kChn2_VI_H, viHeight);
            thisini->SetConfig(Config::kChn2_SrcFrmRate,srcFramRate);
            thisini->SetConfig(Config::kChn2_M_VENC_W,viParam[viNum].stVencSize.u32Width);
            thisini->SetConfig(Config::kChn2_M_VENC_H,viParam[viNum].stVencSize.u32Height);
            thisini->SetConfig(Config::kChn2_S0_VENC_W,viParam[viNum].stMinorSize.u32Width);
            thisini->SetConfig(Config::kChn2_S0_VENC_H,viParam[viNum].stMinorSize.u32Height);
            thisini->SetConfig(Config::kChn2_M_DstFrmRate, viParam[viNum].u32DstFrameRate[0]);
            thisini->SetConfig(Config::kChn2_S0_DstFrmRate, viParam[viNum].u32DstFrameRate[1]);
            thisini->SetConfig(Config::kChn2_VI_X,viParam[viNum].u32X);
            thisini->SetConfig(Config::kChn2_VI_Y,viParam[viNum].u32Y);
            thisini->SetConfig(Config::kChn2_VI_Width,viParam[viNum].u32W);
            thisini->SetConfig(Config::kChn2_VI_Height,viParam[viNum].u32H);

            thisini->SaveConfig();
            break;
        case 3:
            thisini->SetConfig(Config::kChn3_VI_W, viWidth);
            thisini->SetConfig(Config::kChn3_VI_H, viHeight);
            thisini->SetConfig(Config::kChn3_SrcFrmRate,srcFramRate);
            thisini->SetConfig(Config::kChn3_M_VENC_W,viParam[viNum].stVencSize.u32Width);
            thisini->SetConfig(Config::kChn3_M_VENC_H,viParam[viNum].stVencSize.u32Height);
            thisini->SetConfig(Config::kChn3_S0_VENC_W,viParam[viNum].stMinorSize.u32Width);
            thisini->SetConfig(Config::kChn3_S0_VENC_H,viParam[viNum].stMinorSize.u32Height);
            thisini->SetConfig(Config::kChn3_M_DstFrmRate, viParam[viNum].u32DstFrameRate[0]);
            thisini->SetConfig(Config::kChn3_S0_DstFrmRate, viParam[viNum].u32DstFrameRate[1]);
            thisini->SetConfig(Config::kChn3_VI_X,viParam[viNum].u32X);
            thisini->SetConfig(Config::kChn3_VI_Y,viParam[viNum].u32Y);
            thisini->SetConfig(Config::kChn3_VI_Width,viParam[viNum].u32W);
            thisini->SetConfig(Config::kChn3_VI_Height,viParam[viNum].u32H);

            thisini->SaveConfig();
            break;
        default:
            break;
    }


    return setViRect;
}

bool  startRtsp(int vi,int *param)
{
    int j = 0;
    char rtspNum[8] = {0};
    video_type vType;
    for(j = 0; j < VencStreamCnt; j++) {
        memset(rtspNum,0,sizeof(rtspNum));
        sprintf(rtspNum,"/%d",2*vi+j);
        switch (viParam[vi].enType[j]) {
            case PT_H265:
                vType = VIDEO_H265;
                break;
            case PT_H264:
                vType = VIDEO_H264;
                break;
            default:
                break;
        }
        if(viParam[vi].AI_type == AUDIO_NULL) {
//            printf("vi %d rtsp start null audio,type-->%d\n",vi,coderFormat[vi]);
            g_stream[2*vi+j] = crtsps_openstream(vType,AUDIO_NULL,rtspNum,NULL);
        }
        else {
//            printf("start %d rtsp type-->%d\n",2*vi+j,coderFormat[vi]);
            switch (viParam[vi].AI_type) {
                case 1:
                    printf("start %d rtsp aac type\n",2*vi+j);
                    g_stream[2*vi+j] = crtsps_openstream(vType,AUDIO_AAC,rtspNum,param);
                    break;
                case 2:
//                    printf("start %d rtsp G711 type\n",2*vi+j);
                    g_stream[2*vi+j] = crtsps_openstream(vType,AUDIO_G711A,rtspNum,param);
                    break;
                default:
                    break;
            }
        }

    }

    return true;
}

/*
 * 动态检测IT6602汇报的分辨率和帧率
 * 初始化输入，编码
 * 重启 VENC-RTSP-MULTICAST 线程
 *
 * */
HI_BOOL G711Init = HI_FALSE;

void* timeCheckIt66021(void *vi_)
{
    int vi = *(int*)vi_;

    int sockFd = 0;   //暂时支持最大4输入
    bool startFlg = true;

    bool  b_detached = true;
    pthread_t snap_tid = 0;
    pthread_t rcv_jpeg_tid = 0;
    pthread_t vi_venc_tid = 0;
    pthread_t venc_net_tid = 0;

    int  ret = 0;
    RECT_S stCapRect;
    uint diffNum = 0;
    char buff[1024] = { 0 };
    struct sockaddr_nl src_addr;
    struct nlmsghdr *nlh = NULL;
    struct iovec iov;
    struct msghdr msg;
    struct msg_struct* ms;
    struct msg_struct diffmsg , snap_msg;

    //1.IT66021驱动
    sockFd = socket(PF_NETLINK, SOCK_RAW, NETLINK_IT66021FNBASE+vi);
    if (sockFd < 0) {
        printf("chris %d Error: %s\n",vi, strerror(errno));
        COMMON_PRT("create socket error\n");
        return NULL;
    }
    else {
        printf("it66021-->%d:fd->%d\n",vi,sockFd);
    }

    memset(&src_addr, 0, sizeof(struct sockaddr_nl));
    src_addr.nl_family = PF_NETLINK;
    src_addr.nl_pid = 0;
    src_addr.nl_groups = netlink_group_mask(GROUP_IT66021FNBASE+vi);

    ret = bind(sockFd, (struct sockaddr *)&src_addr, sizeof(struct sockaddr_nl));
    if (ret < 0) {
        close(sockFd);
        COMMON_PRT("bind socket failed\n");
        return NULL;
    }
    nlh = (nlmsghdr*)malloc(NLMSG_SPACE(MAX_PAYLOAD));
    if (!nlh) {
        close(sockFd);
        COMMON_PRT("failed\n");
        return NULL;
    }
    memset(nlh, 0, NLMSG_SPACE(MAX_PAYLOAD));
    iov.iov_base = (void *)nlh;
    iov.iov_len = NLMSG_SPACE(MAX_PAYLOAD);

    memset(&msg, 0, sizeof(struct msghdr));
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;


    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<Config>::getInstance();

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));
    int param[2] = { 1, enAiSampleRate };
    himppmaster->SetUserPic(vi);
    VPSS_GRP vpssGrp = 0;
    VPSS_CHN vpssChn = 0;
    VENC_CHN vencChn = 0;




    while(l_it66021fn_running) {
        // sem_wait(&sem1);

        recvmsg(sockFd, &msg, 0);
        memcpy(&buff, NLMSG_DATA(nlh), 1024);
        ms = (struct msg_struct*)buff;
        //  printf("it get vi %d w-->%d,h-->%d,freq--->%d\n",vi,ms->video_width,ms->video_height,ms->video_freq);

        if(startFlg) {               //输入第一次启动
            startFlg = false;
            memcpy(&diffmsg,ms,sizeof(struct msg_struct));
            memcpy(&snap_msg,ms,sizeof(struct msg_struct));
            if((ms->video_width == 0) && (ms->video_height == 0)) {    //输入端无信号,使能VI无信号，并保存参数
                COMMON_PRT("vi %d no signal\n",vi);
                vi_flag[vi] = 0;
                stCapRect = synViParam(ms->video_width,ms->video_height,ms->video_freq,vi,ms->video_interlaced);
                viSet[vi].viStRate = viParam[vi].u32SrcFrmRate;
                viSet[vi].viStSize.u32Width = stCapRect.u32Width;
                viSet[vi].viStSize.u32Height = stCapRect.u32Height;
                himppmaster->ViModuleInit(vi,SAMPLE_VI_MODE,stCapRect,viParam[vi].vi_model);

                while(1) {
                    if(himppmaster->EnableUserPic(vi))
                        break;
                    else
                        sleep(1);
                }
                ret = himppmaster->VencModuleInit(viParam[vi].stVencSize, viParam[vi].stMinorSize, viParam[vi].stMinorSize, viParam[vi].stSnapSize,viParam[vi].enType, \
                                                  viParam[vi].enRcMode, viParam[vi].u32Profile, viParam[vi].u32Gop, viParam[vi].u32BitRate, viParam[vi].u32SrcFrmRate, viParam[vi].u32DstFrameRate, \
                                                  viParam[vi].u32MinQp, viParam[vi].u32MaxQp, viParam[vi].u32MinIQp, viParam[vi].u32IQp, viParam[vi].u32PQp, viParam[vi].u32BQp,  \
                                                  viParam[vi].u32MinIProp, viParam[vi].u32MaxIProp,vi,HI_FALSE);
                himppmaster->VencSnapInit(vi+8,viParam[vi].stSnapSize);
                if(ret) {
                    ex_flag[vi] = true;
                }
                else {
                    ex_flag[vi] = false;
                }
            }
            else {
                //同步输入信息
                vi_flag[vi] = 1;
                stCapRect = synViParam(ms->video_width,ms->video_height,ms->video_freq,vi,ms->video_interlaced);
                viSet[vi].viStRate = viParam[vi].u32SrcFrmRate;
                viSet[vi].viStSize.u32Width = stCapRect.u32Width;
                viSet[vi].viStSize.u32Height = stCapRect.u32Height;
                himppmaster->ViModuleInit(vi,SAMPLE_VI_MODE, stCapRect, viParam[vi].vi_model);
                if(1 == ms->video_interlaced) {
                    himppmaster->EnableUserPic(vi);
                }
                ret = himppmaster->VencModuleInit(viParam[vi].stVencSize, viParam[vi].stMinorSize, viParam[vi].stMinorSize, viParam[vi].stSnapSize,viParam[vi].enType, \
                                                  viParam[vi].enRcMode, viParam[vi].u32Profile, viParam[vi].u32Gop, viParam[vi].u32BitRate, viParam[vi].u32SrcFrmRate, viParam[vi].u32DstFrameRate, \
                                                  viParam[vi].u32MinQp, viParam[vi].u32MaxQp, viParam[vi].u32MinIQp, viParam[vi].u32IQp, viParam[vi].u32PQp, viParam[vi].u32BQp,  \
                                                  viParam[vi].u32MinIProp, viParam[vi].u32MaxIProp,vi,HI_FALSE);
                himppmaster->VencSnapInit(vi+8,viParam[vi].stSnapSize);
                if(ret) {
                    ex_flag[vi] = true;
                }
                else {
                    ex_flag[vi] = false;
                }
            }
            vpssGrp = vi;
            vencChn = 2 * vi + 1;
            himppmaster->VpssModuleInit(vpssGrp,vpssChn);
            himppmaster->VencBindVpss(vencChn,vpssGrp);

            vpssGrp = VI_PORT_NUMBER + vi;
            vencChn = 8 + vi;
            himppmaster->VpssModuleInit(vpssGrp,vpssChn);
            himppmaster->VencBindVpss(vencChn,vpssGrp);
            startRtsp(vi,param);
            /* 开启对应的VI->VENC VENC->NET线程 */
            vi_src_chang[vi] = false;
            pth_vi_net[vi] = true;
            vi_venc_tid = CreateThread(pth_vi_venc, 0, SCHED_FIFO, b_detached, &vi);
            if(vi_venc_tid == 0){
                SAMPLE_PRT("pthread_create() failed: pth_vi_venc:%d \n",vi);
            }
            venc_net_tid = CreateThread(pth_venc_net, 0, SCHED_FIFO, b_detached, &vi);
            if(venc_net_tid == 0){
                SAMPLE_PRT("pthread_create() failed: pth_venc_net:%d \n",vi);
            }

            /* 开启对应的抓图发送线程 和JPEG接收线程 */
            pth_snap[vi] = true;
            snap_tid = CreateThread(pth_snap_net,0,SCHED_FIFO,b_detached,&vi);
            if(0 ==  snap_tid) {
                SAMPLE_PRT("pthread_create() failed: pth_snap_net:%d \n",vi);
            }
            pth_rcv_jpeg[vi] = true;
            rcv_jpeg_tid = CreateThread(pth_rcv_snap_jpeg,0,SCHED_FIFO,b_detached,&vi);
            if(0 ==  rcv_jpeg_tid) {
                SAMPLE_PRT("pthread_create() failed: pth_rcv_jpeg:%d \n",vi);
            }

            usleep(50000);
        }
        else {
            if((diffmsg.video_width != ms->video_width) || (diffmsg.video_height != ms->video_height) || (diffmsg.video_freq != ms->video_freq))
            {   //输入端VI分辨率更改
                COMMON_PRT("IT66021FN %d message:width:%d height:%d frameRate:%d scanMode:%d audioSampleRate:%d\n",vi,ms->video_width,ms->video_height, \
                           ms->video_freq,ms->video_interlaced,ms->audio_freq);
                COMMON_PRT("IT source %d message:width:%d height:%d frameRate:%d\n",vi,diffmsg.video_width,diffmsg.video_height, \
                           diffmsg.video_freq);
                if(diffNum < 1) {
                    diffNum++;
                    continue;
                }
                else {
                    memcpy(&diffmsg,ms,sizeof(struct msg_struct));
                    diffNum = 0;
                    for(int n  = vi*2; n < (vi+1)*2; n++)
                    {
                        crtsps_closestream(g_stream[n]);
                    }
                    vi_src_chang[vi] = true;           //暂停VI->venc venc->net snap
                    usleep(10000);
                    if((viParam[vi].u32ViWidth == 0) && (viParam[vi].u32ViHeigth == 0)) {   //无信号--》有信号
                        vi_flag[vi] = 1;
                        stCapRect =  synViParam(ms->video_width,ms->video_height,ms->video_freq,vi,ms->video_interlaced);
                        COMMON_PRT("new get VI  %d message:width:%d height:%d frameRate:%d\n",vi,viParam[vi].u32ViWidth,viParam[vi].u32ViHeigth, \
                                   viParam[vi].u32SrcFrmRate);
                        viSet[vi].viStSize.u32Width = stCapRect.u32Width;
                        viSet[vi].viStSize.u32Height = stCapRect.u32Height;
                        viSet[vi].viStRate = viParam[vi].u32SrcFrmRate;

                        himppmaster->retartVIVPSSVENC(vi,HI_TRUE,HI_TRUE);
                        if(ret) {
                            ex_flag[vi] = true;
                        }
                        else {
                            ex_flag[vi] = false;
                        }
                        startRtsp(vi,param);
                        usleep(5000);
                    }
                    else if((ms->video_width == 0) && (ms->video_height == 0)) {   //有信号--》无信号
                        vi_flag[vi] = 0;
                        stCapRect = synViParam(ms->video_width,ms->video_height,ms->video_freq,vi,ms->video_interlaced);
                        COMMON_PRT("new get VI %d message:width:%d height:%d frameRate:%d\n",vi,viParam[vi].u32ViWidth,viParam[vi].u32ViHeigth, \
                                   viParam[vi].u32SrcFrmRate);
                        viSet[vi].viStRate = viParam[vi].u32SrcFrmRate;
                        viSet[vi].viStSize.u32Width = stCapRect.u32Width;
                        viSet[vi].viStSize.u32Height = stCapRect.u32Height;
                        himppmaster->retartVIVPSSVENC(vi,HI_FALSE,HI_TRUE);
                        if(ret) {
                            ex_flag[vi] = true;
                        }
                        else {
                            ex_flag[vi] = false;
                        }
                        startRtsp(vi,param);
                        usleep(5000);
                    }
                    else {
                        vi_flag[vi] = 1;

                        stCapRect = synViParam(ms->video_width,ms->video_height,ms->video_freq,vi,ms->video_interlaced);
                        COMMON_PRT("new get VI %d message:width:%d height:%d frameRate:%d\n",vi,viParam[vi].u32ViWidth,viParam[vi].u32ViHeigth, \
                                   viParam[vi].u32SrcFrmRate);
                        viSet[vi].viStRate = viParam[vi].u32SrcFrmRate;
                        viSet[vi].viStSize.u32Width = stCapRect.u32Width;
                        viSet[vi].viStSize.u32Height = stCapRect.u32Height;
                        himppmaster->retartVIVPSSVENC(vi,HI_TRUE,HI_TRUE);

                        if(ret) {
                            ex_flag[vi] = true;
                        }
                        else {
                            ex_flag[vi] = false;
                        }
                        startRtsp(vi,param);
                    }

                    vi_src_chang[vi] = false;
                }
            }
        }
    }

    printf("check it66021 thread %d quit error\n",vi);

    return (void*)0;
}

void* timeCheckIt6801(void* arg)
{
    int skfd;
    int ret;
    user_msg_info u_info;
    socklen_t len;
    struct nlmsghdr *nlh = NULL;
    struct sockaddr_nl saddr, daddr;

    VI_INFO viSrc[VI_PORT_NUMBER];
    VI_INFO itInfo;
    RECT_S stCapRect;
    VPSS_GRP vpssGrp = 0;
    VPSS_CHN vpssChn = 0;
    VENC_CHN vencChn = 0;
    pthread_t snap_tid[VI_PORT_NUMBER];
    pthread_t rcv_jpeg_tid[VI_PORT_NUMBER];
    pthread_t vi_venc_tid[VI_PORT_NUMBER];
    pthread_t venc_net_tid[VI_PORT_NUMBER];
    bool  b_detached = true;

    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<Config>::getInstance();

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));
    int param[2] = { 1, enAiSampleRate };

    for(uint i = 0; i < g_vi_en_number; i++) {
        himppmaster->SetUserPic(i);
    }

    bool viStart[VI_PORT_NUMBER] = {false,false,false,false};


    skfd = socket(AF_NETLINK, SOCK_RAW, NETLINK_VIDEO_PROTOCOL);
    if (skfd == -1) {
        perror("create socket error\n");
    }

    memset(&saddr, 0, sizeof(saddr));
    saddr.nl_family = AF_NETLINK; //AF_NETLINK
    saddr.nl_pid = USER_PORT;
    saddr.nl_groups = 0;
    if (bind(skfd, (struct sockaddr *)&saddr, sizeof(saddr)) != 0) {
        perror("bind() error\n");
        close(skfd);
    }

    memset(&daddr, 0, sizeof(daddr));
    daddr.nl_family = AF_NETLINK;
    daddr.nl_pid = 0;
    daddr.nl_groups = 0;

    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_PLOAD));
    memset(nlh, 0, sizeof(struct nlmsghdr));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_PLOAD);
    nlh->nlmsg_flags = 0;
    nlh->nlmsg_type = 0;
    nlh->nlmsg_seq = 0;
    nlh->nlmsg_pid = saddr.nl_pid;
    int vi = 0;
    int diffNum[VI_PORT_NUMBER] = {0,0,0,0};
    while(pth_it6801_running )
    {
        memset(&u_info, 0, sizeof(u_info));
        len = sizeof(struct sockaddr_nl);
        ret = recvfrom(skfd, &u_info, sizeof(user_msg_info), 0, (struct sockaddr *)&daddr, &len);
        if (!ret) {
            perror("recv form kernel error\n");
            close(skfd);
            exit(-1);
        }
        if(g_vi_en_number <= u_info.vf.video_ch)
            continue;

        itInfo.viPort = u_info.vf.video_ch;
        itInfo.viSize.u32Width = u_info.vf.video_width;
        itInfo.viSize.u32Height = u_info.vf.video_height;
        itInfo.srcFrameRate = u_info.vf.video_freq;
        if(itInfo.viPort >= g_vi_en_number)         //4kedid，则使能0 1VI； 1080P eidi 则使能0 1 2VI
            continue;
        if(!viStart[itInfo.viPort]) {       //VI第一次使能
            printf("ch:%d w:%d h:%d fps:%d scan:%d\n",u_info.vf.video_ch,u_info.vf.video_width,u_info.vf.video_height,u_info.vf.video_freq);
            viStart[itInfo.viPort] = true;
            memset(&viSrc[itInfo.viPort],0,sizeof(VI_INFO));
            memcpy(&viSrc[itInfo.viPort],&itInfo,sizeof(VI_INFO));
            stCapRect = synViParam(itInfo.viSize.u32Width,itInfo.viSize.u32Height,itInfo.srcFrameRate,itInfo.viPort,0);
            viSet[itInfo.viPort].viStRate = viParam[itInfo.viPort].u32SrcFrmRate;
            viSet[itInfo.viPort].viStSize.u32Width = stCapRect.u32Width;
            viSet[itInfo.viPort].viStSize.u32Height = stCapRect.u32Height;
            himppmaster->ViModuleInit(itInfo.viPort,SAMPLE_VI_MODE,stCapRect,viParam[itInfo.viPort].vi_model);

            if((0 == itInfo.viSize.u32Width) || (0 == itInfo.viSize.u32Height) ) {
                vi_flag[itInfo.viPort] = 0;
                while(1) {
                    if(himppmaster->EnableUserPic(itInfo.viPort))
                        break;
                    else
                        sleep(1);
                }
            }
            else {
                vi_flag[itInfo.viPort] = 1;
            }
            vi = itInfo.viPort;

            himppmaster->VencModuleInit(viParam[vi].stVencSize, viParam[vi].stMinorSize, viParam[vi].stMinorSize, viParam[vi].stSnapSize,viParam[vi].enType, \
                                              viParam[vi].enRcMode, viParam[vi].u32Profile, viParam[vi].u32Gop, viParam[vi].u32BitRate, viParam[vi].u32SrcFrmRate, viParam[vi].u32DstFrameRate, \
                                              viParam[vi].u32MinQp, viParam[vi].u32MaxQp, viParam[vi].u32MinIQp, viParam[vi].u32IQp, viParam[vi].u32PQp, viParam[vi].u32BQp,  \
                                              viParam[vi].u32MinIProp, viParam[vi].u32MaxIProp,vi,HI_FALSE);
            himppmaster->VencSnapInit(vi+8,viParam[vi].stSnapSize);

            vpssGrp = vi;
            vencChn = 2 * vi + 1;
            himppmaster->VpssModuleInit(vpssGrp,vpssChn);
            himppmaster->VencBindVpss(vencChn,vpssGrp);

            vpssGrp = VI_PORT_NUMBER + vi;
            vencChn = 8 + vi;
            himppmaster->VpssModuleInit(vpssGrp,vpssChn);
            himppmaster->VencBindVpss(vencChn,vpssGrp);
            startRtsp(vi,param);
            /* 开启对应的VI->VENC VENC->NET线程 */
            vi_src_chang[vi] = false;
            pth_vi_net[vi] = true;
            vi_venc_tid[vi] = CreateThread(pth_vi_venc, 0, SCHED_FIFO, b_detached, &vi);
            if(vi_venc_tid[vi] == 0){
                SAMPLE_PRT("pthread_create() failed: pth_vi_venc:%d \n",vi);
            }
            venc_net_tid[vi] = CreateThread(pth_venc_net, 0, SCHED_FIFO, b_detached, &vi);
            if(venc_net_tid[vi] == 0){
                SAMPLE_PRT("pthread_create() failed: pth_venc_net:%d \n",vi);
            }

            /* 开启对应的抓图发送线程 和JPEG接收线程 */
            pth_snap[vi] = true;
            snap_tid[vi] = CreateThread(pth_snap_net,0,SCHED_FIFO,b_detached,&vi);
            if(0 ==  snap_tid[vi]) {
                SAMPLE_PRT("pthread_create() failed: pth_snap_net:%d \n",vi);
            }
            pth_rcv_jpeg[vi] = true;
            rcv_jpeg_tid[vi] = CreateThread(pth_rcv_snap_jpeg,0,SCHED_FIFO,b_detached,&vi);
            if(0 ==  rcv_jpeg_tid[vi]) {
                SAMPLE_PRT("pthread_create() failed: pth_rcv_jpeg:%d \n",vi);
            }
            usleep(20000);
        }
        else {
            if((itInfo.viSize.u32Width != viSrc[itInfo.viPort].viSize.u32Width) || (itInfo.viSize.u32Height != viSrc[itInfo.viPort].viSize.u32Height) ||(itInfo.srcFrameRate != viSrc[itInfo.viPort].srcFrameRate)) {
                diffNum[itInfo.viPort]++;
                if(diffNum[itInfo.viPort] > 1) {
                    printf("ch:%d w:%d h:%d fps:%d scan:%d\n",u_info.vf.video_ch,u_info.vf.video_width,u_info.vf.video_height,u_info.vf.video_freq);
                    diffNum[itInfo.viPort] = 0;
                    memset(&viSrc[itInfo.viPort],0,sizeof(VI_INFO));
                    memcpy(&viSrc[itInfo.viPort],&itInfo,sizeof(VI_INFO));
                    vi = itInfo.viPort;
                    stCapRect = synViParam(itInfo.viSize.u32Width,itInfo.viSize.u32Height,itInfo.srcFrameRate,itInfo.viPort,0);

                    for(int n  = vi*2; n < (vi+1)*2; n++)
                    {
                        crtsps_closestream(g_stream[n]);
                    }
                    vi_src_chang[vi] = true;           //暂停VI->venc venc->net snap
                    usleep(10000);
                    if((0 == itInfo.viSize.u32Width) || (0 == itInfo.viSize.u32Height) ) {
                        vi_flag[itInfo.viPort] = 0;
                        himppmaster->retartVIVPSSVENC(vi,HI_FALSE,HI_TRUE);
                    }
                    else {
                        vi_flag[itInfo.viPort] = 1;
                        himppmaster->retartVIVPSSVENC(vi,HI_TRUE,HI_TRUE);
                    }

                    startRtsp(vi,param);
                    vi_src_chang[vi] = false;
                    usleep(50000);
                }
                else {
                    usleep(50000);
                }
            }
        }

    }
    close(skfd);

    free((void *)nlh);
     printf("timeCheckIt6801 thread quit error\n");
    return (void*)0;
}

//检测SDI

void* timeCheckGs2971a(void *vi_)
{

    int vi = *(int*)vi_;
    int len  = 0;
    char data[MAXDATA] = {0};
    struct msg_struct viInfo,tmpInfo;
    struct msg_struct *ms =NULL;
    memset(&viInfo,0,sizeof(struct msg_struct));
    memset(&tmpInfo,0,sizeof(struct msg_struct));
    bool start = false;
    RECT_S stCapRect;
    int ret = 0;
    pthread_t snap_tid = 0;
    pthread_t rcv_jpeg_tid = 0;
    pthread_t vi_venc_tid = 0;
    pthread_t venc_net_tid = 0;
    bool b_detached = true;

    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<Config>::getInstance();

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));
    int param[2] = { 1, enAiSampleRate };
    UDPSocket *sdiSocket = new UDPSocket();
    sdiSocket->CreateUDPServer(SDISOCKTPORT,false);     //阻塞

    himppmaster->SetUserPic(vi);

    while(l_gs2971a_running) {

        memset(data,0,MAXDATA);
        len = sdiSocket->RecvFrom(data,MAXDATA);

        if(len > 0) {

            ms = (struct msg_struct*)data;
            memcpy(&tmpInfo,ms,sizeof(struct msg_struct));
            //    printf("gs2971a get new vi info:w =%d h=%d frameRate=%d,interlaced=%d\n",tmpInfo.video_width,tmpInfo.video_height,  \
            //         tmpInfo.video_freq,tmpInfo.video_interlaced);

            if((tmpInfo.video_width != viInfo.video_width) || (tmpInfo.video_height != viInfo.video_height) ||  \
               (tmpInfo.video_freq != viInfo.video_freq) || (tmpInfo.video_interlaced != viInfo.video_interlaced) || !start) {
                printf("gs2971a get new vi info:w =%d h=%d frameRate=%d,interlaced=%d\n",tmpInfo.video_width,tmpInfo.video_height,  \
                       tmpInfo.video_freq,tmpInfo.video_interlaced);
                if((tmpInfo.video_width == 0) || (tmpInfo.video_height == 0)) {
                    vi_flag[vi] = false;
                }
                else {
                    vi_flag[vi] = true;
                }
                viInfo = tmpInfo;

                stCapRect = synViParam(tmpInfo.video_width,tmpInfo.video_height,tmpInfo.video_freq,vi,tmpInfo.video_interlaced);
                COMMON_PRT("NEW VI        %d message:width:%d height:%d frameRate:%d\n",vi,viParam[vi].u32ViWidth,viParam[vi].u32ViHeigth, \
                           viParam[vi].u32SrcFrmRate);
                viSet[vi].viStRate = viParam[vi].u32SrcFrmRate;
                viSet[vi].viStSize.u32Width = stCapRect.u32Width;
                viSet[vi].viStSize.u32Height = stCapRect.u32Height;
                if(start) {
                    if(vi_flag[vi] == false) {
                        himppmaster->retartVIVPSSVENC(vi,HI_FALSE,HI_TRUE);
                    }
                    else {
                        himppmaster->retartVIVPSSVENC(vi,HI_TRUE,HI_TRUE);
                    }

                }


                if(!start) {
                    himppmaster->ViModuleInit(vi,SAMPLE_VI_MODE,stCapRect,viParam[vi].vi_model);
                    if(vi_flag[vi] == false) {

                        while(1) {
                            if(himppmaster->EnableUserPic(vi))
                                break;
                            else
                                sleep(1);
                        }
                    }

                    ret = himppmaster->VencModuleInit(viParam[vi].stVencSize, viParam[vi].stMinorSize, viParam[vi].stMinorSize, viParam[vi].stSnapSize,viParam[vi].enType, \
                                                      viParam[vi].enRcMode, viParam[vi].u32Profile, viParam[vi].u32Gop, viParam[vi].u32BitRate, viParam[vi].u32SrcFrmRate, viParam[vi].u32DstFrameRate, \
                                                      viParam[vi].u32MinQp, viParam[vi].u32MaxQp, viParam[vi].u32MinIQp, viParam[vi].u32IQp, viParam[vi].u32PQp, viParam[vi].u32BQp,  \
                                                      viParam[vi].u32MinIProp, viParam[vi].u32MaxIProp,vi,HI_FALSE);
                    /* 开启对应的VI->VENC VENC->NET线程 */
                    pth_vi_net[vi] = true;
                    vi_src_chang[vi] = false;
                    vi_venc_tid = CreateThread(pth_vi_venc, 0, SCHED_FIFO, b_detached, &vi);
                    if(vi_venc_tid == 0){
                        SAMPLE_PRT("pthread_create() failed: pth_vi_venc:%d \n",vi);
                    }
                    venc_net_tid = CreateThread(pth_venc_net, 0, SCHED_FIFO, b_detached, &vi);
                    if(venc_net_tid == 0){
                        SAMPLE_PRT("pthread_create() failed: pth_venc_net:%d \n",vi);
                    }

                    /* 开启对应的抓图发送线程 和JPEG接收线程 */
                    pth_snap[vi] = true;
                    snap_tid = CreateThread(pth_snap_net,0,SCHED_FIFO,b_detached,&vi);
                    if(0 ==  snap_tid) {
                        SAMPLE_PRT("pthread_create() failed: pth_snap_net:%d \n",vi);
                    }
                    pth_rcv_jpeg[vi] = true;
                    rcv_jpeg_tid = CreateThread(pth_rcv_snap_jpeg,0,SCHED_FIFO,b_detached,&vi);
                    if(0 ==  rcv_jpeg_tid) {
                        SAMPLE_PRT("pthread_create() failed: pth_rcv_jpeg:%d \n",vi);
                    }

                }
                start = true;

                if(ret) {
                    ex_flag[vi] = true;
                }
                else {
                    ex_flag[vi] = false;
                }

                startRtsp(vi,param);
                vi_src_chang[vi] = false;
            }
        }
        else {
            printf("gs2971a get error infomation\n");
            continue;
        }
    }
}


uint GetSystemInfo(char *buf_send, bool isOldFormat = true)
{
    char localip[20] = "";
    char macaddr[20] = "";
    uint delay = 0;
    // 读取版本号
    string ver = ReadSthFile("/version/version");

    // 读取uuid
    string uuid = ReadSthFile("/uuid/uuid");

    // 读取node name
    string name = ReadSthFile("/data/in_node_name");

    // 获取本地ip地址
    NetworkInfo *netinfo = new NetworkInfo();
    if( !netinfo->GetNetworkInfo(ETH0, NetworkInfo::kADDR, localip) ){
        COMMON_PRT("Get ip failed!\n");
        sprintf(localip, "%s", "get_ip_error");
    }
    if( !netinfo->GetNetworkInfo(ETH0, NetworkInfo::kHWADDR, macaddr) ){
        COMMON_PRT("Get mac failed!");
        sprintf(macaddr, "%s", "get_mac_error");
    }
    string timer( localip );
    // 取得ip段的最后一位
    timer = timer.c_str() + timer.find(".", timer.find(".", timer.find(".") + 1) + 1) + strlen(".");
    // 根据ip地址计算回复延迟
    delay = atoi(timer.c_str()) * 1000; //COMMON_PRT("delay = %d us\n", delay);
    usleep(delay);

    if(isOldFormat)
        sprintf(buf_send, "BKIP:%s,%s", localip, ver.c_str());
    else{
        sprintf(buf_send, "{\n\"node_model\":\"%s\",\n\"node_ip\":\"%s\",\n\"node_mac\":\"%s\",\n\"node_name\":\"%s\",\n\"version\":%s,\n\"uuid\":\"%s\"\n}", NODE_MODEL, localip, macaddr, name.c_str(), ver.c_str(), uuid.c_str());
    }

    return delay;
}

/*
 * 功能： 启动广播监听，并回复ip和版本号，扫描和回复格式兼容hi3536输出板子的扫描。
 * 回复 BKIP:xx.xx.xx.xx,<A>ver:x.x.x.x,rtsp://ip/0
 * 输入： void *arg：指定监听端口
 * 返回： 无
 */

void *udpBroadCastProc(void *arg)
{
    int  len = 0;
    uint port = *(uint*)arg;
    char buf[MAXDATA] = "";
    COMMON_PRT("starting broadcast listened at port: %d\n", port);

    UDPSocket *broadsocket = new UDPSocket();
    broadsocket->CreateUDPServer(port, false); //设为阻塞
    printf("broadcaset mcsocket creage ok\n");
#if 0
    broadsocket->AddBroadCast(ETH0); //加入eth0网卡的广播地址
#else
    broadsocket->AddBroadCast(); //使用255.255.255.255广播地址
    broadsocket->BindLocalAddr(ETH0);
#endif

    while(1){
        memset(buf, '\0', sizeof(buf));
        if( (len = broadsocket->RecvFrom(buf, MAXDATA)) > 0){
            buf[len] = '\0'; COMMON_PRT("recv data: %s\n", buf);

            char buf_send[128] = "";
            uint delay = 0;
            vector<string> cmdlist;

            SplitString(buf, cmdlist, ":");
            if(cmdlist[0] == "GTIP"){
                delay = GetSystemInfo(buf_send);
            }
            else if(cmdlist[0] == "FIND_NODE_UNI" ||
                    cmdlist[0] == "FIND_NODE_BOARD"){ // JSON格式回复
                delay = GetSystemInfo(buf_send, false);
            }
            else{
                COMMON_PRT("Scan instruction [%s] error.", cmdlist[0].c_str());
                continue;
            }

            if(delay == 0){
                sprintf(buf_send, "%s", "[ERROR_CODE_00]");
                COMMON_PRT("get system info failed!");
            }
            else{
                usleep(delay);
            }
            COMMON_PRT("send data: %s", buf_send);

            struct sockaddr_in clientaddr = broadsocket->GetClientSockaddr();
            if(cmdlist.size() == 1){ // NOTE: UDP回声回复
                for (uint i = 0; i < 3; i++){
                    if(cmdlist[0] == "GTIP" ||
                       cmdlist[0] == "FIND_NODE_UNI"){
                        broadsocket->SendTo(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr),
                                            ntohs(((struct sockaddr_in*)&clientaddr)->sin_port),
                                            buf_send,
                                            strlen(buf_send));
                    }
                    else if(cmdlist[0] == "FIND_NODE_BOARD"){
                        broadsocket->SendTo("255.255.255.255",
                                            ntohs(((struct sockaddr_in*)&clientaddr)->sin_port),
                                            buf_send,
                                            strlen(buf_send));
                    }
                }
            }
            else if(cmdlist.size() == 2){ //NOTE: 回复给指定端口
                for (uint i = 0; i < 3; i++){
                    if(cmdlist[0] == "GTIP" ||
                       cmdlist[0] == "FIND_NODE_UNI"){
                        broadsocket->SendTo(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr),
                                            atoi(cmdlist[1].c_str()),
                                buf_send,
                                strlen(buf_send));
                    }
                    else if(cmdlist[0] == "FIND_NODE_BOARD"){
                        broadsocket->SendTo("255.255.255.255",
                                            atoi(cmdlist[1].c_str()),
                                buf_send,
                                strlen(buf_send));
                    }

                    usleep(delay/10);
                }
            }
            else{
                COMMON_PRT("The number of scan instruction [%d] error.", cmdlist.size());
                continue;
            }
        }
    }

    delete broadsocket;

    return (void*)1;
}

void* udpServerProc(void *arg)
{
    int  len = 0;
    uint port = *(uint*)arg;
    char data[MAXDATA]  = "";
    COMMON_PRT("starting udp server at port: %d\n", port);

    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(port, false);
    printf("udpsv mcsocket creage ok\n");

    while( 1 ){
        memset(data, '\0', sizeof(data));
        if( (len = udpsocket->RecvFrom(data, MAXDATA)) > 0){
            COMMON_PRT("recv udp data: %s\n", data);
            struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
            sprintf(gClientIP, "%s", inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
            gClientPort = ntohs(((struct sockaddr_in*)&clientaddr)->sin_port);

            if(!strcmp(data, "RSET:")){ //重启指令
                udpsocket->EchoTo("(OK)", strlen("(OK)"));
                system("reboot\n");
                break;
            }
            else
            {
                JsonParse* jsonparse = new JsonParse(data);
                if(jsonparse->CheckJsonFormat()){
                    jsonparse->IterationParseStart();
                }
                else{
                    printf("Illegal format of JSON\n");
                }
            }
        }
        else
            break;
    }

    delete udpsocket;
    return (void*)0;
}

void *httpServerhandler(void*)
{
    // start http server
    auto thisini = Singleton<Config>::getInstance();
    auto http_server = Singleton<HttpServer>::getInstance();
    http_server->Init( thisini->GetConfig(Config::kWebPort) );
    // add http handler
    http_server->AddHandler("/reboot",       ResetHandler);
    http_server->AddHandler("/up_progress",  UpdataHandler);
    http_server->AddHandler("/transmit",     TransmitHandler);
    http_server->AddHandler("/set_osd",      SetosdHandler);
    http_server->AddHandler("/set_sys",      SetnetHandler);
    http_server->AddHandler("/set_adv",      SetadvHandler);
    http_server->AddHandler("/set_output",   SetpreviewHandler);

    //http_server->AddHandler("/get_status",       GetperplanHandler);
    http_server->AddHandler("/get_sys",      GetnetsettingHandler);
    //http_server->AddHandler("/get_input",  GetaudiosettingHandler);
    http_server->Start();

    return (void*)1;
}



void* pth_snap_net(void * vi_port)
{
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    UDPSocket *udpsocket = new UDPSocket();
    int socket = udpsocket->CreateUDPClient(nullptr,0, false); //设为阻塞
    udpsocket->BindLocalAddr(ETH0);
    setsockopt(socket, IPPROTO_IP, IP_MULTICAST_TTL, (char *)&ttl, sizeof(ttl));

    HI_U32 snap_venc_port = 8 + *(uint*)vi_port;
    himppmaster->GetVencRateSnap(snap_venc_port, udpsocket);

    printf("======thread :pth_snap %d end ======\n",vi_port);

    return (void*)0;
}

void* pth_rcv_snap_jpeg(void* arg)
{
    int  vi_port = *(int*)arg;
    int  len  = 0;
    char data[100] = "";
    vector<string> content;

    UDPSocket *udpsocket = new UDPSocket();
    udpsocket->CreateUDPServer(viParam[vi_port].snapPort, false); //设为阻塞
    printf("[%d]snap port:%d\n",vi_port,viParam[vi_port].snapPort);

    while(pth_rcv_jpeg[vi_port]){
        memset(data, '\0', sizeof(data));
        content.clear();
        if( (len = udpsocket->RecvFrom(data, 100)) > 0){
            if (strstr(data, "JPEG:") ){
                if(HI_TRUE == viParam[vi_port].Snapmcast_enable) {          //组播抓图使能时 JPEG单播抓图过滤
                    continue;
                }

                if( strlen(data) == strlen("JPEG:") ){
                    struct sockaddr_in clientaddr = udpsocket->GetClientSockaddr();
                    content.push_back(inet_ntoa(((struct sockaddr_in*)&clientaddr)->sin_addr));
                    content.push_back(int2str(ntohs(((struct sockaddr_in*)&clientaddr)->sin_port)));
                }
                else
                    SplitString(data + 5, content, ",");

                memset(snap_jpeg_ip[vi_port],0,IPSIZE);
                strcpy(snap_jpeg_ip[vi_port],content[0].c_str());
                snap_jpeg_port[vi_port] =  (uint)str2int(content[1]);
                jpeg_flag[vi_port] = true;
//                COMMON_PRT("ip: %s, port: %d\n", snap_jpeg_ip, snap_jpeg_port);
            }
        }
    }

    delete udpsocket;
    printf("======thread :pth_rcv_snap_jpeg %d end ======\n",vi_port);
    return 0;

}

bool SynStateDetect(bool sync_ok, const HI_U32 sync_offline, const HI_S32 clock_swing_sum)
{
    /** NOTE: 根读取VCXO上一秒的设定值的算术和，判断是否锁定
     *正常情况下，VXCO只有-512，-20，20，511四个值，每秒统计60次
     **/
    //正负摆动 < 10
    if( (sync_offline < 1200) &&
        (clock_swing_sum > (-20 * 10) && clock_swing_sum < (20 * 10)) &&
        !sync_ok){
        // NOTE: 新建同步成功的文件,该文件是提供给netserver获取节点同步状态用的
        FILE* sync_fd = NULL;
        sync_fd = fopen("/tmp/sync", "w");
        fprintf(sync_fd, "%d", 1);
        fflush(sync_fd);
        fclose(sync_fd);

        sync_ok = true;
    }
    // 正负一半以上的最大摆幅
    else if( (sync_offline >= 1200) ||
             ((clock_swing_sum < (-512 * 30) || clock_swing_sum > (511 * 30)) &&
              sync_ok)){
        // NOTE: 新建同步成功的文件,该文件是提供给netserver获取节点同步状态用的
        FILE* sync_fd = NULL;
        sync_fd = fopen("/tmp/sync", "w");
        fprintf(sync_fd, "%d", 0);
        fflush(sync_fd);
        fclose(sync_fd);

        sync_ok = false;
    }
    else if (access("/tmp/sync", F_OK) == -1){ // 如果指定的文件不存在
        sync_ok = false;
    }

    return sync_ok;
}




HI_VOID* GetTimeTick(HI_VOID *arg){
    uint sync_offline = 0;
    int last_time_offset = 0;
    system("rm /tmp/sync -rf"); // NOTE: 删除同步成功的文件,该文件是提供给netserver获取节点同步状态用的
    sleep(1);

    HI_U32 m1 = 1, m2 = 1;
    HI_S32 last_clock_swing_sum = 0;
    bool master_slave = false;
    HI_S32 tick_delta = 0;


    //打开中断驱动通信文件
    char irqfile[24] = "";
    sprintf(irqfile, "/dev/interrupt%d", 2);
    KoData kodata;

    HI_S32 irqfd = open(irqfile, 0); //阻塞读取； NOTE: 非阻塞 O_RDONLY|O_NONBLOCK
    if (irqfd < 0){
        irqfd = -1;
        printf("open %s error: %s.", irqfile, strerror(errno));
    }
    //共享内存的创建
    int shmid = -1;//接收句柄
    shmid = shmget(IPC_KEY, SHARE_MAX, IPC_CREAT | 0664);
    if(shmid < 0){
        perror("shmget errnr");
    }
    void *shm_start = shmat(shmid,NULL,0 );
    if(shm_start == (void *) -1){
        perror("shmat error");
    }
    TransMit msg = {50000, 0, 1, 1};
    memcpy(shm_start, &msg, sizeof(msg));

    int tmpId = 0;


    while( 1 ){

        //Step 2
        //阻塞VS的中断，这个不容易丢掉
        if(irqfd >= 0)
            read(irqfd, &kodata, sizeof(kodata));

        //Step 3
        //看是否锁定，当锁定后，才能进行比对工作，锁定前都是假象
        if(!master_slave){
            //*** 最核心的操作，内存复制 ***
            memcpy(&msg, shm_start, sizeof(msg));
            //            printf("%d | %d | %d | %d\n", msg.clock_swing_sum, msg.tick_delta, msg.m1, msg.m2);

            int clock_swing_sum = msg.clock_swing_sum;
            // NOTE: 判断同步组播信号丢失
            if(clock_swing_sum == last_clock_swing_sum){
                if(sync_offline < 1200)
                    ++sync_offline;
            }
            else{
                sync_offline = 0;
                last_clock_swing_sum = clock_swing_sum;
            }
            // 检测是否同步的状态
            sync_ok = SynStateDetect(sync_ok, sync_offline, clock_swing_sum);

            if(sync_ok){
                tick_delta = msg.tick_delta;
                if(msg.m1)
                    m1 = msg.m1;
                if(msg.m2)
                    m2 = msg.m2;
            }

        }
        //Step 7: 本板全局时间戳 = 本板时间戳（驱动汇报） + 时间差（TickMaster汇报）
        glFrameID = (((int)(kodata.frame_num / m1) + tick_delta + 60) * m2) % 60;
        if(glFrameID == tmpId) {
            printf("get same Id:%d\n",glFrameID);
        }
        tmpId = glFrameID;


//        printf("%d = %d + %d + %d\n",glFrameID, (int)kodata.frame_num, glLocalTickDelta, diff_index);
        Hi_SetReg(0x130d0710, glFrameID);//将FrameID写到寄存器暂存，interrupt_vi.ko中读
    }

    //delete udpsocket;

    if(irqfd >= 0)
        close(irqfd);
    shmdt(shm_start);

    return (HI_VOID*)1;
}


void* pth_vi_venc(void *arg)
{
    uint vi_port = *(uint*)arg;
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    himppmaster->viGetToVenc(vi_port);

    printf("======thread :pth_vi_venc %d end ======\n",vi_port);
    return (void*)0;
}

void* pth_venc_net(void *arg)
{
    uint vi_port = *(uint*)arg;
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    himppmaster->StartLoop(2*vi_port, 2*(vi_port+1), vi_port);

    printf("======thread :pth_venc_net %d end ======\n",vi_port);
    return (void*)0;
}



/*
 *
 *
 *1、初始化输入数量
 *2、根据/data/venc.ini配置文件，初始化viParam
 *
 * */

void Audio_reg_init()
{
    HI_U32 reg_value;
    //HDMI 0
    Hi_GetReg(GPIO0_7_ADR,&reg_value);
    reg_value |= GPIO0_7_SET;
    AI_value_HD[0] = reg_value;

    Hi_GetReg(GPIO0_7_ADR,&reg_value);//3.5mm
    reg_value &= (~GPIO0_7_SET);
    AI_value_35[0] = reg_value;

    //HDMI 1

    Hi_GetReg(GPIO0_2_ADR,&reg_value);
    reg_value |= GPIO0_2_SET;
    AI_value_HD[1] = reg_value;


    Hi_GetReg(GPIO0_7_ADR,&reg_value);//3.5mm
    reg_value &= (~GPIO0_7_SET);

    AI_value_35[1] = reg_value;

    //HDMI 2
    Hi_GetReg(GPIO0_7_ADR,&reg_value);
    reg_value &= (~GPIO0_7_SET);

    AI_value_35[2] = reg_value;


    Hi_GetReg(GPIO0_2_ADR,&reg_value);
    reg_value &= (~GPIO0_2_SET);

    AI_value_HD[2] = reg_value;

    //HDMI 3
    Hi_GetReg(GPIO0_7_ADR,&reg_value);
    reg_value &= (~GPIO0_7_SET);

    AI_value_35[3] = reg_value;

}


bool InitParamte()
{
    auto thisini = Singleton<Config>::getInstance();
    char tickModule[32] = {0};
    memset(tickModule,0,sizeof(tickModule));
    sprintf(tickModule,"%s",string(thisini->GetConfig(Config::kTickModule)).c_str());
    if(!strncmp(tickModule,"master",strlen("master"))) {
        gb_master = true;
    }
    else {
        gb_master = false;
    }
    HI_U32 i = 0;
    viNumber = 4;
    kerrFrameNum = str2int(thisini->GetConfig(Config::KErrFrameNum));

    for(i = 0; i < VI_PORT_NUMBER; i++) {
        memset(&viParam[i],0,sizeof(PARAMETER));
        switch (i) {
            case 0:
                viParam[0].enRcMode[0] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn0_M_RcMode));
                viParam[0].enRcMode[1] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn0_S0_RcMode));
                viParam[0].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn0_M_VENC_Type));
                viParam[0].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn0_S0_VENC_Type));
                viParam[0].enViMode = SAMPLE_VI_MODE;
                viParam[0].input_type = 1;
                viParam[0].sw_color_expand = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_SW_Color));
                viParam[0].mcast_enable[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_M_McastEnable));
                viParam[0].mcast_enable[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_S0_McastEnable));
                if(viParam[0].mcast_enable[0]) {
                    sprintf(viParam[0].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn0_M_McastIp)).c_str());
                    viParam[0].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn0_M_McastPort).c_str());
                }
                if(viParam[0].mcast_enable[1]) {
                    sprintf(viParam[0].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn0_S0_McastIp)).c_str());
                    viParam[0].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn0_S0_McastPort).c_str());
                }
                viParam[0].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_Osd_Enable));
                viParam[0].SnapQuality = str2int(thisini->GetConfig(Config::kChn0_Snap_Quality));
                viParam[0].snapPort = str2int( thisini->GetConfig(Config::kChn0_SnapPort).c_str());
                viParam[0].SnapLen = str2int(thisini->GetConfig(Config::kChn0_SnapLen).c_str());
                viParam[0].SnapDelay = str2int(thisini->GetConfig(Config::kChn0_Snap_Delay).c_str());
                viParam[0].stMinorSize = {str2int(thisini->GetConfig(Config::kChn0_S0_VENC_W)),
                                          str2int(thisini->GetConfig(Config::kChn0_S0_VENC_H))};
                viParam[0].stSnapSize = {str2int(thisini->GetConfig(Config::kChn0_Snap_W)),
                                      str2int(thisini->GetConfig(Config::kChn0_Snap_H))};
                viParam[0].Snapmcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_Snap_McastEnable));
                memset(viParam[0].Snapmcast_ip,0,IPSIZE);
                sprintf(viParam[0].Snapmcast_ip,"%s",string(thisini->GetConfig(Config::kChn0_Snap_McastIp)).c_str());
                viParam[0].Snapmcast_port = str2int(thisini->GetConfig(Config::kChn0_Snap_McastPort).c_str());
                viParam[0].Snapmcast_freq = str2int(thisini->GetConfig(Config::kChn0_Snap_McastFreq).c_str());
                viParam[0].stVencSize = {str2int(thisini->GetConfig(Config::kChn0_M_VENC_W)),
                                         str2int(thisini->GetConfig(Config::kChn0_M_VENC_H))};
                viParam[0].u32BitRate[0] = str2int(thisini->GetConfig(Config::kChn0_M_BitRate));
                viParam[0].u32BitRate[1] = str2int(thisini->GetConfig(Config::kChn0_S0_BitRate));
                viParam[0].u32BQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_B_QP));
                viParam[0].u32BQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_B_QP));
                viParam[0].u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn0_M_DstFrmRate));
                viParam[0].u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn0_S0_DstFrmRate));
                viParam[0].u32Gop[0] = str2int(thisini->GetConfig(Config::kChn0_M_Gop));
                viParam[0].u32Gop[1] = str2int(thisini->GetConfig(Config::kChn0_S0_Gop));
                viParam[0].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_VI_Crop_Enable));
                if(viParam[0].vi_crop_enable) {
                    viParam[0].u32X = str2int(thisini->GetConfig(Config::kChn0_VI_X));
                    viParam[0].u32Y = str2int(thisini->GetConfig(Config::kChn0_VI_Y));
                    viParam[0].u32W = str2int(thisini->GetConfig(Config::kChn0_VI_Width));
                    viParam[0].u32H = str2int(thisini->GetConfig(Config::kChn0_VI_Height));
                }
                viParam[0].u32IQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_I_QP));
                viParam[0].u32IQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_I_QP));
                viParam[0].u32MaxIProp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MAXI_PROP));
                viParam[0].u32MaxIProp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MAXI_PROP));
                viParam[0].u32MaxQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MAX_QP));
                viParam[0].u32MaxQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MAX_QP));
                viParam[0].u32MinIProp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MINI_PROP));
                viParam[0].u32MinIProp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MINI_PROP));
                viParam[0].u32MinIQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MinI_Qp));
                viParam[0].u32MinIQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MinI_Qp));
                viParam[0].u32MinQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_MIN_QP));
                viParam[0].u32MinQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_MIN_QP));
                viParam[0].u32PQp[0] = str2int(thisini->GetConfig(Config::kChn0_M_P_QP));
                viParam[0].u32PQp[1] = str2int(thisini->GetConfig(Config::kChn0_S0_P_QP));
                viParam[0].u32Profile[0] = str2int(thisini->GetConfig(Config::kChn0_M_Profile));
                viParam[0].u32Profile[1] = str2int(thisini->GetConfig(Config::kChn0_S0_Profile));
                viParam[0].u32SrcFrmRate = str2int(thisini->GetConfig(Config::kChn0_SrcFrmRate));
                viParam[0].u32ViHeigth = str2int(thisini->GetConfig(Config::kChn0_VI_H));
                viParam[0].u32ViWidth = str2int(thisini->GetConfig(Config::kChn0_VI_W));
                viParam[0].venc_SAME_INPUT[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_M_VENC_SAME_INPUT));
                viParam[0].venc_SAME_INPUT[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_S0_VENC_SAME_INPUT));
                viParam[0].vi_model = str2int(thisini->GetConfig(Config::kChn0_VI_Module));
                break;
            case 1:
                viParam[1].enRcMode[0] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn1_M_RcMode));
                viParam[1].enRcMode[1] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn1_S0_RcMode));
                viParam[1].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn1_M_VENC_Type));
                viParam[1].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn1_S0_VENC_Type));
                viParam[1].sw_color_expand = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_SW_Color));
                viParam[1].enViMode = SAMPLE_VI_MODE;
                viParam[1].input_type = 1;
                viParam[1].mcast_enable[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_M_McastEnable));
                viParam[1].mcast_enable[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_S0_McastEnable));
                if(viParam[1].mcast_enable[0]) {
                    sprintf(viParam[0].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn1_M_McastIp)).c_str());
                    viParam[1].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn1_M_McastPort).c_str());
                }
                if(viParam[1].mcast_enable[1]) {
                    sprintf(viParam[1].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn1_S0_McastIp)).c_str());
                    viParam[1].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn1_S0_McastPort).c_str());
                }
                viParam[1].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_Osd_Enable));
                viParam[1].SnapQuality = str2int(thisini->GetConfig(Config::kChn1_Snap_Quality));
                viParam[1].SnapLen = str2int(thisini->GetConfig(Config::kChn1_SnapLen).c_str());
                viParam[1].SnapDelay = str2int(thisini->GetConfig(Config::kChn1_Snap_Delay).c_str());
                viParam[1].snapPort = str2int( thisini->GetConfig(Config::kChn1_SnapPort).c_str());
                viParam[1].stMinorSize = {str2int(thisini->GetConfig(Config::kChn1_S0_VENC_W)),
                                          str2int(thisini->GetConfig(Config::kChn1_S0_VENC_H))};
                viParam[1].stSnapSize = {str2int(thisini->GetConfig(Config::kChn1_Snap_W)),
                                      str2int(thisini->GetConfig(Config::kChn1_Snap_H))};
                viParam[1].Snapmcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_Snap_McastEnable));
                memset(viParam[1].Snapmcast_ip,0,IPSIZE);
                sprintf(viParam[1].Snapmcast_ip,"%s",string(thisini->GetConfig(Config::kChn1_Snap_McastIp)).c_str());
                viParam[1].Snapmcast_port = str2int(thisini->GetConfig(Config::kChn1_Snap_McastPort).c_str());
                viParam[1].Snapmcast_freq = str2int(thisini->GetConfig(Config::kChn1_Snap_McastFreq).c_str());
                viParam[1].stVencSize = {str2int(thisini->GetConfig(Config::kChn1_M_VENC_W)),
                                         str2int(thisini->GetConfig(Config::kChn1_M_VENC_H))};
                viParam[1].u32BitRate[0] = str2int(thisini->GetConfig(Config::kChn1_M_BitRate));
                viParam[1].u32BitRate[1] = str2int(thisini->GetConfig(Config::kChn1_S0_BitRate));
                viParam[1].u32BQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_B_QP));
                viParam[1].u32BQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_B_QP));
                viParam[1].u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn1_M_DstFrmRate));
                viParam[1].u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn1_S0_DstFrmRate));
                viParam[1].u32Gop[0] = str2int(thisini->GetConfig(Config::kChn1_M_Gop));
                viParam[1].u32Gop[1] = str2int(thisini->GetConfig(Config::kChn1_S0_Gop));
                viParam[1].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_VI_Crop_Enable));
                if(viParam[1].vi_crop_enable) {
                    viParam[1].u32X = str2int(thisini->GetConfig(Config::kChn1_VI_X));
                    viParam[1].u32Y = str2int(thisini->GetConfig(Config::kChn1_VI_Y));
                    viParam[1].u32W = str2int(thisini->GetConfig(Config::kChn1_VI_Width));
                    viParam[1].u32H = str2int(thisini->GetConfig(Config::kChn1_VI_Height));
                }
                viParam[1].u32IQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_I_QP));
                viParam[1].u32IQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_I_QP));
                viParam[1].u32MaxIProp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MAXI_PROP));
                viParam[1].u32MaxIProp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MAXI_PROP));
                viParam[1].u32MaxQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MAX_QP));
                viParam[1].u32MaxQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MAX_QP));
                viParam[1].u32MinIProp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MINI_PROP));
                viParam[1].u32MinIProp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MINI_PROP));
                viParam[1].u32MinIQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MinI_Qp));
                viParam[1].u32MinIQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MinI_Qp));
                viParam[1].u32MinQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_MIN_QP));
                viParam[1].u32MinQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_MIN_QP));
                viParam[1].u32PQp[0] = str2int(thisini->GetConfig(Config::kChn1_M_P_QP));
                viParam[1].u32PQp[1] = str2int(thisini->GetConfig(Config::kChn1_S0_P_QP));
                viParam[1].u32Profile[0] = str2int(thisini->GetConfig(Config::kChn1_M_Profile));
                viParam[1].u32Profile[1] = str2int(thisini->GetConfig(Config::kChn1_S0_Profile));
                viParam[1].u32SrcFrmRate = str2int(thisini->GetConfig(Config::kChn1_SrcFrmRate));
                viParam[1].u32ViHeigth = str2int(thisini->GetConfig(Config::kChn1_VI_H));
                viParam[1].u32ViWidth = str2int(thisini->GetConfig(Config::kChn1_VI_W));
                viParam[1].venc_SAME_INPUT[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_M_VENC_SAME_INPUT));
                viParam[1].venc_SAME_INPUT[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_S0_VENC_SAME_INPUT));
                viParam[1].vi_model = str2int(thisini->GetConfig(Config::kChn1_VI_Module));
                break;
            case 2:
                viParam[2].enRcMode[0] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn2_M_RcMode));
                viParam[2].enRcMode[1] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn2_S0_RcMode));
                viParam[2].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn2_M_VENC_Type));
                viParam[2].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn2_S0_VENC_Type));
                viParam[2].sw_color_expand = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_SW_Color));
                viParam[2].enViMode = SAMPLE_VI_MODE;
                viParam[2].input_type = 1;
                viParam[2].mcast_enable[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_M_McastEnable));
                viParam[2].mcast_enable[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_S0_McastEnable));
                if(viParam[2].mcast_enable[0]) {
                    sprintf(viParam[2].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn2_M_McastIp)).c_str());
                    viParam[2].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn2_M_McastPort).c_str());
                }
                if(viParam[2].mcast_enable[1]) {
                    sprintf(viParam[2].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn2_S0_McastIp)).c_str());
                    viParam[2].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn2_S0_McastPort).c_str());
                }
                viParam[2].osd_enable  = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_Osd_Enable));
                viParam[2].SnapQuality = str2int(thisini->GetConfig(Config::kChn2_Snap_Quality));
                viParam[2].SnapLen = str2int(thisini->GetConfig(Config::kChn2_SnapLen).c_str());
                viParam[2].SnapDelay = str2int(thisini->GetConfig(Config::kChn2_Snap_Delay).c_str());
                viParam[2].snapPort = str2int( thisini->GetConfig(Config::kChn2_SnapPort).c_str());
                viParam[2].stMinorSize = {str2int(thisini->GetConfig(Config::kChn2_S0_VENC_W)),
                                          str2int(thisini->GetConfig(Config::kChn2_S0_VENC_H))};
                viParam[2].stSnapSize = {str2int(thisini->GetConfig(Config::kChn2_Snap_W)),
                                      str2int(thisini->GetConfig(Config::kChn2_Snap_H))};
                memset(viParam[2].Snapmcast_ip,0,IPSIZE);
                viParam[2].Snapmcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_Snap_McastEnable));
                sprintf(viParam[2].Snapmcast_ip,"%s",string(thisini->GetConfig(Config::kChn2_Snap_McastIp)).c_str());
                viParam[2].Snapmcast_port = str2int(thisini->GetConfig(Config::kChn2_Snap_McastPort).c_str());
                viParam[2].Snapmcast_freq = str2int(thisini->GetConfig(Config::kChn2_Snap_McastFreq).c_str());
                viParam[2].stVencSize = {str2int(thisini->GetConfig(Config::kChn2_M_VENC_W)),
                                         str2int(thisini->GetConfig(Config::kChn2_M_VENC_H))};
                viParam[2].u32BitRate[0] = str2int(thisini->GetConfig(Config::kChn2_M_BitRate));
                viParam[2].u32BitRate[1] = str2int(thisini->GetConfig(Config::kChn2_S0_BitRate));
                viParam[2].u32BQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_B_QP));
                viParam[2].u32BQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_B_QP));
                viParam[2].u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn2_M_DstFrmRate));
                viParam[2].u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn2_S0_DstFrmRate));
                viParam[2].u32Gop[0] = str2int(thisini->GetConfig(Config::kChn2_M_Gop));
                viParam[2].u32Gop[1] = str2int(thisini->GetConfig(Config::kChn2_S0_Gop));
                viParam[2].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_VI_Crop_Enable));
                if(viParam[2].vi_crop_enable) {
                    viParam[2].u32X = str2int(thisini->GetConfig(Config::kChn2_VI_X));
                    viParam[2].u32Y = str2int(thisini->GetConfig(Config::kChn2_VI_Y));
                    viParam[2].u32W = str2int(thisini->GetConfig(Config::kChn2_VI_Width));
                    viParam[2].u32H = str2int(thisini->GetConfig(Config::kChn2_VI_Height));
                }
                viParam[2].u32IQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_I_QP));
                viParam[2].u32IQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_I_QP));
                viParam[2].u32MaxIProp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MAXI_PROP));
                viParam[2].u32MaxIProp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MAXI_PROP));
                viParam[2].u32MaxQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MAX_QP));
                viParam[2].u32MaxQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MAX_QP));
                viParam[2].u32MinIProp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MINI_PROP));
                viParam[2].u32MinIProp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MINI_PROP));
                viParam[2].u32MinIQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MinI_Qp));
                viParam[2].u32MinIQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MinI_Qp));
                viParam[2].u32MinQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_MIN_QP));
                viParam[2].u32MinQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_MIN_QP));
                viParam[2].u32PQp[0] = str2int(thisini->GetConfig(Config::kChn2_M_P_QP));
                viParam[2].u32PQp[1] = str2int(thisini->GetConfig(Config::kChn2_S0_P_QP));
                viParam[2].u32Profile[0] = str2int(thisini->GetConfig(Config::kChn2_M_Profile));
                viParam[2].u32Profile[1] = str2int(thisini->GetConfig(Config::kChn2_S0_Profile));
                viParam[2].u32SrcFrmRate = str2int(thisini->GetConfig(Config::kChn2_SrcFrmRate));
                viParam[2].u32ViHeigth = str2int(thisini->GetConfig(Config::kChn2_VI_H));
                viParam[2].u32ViWidth = str2int(thisini->GetConfig(Config::kChn2_VI_W));
                viParam[2].venc_SAME_INPUT[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_M_VENC_SAME_INPUT));
                viParam[2].venc_SAME_INPUT[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_S0_VENC_SAME_INPUT));
                viParam[2].vi_model = str2int(thisini->GetConfig(Config::kChn2_VI_Module));
                break;
            case 3:
                viParam[3].enRcMode[0] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn3_M_RcMode));
                viParam[3].enRcMode[1] = (SAMPLE_RC_E)str2int(thisini->GetConfig(Config::kChn3_S0_RcMode));
                viParam[3].enType[0] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn3_M_VENC_Type));
                viParam[3].enType[1] = (PAYLOAD_TYPE_E)str2int(thisini->GetConfig(Config::kChn3_S0_VENC_Type));
                viParam[3].sw_color_expand = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_SW_Color));
                viParam[3].enViMode = SAMPLE_VI_MODE;
                viParam[3].input_type = 1;
                viParam[3].mcast_enable[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_M_McastEnable));
                viParam[3].mcast_enable[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_S0_McastEnable));
                if(viParam[3].mcast_enable[0]) {
                    sprintf(viParam[3].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn3_M_McastIp)).c_str());
                    viParam[3].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn3_M_McastPort).c_str());
                }
                if(viParam[3].mcast_enable[1]) {
                    sprintf(viParam[3].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn3_S0_McastIp)).c_str());
                    viParam[3].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn3_S0_McastPort).c_str());
                }
                viParam[3].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_Osd_Enable));
                viParam[3].SnapQuality = str2int(thisini->GetConfig(Config::kChn3_Snap_Quality));
                viParam[3].SnapLen = str2int(thisini->GetConfig(Config::kChn3_SnapLen).c_str());
                viParam[3].SnapDelay = str2int(thisini->GetConfig(Config::kChn3_Snap_Delay).c_str());
                viParam[3].snapPort = str2int( thisini->GetConfig(Config::kChn3_SnapPort).c_str());
                viParam[3].stMinorSize = {str2int(thisini->GetConfig(Config::kChn3_S0_VENC_W)),
                                          str2int(thisini->GetConfig(Config::kChn3_S0_VENC_H))};
                viParam[3].stSnapSize = {str2int(thisini->GetConfig(Config::kChn3_Snap_W)),
                                      str2int(thisini->GetConfig(Config::kChn3_Snap_H))};
                viParam[3].Snapmcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_Snap_McastEnable));
                memset(viParam[3].Snapmcast_ip,0,IPSIZE);
                sprintf(viParam[3].Snapmcast_ip,"%s",string(thisini->GetConfig(Config::kChn3_Snap_McastIp)).c_str());
                viParam[3].Snapmcast_port = str2int(thisini->GetConfig(Config::kChn3_Snap_McastPort).c_str());
                viParam[3].Snapmcast_freq = str2int(thisini->GetConfig(Config::kChn3_Snap_McastFreq).c_str());
                viParam[3].stVencSize = {str2int(thisini->GetConfig(Config::kChn3_M_VENC_W)),
                                         str2int(thisini->GetConfig(Config::kChn3_M_VENC_H))};
                viParam[3].u32BitRate[0] = str2int(thisini->GetConfig(Config::kChn3_M_BitRate));
                viParam[3].u32BitRate[1] = str2int(thisini->GetConfig(Config::kChn3_S0_BitRate));
                viParam[3].u32BQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_B_QP));
                viParam[3].u32BQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_B_QP));
                viParam[3].u32DstFrameRate[0] = str2int(thisini->GetConfig(Config::kChn3_M_DstFrmRate));
                viParam[3].u32DstFrameRate[1] = str2int(thisini->GetConfig(Config::kChn3_S0_DstFrmRate));
                viParam[3].u32Gop[0] = str2int(thisini->GetConfig(Config::kChn3_M_Gop));
                viParam[3].u32Gop[1] = str2int(thisini->GetConfig(Config::kChn3_S0_Gop));
                viParam[3].vi_crop_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_VI_Crop_Enable));
                if(viParam[3].vi_crop_enable) {
                    viParam[3].u32X = str2int(thisini->GetConfig(Config::kChn3_VI_X));
                    viParam[3].u32Y = str2int(thisini->GetConfig(Config::kChn3_VI_Y));
                    viParam[3].u32W = str2int(thisini->GetConfig(Config::kChn3_VI_Width));
                    viParam[3].u32H = str2int(thisini->GetConfig(Config::kChn3_VI_Height));
                }
                viParam[3].u32IQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_I_QP));
                viParam[3].u32IQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_I_QP));
                viParam[3].u32MaxIProp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MAXI_PROP));
                viParam[3].u32MaxIProp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MAXI_PROP));
                viParam[3].u32MaxQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MAX_QP));
                viParam[3].u32MaxQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MAX_QP));
                viParam[3].u32MinIProp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MINI_PROP));
                viParam[3].u32MinIProp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MINI_PROP));
                viParam[3].u32MinIQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MinI_Qp));
                viParam[3].u32MinIQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MinI_Qp));
                viParam[3].u32MinQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_MIN_QP));
                viParam[3].u32MinQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_MIN_QP));
                viParam[3].u32PQp[0] = str2int(thisini->GetConfig(Config::kChn3_M_P_QP));
                viParam[3].u32PQp[1] = str2int(thisini->GetConfig(Config::kChn3_S0_P_QP));
                viParam[3].u32Profile[0] = str2int(thisini->GetConfig(Config::kChn3_M_Profile));
                viParam[3].u32Profile[1] = str2int(thisini->GetConfig(Config::kChn3_S0_Profile));
                viParam[3].u32SrcFrmRate = str2int(thisini->GetConfig(Config::kChn3_SrcFrmRate));
                viParam[3].u32ViHeigth = str2int(thisini->GetConfig(Config::kChn3_VI_H));
                viParam[3].u32ViWidth = str2int(thisini->GetConfig(Config::kChn3_VI_W));
                viParam[3].venc_SAME_INPUT[0] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_M_VENC_SAME_INPUT));
                viParam[3].venc_SAME_INPUT[1] = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_S0_VENC_SAME_INPUT));
                viParam[3].vi_model = str2int(thisini->GetConfig(Config::kChn3_VI_Module));
                break;
            default:
                printf("paramte init error\n");
                break;
        }
    }

    viParam[0].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_Osd_Enable));
    viParam[1].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_Osd_Enable));
    viParam[2].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_Osd_Enable));
    viParam[3].osd_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_Osd_Enable));



    viParam[0].mcast_enable[0] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_M_McastEnable));
    viParam[0].mcast_enable[1] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kChn0_S0_McastEnable));
    viParam[1].mcast_enable[0] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_M_McastEnable));
    viParam[1].mcast_enable[1] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kChn1_S0_McastEnable));
    viParam[2].mcast_enable[0] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_M_McastEnable));
    viParam[2].mcast_enable[1] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kChn2_S0_McastEnable));
    viParam[3].mcast_enable[0] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_M_McastEnable));
    viParam[3].mcast_enable[1] =  (HI_BOOL)str2int(thisini->GetConfig(Config::kChn3_S0_McastEnable));



    sprintf(viParam[0].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn0_M_McastIp)).c_str());
    sprintf(viParam[0].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn0_S0_McastIp)).c_str());
    sprintf(viParam[1].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn1_M_McastIp)).c_str());
    sprintf(viParam[1].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn1_S0_McastIp)).c_str());
    sprintf(viParam[2].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn2_M_McastIp)).c_str());
    sprintf(viParam[2].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn2_S0_McastIp)).c_str());
    sprintf(viParam[3].mcast_ip[0], "%s", string(thisini->GetConfig(Config::kChn3_M_McastIp)).c_str());
    sprintf(viParam[3].mcast_ip[1], "%s", string(thisini->GetConfig(Config::kChn3_S0_McastIp)).c_str());

    viParam[0].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn0_M_McastPort).c_str());
    viParam[0].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn0_S0_McastPort).c_str());
    viParam[1].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn1_M_McastPort).c_str());
    viParam[1].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn1_S0_McastPort).c_str());
    viParam[2].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn2_M_McastPort).c_str());
    viParam[2].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn2_S0_McastPort).c_str());
    viParam[3].mcast_port[0] = atoi(thisini->GetConfig(Config::kChn3_M_McastPort).c_str());
    viParam[3].mcast_port[1] = atoi(thisini->GetConfig(Config::kChn3_S0_McastPort).c_str());

    viParam[0].vi_model = str2int(thisini->GetConfig(Config::kChn0_VI_Module));
    viParam[1].vi_model = str2int(thisini->GetConfig(Config::kChn1_VI_Module));
    viParam[2].vi_model = str2int(thisini->GetConfig(Config::kChn2_VI_Module));
    viParam[3].vi_model = str2int(thisini->GetConfig(Config::kChn3_VI_Module));

    edid_4k = atoi(thisini->GetConfig(Config::kEdid).c_str());  //1=4k
    if(1 == edid_4k) {
        g_vi_en_number = 2;
    }
    else {
#ifdef __EDC_VENC__
        g_vi_en_number = 3;
#else
        g_vi_en_number = 4;
#endif
    }

#ifndef __EDC_VENC__
    Audio_reg_init();
#endif
    return true;
}




void AudioParamInit()
{
    auto thisini = Singleton<Config>::getInstance();
    HI_S32 i = 0;

    for(i = 0; i < AI_PORT_NUMBER; i++)
    {
        switch (i) {
            case 0:
                aiParam[0].Amcast_enable = (HI_BOOL)str2int(thisini->GetConfig(Config::kHdmi0_Au_McastEnable));
                aiParam[0].AI_enable = aiParam[0].Amcast_enable;
                memset(aiParam[0].Amcast_ip,0,IPSIZE);
                strcpy(aiParam[0].Amcast_ip,thisini->GetConfig(Config::kHdmi0_Au_McastIp).c_str());
                aiParam[0].Amcast_port = str2int(thisini->GetConfig(Config::kHdmi0_Au_McastPort));

                break;
            case 1:
                if(versionFlag == HDMI_4) {
                    aiParam[1].Amcast_enable =  (HI_BOOL)str2int(thisini->GetConfig(Config::kHdmi1_Au_McastEnable));
                    aiParam[1].AI_enable = aiParam[1].Amcast_enable;
                    memset(aiParam[1].Amcast_ip,0,IPSIZE);
                    strcpy(aiParam[1].Amcast_ip,thisini->GetConfig(Config::kHdmi1_Au_McastIp).c_str());
                    aiParam[1].Amcast_port = str2int(thisini->GetConfig(Config::kHdmi1_Au_McastPort));
                }
                else if(versionFlag == MIX_4) {
                    aiParam[2].Amcast_enable =  (HI_BOOL)str2int(thisini->GetConfig(Config::kHdmi1_Au_McastEnable));
                    aiParam[2].AI_enable = aiParam[2].Amcast_enable;
                    memset(aiParam[2].Amcast_ip,0,IPSIZE);
                    strcpy(aiParam[2].Amcast_ip,thisini->GetConfig(Config::kHdmi1_Au_McastIp).c_str());
                    aiParam[2].Amcast_port = str2int(thisini->GetConfig(Config::kHdmi1_Au_McastPort));
                }
                break;
            case 2:
                if(versionFlag == HDMI_4) {
                    aiParam[2].Amcast_enable =  (HI_BOOL)str2int(thisini->GetConfig(Config::kAnalog_Au_McastEnable));
                    aiParam[2].AI_enable = aiParam[2].Amcast_enable;
                    memset(aiParam[2].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[2].Amcast_ip,"%s",thisini->GetConfig(Config::kAnalog_Au_McastIp).c_str());
                    aiParam[2].Amcast_port = str2int(thisini->GetConfig(Config::kAnalog_Au_McastPort));
                }
                else if(versionFlag == MIX_4) {
                    aiParam[1].Amcast_enable =  (HI_BOOL)str2int(thisini->GetConfig(Config::kAnalog_Au_McastEnable));
                    aiParam[1].AI_enable = aiParam[1].Amcast_enable;
                    memset(aiParam[1].Amcast_ip,0,IPSIZE);
                    sprintf(aiParam[1].Amcast_ip,"%s",thisini->GetConfig(Config::kAnalog_Au_McastIp).c_str());
                    aiParam[1].Amcast_port = str2int(thisini->GetConfig(Config::kAnalog_Au_McastPort));
                }
                break;
            default:
                break;
        }
    }
}

bool InitHisiSysterm()
{
    HI_U32 i = 0;
    HI_S32 vdecMax = 1;    //开启一个VDEC通道
    HI_S32 vpssMax = 64;
    HI_S32 vencMax = 8*VI_PORT_NUMBER;
    VencStreamCnt = 2;   //1 VI--->2 VENC
    aiNumber = 3;
    auto himppmaster = Singleton<HimppMaster>::getInstance();
    auto thisini = Singleton<Config>::getInstance();
    /*
     * 初始化系统,VI由IT66021初始化
     * */
    himppmaster->InitHimpp(vdecMax,vpssMax,vencMax);
    himppmaster->VoModeuleInit();
#ifdef __EDC_VENC__
    VPSS_GRP vpssGrp = 8;       //环出VPSS
    himppmaster->VpssModuleInit(vpssGrp);
    himppmaster->VpssBindVo(vpssGrp,0,0,0);
#endif
    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));

    for(i = 0; i < AI_PORT_NUMBER; i++) {
        himppmaster->AiModeuleInit(i,enAiSampleRate);
    }

    return true;
}


void *recvMessageProc(void*)
{
    // 创建进程间的消息队列
    c_mainmsgid = CreateMessageQueue(c_mainmsgpath);
    SetMessageQueue(c_mainmsgid);
    //GetMessageQueueStatus(c_mainmsgid);

    struct msg_st msgdata;

    JsonParse* jsonparse = new JsonParse();
    while(l_recvrunning == true){
        memset(&msgdata, 0, sizeof(struct msg_st));

        if(msgrcv(c_mainmsgid, (void*)&msgdata,
                  BUFF_SIZE, c_mainmsgtype, 0) == -1){ // 阻塞读取。若设非阻塞，最后一个参数设为IPC_NOWAIT
            COMMON_PRT("msgrcv failed with errno: %s", strerror(errno));
            sleep(1);
            continue;
        }

        //printf("mig    1   --------------------  msgdata=%s\n",msgdata.mtext.msg);
        strncpy(gClientIP, msgdata.mtext.ip, SZ_IPADDR);
        gClientIP[SZ_IPADDR-1] = '\0';
        gClientPort = msgdata.mtext.port;
        COMMON_PRT("client ip: %s, port: %d. You recv message:", gClientIP, gClientPort);
        COMMON_PRT("%s", msgdata.mtext.msg);


        jsonparse->LoadJson(msgdata.mtext.msg);
        if(jsonparse->CheckJsonFormat()){
            jsonparse->IterationParseStart();
        }
        else{
            COMMON_PRT("Illegal format of JSON");
        }
        jsonparse->ReleaseJson(); //NOTE: Paired with LoadJson()

    }

    delete jsonparse;

     //销毁进程间的消息队列
     DeleteMessageQueue(c_mainmsgid);

    return (void*)1;
}

int  GetHardWareInfo(char* res)
{
    FILE* f = fopen("/version/hardware_info", "rb");
    if(!f) return 0;

    char buf[512] = {0};
    int len = fread(buf,1,512,f);
    if(len <= 0) {
        COMMON_PRT("fread hareware_info error\n");
        return 0;
    }


    char *pos1 = strstr(buf, "board\": \"");
    pos1 += strlen("board\": \"");
    if(!pos1)
        return 0;
    else{
        COMMON_PRT("get start:%s\n",pos1);
        char *pos2 = strchr(pos1, '\"');
        if(!pos2)
            return 0;
        else
            COMMON_PRT("get end:%s\n",pos2);


        strncpy(res, pos1, pos2 - pos1);
        return strlen(res);
    }

    return NULL;
}


bool readHdVersion(char *hd_version)
{
    FILE *hdFp = NULL;
    int rdLen = 0;
    char buff[256] = {0};

    char *st = NULL, *end = NULL;

    if(NULL == (hdFp = fopen(HDPATH,"r"))) {
        printf("open hardware_info error\n");
        return false;
    }
    memset(buff,0,256);
    memset(hd_version,0,32);

    rdLen = fread(buff,1,256,hdFp);
    if(rdLen > 0) {
        printf("read hardware_info:%s\n",buff);
        st = strstr(buff,"\"board\": \"");
        st += strlen("\"board\": \"");
        end = strchr(st,'\"');
        strncpy(hd_version,st,end-st);
        printf("get hdVersion:%s\n",hd_version);
    }
    else{
        fclose(hdFp);
        return false;
    }
    fclose(hdFp);
    return true;
}

void* audioproc(void *arg)
{
    AUDIO_DEV aiDev  = *(AUDIO_DEV*)arg;

    auto himppmaster = Singleton<HimppMaster>::getInstance();
    himppmaster->StartAudioEnc(aiDev);
    return NULL;
}

bool audioPush()
{
    for(int i = 0; i < AI_PORT_NUMBER; i++) {
        audio_tid[i] = CreateThread(audioproc, 0, SCHED_FIFO, true, &i);
        if(audio_tid[i] == 0){
            COMMON_PRT("pthread_create() failed: net udpBroadCastProc");
        }
        else
            printf("create audio :%d thread ok\n",i);

        usleep(50000);
    }

    return true;
}


int versionFlag = 0;   //0 -- 4HDMI 1 -- 4MIX
bool checkFlag = true;

int main()
{
    bool b_detached = true; //设置线程为分离模式
    // 1. 挂载系统中断信号，检测Ctrl+C按键
    signal(SIGHUP, HandleSig); //NOTE: 关闭终端
    signal(SIGINT, HandleSig); //NOTE: 检测Ctrl+C按键
    signal(SIGTERM, HandleSig); //NOTE: 检测killall 命令
    signal(SIGSEGV, HandleSig); //NOTE: 检测 Segmentation fault!
    // 2.读取当前板子的硬件版本号，本程序仅支持4MIX输入，4HDMI输入
    char version[32] ={0};
    memset(version,0,sizeof(version));
    readHdVersion(version);
    HI_U32 reg_value = 0;

    if(!strncmp(version,F_HDMIVERSION,strlen(F_HDMIVERSION))) {
        printf("this chip is 4hdmi input\n");
        versionFlag = HDMI_4;
    }
    else if(!strncmp(version,F_MIXVERSION,strlen(F_MIXVERSION))) {
        printf("this chip is 4mix input\n");
        versionFlag = MIX_4;
    }

    // 3.创建读取/data/venc.in配置文件的单例
    auto thisini = Singleton<Config>::getInstance();
    thisini->ReadConfig();
    thisini->PrintConfig();
    thisini->SaveConfig();

    //4.读取配置，初始化配置参数
    InitParamte();
    auto himppmaster = Singleton<HimppMaster>::getInstance();

    //5. 版本号
    /* NOTE: 生成版本号: <主版本号>.<次版本号>.<修订版本号>.<编译日期>_<阶段版本> */
    HI_U32 u32ChipId = 0;
    if ( HI_SUCCESS != HI_MPI_SYS_GetChipId(&u32ChipId)) //获取芯片ID
        u32ChipId = 0;
    if ( !GenerateVersion(sw_version, u32ChipId) )
        COMMON_PRT("generate version failed!\n");
    if ( !GenerateUUID() )
        COMMON_PRT("generate uuid failed!\n");

    //6.音视频匹配
    //4HDIM启用HDMI0(AIDEV=0) HDMI2(AIDEV=1) 3.5mm(AIDEV=0)音频；
    //4混合启用SDI(AIDEV=0)，HDMI(AIDEV=2) 3.5mm(AIDEV=1)音频
#ifdef __EDC_VENC__
    if(!strcmp(string(thisini->GetConfig(Config::kChn0_AiDev)).c_str(),"HDMI")){    //设备号0
        printf("EDC vidoe0 hdmi0 audio enable, ai dev=0\n");
        viParam[0].AI_enable = HI_TRUE;
        viParam[0].aiDev = 0;
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn0_AiDev)).c_str(),"3.5mm")){  //3.5mm
        printf("EDC vidoe0 3.5mm audio enable, ai dev=2\n");
        viParam[0].AI_enable = HI_TRUE;
        viParam[0].aiDev = 2;
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn0_AiDev)).c_str(),"NULL")) {  //无音频
        viParam[0].AI_enable = HI_FALSE;
        printf("EDC vi 0 audio null\n");
        Aechn[0] = -1;
    }



    if(!strcmp(string(thisini->GetConfig(Config::kChn1_AiDev)).c_str(),"HDMI")){//设备号1
        viParam[1].AI_enable = HI_TRUE;
        viParam[1].aiDev = 1;
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn1_AiDev)).c_str(),"3.5mm")){  //3.5mm
        viParam[1].AI_enable = HI_TRUE;
        viParam[1].aiDev = 2;
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn1_AiDev)).c_str(),"NULL")) {  //无音频
        printf("4I video1 audio disable\n");
        viParam[1].AI_enable = HI_FALSE;
        Aechn[1] = -1;
    }


    if(!strcmp(string(thisini->GetConfig(Config::kChn2_AiDev)).c_str(),"3.5mm")){   //3.5mm
        viParam[2].AI_enable = HI_TRUE;
        viParam[2].aiDev = 2;
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn2_AiDev)).c_str(),"HDMI")) {
        printf("4I video2 audio disable\n");
        viParam[2].AI_enable = HI_FALSE;
        Aechn[2] = -1;
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn2_AiDev)).c_str(),"NULL")) {  //无音频
        printf("4I video2 audio disable\n");
        viParam[2].AI_enable = HI_FALSE;
        Aechn[2] = -1;
    }


#else
    if(!strcmp(string(thisini->GetConfig(Config::kChn0_AiDev)).c_str(),"HDMI")){    //设备号0

        if((HDMI_4 == versionFlag) && g_4hdmi_ai_flag) {
            printf("4HDMI vidoe0 hdmi0 audio enable, ai dev=0\n");
            viParam[0].AI_enable = HI_TRUE;
            viParam[0].aiDev = 0;
            Aechn[0] = HDMI4_AI_HDMI0_I2S;
            auRtsp_enable[HDMI4_AI_HDMI0_I2S] = HI_TRUE;
            Hi_GetReg(GPIO0_7_ADR,&reg_value);
            reg_value |= GPIO0_7_SET;
            Hi_SetReg(GPIO0_7_ADR,reg_value);
            i2s_enable = HI_FALSE;
            AI_value_HD[0] = reg_value;
        }

        else if(MIX_4 ==  versionFlag) {
            printf("4MIX vidoe0 sdi audio enable, ai dev=0\n");
            viParam[0].AI_enable = HI_TRUE;
            viParam[0].aiDev = 0;
            Aechn[0] = MIX4_AI_SDI;
            auRtsp_enable[MIX4_AI_SDI] = HI_TRUE;
        }
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn0_AiDev)).c_str(),"3.5mm")){  //3.5mm
        if(HDMI_4 == versionFlag) {
            printf("4HDMI vidoe0 3.5mm audio enable, ai dev=0\n");
            viParam[0].AI_enable = HI_TRUE;
            viParam[0].aiDev = 0;
            Aechn[0] = HDMI4_AI_HDMI0_I2S;
            auRtsp_enable[HDMI4_AI_HDMI0_I2S] = HI_TRUE;
            Hi_GetReg(GPIO0_7_ADR,&reg_value);
            reg_value &= (~GPIO0_7_SET);
            Hi_SetReg(GPIO0_7_ADR,reg_value);
            i2s_enable = HI_TRUE;
            AI_value_35[0] = reg_value;
        }
        else if(MIX_4 == versionFlag) {
            printf("4MIX vidoe0 3.5mm audio enable, ai dev=1\n");
            viParam[0].AI_enable = HI_TRUE;
            viParam[0].aiDev = 1;
            Aechn[0] = MIX4_AI_I2S;
            auRtsp_enable[MIX4_AI_I2S] = HI_TRUE;
        }
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn0_AiDev)).c_str(),"NULL")) {  //无音频
        viParam[0].AI_enable = HI_FALSE;
        printf("vi 0 audio null\n");
        Aechn[0] = -1;
    }



    if(!strcmp(string(thisini->GetConfig(Config::kChn1_AiDev)).c_str(),"HDMI")){//设备号1
        if(HDMI_4 == versionFlag) {
            viParam[1].AI_enable = HI_TRUE;
            viParam[1].aiDev = 1;
            Aechn[1] = HDMI4_AI_HDMI1_2;
            auRtsp_enable[HDMI4_AI_HDMI1_2] = HI_TRUE;
            Hi_GetReg(GPIO0_2_ADR,&reg_value);
            reg_value |= GPIO0_2_SET;
            Hi_SetReg(GPIO0_2_ADR,reg_value);

            AI_value_HD[1] = reg_value;
        }
        else if(MIX_4 == versionFlag) {
            viParam[1].AI_enable = HI_FALSE;
            Aechn[1] = -1;
        }

    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn1_AiDev)).c_str(),"3.5mm")){  //3.5mm
        if(HDMI_4 == versionFlag) {
            printf("4HDMI vidoe1 3.5mm audio enable, ai dev=0\n");
            viParam[1].AI_enable = HI_TRUE;
            viParam[1].aiDev = 0;
            Aechn[1] = HDMI4_AI_HDMI0_I2S;
            auRtsp_enable[HDMI4_AI_HDMI0_I2S] = HI_TRUE;
            Hi_GetReg(GPIO0_7_ADR,&reg_value);
            reg_value &= (~GPIO0_7_SET);
            Hi_SetReg(GPIO0_7_ADR,reg_value);
            i2s_enable = HI_TRUE;
            AI_value_35[1] = reg_value;
        }
        else if(MIX_4 == versionFlag) {
            printf("4MIX vidoe1 3.5mm audio enable, ai dev=1\n");
            viParam[1].AI_enable = HI_TRUE;
            viParam[1].aiDev = 1;
            Aechn[1] = MIX4_AI_I2S;
            auRtsp_enable[MIX4_AI_I2S] = HI_TRUE;
        }
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn1_AiDev)).c_str(),"NULL")) {  //无音频
        printf("4I video1 audio disable\n");
        viParam[1].AI_enable = HI_FALSE;
        Aechn[1] = -1;
    }

    if(!strcmp(string(thisini->GetConfig(Config::kChn2_AiDev)).c_str(),"3.5mm")){   //3.5mm
        if(HDMI_4 == versionFlag) {
            printf("4HDMI vidoe2 3.5mm audio enable, ai dev=2\n");
            viParam[2].AI_enable = HI_TRUE;
            viParam[2].aiDev = 0;
            Aechn[2] = HDMI4_AI_HDMI0_I2S;
            auRtsp_enable[HDMI4_AI_HDMI0_I2S] = HI_TRUE;
            Hi_GetReg(GPIO0_7_ADR,&reg_value);
            reg_value &= (~GPIO0_7_SET);
            Hi_SetReg(GPIO0_7_ADR,reg_value);
            i2s_enable = HI_TRUE;
            AI_value_35[2] = reg_value;
        }
        else if(MIX_4 == versionFlag) {
            printf("4MIX vidoe1 3.5mm audio enable, ai dev=1\n");
            viParam[2].AI_enable = HI_TRUE;
            viParam[2].aiDev = 1;
            Aechn[2] = MIX4_AI_I2S;
            auRtsp_enable[MIX4_AI_I2S] = HI_TRUE;
        }

    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn2_AiDev)).c_str(),"HDMI")) {
        if((HDMI_4  == versionFlag) && g_4hdmi_ai_flag) {
            printf("4HDMI vidoe2 HDMI audio enable, ai dev=1\n");
            viParam[2].AI_enable = HI_TRUE;
            viParam[2].aiDev = 1;
            Aechn[2] = HDMI4_AI_HDMI1_2;
            auRtsp_enable[HDMI4_AI_HDMI1_2] = HI_TRUE;
            Hi_GetReg(GPIO0_2_ADR,&reg_value);
            reg_value &= (~GPIO0_2_SET);
            Hi_SetReg(GPIO0_2_ADR,reg_value);
            AI_value_HD[2] = reg_value;
        }
        else if( MIX_4 == versionFlag) {
            viParam[2].AI_enable = HI_FALSE;
            Aechn[2] = -1;
        }
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn2_AiDev)).c_str(),"NULL")) {  //无音频
        printf("4I video2 audio disable\n");
        viParam[2].AI_enable = HI_FALSE;
        Aechn[2] = -1;
    }

    if(!strcmp(string(thisini->GetConfig(Config::kChn3_AiDev)).c_str(),"3.5mm")){   //3.5mm
        if(HDMI_4 == versionFlag) {
            printf("4HDMI vidoe3 3.5mm audio enable, ai dev=2\n");
            viParam[3].AI_enable = HI_TRUE;
            viParam[3].aiDev = 0;
            Aechn[3] = HDMI4_AI_HDMI0_I2S;
            auRtsp_enable[HDMI4_AI_HDMI0_I2S] = HI_TRUE;
            Hi_GetReg(GPIO0_7_ADR,&reg_value);
            reg_value &= (~GPIO0_7_SET);
            Hi_SetReg(GPIO0_7_ADR,reg_value);
            i2s_enable = HI_TRUE;
            AI_value_35[3] = reg_value;
        }
        else if(MIX_4 == versionFlag) {
            printf("4MIX vidoe3 3.5mm audio enable, ai dev=1\n");
            viParam[3].AI_enable = HI_TRUE;
            viParam[3].aiDev = 1;
            Aechn[3] = MIX4_AI_I2S;
            auRtsp_enable[MIX4_AI_I2S] = HI_TRUE;
        }
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn3_AiDev)).c_str(),"HDMI")) {
        if(HDMI_4  ==  versionFlag) {
            viParam[3].AI_enable = HI_TRUE;
            viParam[3].aiDev = 2;
            Aechn[3] = HDMI4_AI_HDMI3;
            auRtsp_enable[HDMI4_AI_HDMI3] = HI_TRUE;
        }
        else if(MIX_4 == versionFlag) {
            printf("4MIX vidoe3 HDMI audio enable, ai dev=2\n");
            viParam[3].AI_enable = HI_TRUE;
            viParam[3].aiDev = 2;
            Aechn[3] = MIX4_AI_HMID3;
            auRtsp_enable[MIX4_AI_HMID3] = HI_TRUE;
        }
    }
    else if(!strcmp(string(thisini->GetConfig(Config::kChn3_AiDev)).c_str(),"NULL")) {  //无音频
        printf("4I video2 audio disable\n");
        viParam[3].AI_enable = HI_FALSE;
        Aechn[3] = -1;
    }
#endif

    //7.根据参数初始化海思系统
    AudioParamInit();
    InitHisiSysterm();

    //8.输入组播
    if(gbTimeTick){
        extern HI_VOID* GetTimeTick(HI_VOID *arg);
        //priority = 99;
        pthread_t tid = CreateThread(GetTimeTick, 99, SCHED_FIFO, b_detached, NULL);
        if(tid == 0){
            printf("pthread_create() failed: GetTimeTick");
        }
    }

    //9.初始化RTSP端口  AAC  G771A
    if(crtsps_start((short)554) < 0){
        printf("rtsp server start failed!\n");
    }

    AUDIO_SAMPLE_RATE_E enAiSampleRate = (AUDIO_SAMPLE_RATE_E)str2int(thisini->GetConfig(Config::kAiSampleRate));
    HI_S32 bitRate = str2int(thisini->GetConfig(Config::kAiBitRate));

    for(uint i = 0; i < VI_PORT_NUMBER; i++) {
        switch (i) {
            case 0:
                coderFormat[0] = (enum audio_type)str2int(thisini->GetConfig(Config::kChn0_AiCoderFormat));
                swAudioFormat(0);
                break;
            case 1:
                coderFormat[1] = (enum audio_type)str2int(thisini->GetConfig(Config::kChn1_AiCoderFormat));
                swAudioFormat(1);
                break;
            case 2:
                coderFormat[2] = (enum audio_type)str2int(thisini->GetConfig(Config::kChn2_AiCoderFormat));
                swAudioFormat(2);
                break;
            case 3:
                coderFormat[3] = (enum audio_type)str2int(thisini->GetConfig(Config::kChn3_AiCoderFormat));
                swAudioFormat(3);
                break;
            default:
                break;
        }
    }





    for(int i = 0; i < AI_PORT_NUMBER; i++) {
        g_audio_aac[i] = new aacenc();
        g_audio_aac[i]->open(bitRate,enAiSampleRate,AACLC);
    }
    g711_init();    //G771A4路共用一个G771A软编码

    //10.VI驱动检测线程开启
    //4HDMI--IT66021
    //4MIX--0(SDI,gs2971a); 1(未完成); 2(DVI转HDMI,IT66021); 3(HDMI,IT66021)



#ifdef __EDC_VENC__
    pthread_t  th_it6801  = CreateThread(timeCheckIt6801, 0, SCHED_FIFO, b_detached, NULL);
    if(th_it6801 == 0){
        printf("pthread_create() failed: it6801Get\n");
    }

#else
    int vi_0 = 0;
    if(HDMI_4 == versionFlag) {

        pthread_t tid_vi0 = CreateThread(timeCheckIt66021, 99, SCHED_FIFO, b_detached, &vi_0);

        if(tid_vi0 == 0){
            printf("pthread_create() failed: 0timeCheckIt66021\n");
        }
    }
    else if(MIX_4 ==  versionFlag) {

        pthread_t tid_gs2971a = CreateThread(timeCheckGs2971a,99,SCHED_FIFO,true,&vi_0);
        if(0 ==  tid_gs2971a) {
            printf("pthread_create faliled: timecheckGs2971a\n");
        }
    }


    if(HDMI_4 ==  versionFlag) {
        int vi_1 = 1;
        pthread_t tid_vi1 = CreateThread(timeCheckIt66021, 99, SCHED_FIFO, b_detached, &vi_1);
        if(tid_vi1 == 0){
            printf("pthread_create() failed: 1timeCheckIt66021");
        }
    }
    else if(MIX_4 == versionFlag) {

    }


    if(edid_4k != 1) {
        if((HDMI_4 == versionFlag) || (MIX_4 == versionFlag)) {
            int vi_2 = 2;
            pthread_t tid_vi2 = CreateThread(timeCheckIt66021, 99, SCHED_FIFO, b_detached, &vi_2);
            if(tid_vi2 == 0){
                printf("pthread_create()  failed: 2timeCheckIt66021");
            }
        }

        if((HDMI_4 ==  versionFlag) || (MIX_4 == versionFlag)) {
            int vi_3 = 3;
            pthread_t tid_vi3 = CreateThread(timeCheckIt66021, 99, SCHED_FIFO, b_detached, &vi_3);
            if(tid_vi3 == 0){
                printf("pthread_create() failed: 3timeCheckIt66021");
            }
        }
    }
#endif
    //10. 启动 消息队列 的线程
    pthread_t udpserver_tid = CreateThread(recvMessageProc, 0, SCHED_FIFO, b_detached, NULL);
    if(udpserver_tid == 0){
        COMMON_PRT("pthread_create() failed: net recvMessageProc");
    }

    sleep(2);
    audioPush();



    while(1)
        sleep(1000000);



    // 14. 退出 void *it66021fn(void*) 线程
    l_it66021fn_running = false;
    l_gs2971a_running  = false;

    // 15. 销毁HiMpp开启的各个模块
    himppmaster->VencModuleDestroy(0,8);
    himppmaster->VpssModuleDestroy();
    himppmaster->VoModuleDestroy();
    himppmaster->AudioModuleDestroy();

    return 1;


}
