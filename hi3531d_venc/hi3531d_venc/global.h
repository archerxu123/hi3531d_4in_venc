#ifndef GLOBAL_H
#define GLOBAL_H

#include <cstring>
#include <singleton.h>
#include <common.h>
#include <linux/netlink.h>
#include "audio_g711.h"
#include "librtsps_i.h"
#include "hi3531d_common/sample_comm.h"
#include "hi_comm_ive.h"


#define VI_PORT_NUMBER      4
#define AI_PORT_NUMBER      3
#define THREAD_CANCEL "cancel"
#define BLK_MAX         2

#define VI_CH0_REG  0x130D05F0
#define VI_CH8_REG  0x130D85F0
#define VI_CH16_REG 0x130E05F0
#define VI_CH24_REG 0x130E85F0

#define CH_REG_NEWER 0x10504
#define VI_REG_WON      0x130c0000

static bool g_isupdata = false;
#define HI31D_4HDMI     "SNJ2301fd_100_V2"
#define HI31D_4MUX      "SNJ"
#define VD_CHN_MAX      8
#define CMD_SN_NUM      16
#define Data_BUF_DEEP   16
#define RECV_BUF_SIZE   512
#define PACK_LEN        1300
#define INFO_HEADER     16
#define SZ_IPADDR       (24)

#define FLG_MULTI       0x80000000
#define FLG_IDR         0x40000000
#define FLG_265         0x20000000

#define ch0_skip_y_cfg      0x10900
#define ch1_skip_y_cfg      0x11900
#define ch2_skip_y_cfg      0x12900
#define ch3_skip_y_cfg      0x13900
#define ch4_skip_y_cfg      0x14900
#define ch5_skip_y_cfg      0x15900
#define ch6_skip_y_cfg      0x16900
#define ch7_skip_y_cfg      0x17900
#define ch8_skip_y_cfg      0x18900
#define ch9_skip_y_cfg      0x19900
#define ch10_skip_y_cfg     0x1A900
#define ch11_skip_y_cfg     0x1B900
#define ch12_skip_y_cfg     0x1C900
#define ch13_skip_y_cfg     0x1D900
#define ch14_skip_y_cfg     0x1E900
#define ch15_skip_y_cfg     0x1F900
#define ch16_skip_y_cfg     0x20900
#define ch17_skip_y_cfg     0x21900
#define ch18_skip_y_cfg     0x22900
#define ch19_skip_y_cfg     0x23900
#define ch20_skip_y_cfg     0x24900
#define ch21_skip_y_cfg     0x25900
#define ch22_skip_y_cfg     0x26900
#define ch23_skip_y_cfg     0x27900
#define ch24_skip_y_cfg     0x28900
#define ch25_skip_y_cfg     0x29900
#define ch26_skip_y_cfg     0x2A900
#define ch27_skip_y_cfg     0x2B900
#define ch28_skip_y_cfg     0x2C900
#define ch29_skip_y_cfg     0x2D900
#define ch30_skip_y_cfg     0x2E900
#define ch31_skip_y_cfg     0x2F900

#define vicap_base_reg      0x130c0000

//#define INFO_HEADER_H0  0   //固定‘S’
//#define INFO_HEADER_H1  1   //固定‘L’
//#define INFO_HEADER_SN  2   //包序号：0～255；区分不同的包
//#define INFO_HEADER_TP  3   //总包数
//#define INFO_HEADER_CP  5   //当前包号
//#define INFO_HEADER_FD  7   //帧号：0～59
//#define INFO_HEADER_FG  8   //标志：b0:是否是I帧，b1:是否是265
//#define INFO_HEADER_FQ  9   //帧率
//#define INFO_HEADER_VA  10  //标志：'V'：视频，‘A’:音频



#define INFO_HEADER_H0  0   //固定‘S’
#define INFO_HEADER_H1  1   //固定‘L’
#define INFO_HEADER_SN  2   //包序号：0～255；区分不同的包
#define INFO_HEADER_TP  3   //总包数
#define INFO_HEADER_CP  5   //当前包号
#define INFO_HEADER_FD  7   //帧号：0～59
#define INFO_HEADER_FG  8   //标志：b0:是否是I帧，b1:是否是265; //音频编码格式:00：pcm,01:g711a,10:aac
#define INFO_HEADER_FQ  9   //帧率
#define INFO_HEADER_WW  10  //宽度（2字节）
#define INFO_HEADER_HH  12  //高度（2字节）
#define INFO_HEADER_VE  14  //编码通道号
#define INFO_HEADER_VA  15  //标志：'V'：视频，‘A’:音频

#define IPSIZE 16

#define AMCAST_LEN 2048

#define SWCOLORFILE     "./color.bin"
#define HDPATH      "/version/hardware_info"

#define F_HDMIVERSION    "SHNJ2301fd_300_V1"
#define F_MIXVERSION     "SHNJ2301fd_400_V1"

#define SDISOCKTPORT    22467
#define HDMI_4  0
#define MIX_4   1
#define HDMI4_AI_HDMI0_I2S      0   //GPIO0_7拉高，启用HDMI0音频；GPIO0_7拉低，启用3.5MM音频
#define HDMI4_AI_HDMI1_2        1   //GPIO0_2拉高，启用HDMI1音频；GPIO0_2拉低，启用HDMI2音频
#define HDMI4_AI_HDMI3          2   //默认启用HDMI3音频

#define GPIO0_2_ADR     0x12150010
#define GPIO0_2_SET     0x4

#define GPIO0_7_ADR     0x12150200
#define GPIO0_7_SET     0x80


#define MIX4_AI_I2S         1
#define MIX4_AI_SDI         0
#define MIX4_AI_HMID3       2

#define MIN(a,b)     a>b?b:a
#define IPC_KEY     0x123
#define SHARE_MAX   32
#include <hi_common.h>

#define NETLINK_VIDEO_PROTOCOL     30
#define MAX_PLOAD        100
#define USER_PORT        100

#define VI_MAX_W           3840
#define VI_MAX_H           2160

typedef struct viParameter_
{
    SAMPLE_VI_MODE_E enViMode;
    HI_U32 u32ViWidth;
    HI_U32 u32ViHeigth;
    HI_U32 u32SrcFrmRate;
    SIZE_S stVencSize;
    SIZE_S stMinorSize;
    SIZE_S stSnapSize;
    HI_U32 u32Gop[2];           //0--Master   1--Slave
    HI_U32 u32Profile[2];       //0--Master   1--Slave
    HI_U32 u32BitRate[2];       //0--Master   1--Slave
    HI_U32 u32DstFrameRate[2];  //0--Master   1--Slave
    PAYLOAD_TYPE_E enType[2];   //0--Master   1--Slave
    SAMPLE_RC_E enRcMode[2];    //0--Master   1--Slave
    HI_U32 u32MinQp[2];         //0--Master   1--Slave
    HI_U32 u32MaxQp[2];
    HI_U32 u32MinIProp[2];
    HI_U32 u32MaxIProp[2];
    HI_U32 u32MinIQp[2];
    HI_U32 u32IQp[2];
    HI_U32 u32PQp[2];
    HI_U32 u32BQp[2];
    //RTSP音频
    HI_BOOL AI_enable;
    enum audio_type AI_type;
    AUDIO_DEV aiDev;
    //组播
    HI_BOOL mcast_enable[2];
    HI_CHAR mcast_ip[2][IPSIZE];
    HI_S32 mcast_port[2];
    HI_BOOL Snapmcast_enable;
    HI_CHAR Snapmcast_ip[20];
    HI_S32 Snapmcast_port;
    HI_S32 Snapmcast_freq;
    HI_S32 SnapQuality;
    HI_S32 snapPort;
    HI_S32 SnapDelay;
    HI_S32 SnapLen;
    HI_S32 input_type;    //0--VI隔行图像  1--VI逐行图像
    HI_BOOL osd_enable;
    HI_BOOL vi_crop_enable;
    HI_U32 u32X;
    HI_U32 u32Y;
    HI_U32 u32W;
    HI_U32 u32H;
    HI_BOOL venc_SAME_INPUT[2];
    HI_BOOL sw_color_expand;
    HI_S32 vi_model;
}PARAMETER;


typedef struct {
    HI_BOOL AI_enable = HI_FALSE;
    HI_S32  AI_bitRate = 128000;
    AUDIO_SAMPLE_RATE_E AI_sampleRate = AUDIO_SAMPLE_RATE_48000;
    HI_BOOL Amcast_enable = HI_FALSE;
    HI_CHAR Amcast_ip[IPSIZE]=  "0.0.0.0";
    HI_S32  Amcast_port = 0;
}AIPARAM;

typedef struct
{
    SIZE_S viStSize;
    HI_U32 viStRate;
}VISETPARAM;

typedef struct TransMit{
    int clock_swing_sum;
    int tick_delta;
    uint m1;
    uint m2;
}TransMit;

typedef struct videofarmat {
    unsigned int video_ch;
    unsigned int video_width;
    unsigned int video_height;
    unsigned int video_freq;
    unsigned int video_interlaced;
    unsigned int video_hdcp;
    unsigned int augio_freq;
} Media_Input_Fmt_new;

typedef struct _user_msg_info
{
    struct nlmsghdr hdr;
    struct videofarmat vf;
} user_msg_info;


typedef struct vi_info_ {
    HI_S32 viPort;
    HI_S32 srcFrameRate;
    SIZE_S viSize;
}VI_INFO;

extern bool gbTimeTick;
extern int  glFrameID;
extern int  glLocalTickDelta;
extern int  LocalTickDelta;
extern bool sync_ok;
extern int  sync_offset;

extern int repeat_time[8];
extern HI_BOOL aumcast_enable[3];
extern HI_BOOL auRtsp_enable[3];
extern char aumcast_ip[3][IPSIZE];
extern int aumcast_port[4];
extern int aumcast_sv_port[3];



extern int SnapDelay[4];
extern int VencStreamCnt;
extern int Venc1StreamCnt;
extern int Venc2StreamCnt;
extern int Venc3StreamCnt;
extern video_type v_type[8];
extern unsigned int u32DstFrameRate[8];

extern char gClientIP[];
extern int  gClientPort;

extern int diff_index;
extern IVE_MEM_INFO_S  pstMap;
extern VISETPARAM viSet[4];

extern HI_BOOL g_vi0_hdmi;
extern HI_BOOL g_vi1_hdmi;
extern HI_BOOL g_vi2_hdmi;
extern HI_BOOL g_vi3_hdmi;
extern int versionFlag;

extern pthread_t audio_tid[4];
extern int errFrame[4];
extern bool checkFlag;
extern bool ex_flag[4];
extern HI_BOOL audioFlag[3];         //判断AI是否需要初始化
extern int edid_4k;
extern HI_BOOL i2s_enable;
extern bool vi_src_chang[4];        //前端信号源改变，VI VENC重新初始化，IV-GET VENC SNAP线程暂停
extern bool pth_vi_net[4];
extern bool pth_snap[4];
extern bool osd_chng[4];            //动态修改OSD标志
extern bool snap_vh_chang[4];       //COMMANDMAP 动态修改抓图宽高
extern uint snap_jpeg_port[4];
extern char snap_jpeg_ip[4][IPSIZE];
extern bool jpeg_flag[4];
extern bool gb_master;
extern int g_vi_en_number;     //VI启用数量，1080P 4个VI 4k30 2个VI
extern bool pth_ai_push[AI_PORT_NUMBER];
extern AIPARAM   aiParam[AI_PORT_NUMBER];





void* multiSnapProc(void *arg);
void* multiSnapProc1(void *arg);
void* multiSnapProc2(void *arg);
void* multiSnapProc3(void *arg);
void *snap0ServerProc(void *arg);
void *snap1ServerProc(void *arg);
void *snap2ServerProc(void *arg);
void *snap3ServerProc(void *arg);

bool reSetSnap(int num,VENC_CHN snapChn,SIZE_S snapSize);



#endif // GLOBAL_H
