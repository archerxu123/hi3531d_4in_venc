#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include "himpp_master.h"
#include "mytimer.h"
#include "udpsocket/udpsocket.h"
#include "cnFreeType.h"

#ifdef __TINY_RTSP_SERVER__
tiny_rtsp_server_i* g_prtspserver   = 0;
#else
aacenc *g_audio_aac[AI_PORT_NUMBER];
#endif

int g_stream[8];
VB_BLK VbBlk;
int osd_enable[VI_PORT_NUMBER];
bool osd_reset[VI_PORT_NUMBER] = {false,false,false,false};
unsigned short testData[VI_PORT_NUMBER][30];
slangBmp *chnBmp[VI_PORT_NUMBER];
IMAGEDATA charColor[VI_PORT_NUMBER];
POINT_S startPoint[VI_PORT_NUMBER];
bool l_startloop[4] = {true,true,true,true};
bool mult_snap_start[4] = {true,true,true,true};

int vi_flag[VI_PORT_NUMBER]={1,1,1,1};
bool mult_snap[4] = {true,true,true,true};
int   kerrFrameNum = 0;
bool gbTimeTick = true;
int  glFrameID = -1;
int  glLocalTickDelta = 0;
int  LocalTickDelta = 0;
bool sync_ok = false;
int  sync_offset = 0;

IVE_MEM_INFO_S  pstMap;

bool pth_vi_net[4] = {true,true,true,true};
bool pth_snap[4] = {true,true,true,true};
bool pth_ai_push[AI_PORT_NUMBER] = {true,true,true};
bool osd_chng[4] = {false,false,false,false};
bool snap_vh_chang[4] = {false,false,false,false};
bool jpeg_flag[4] = {false,false,false,false};          //获取单播抓图指令
char snap_jpeg_ip[4][IPSIZE];                           //JPEG抓图IP
uint snap_jpeg_port[4];                                 //JPEG抓图端口


unsigned int vi_offset_addr[4][8] = {ch0_skip_y_cfg,ch1_skip_y_cfg,ch2_skip_y_cfg,ch3_skip_y_cfg,ch4_skip_y_cfg,ch5_skip_y_cfg,ch6_skip_y_cfg,ch7_skip_y_cfg,  \
                                     ch8_skip_y_cfg,ch9_skip_y_cfg,ch10_skip_y_cfg,ch11_skip_y_cfg,ch12_skip_y_cfg,ch13_skip_y_cfg,ch14_skip_y_cfg,ch15_skip_y_cfg, \
                                     ch16_skip_y_cfg,ch17_skip_y_cfg,ch18_skip_y_cfg,ch19_skip_y_cfg,ch20_skip_y_cfg,ch21_skip_y_cfg,ch22_skip_y_cfg,ch23_skip_y_cfg,   \
                                     ch24_skip_y_cfg,ch25_skip_y_cfg,ch26_skip_y_cfg,ch27_skip_y_cfg,ch28_skip_y_cfg,ch29_skip_y_cfg,ch30_skip_y_cfg,ch31_skip_y_cfg,};

struct timeval    tv;
struct timezone tz;

struct tm         *p;
/*void sysLocalTime()
{
    struct timeval    tv;
    struct timezone tz;

    struct tm         *p;

    gettimeofday(&tv, &tz);
    p = localtime(&tv.tv_sec);
    printf("time_now:%d-%d-%d-%d-%d-%d.%ld\n", 1900+p->tm_year, 1+p->tm_mon, p->tm_mday, p->tm_hour, p->tm_min, p->tm_sec, tv.tv_usec);

}*/
#if 1
HimppMaster::HimppMaster()
{

}

HimppMaster::~HimppMaster()
{
    HI_S32 s32Ret;
    int vi_port  = 0;
    VENC_CHN veChn = 0;
    VPSS_GRP  vpssGrp = 0;
    for(vi_port = 0; vi_port < g_vi_en_number; vi_port++) {
        veChn = vi_port * 2 + 1;
        vpssGrp = vi_port;
        VencUnbindVpss(veChn,vpssGrp);
        VencDestroy(veChn);
        vpssStop(vpssGrp);

        veChn = vi_port + 8;
        vpssGrp = VI_PORT_NUMBER + vi_port;
        VencUnbindVpss(veChn,vpssGrp);
        VencDestroy(veChn);
        vpssStop(vpssGrp);

        veChn = 2*vi_port;
        VencDestroy(veChn);
        s32Ret = HI_MPI_VI_SetFrameDepth(ViChn_[vi_port], 0);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("HI_MPI_VI_SetFrameDepth failed, err:0x%x!\n", s32Ret);
        }
        StopViModule(vi_port,vi_port);

    }
    VoModuleDestroy();
    AudioModuleDestroy();

    s32Ret = HI_MPI_VB_ReleaseBlock(VbBlk);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VB_ReleaseBlock failed, err:0x%x!\n", s32Ret);
    }

    SAMPLE_COMM_SYS_Exit();
}
#endif


bool HimppMaster::InitHimpp(HI_S32 vdecMaxCnt,HI_S32 vpssMaxCnt, HI_S32 vencMaxCnt)
{
    //VpssGrpCnt_ = s32GrpCnt;
    HI_S32 s32Ret;
    HI_U32 u32BlkSize;
    VB_CONF_S stVbConf;




    memset(&stVbConf,0,sizeof(VB_CONF_S));
    stVbConf.u32MaxPoolCnt = 64; //整个系统缓存池个数
    if(1 == edid_4k) {
        stMaxSize_.u32Width = 3840;
        stMaxSize_.u32Height = 2160;
    }
    else {
        stMaxSize_.u32Width = 1920;
        stMaxSize_.u32Height = 1080;
    }

    SAMPLE_PRT("chris width %d,height %d\n",stMaxSize_.u32Width,stMaxSize_.u32Height);

    //step  1: init variable
    u32BlkSize = HI35xx_COMM_SYS_CalcPicVbBlkSize(&stMaxSize_, SAMPLE_PIXEL_FORMAT, SAMPLE_SYS_ALIGN_WIDTH,COMPRESS_MODE_SEG);
    SAMPLE_PRT("chris blksize %d\n",u32BlkSize);



    stVbConf.astCommPool[0].u32BlkSize = u32BlkSize;
    stVbConf.astCommPool[0].u32BlkCnt = 64;

    //step 2: mpp system init.
    MPP_SYS_CONF_S stSysConf = {0};

#if 1

    stSysConf.u32AlignWidth = SAMPLE_SYS_ALIGN_WIDTH;
    s32Ret = HI_MPI_SYS_SetConf(&stSysConf);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_SYS_SetConf failed\n");
        return false;
    }

    s32Ret = HI_MPI_SYS_Init();
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_SYS_Init failed!\n");
        return false;
    }

#endif

    s32Ret = SAMPLE_COMM_SYS_Init(&stVbConf);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("system init failed with %d!\n", s32Ret);
        system("reboot\n"); //保证程序不会进入无限循环的重启
    }





    //开启一个解码通道
    s32Ret = HI35xx_COMM_SYS_MemConfig(vdecMaxCnt, vpssMaxCnt, vencMaxCnt, VoLayer_);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI35xx_COMM_SYS_MemConfig err 0x%x\n",s32Ret);
        return false;
    }


    if(INITIVE){
        s32Ret = SAMPLE_COMM_IVE_CreateMemInfo(&pstMap,sizeof(IVE_MAP_LUT_MEM_S));
        if (HI_SUCCESS != s32Ret)
        {
            printf("SAMPLE_COMM_IVE_CreateMemInfo err:0x%x\n", s32Ret);
        }
        int len = 0;
        getFileAll((char*)pstMap.pu8VirAddr, SWCOLORFILE, &len);
        if( len == 0 )
        {
            printf("open file error!");
            return false;
        }
    }




    return true;
}


int open_all()
{
    printf("aa\n");
    return 0;

}



bool HimppMaster::VpssModuleInit(HI_S32 vpssGrp, HI_S32 vpssChn)
{
    HI_S32 s32Ret;
    VPSS_GRP_ATTR_S stGrpAttr;
    memset(&stGrpAttr,0,sizeof(VPSS_GRP_ATTR_S));
    stGrpAttr.u32MaxW   = stMaxSize_.u32Width;
    stGrpAttr.u32MaxH   = stMaxSize_.u32Height;
    stGrpAttr.enPixFmt  = SAMPLE_PIXEL_FORMAT;
    stGrpAttr.bIeEn     = HI_FALSE;
    stGrpAttr.bNrEn     = HI_TRUE;
    stGrpAttr.bDciEn    = HI_FALSE;
    stGrpAttr.bHistEn   = HI_FALSE;
    stGrpAttr.bEsEn     = HI_FALSE;
    stGrpAttr.enDieMode = VPSS_DIE_MODE_NODIE;
    SIZE_S vpssSize,maxSize;
    HI_S32 vi_port;

    if(vpssGrp >= VI_PORT_NUMBER) {
        vi_port = vpssGrp - VI_PORT_NUMBER;
        vpssSize.u32Width = viParam[vi_port].stSnapSize.u32Width;
        vpssSize.u32Height = viParam[vi_port].stSnapSize.u32Height;
    }
    else {
        vi_port = vpssGrp;
        vpssSize.u32Width = viParam[vi_port].stMinorSize.u32Width;
        vpssSize.u32Height = viParam[vi_port].stMinorSize.u32Height;
    }


    maxSize.u32Width = viParam[vi_port].u32ViWidth;
    maxSize.u32Height = viParam[vi_port].u32ViHeigth;

#ifdef __EDC_VENC__
    if(8 ==  vpssGrp) {
        vpssSize.u32Width = 1920;
        vpssSize.u32Height = 1080;
        maxSize.u32Width = 3840;
        maxSize.u32Height = 2160;
    }

#endif
    printf("%d srt vpss max size:%d %d\n",vi_port,maxSize.u32Width,maxSize.u32Height);

    s32Ret = HI35xx_VPSS_Start(vpssGrp,vpssChn,vpssSize,maxSize);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("Start Vpss failed %d\n",s32Ret);
        return false;
    }
    return true;
}


bool HimppMaster::VpssBindVo(VPSS_GRP vpssGrp, VPSS_CHN vpssChn, VO_LAYER VoLayer, VO_CHN VoChn)
{
    HI_S32 s32Ret;
    s32Ret =   HI35xx_COMM_VO_BindVpss(vpssGrp,vpssChn,VoLayer,VoChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("start HI35xx_COMM_VO_BindVpss failed\n");
        return false;
    }
    return true;
}

bool HimppMaster::ViModuleInit(HI_S32 vi_port,SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type)
{
    HI_S32 s32Ret;
    stViSize_[vi_port].u32Width = stCapRect.u32Width;
    stViSize_[vi_port].u32Height = stCapRect.u32Height;

    s32Ret= HI35xx_COMM_VI_Start(ViDev_[vi_port], ViChn_[vi_port], enViMode, stCapRect, input_type);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("start vi failed\n");
        return false;
    }
    s32Ret = HI_MPI_VI_SetFrameDepth(ViChn_[vi_port], 1);
    if (HI_SUCCESS != s32Ret)
    {
        printf("set max depth err:0x%x\n", s32Ret);
        return s32Ret;
    }

    SAMPLE_PRT("chris videv%d vichn%d\n",ViDev_[vi_port],ViChn_[vi_port]);
    return true;
}

bool HimppMaster::VencModuleInit(SIZE_S stVencSize, SIZE_S stMinor1Size,SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp,HI_S32 viPort,HI_BOOL setFlag)
{
    HI_S32 s32Ret,i;
    switch (viPort) {
        case 0:
            stVencSize_[0] = stVencSize;
            stVencSize_[1] = stMinor1Size;
            stVencSize_[2] = stMinor2Size;
            for(i = 0; i < VencStreamCnt; i++){
                printf("Venc_chn=%d,w=%d,h=%d,sframerate=%d,dframerate=%d\n",VencChn_[viPort*VencStreamCnt+i],stVencSize_[i].u32Width,stVencSize_[i].u32Height,u32SrcFrmRate,u32DstFrameRate[i]);
                s32Ret = HI35xx_COMM_VENC_Start(viPort,VencChn_[viPort*VencStreamCnt+i], enType[i], stMaxSize_, stVencSize_[i], enRcMode[i], u32Profile[i],
                        u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                        u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                        u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i],setFlag);
                if (HI_SUCCESS != s32Ret){
                    SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[viPort*VencStreamCnt+i]);
                    usleep(20000);
                }
                else {
                    SAMPLE_PRT("Start VencChn[%d] succed\n",VencChn_[viPort*VencStreamCnt+i]);
                }
            }
            break;
        case 1:
            stVenc1Size_[0] = stVencSize;
            stVenc1Size_[1] = stMinor1Size;
            stVenc1Size_[2] = stMinor2Size;
            for(i = 0;i<VencStreamCnt;i++){
                printf("Venc_chn=%d,w=%d,h=%d,sframerate=%d,dframerate=%d\n",VencChn_[viPort*VencStreamCnt+i],stVenc1Size_[i].u32Width,stVenc1Size_[i].u32Height,u32SrcFrmRate,u32DstFrameRate[i]);
                s32Ret = HI35xx_COMM_VENC_Start(viPort,VencChn_[viPort*VencStreamCnt+i], enType[i], stMaxSize_, stVenc1Size_[i], enRcMode[i], u32Profile[i],
                        u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                        u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                        u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i],setFlag);
                if (HI_SUCCESS != s32Ret){
                    SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[viPort*VencStreamCnt+i]);
                    return false;
                }
            }
            break;
        case 2:
            stVenc2Size_[0] = stVencSize;
            stVenc2Size_[1] = stMinor1Size;
            stVenc2Size_[2] = stMinor2Size;
            for(i = 0; i<VencStreamCnt; i++){
                printf("Venc_chn=%d,w=%d,h=%d,sframerate=%d,dframerate=%d\n",VencChn_[viPort*VencStreamCnt+i],stVenc2Size_[i].u32Width,stVenc2Size_[i].u32Height,u32SrcFrmRate,u32DstFrameRate[i]);
                s32Ret = HI35xx_COMM_VENC_Start(viPort,VencChn_[viPort*VencStreamCnt+i], enType[i], stMaxSize_, stVenc2Size_[i], enRcMode[i], u32Profile[i],
                        u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                        u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                        u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i],setFlag);
                if (HI_SUCCESS != s32Ret){
                    SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[viPort*VencStreamCnt+i]);
                    return false;
                }
            }
            break;
        case 3:
            stVenc3Size_[0] = stVencSize;
            stVenc3Size_[1] = stMinor1Size;
            stVenc3Size_[2] = stMinor2Size;
            for(i = 0;i<VencStreamCnt;i++){
                printf("Venc_chn=%d,w=%d,h=%d,sframerate=%d,dframerate=%d\n",VencChn_[viPort*VencStreamCnt+i],stVenc3Size_[i].u32Width,stVenc3Size_[i].u32Height,u32SrcFrmRate,u32DstFrameRate[i]);
                s32Ret = HI35xx_COMM_VENC_Start(viPort,VencChn_[viPort*VencStreamCnt+i], enType[i], stMaxSize_, stVenc3Size_[i], enRcMode[i], u32Profile[i],
                        u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                        u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                        u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i],setFlag);
                if (HI_SUCCESS != s32Ret){
                    SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[viPort*VencStreamCnt+i]);
                    return false;
                }
            }
            break;
        default:
            break;
    }

    return true;
}

/*
 * VPSS 4 - VENC 8
 * VPSS 5 - VENC 9
 * VPSS 6 - VENC 10
 * VPSS 7 - VENC 11
 * */

HI_S32 HimppMaster::VencSnapInit(HI_S32 veChn,SIZE_S stSize)
{
    HI_S32 s32Ret;
    s32Ret = HI35xx_COMM_VENC_SnapStart(veChn, &stSize);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI35xx_COMM_VENC_SnapStart  %d failed with: 0x%x",veChn, s32Ret);
    }
}


bool HimppMaster::VencSetWH(VENC_CHN veChn,PAYLOAD_TYPE_E type,SIZE_S stVencSize,HI_S32 dstFrameRate)
{
    HI_S32 s32Ret = 0;
    VENC_CHN_ATTR_S stVencChnAttr;
    int vi_port = 0;
    if(type ==  PT_JPEG)
        vi_port = veChn - 8;
    else
        vi_port = veChn/2;


    s32Ret = HI_MPI_VENC_StopRecvPic(veChn);
    if(s32Ret != HI_SUCCESS) {
        SAMPLE_PRT("HI_MPI_VENC_StopRecvPic  %d failed with: 0x%x",veChn, s32Ret);
        return false;
    }

    s32Ret  = HI_MPI_VENC_GetChnAttr(veChn,&stVencChnAttr);
    if(s32Ret != HI_SUCCESS) {
        SAMPLE_PRT("HI_MPI_VENC_GetChnAttr  %d failed with: 0x%x",veChn, s32Ret);
    }

    switch (type) {
        case PT_H264:
            stVencChnAttr.stVeAttr.stAttrH264e.u32PicWidth = stVencSize.u32Width;
            stVencChnAttr.stVeAttr.stAttrH264e.u32PicHeight = stVencSize.u32Height;
            break;
        case PT_H265:
            stVencChnAttr.stVeAttr.stAttrH265e.u32PicWidth = stVencSize.u32Width;
            stVencChnAttr.stVeAttr.stAttrH265e.u32PicHeight = stVencSize.u32Height;
            break;
        case PT_JPEG:
            stVencChnAttr.stVeAttr.stAttrJpege.u32PicWidth = stVencSize.u32Width;
            stVencChnAttr.stVeAttr.stAttrJpege.u32PicHeight = stVencSize.u32Height;
            break;
        default:
            break;
    }


    s32Ret = HI_MPI_VENC_SetChnAttr(veChn, &stVencChnAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_SetChnAttr [%d] error %#x!\n",veChn, s32Ret);
    }


    if(PT_JPEG == type) {
        VENC_PARAM_JPEG_S stParamJpeg;
        s32Ret = HI_MPI_VENC_GetJpegParam(veChn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetJpegParam [%d] err 0x%x\n",veChn,s32Ret);
        }

        stParamJpeg.u32Qfactor = viParam[vi_port].SnapQuality;
        s32Ret = HI_MPI_VENC_SetJpegParam(veChn, &stParamJpeg);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetJpegParam [%d] err 0x%x\n",veChn,s32Ret);
        }
    }

    if(0 != dstFrameRate) {
        VENC_FRAME_RATE_S venc_jpeg_rate;
        s32Ret = HI_MPI_VENC_GetFrameRate(veChn,&venc_jpeg_rate);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_GetFrameRate [%d] err 0x%x\n",veChn,s32Ret);
            return false;
        }

        venc_jpeg_rate.s32SrcFrmRate = viParam[vi_port].u32SrcFrmRate;
        venc_jpeg_rate.s32DstFrmRate = viParam[vi_port].Snapmcast_freq;
        s32Ret = HI_MPI_VENC_SetFrameRate(veChn,&venc_jpeg_rate);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_VENC_SetFrameRate [%d]err 0x%x\n",veChn,s32Ret);
            return false;
        }
    }

    s32Ret = HI_MPI_VENC_StartRecvPic(veChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_StartRecvPic [%d] error %#x! \n", veChn,s32Ret);
        return false;
    }

    return true;
}

bool HimppMaster::VencSnapDestroy(VENC_CHN veChn)
{
    HI_S32 s32Ret;
    s32Ret = SAMPLE_COMM_VENC_Stop(veChn);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("SAMPLE_COMM_VENC_Stop failed, err:0x%x!\n", s32Ret);
    }

    s32Ret = HI_MPI_VENC_DestroyChn(veChn);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", \
                   veChn, s32Ret);
    }
    SAMPLE_PRT("himpp venc snap module destroy succeed!\n");

    return true;
}

#if 0
bool HimppMaster::Venc1ModuleInit(SIZE_S stVencSize, SIZE_S stMinor1Size, SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp)
{
    HI_S32 s32Ret,i;
    stVenc1Size_[0] = stVencSize;
    stVenc1Size_[1] = stMinor1Size;
    stVenc1Size_[2] = stMinor2Size;

    // 编解码后用来在屏幕上输出的(主码流)
    for(i = 0;i<Venc1StreamCnt-VencStreamCnt;i++){
        printf("venc_chn%d,w=%d,h=%d\n",i+VencStreamCnt,stVencSize_[i].u32Width,stVencSize_[i].u32Height);
        s32Ret = HI35xx_COMM_VENC_Start(VencChn_[i+VencStreamCnt], enType[i], stMaxSize_, stVenc1Size_[i], enRcMode[i], u32Profile[i],
                u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i]);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[i+VencStreamCnt]);
            return false;
        }
    }

    // 用于预览抓图
    VencSnapChn_[1] = VencChn_[Venc3StreamCnt+1];
    printf("snap_chn%d,w=%d,h=%d\n",Venc3StreamCnt+1,stSnapSize.u32Width,stSnapSize.u32Height);
    s32Ret = HI35xx_COMM_VENC_Start(VencSnapChn_[1], PT_JPEG, stMaxSize_, stSnapSize, (SAMPLE_RC_E)0, 0,
            0, 0, 0,
            0, 0, 0,
            0, 0, 0, 0, 0, 1);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("Start VencSnapChn[%d] failed!\n", VencSnapChn_[1]);
        return false;
    }

    return true;
}

bool HimppMaster::Venc2ModuleInit(SIZE_S stVencSize, SIZE_S stMinor1Size, SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp)
{
    HI_S32 s32Ret,i;
    stVenc2Size_[0] = stVencSize;
    stVenc2Size_[1] = stMinor1Size;
    stVenc2Size_[2] = stMinor2Size;

    // 编解码后用来在屏幕上输出的(主码流)
    for(i = 0;i<Venc2StreamCnt-Venc1StreamCnt;i++){
        printf("venc_chn%d,w=%d,h=%d\n",i+Venc1StreamCnt,stVencSize_[i].u32Width,stVencSize_[i].u32Height);
        s32Ret = HI35xx_COMM_VENC_Start(VencChn_[i+Venc1StreamCnt], enType[i], stMaxSize_, stVenc1Size_[i], enRcMode[i], u32Profile[i],
                u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i]);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[i+Venc1StreamCnt]);
            return false;
        }
    }

    // 用于预览抓图
    VencSnapChn_[2] = VencChn_[Venc3StreamCnt+2];
    printf("snap_chn%d,w=%d,h=%d\n",Venc3StreamCnt+2,stSnapSize.u32Width,stSnapSize.u32Height);
    s32Ret = HI35xx_COMM_VENC_Start(VencSnapChn_[2], PT_JPEG, stMaxSize_, stSnapSize, (SAMPLE_RC_E)0, 0,
            0, 0, 0,
            0, 0, 0,
            0, 0, 0, 0, 0, 2);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("Start VencSnapChn[%d] failed!\n", VencSnapChn_[2]);
        return false;
    }

    return true;
}

bool HimppMaster::Venc3ModuleInit(SIZE_S stVencSize, SIZE_S stMinor1Size, SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp)
{
    HI_S32 s32Ret,i;
    stVenc3Size_[0] = stVencSize;
    stVenc3Size_[1] = stMinor1Size;
    stVenc3Size_[2] = stMinor2Size;

    // 编解码后用来在屏幕上输出的(主码流)
    for(i = 0;i<Venc3StreamCnt-Venc2StreamCnt;i++){
        printf("venc_chn%d,w=%d,h=%d\n",i+Venc2StreamCnt,stVencSize_[i].u32Width,stVencSize_[i].u32Height);
        s32Ret = HI35xx_COMM_VENC_Start(VencChn_[i+Venc2StreamCnt], enType[i], stMaxSize_, stVenc1Size_[i], enRcMode[i], u32Profile[i],
                u32Gop[i], u32BitRate[i], u32SrcFrmRate,
                u32DstFrameRate[i], u32MinQp[i], u32MaxQp[i],
                u32MinIQp[i], u32IQp[i], u32PQp[i], u32BQp[i], u32MinIProp[i], u32MaxIProp[i]);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("Start Vencchn[%d] failed!\n", VencChn_[i+Venc2StreamCnt]);
            return false;
        }
    }

    // 用于预览抓图
    VencSnapChn_[3] = VencChn_[Venc3StreamCnt+3];
    printf("snap_chn%d,w=%d,h=%d\n",Venc3StreamCnt+3,stSnapSize.u32Width,stSnapSize.u32Height);
    s32Ret = HI35xx_COMM_VENC_Start(VencSnapChn_[3], PT_JPEG, stMaxSize_, stSnapSize, (SAMPLE_RC_E)0, 0,
            0, 0, 0,
            0, 0, 0,
            0, 0, 0, 0, 0, 3);
    if (HI_SUCCESS != s32Ret){
        COMMON_PRT("Start VencSnapChn[%d] failed!\n", VencSnapChn_[3]);
        return false;
    }

    return true;
}

#endif

bool HimppMaster::VoModeuleInit()
{

    HI_S32 s32Ret;
    VO_PUB_ATTR_S stVoPubAttr;
    VO_VIDEO_LAYER_ATTR_S stLayerAttr;

    stVoPubAttr.enIntfSync = VO_OUTPUT_1080P60;
    stVoPubAttr.enIntfType = VO_INTF_HDMI;
    stVoPubAttr.u32BgColor = 0x009632;

    s32Ret = SAMPLE_COMM_VO_StartDev(VoDev_, &stVoPubAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_StartDev failed!\n");
        return false;
    }

    memset(&(stLayerAttr), 0 , sizeof(VO_VIDEO_LAYER_ATTR_S));
    s32Ret = SAMPLE_COMM_VO_GetWH(stVoPubAttr.enIntfSync, \
                                  &stLayerAttr.stImageSize.u32Width, \
                                  &stLayerAttr.stImageSize.u32Height, \
                                  &stLayerAttr.u32DispFrmRt);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_GetWH failed!\n");
        return false;
    }
    stLayerAttr.enPixFormat = SAMPLE_PIXEL_FORMAT;
    stLayerAttr.stDispRect.s32X       = 0;
    stLayerAttr.stDispRect.s32Y       = 0;
    stLayerAttr.stDispRect.u32Width   = stLayerAttr.stImageSize.u32Width;
    stLayerAttr.stDispRect.u32Height  = stLayerAttr.stImageSize.u32Height;
    s32Ret = SAMPLE_COMM_VO_StartLayer(VoLayer_, &stLayerAttr);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_StartLayer failed!\n");
        return false;
    }

    if (stVoPubAttr.enIntfType & VO_INTF_HDMI){
        if (HI_SUCCESS != SAMPLE_COMM_VO_HdmiStart(stVoPubAttr.enIntfSync)){
            COMMON_PRT("Start SAMPLE_COMM_VO_HdmiStart failed!\n");
            return false;
        }
    }

    s32Ret = HI35xx_COMM_VO_StartChn(VoLayer_, VoChn_);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("Start SAMPLE_COMM_VO_StartChn failed!\n");
        return false;
    }

    return true;
}

bool HimppMaster::BindPreViewSys()
{
    /*if(osd_enable[0] == 0){
        if (HI_SUCCESS != HI35xx_COMM_VI_BindVenc(ViDev_[0], VencChn_[0], ViChn_[0]))
        {
            SAMPLE_PRT("Vi%d bind Venc failed!\n",ViDev_[0]);
            return false;
        }
    }*/
    VPSS_GRP VpssGrp = 0;
    if (HI_SUCCESS != HI35xx_COMM_VI_BindVpss(ViDev_[0], VpssGrp, ViChn_[0]))
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencChn_[1], VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencSnapChn_[0], VpssGrp, VPSS_CHN1)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    /*if(osd_enable[1] == 0){
        if (HI_SUCCESS != HI35xx_COMM_VI_BindVenc(ViDev_[1], VencChn_[2], ViChn_[1]))
        {
            SAMPLE_PRT("Vi%d bind Venc failed!\n",ViDev_[1]);
            return false;
        }
    }*/
    VpssGrp = 1;
    if (HI_SUCCESS != HI35xx_COMM_VI_BindVpss(ViDev_[1], VpssGrp, ViChn_[1]))
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencChn_[3], VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencSnapChn_[1], VpssGrp, VPSS_CHN1)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    /*if(osd_enable[2] == 0){
        if (HI_SUCCESS != HI35xx_COMM_VI_BindVenc(ViDev_[2], VencChn_[4], ViChn_[2]))
        {
            SAMPLE_PRT("Vi%d bind Venc failed!\n",ViDev_[2]);
            return false;
        }
    }*/
    VpssGrp = 2;
    if (HI_SUCCESS != HI35xx_COMM_VI_BindVpss(ViDev_[2], VpssGrp, ViChn_[2]))
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencChn_[5], VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencSnapChn_[2], VpssGrp, VPSS_CHN1)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    /*if(osd_enable[3] == 0){
        if (HI_SUCCESS != HI35xx_COMM_VI_BindVenc(ViDev_[3], VencChn_[6], ViChn_[3]))
        {
            SAMPLE_PRT("Vi%d bind Venc failed!\n",ViDev_[3]);
            return false;
        }
    }*/
    VpssGrp = 3;
    if (HI_SUCCESS != HI35xx_COMM_VI_BindVpss(ViDev_[3], VpssGrp, ViChn_[3]))
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencChn_[7], VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(VencSnapChn_[3], VpssGrp, VPSS_CHN1)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    return true;
}

/*
 * VI0 -- VPSS 0 -- VENC 8
 * VI1 -- VPSS 1 -- VENC 9
 * VI2 -- VPSS 2 -- VENC 10
 * VI3 -- VPSS 3 -- VENC 11
 * */

bool HimppMaster::SnapBindPreViewSys(VI_DEV viDev,VI_CHN viChn)
{
    VPSS_GRP  VpssGrp = viDev;
    if (HI_SUCCESS != HI35xx_COMM_VI_BindVpss(viDev, VpssGrp, viChn))
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_BindVpss(8+viDev, VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    return true;
}

bool HimppMaster::SnapUNBindPreViewSys(VI_DEV viDev,VI_CHN viChn)
{
    VPSS_GRP  VpssGrp = viDev;
    if (HI_SUCCESS != HI35xx_COMM_VI_UnBindVpss(viDev, VpssGrp, viChn))
    {
        SAMPLE_PRT("Vi bind Vpss failed!\n");
        return false;
    }
    if (HI_SUCCESS != SAMPLE_COMM_VENC_UnBindVpss(8+viDev, VpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    return true;
}

bool HimppMaster::SnapUNBindVpss(VENC_CHN snapVenc)
{
    VPSS_GRP vpssGrp = snapVenc - 8;
    if (HI_SUCCESS != SAMPLE_COMM_VENC_UnBindVpss(snapVenc, vpssGrp, VPSS_CHN0)){
        SAMPLE_PRT("Venc bind Vpss failed!\n");
        return false;
    }

    return true;

}



int FpsPoolCnt[4] = {60,60,60,60};
bool bFpsPool[60][4];
int last_gframeID[4] = {0,0,0,0};
int freq[4];
int local_tick[4] = {0,0,0,0}, err_count[4] = {0,0,0,0};

HI_U32 get_pts(int vi_port){

    freq[vi_port] = viParam[vi_port].u32SrcFrmRate;
    for(int i = 0; i < FpsPoolCnt[vi_port]; i++)
        bFpsPool[vi_port][i] = false;

    double frame_step = (FpsPoolCnt[vi_port] * 1.0) / freq[vi_port];
    double drop_tick = 0.0;
    int fps_id = 0;
    for(int i = 0; i < freq[vi_port]; i++){
        bFpsPool[vi_port][fps_id] = true;
        drop_tick += frame_step;
        fps_id = min(59, (int)(drop_tick + 0.5));
    }

    int closest = 0;
    for(int i = 0; i < FpsPoolCnt[vi_port] / 2; i++){
        //NOTE: 往后查找 i 位
        if(bFpsPool[vi_port][(glFrameID + i) % FpsPoolCnt[vi_port]]){

            closest = (glFrameID + i) % FpsPoolCnt[vi_port];
            break;
        }

        //NOTE: 往前查找 i 位
        if(bFpsPool[vi_port][(glFrameID + FpsPoolCnt[vi_port] - i) % FpsPoolCnt[vi_port]]){

            closest = (glFrameID + FpsPoolCnt[vi_port] - i) % FpsPoolCnt[vi_port];
            break;
        }
    }

    // NOTE: 在池子中查找 上次使用的id 的下一个可用id
    int lastnext = 0;
    for(int i = 1; i < FpsPoolCnt[vi_port]; i++){
        if(bFpsPool[vi_port][(i + local_tick[vi_port]) % FpsPoolCnt[vi_port]]){
            lastnext = (i + local_tick[vi_port]) % FpsPoolCnt[vi_port];
            break;
        }
    }

    if(last_gframeID[vi_port]!= glFrameID){
        int date1 = abs(lastnext - closest);
        int date2 = 60 - date1;
        if(date1 > (2 * 60 / freq[vi_port]) &&
           date2 > (2 * 60 / freq[vi_port])){
            lastnext = closest;
        }
    }
    //printf("---------------------------c: %d, l: %d\n", closest, lastnext);
    last_gframeID[vi_port] = glFrameID;

    local_tick[vi_port] = lastnext;
    return local_tick[vi_port];

}

bool HimppMaster::StartLoop(HI_U32 streamstart, HI_U32 streamend, HI_U32 vi_port)
{

    HI_S32 s32Ret,packlen;
    HI_S32 maxfd = 0;
    HI_S32 j,i;
    HI_U8 *pu8Addr = NULL;

    VIDEO_FRAME_INFO_S stFrame;
    HI_U64 sys_pts = 0;

    HI_S32 VencFd[16];
    for(i = streamstart; i < streamend; i++){
        VencFd[i] = HI_MPI_VENC_GetFd(VencChn_[i]);
        if (VencFd[i] < 0){
            SAMPLE_PRT("HI_MPI_VENC_GetFd failed with %#x!\n", VencFd[i]);
            return false;
        }
        if (maxfd <= VencFd[i])
        {
            maxfd = VencFd[i];
        }
        printf("venc=%d, maxfd=%d,vencFd=%d\n",VencChn_[i],maxfd,VencFd[i]);
    }

    fd_set read_fds;
    VENC_CHN_STAT_S stStat;
    VENC_STREAM_S stVencStream;
    struct timeval TimeoutVal;
    pu8Addr = (HI_U8*)malloc(stMaxSize_.u32Width*stMaxSize_.u32Height*3/2);

    HI_U16 frame_number[VD_CHN_MAX] = {0};
    memset(frame_number,0,sizeof(frame_number));
    char frame_data[INFO_HEADER + PACK_LEN];
    memset(frame_data,0, (INFO_HEADER + PACK_LEN));
    frame_data[INFO_HEADER_H0] = 'S';
    frame_data[INFO_HEADER_H1] = 'L';
    frame_data[INFO_HEADER_VA] = 'V';


    UDPSocket *MultiSendSocket = new UDPSocket();
    int socket = MultiSendSocket->CreateUDPClient(nullptr,0, false); //设为阻塞
    MultiSendSocket->BindLocalAddr(ETH0);

    setsockopt(socket, IPPROTO_IP, IP_MULTICAST_TTL, (char *)&ttl, sizeof(ttl));
    int value = (PACK_LEN + INFO_HEADER)*16;
    setsockopt(socket, SOL_SOCKET, SO_SNDBUF,(char *)&value, sizeof(value));

    while(pth_vi_net[vi_port]){
        if(vi_src_chang[vi_port]) {         //输入源改变分辨率 需要重启VI VENC则暂停VENC GET
            usleep(10000);
            continue;
        }

        FD_ZERO(&read_fds);
        for (i = streamstart; i < streamend; i++){
            FD_SET(VencFd[i], &read_fds);
        }
        TimeoutVal.tv_sec  = 1;
        TimeoutVal.tv_usec = 0;
        s32Ret = select(maxfd + 1, &read_fds, NULL, NULL, &TimeoutVal);
        if (s32Ret < 0){
            SAMPLE_PRT("select failed!\n");
            break;
        }
        else if (s32Ret == 0){
            SAMPLE_PRT("get venc%d stream time out,%d exit thread\n",maxfd,vi_port);
            continue;
        }
        else{
            for(i = streamstart; i < streamend; i++){
                if (FD_ISSET(VencFd[i], &read_fds)){
                    /***************************************************
                        step 2.1 : query how many packs in one-frame stream.
                        ***************************************************/
                    s32Ret = HI_MPI_VENC_Query(VencChn_[i], &stStat);
                    if (HI_SUCCESS != s32Ret){
                        //SAMPLE_PRT("HI_MPI_VENC_Query chn[%d] failed with %#x!\n", VencChn_[i], s32Ret);
                        break;
                    }
                    if(0 == stStat.u32CurPacks){
                        SAMPLE_PRT("NOTE: Current frame is NULL!\n");
                        continue;
                    }
                    /***************************************************
                        step 2.3 : malloc corresponding number of pack nodes.
                        ***************************************************/


                    memset(&stVencStream, 0, sizeof(stVencStream));
                    stVencStream.pstPack = (VENC_PACK_S*)malloc(sizeof(VENC_PACK_S) * stStat.u32CurPacks);
                    if (NULL == stVencStream.pstPack){
                        SAMPLE_PRT("malloc stream pack failed!\n");
                        break;
                    }
                    /***************************************************
                        step 2.4 : call mpi to get one-frame stream
                        ***************************************************/
                    stVencStream.u32PackCount = stStat.u32CurPacks;
                    s32Ret = HI_MPI_VENC_GetStream(VencChn_[i], &stVencStream, 1);
                    if (HI_SUCCESS != s32Ret){
                        free(stVencStream.pstPack);
                        stVencStream.pstPack = NULL;
                        SAMPLE_PRT("HI_MPI_VENC_GetStream %d failed with %#x!\n", VencChn_[i], s32Ret);
                        break;
                    }

                    HI_U32 u32Len = 0;

                    for (j = 0; j < stVencStream.u32PackCount; j++){
                        memcpy(pu8Addr + u32Len,
                               stVencStream.pstPack[j].pu8Addr + stVencStream.pstPack[j].u32Offset,
                               stVencStream.pstPack[j].u32Len - stVencStream.pstPack[j].u32Offset);
                        u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);
                    }


                    packlen = u32Len;
                    bool i_frame = false;
                    if((*((char*)pu8Addr+4) == 0x67)||(*((char*)pu8Addr+4) == 0x40)){
                        i_frame = true;
                    }
#ifdef __TINY_RTSP_SERVER__
                    g_prtspserver->push_live_video_data( g_stream[i], (void*)pu8Addr, u32Len, stVencStream.pstPack[0].u64PTS/1000 );
#else

                    HI_MPI_SYS_GetCurPts(&sys_pts);
                    crtsps_pushvideo( g_stream[i], (void*)pu8Addr, u32Len, true, sys_pts/1000 );

#endif
                    int vi_type = 0;
                    if(i == (streamend -1))
                        vi_type  = 1;
                    else
                        vi_type = 0;

                    if(viParam[vi_port].mcast_enable[vi_type]){

                        //写入265标识
                        int video_type = 0;
                        if(viParam[vi_port].enType[vi_type] == PT_H265)
                            video_type = 2;


                        if( vi_type == 0)
                        {
                            frame_data[INFO_HEADER_WW] = viParam[vi_port].stVencSize.u32Width;
                            frame_data[INFO_HEADER_HH] = viParam[vi_port].stVencSize.u32Height;
                        }
                        else
                        {
                            frame_data[INFO_HEADER_WW] = viParam[vi_port].stMinorSize.u32Width;
                            frame_data[INFO_HEADER_HH] = viParam[vi_port].stMinorSize.u32Height;
                        }
                        frame_data[INFO_HEADER_VE] = i&0xFF;


                        if(frame_number[i] > 255)
                            frame_number[i] = 0;
                        frame_data[INFO_HEADER_SN] = frame_number[i];
                        frame_number[i]++;

                        if(u32Len%PACK_LEN > 0)
                            *(HI_U16*)&frame_data[INFO_HEADER_TP] = u32Len/PACK_LEN + 1;
                        else
                            *(HI_U16*)&frame_data[INFO_HEADER_TP] = u32Len/PACK_LEN;


                        frame_data[INFO_HEADER_FG] = i_frame | video_type;
                        frame_data[INFO_HEADER_FQ] = viParam[vi_port].u32DstFrameRate[vi_type];
                        frame_data[INFO_HEADER_FD] = stVencStream.pstPack[0].u64PTS >> 56;

//                                                printf("vi:%d  frameRate:%d frameId:%d\n",vi_port,frame_data[INFO_HEADER_FQ],frame_data[INFO_HEADER_FD]);

                        *(HI_U16*)&frame_data[INFO_HEADER_CP] = 0;
                        char* bkup_data = (char*)pu8Addr;
                        while (packlen > 0){
                            memcpy(&frame_data[INFO_HEADER], bkup_data, min(PACK_LEN,packlen));
                            bkup_data += min(PACK_LEN,packlen);
                            MultiSendSocket->SendTo(
                                        viParam[vi_port].mcast_ip[vi_type],
                                        viParam[vi_port].mcast_port[vi_type],
                                        (char*)frame_data, min(PACK_LEN,packlen) + INFO_HEADER);
                            packlen -= min(PACK_LEN,packlen);
                            (*(HI_U16*)&frame_data[INFO_HEADER_CP]) ++;                              //指向下一包
                        }

                    }

                    /***************************************************
                            step 2.7 : free pack nodes
                            ***************************************************/
                    s32Ret = HI_MPI_VENC_ReleaseStream(VencChn_[i], &stVencStream);
                    if (HI_SUCCESS != s32Ret){
                        printf("free venc%d failed %#x\n",VencChn_[i],s32Ret);
                        free(stVencStream.pstPack);
                        stVencStream.pstPack = NULL;
                    }
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                }
                //gettimeofday(&stop, 0);
                // tim_subtract(&diff, &start, &stop);
                //printf("venc %d,diff.tv_sec: %d, diff.tv_usec: %d\n", VencChn_[i],diff.tv_sec, diff.tv_usec);
            }
        }
    }

    free(pu8Addr);
    pu8Addr = NULL;
    printf("thread venc to net :%d  end\n",streamstart);
    return true;
}


bool HimppMaster::viGetToVenc(HI_U32 vi_port)
{
    printf("======start %d vi to venc======\n",vi_port);
    HI_U32 viPort = vi_port;
    HI_S32 s32Ret;
    VIDEO_FRAME_INFO_S stFrame;
    HI_S32 s32MilliSec = -1;
    POINT_S chrPoint;
    HI_U32 charOffset = 6;
    HI_U32 bmp_max_height = 0;
    int osdname_len = 0;
    HI_U32 osd_max_horiBearingY = 0;
    HI_U32 vi_addr[6];
    VPSS_GRP ve_min_vpss = vi_port, ve_snap_vpss = 4 + vi_port, vi_loop = 8;
    int n = 0;
    MyTimer *t1 = new MyTimer();

    for(int i = 0; i < 7; i++) {
        vi_addr[i] = vicap_base_reg + vi_offset_addr[vi_port][i];
        printf("0x%x ",vi_addr[i]);
    }
    printf("\n");


    if(HI_TRUE == viParam[vi_port].osd_enable) {
        osdname_len = set_font(vi_port,&bmp_max_height);
        for(int i = 0; i < osdname_len; i++) {
            if(chnBmp[vi_port][i].horiBearingY > osd_max_horiBearingY)
                osd_max_horiBearingY = chnBmp[vi_port][i].horiBearingY;
        }
    }
    while(pth_vi_net[vi_port]) {
//        if(0 == vi_port)
//            t1->Start();

        if(vi_src_chang[vi_port]) {         //输入源改变分辨率 需要重启VI VENC则暂停VI GET
            usleep(10000);
            printf("1\n");
            continue;
        }
        s32Ret = HI_MPI_VI_GetFrame(ViChn_[vi_port], &stFrame, s32MilliSec);
        if (HI_SUCCESS != s32Ret)
        {
            printf("get vi frame err:0x%x\n", s32Ret);
            continue;
        }

//        if(0 == vi_port) {
//            t1->Stop();
//            t1->ShowCostTime();
//            continue;
//        }



#if 1
        HI_U32 pu32Value;
//        if(0 ==  viPort)
//            printf("yaddr_0x%x \n",stFrame.stVFrame.u32PhyAddr[0]);
        for(n = 0; n < 7; n++) {
#ifdef __EDC_VENC__
            if(4 == n)
                n++;
#else
            if(0 == i)
                continue;
#endif
            Hi_GetReg(vi_addr[n], &pu32Value);

//            if(0 == viPort) {
//                printf("%x 0x%x\n",n,pu32Value);
//            }

            if((pu32Value & 0xFFFFFF00) == stFrame.stVFrame.u32PhyAddr[0]){
                pts = ((pu32Value & 0xFF) + 3 + 60) % 60;
                Hi_SetReg(vi_addr[n], 0);
//                if(vi_port == 0)
//                    printf("%d yaddr ;0x%x get 0x%x pts :%d\n",vi_port,stFrame.stVFrame.u32PhyAddr[0],vi_addr[n],pts);
                break;
            }
        }
#endif
        stFrame.stVFrame.u64pts &= 0xFFFFFFFFFFFFFF;
        stFrame.stVFrame.u64pts |= (HI_U64)pts << 56;

        if(vi_flag[vi_port]) {          //输入是否有信号
            if(viParam[vi_port].osd_enable) {           //OSD

                if(osd_chng[vi_port]) {            //commandMap  动态修改了OSD
                    osdname_len = set_font(vi_port,&bmp_max_height);
                    osd_max_horiBearingY = 0;
                    for(uint i = 0 ; i < osdname_len; i++) {
                        if(chnBmp[vi_port][i].horiBearingY > osd_max_horiBearingY)
                            osd_max_horiBearingY = chnBmp[vi_port][i].horiBearingY;
                    }
                    osd_chng[vi_port] = false;
                }

                stFrame.stVFrame.pVirAddr[0] = HI_MPI_SYS_Mmap(stFrame.stVFrame.u32PhyAddr[0], stFrame.stVFrame.u32Width*stFrame.stVFrame.u32Height);
                stFrame.stVFrame.pVirAddr[1] = HI_MPI_SYS_Mmap(stFrame.stVFrame.u32PhyAddr[1], stFrame.stVFrame.u32Width*stFrame.stVFrame.u32Height/2);

                for(int n = 0; n < osdname_len; n++) {
                    if(0 != n) {
                        if((testData[vi_port][n]&0xffff) == 0x0020)//空格
                            chrPoint.s32X += chnBmp[vi_port][n-1].width + 30;
                        else
                            chrPoint.s32X +=  chnBmp[vi_port][n-1].width + charOffset;
                    }
                    else
                        chrPoint.s32X = startPoint[vi_port].s32X;

                    chrPoint.s32Y = startPoint[vi_port].s32Y + osd_max_horiBearingY - chnBmp[vi_port][n].horiBearingY;

                    if(((HI_U32)(chrPoint.s32X + chnBmp[vi_port][n].width) <= stViSize_[vi_port].u32Width)&&((HI_U32)(chrPoint.s32Y) <= stViSize_[vi_port].u32Height))
                        putTextFrame(chnBmp[vi_port][n], chrPoint, &stFrame, charColor[vi_port], false, bmp_max_height);

                }

                HI_MPI_SYS_Munmap(stFrame.stVFrame.pVirAddr[0], stFrame.stVFrame.u32Width*stFrame.stVFrame.u32Height);
                HI_MPI_SYS_Munmap(stFrame.stVFrame.pVirAddr[1], stFrame.stVFrame.u32Width*stFrame.stVFrame.u32Height/2);
            }
        }

        s32Ret = HI_MPI_VPSS_SendFrame(ve_min_vpss, &stFrame, s32MilliSec);   // 次码流
        if (HI_SUCCESS != s32Ret)
        {
            printf("VPSS Send Frame err:0x%x\n",s32Ret);
        }

        if(!snap_vh_chang[vi_port]) {
            s32Ret = HI_MPI_VPSS_SendFrame(ve_snap_vpss, &stFrame, s32MilliSec);   //抓图
            if (HI_SUCCESS != s32Ret)
            {
                printf("VPSS Send Frame err:0x%x\n",s32Ret);
            }
        }
#ifdef __EDC_VENC__
        if( 0 == vi_port) {
            s32Ret = HI_MPI_VPSS_SendFrame( vi_loop, &stFrame, s32MilliSec);   //抓图
            if (HI_SUCCESS != s32Ret)
            {
                printf("VPSS Send Frame err:0x%x\n",s32Ret);
            }
        }
#endif

        s32Ret = HI_MPI_VENC_SendFrame(vi_port*2, &stFrame, s32MilliSec);
        if (HI_SUCCESS != s32Ret)
        {
            printf("VencChn_[%d] Send Frame err:0x%x\n",0, s32Ret);
        }

        s32Ret = HI_MPI_VI_ReleaseFrame(ViChn_[vi_port], &stFrame);
        if (HI_SUCCESS != s32Ret)
        {
            printf("---release vi frame err:0x%x---\n", s32Ret);
        }

    }

    if(viParam[vi_port].osd_enable == HI_TRUE){
        for(int n = 0; n < osdname_len; n++){
            free(chnBmp[vi_port][n].buffer);
            chnBmp[vi_port][n].buffer = NULL;
        }
        free(chnBmp[vi_port]);
        chnBmp[vi_port] = NULL;
    }

    freetype_exit(vi_port);
    HI_MPI_VI_ReleaseFrame(ViChn_[vi_port], &stFrame);
    printf("---> viget %d pthread end <---\n",vi_port);

    return true;
}

/**
@brief      抓取对应JPEG编码通道的数据，更具抓图组播或者JPEG发送对应的JPEG数据
@param      [in]SnapChn  编码通道
@param      [in]socket
@return
@note       说明:
@par        示例:
@author     xujh
 **/
bool HimppMaster::GetVencRateSnap(HI_U32 SnapChn, UDPSocket *socket,HI_U32 tcpflag)
{
    /* Set Venc Fd. */
    HI_S32 s32Ret;
    //HI_U8 *pu8Data = NULL;
    HI_S32 VencFd = HI_MPI_VENC_GetFd(SnapChn);
    if (VencFd < 0){
        COMMON_PRT("HI_MPI_VENC_GetFd failed with %#x!\n", VencFd);
        return false;
    }
    int vi_port  = (SnapChn - 8);
    fd_set read_fds;
    VENC_CHN_STAT_S stStat;
    VENC_STREAM_S stVencStream;
    struct timeval TimeoutVal;
    HI_U8 *pu8Data = (HI_U8*)malloc(1920*1080*3);


    while(pth_snap[vi_port]){
        if(vi_src_chang[vi_port] || snap_vh_chang[vi_port]) {         //输入源改变分辨率 需要重启VI VENC则暂停VENC GET
            usleep(10000);
            continue;
        }

        FD_ZERO(&read_fds);
        FD_SET(VencFd, &read_fds);
        TimeoutVal.tv_sec  = 1;
        TimeoutVal.tv_usec = 0;
        s32Ret = select(VencFd + 1, &read_fds, NULL, NULL, &TimeoutVal);
        if (s32Ret < 0){
            COMMON_PRT("select failed!\n");
            continue;
        }
        else if (s32Ret == 0){
            COMMON_PRT("snap select timeout\n");
            continue;
        }
        else{
            if (FD_ISSET(VencFd, &read_fds)){
                //  snap_running = HI_TRUE;
                s32Ret = HI_MPI_VENC_Query( SnapChn,  &stStat);
                if (HI_SUCCESS != s32Ret){
                    COMMON_PRT("HI_MPI_VENC_Query chn[%d] failed with %#x!\n",  SnapChn, s32Ret);
                    sleep(10);
                    continue;
                }
                if(0 == stStat.u32CurPacks){
                    COMMON_PRT("NOTE: Current frame chn=%d is NULL!\n",SnapChn);
                    continue;
                }

                memset(&stVencStream, 0, sizeof(stVencStream));
                stVencStream.pstPack = (VENC_PACK_S*)malloc(sizeof(VENC_PACK_S) * stStat.u32CurPacks);
                if (NULL == stVencStream.pstPack){
                    COMMON_PRT("malloc stream pack failed!\n");
                    break;
                }

                stVencStream.u32PackCount = stStat.u32CurPacks;

                s32Ret = HI_MPI_VENC_GetStream(SnapChn, &stVencStream, -1);
                if (HI_SUCCESS != s32Ret){
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                }

                HI_U32 u32Len = 0;
                for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++)
                    u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);


                u32Len = 0;
                for (HI_U32 j = 0; j < stVencStream.u32PackCount; j++){
                    memcpy(pu8Data + u32Len,
                           stVencStream.pstPack[j].pu8Addr + stVencStream.pstPack[j].u32Offset,
                           stVencStream.pstPack[j].u32Len - stVencStream.pstPack[j].u32Offset);
                    u32Len += (stVencStream.pstPack[j].u32Len-stVencStream.pstPack[j].u32Offset);
                }

//                if(0 == vi_port)
//                    printf("get jpeg\n");

                if(viParam[vi_port].Snapmcast_enable) {     //使能组播抓图
                    this->SendSnap(pu8Data, u32Len, viParam[vi_port].SnapLen, socket, viParam[vi_port].Snapmcast_ip,viParam[vi_port].Snapmcast_port,viParam[vi_port].SnapDelay,HI_TRUE,vi_port);
                }
                else {
//                    if(0 == vi_port)
//                        printf("snap:%d\n",vi_port);
                    if(jpeg_flag[vi_port]) {
                        this->SendSnap(pu8Data,u32Len,viParam[vi_port].SnapLen,socket,snap_jpeg_ip[vi_port],snap_jpeg_port[vi_port],viParam[vi_port].SnapDelay,HI_FALSE,vi_port);
                        jpeg_flag[vi_port] = false;
                    }
                }

                s32Ret = HI_MPI_VENC_ReleaseStream(SnapChn, &stVencStream);
                if (HI_SUCCESS != s32Ret){
                    free(stVencStream.pstPack);
                    stVencStream.pstPack = NULL;
                    printf("venc release failed--->0x%x\n",s32Ret);
                    break;
                }
                free(stVencStream.pstPack);
            }
        }
    }
    free(pu8Data);

    pu8Data = NULL;
    return true;
}

HI_S16 audio_left[4] = {0,0,0,0}, audio_right[4] = {0,0,0,0};

bool HimppMaster::SendSnap(HI_U8 *pu8Data, HI_U32 u32TotalLen, HI_U32 u32PackLen, UDPSocket *socket, const char* clientip, const uint clientport,HI_S32 SnapDelay,HI_BOOL macsnap,HI_U32 Snap_chN)
{
    SNAP_DATA snapdata;
    HI_S32 sendlen  = 0;
    struct timeval start,stop,diff;

    *(short*)&pu8Data[u32TotalLen] = audio_left[Snap_chN];
    *(short*)&pu8Data[u32TotalLen+2] = audio_right[Snap_chN];


    if( macsnap == HI_TRUE)
    {
        u32TotalLen = u32TotalLen +4;
    }


    //    snapdata.pData = new HI_U8[u32PackLen];
    snapdata.u32TotalPack = (u32TotalLen + u32PackLen - 1) / u32PackLen; //总包数

    for(HI_U32 i = 0; i < snapdata.u32TotalPack; i++){
        //COMMON_PRT("u32TotalLen: %d, now: %d, totalPackNum: %d\n", u32TotalLen, i, snapdata.u32TotalPack);
        snapdata.u32CurrPack = i; //当前包序号
        memcpy(snapdata.pData, pu8Data, MIN2(u32PackLen, u32TotalLen));

        sendlen = socket->SendTo(clientip, clientport, (char*)&snapdata, MIN2((u32PackLen + 2 * sizeof(HI_U32)), (u32TotalLen+2 * sizeof(HI_U32))));
        //COMMON_PRT("snap sendto len: %d\n", sendlen);
        //COMMON_PRT("u32TotalLen: %d, now: %d, totalPackNum: %d     %d\n", u32TotalLen, i, snapdata.u32TotalPack,sendlen);
        u32TotalLen -= u32PackLen;
        pu8Data     += u32PackLen;
        gettimeofday(&start, 0);
        do{
            gettimeofday(&stop, 0);
            tim_subtract(&diff, &start, &stop);
            //printf("diff.tv_sec: %d, diff.tv_usec: %d,mcast_delay %d\n", diff.tv_sec, diff.tv_usec, mcast_delay[i]);
        }while(diff.tv_usec <  SnapDelay);
    }

    return true;
}






bool HimppMaster::SetUserPic(VI_CHN ViChn){

    if(SetNosignal)
        return false;
    SetNosignal =  true;
    HI_S32 s32Ret,a;
    VI_USERPIC_ATTR_S stUsrPic;
    stUsrPic.bPub = HI_TRUE;

    FILE* fBGFile = NULL;
#ifdef __EDC_VENC__
    fBGFile  = fopen("./picture.yuv","rb");

#else

    fBGFile  = fopen("/home/4in_input/picture.yuv","rb");
#endif
    if(fBGFile == NULL){
        stUsrPic.enUsrPicMode = VI_USERPIC_MODE_BGC;
        stUsrPic.unUsrPic.stUsrPicBg.u32BgColor = 0x009632;//0xC8C8C8;
    }else{
        stUsrPic.enUsrPicMode = VI_USERPIC_MODE_PIC;
        HI_U32 u32Width = 1920;
        HI_U32 u32Height = 1080;
        HI_U32 u32PicLStride;
        HI_U32 u32PicCStride;
        HI_U32 u32LumaSize;
        HI_U32 u32ChrmSize;
        HI_U32 u32Size;
        //VB_BLK VbBlk;
        HI_U32 u32PhyAddr;
        HI_U8 *pVirAddr;

        u32PicLStride = CEILING_2_POWER(u32Width, SAMPLE_SYS_ALIGN_WIDTH);
        u32PicCStride = CEILING_2_POWER(u32Width, SAMPLE_SYS_ALIGN_WIDTH);
        u32LumaSize = (u32PicLStride * u32Height);
        u32ChrmSize = (u32PicCStride * u32Height) >> 2;
        u32Size = u32LumaSize + (u32ChrmSize << 1);

        VbBlk = HI_MPI_VB_GetBlock(VB_INVALID_POOLID, u32Size, NULL);
        if (VB_INVALID_HANDLE == VbBlk)
        {
            printf("HI_MPI_VB_GetBlock error VB_INVALID_HANDLE\n");
            return -1;
        }
        /* get physical address*/
        u32PhyAddr = HI_MPI_VB_Handle2PhysAddr(VbBlk);
        if (0 == u32PhyAddr)
        {
            printf("HI_MPI_VB_Handle2PhysAddr error\n");
            return -1;
        }

        pVirAddr = (HI_U8 *) HI_MPI_SYS_Mmap( u32PhyAddr, u32Size );
        if(pVirAddr == NULL)
        {
            printf("Mem dev may not open\n");
            HI_MPI_VB_ReleaseBlock(VbBlk);
            sleep(6);
        }

        /* get pool id */
        stUsrPic.unUsrPic.stUsrPicFrm.u32PoolId = HI_MPI_VB_Handle2PoolId(VbBlk);
        if (VB_INVALID_POOLID == stUsrPic.unUsrPic.stUsrPicFrm.u32PoolId)
        {
            printf("HI_MPI_VB_Handle2PoolId error\n");
            return -1;
        }

        memset(&stUsrPic.unUsrPic.stUsrPicFrm.stVFrame, 0, sizeof(VIDEO_FRAME_S));
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[0] = u32PhyAddr;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[1] = stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[0] + u32LumaSize;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[2] = stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32PhyAddr[1] + u32ChrmSize;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[0] = pVirAddr;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[1] = stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[0] + u32LumaSize;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[2] = stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[1] + u32ChrmSize;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Width = u32Width;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Height = u32Height;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Stride[0] = u32PicLStride;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Stride[1] = u32PicCStride;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Stride[2] = u32PicCStride;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.enPixelFormat = SAMPLE_PIXEL_FORMAT;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.enVideoFormat  = VIDEO_FORMAT_LINEAR;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.enCompressMode = COMPRESS_MODE_NONE;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Field       = VIDEO_FIELD_FRAME;
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u64pts     = (a * 40);
        stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32TimeRef = (a * 2);

        a++;
        if(a >= 100)
            a=0;

        for(int i = 0; i < u32Height; i ++)
            fread((char*)(stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[0]) + stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Width * i, 1, 1920,fBGFile);

        for(int i = 0; i < u32Height/2; i ++)
            fread((char*)(stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.pVirAddr[1]) + stUsrPic.unUsrPic.stUsrPicFrm.stVFrame.u32Width * i, 1, 1920,fBGFile);

        fclose(fBGFile);
    }

    s32Ret = HI_MPI_VI_SetUserPic(ViChn_[ViChn], &stUsrPic); //目前此接口中的参数 VI 通道号未被使用，即设置用户图像针对的是所有 VI 通道，而不是具体某个 VI 通道
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VI_SetUserPic failed, err:0x%x!\n", s32Ret);
    }



    return true;
}

bool HimppMaster::EnableUserPic(VI_CHN ViChn)
{
    HI_S32 s32Ret;
    s32Ret = HI_MPI_VI_EnableUserPic(ViChn_[ViChn]); //启用插入的用户图片
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("HI_MPI_VI_EnableUserPic[%d] failed, err:0x%x!\n", ViChn_[ViChn], s32Ret);
        return false;
    }
    SAMPLE_PRT("HI_MPI_VI_EnableUserPic[%d] ok\n", ViChn_[ViChn]);
    return true;
}

bool HimppMaster::DisableUserPic(VI_CHN ViChn)
{
    HI_MPI_VI_DisableUserPic(ViChn_[ViChn]); //禁用插入的用户图片
    return true;
}

bool HimppMaster::StopViModule(VI_DEV ViDev, VI_CHN ViChn)
{
    HI_S32 s32Ret;
    s32Ret = HI_MPI_VI_DisableUserPic(ViChn_[ViChn]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VI_DisableUserPic failed with %#x\n",s32Ret);
        return false;
    }

    HI35xx_COMM_VI_Stop(ViDev_[ViDev], ViChn_[ViChn]);

    return true;
}

bool HimppMaster::VdecModuleDestroy(HI_U32 u32VdCnt)
{
    HI_S32 s32Ret;
    for (HI_U32 i = 0; i < u32VdCnt; i++){
        s32Ret = SAMPLE_COMM_VDEC_Stop(VdChn_[i]);
        if (HI_SUCCESS != s32Ret){
            SAMPLE_PRT("HI3536_COMM_VENC_SnapStop failed, err:0x%x!\n", s32Ret);
        }
    }

    SAMPLE_PRT("himpp vdec module destroy succeed!");
    return HI_SUCCESS;
}

bool HimppMaster::VencDestroy(VENC_CHN veChn_)
{
    HI_S32 s32Ret;
    s32Ret = SAMPLE_COMM_VENC_Stop(veChn_);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("SAMPLE_COMM_VENC_Stop [%d] failed, err:0x%x!\n", veChn_,s32Ret);
    }
    else {
        SAMPLE_PRT("venc [%d] stop ok\n",veChn_);
    }

    s32Ret = HI_MPI_VENC_DestroyChn(veChn_);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_DestroyChn [%d] failed with %#x!\n", \
                   veChn_, s32Ret);
    }

    return true;
}

bool HimppMaster::VencBindVpss(VENC_CHN veChn_, VPSS_GRP vpssGrp_,VPSS_CHN vpssChn_)
{
    HI_S32 s32Ret;
    s32Ret = SAMPLE_COMM_VENC_BindVpss(veChn_,vpssGrp_,vpssChn_);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("SAMPLE_COMM_VENC_BindVpss ve:[%d] vpssGrp:[%d] [%d]failed, err:0x%x!\n",   \
                   veChn_,vpssGrp_,vpssChn_,s32Ret);
    }

    return true;

}

bool HimppMaster::VencUnbindVpss(VENC_CHN veChn_, VPSS_GRP vpssGrp_,VPSS_CHN vpssChn_)
{
    HI_S32 s32Ret;
    s32Ret = SAMPLE_COMM_VENC_UnBindVpss(veChn_,vpssGrp_,vpssChn_);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("SAMPLE_COMM_VENC_UnBindVpss ve:[%d] vpssGrp:[%d] [%d]failed, err:0x%x!\n",   \
                   veChn_,vpssGrp_,vpssChn_,s32Ret);
    }

    return true;
}

bool HimppMaster::VencModuleDestroy(HI_U32 u32VeChnStart, HI_U32 u32VeChnCnt)
{
    HI_S32 s32Ret;

    for (HI_U32 i = u32VeChnStart; i < u32VeChnCnt; i++){
        if(((i < 8)&&ex_flag[i/2]) || ((i >=8)&&(ex_flag[i-8]))) {
            if(i%2) {
                SAMPLE_COMM_VENC_UnBindVpss(i,4+i/2,VPSS_CHN0);
            }

            s32Ret = SAMPLE_COMM_VENC_Stop(VencChn_[i]);
            if (HI_SUCCESS != s32Ret){
                SAMPLE_PRT("SAMPLE_COMM_VENC_Stop failed, err:0x%x!\n", s32Ret);
            }

            s32Ret = HI_MPI_VENC_DestroyChn(VencChn_[i]);
            if (HI_SUCCESS != s32Ret)
            {
                SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", \
                           VencChn_[i], s32Ret);
                sleep(1);
            }
        }

    }

    SAMPLE_PRT("himpp venc module destroy succeed!\n");
    return HI_SUCCESS;
}

bool HimppMaster::SnapModuleDestroy(HI_U32 VencSnapChn)
{
    HI_S32 s32Ret;

    s32Ret = SAMPLE_COMM_VENC_Stop(VencSnapChn_[VencSnapChn]);
    if (HI_SUCCESS != s32Ret){
        SAMPLE_PRT("SAMPLE_COMM_VENC_Stop failed, err:0x%x!\n", s32Ret);
    }
    s32Ret = HI_MPI_VENC_DestroyChn(VencSnapChn_[VencSnapChn]);
    if (HI_SUCCESS != s32Ret)
    {
        SAMPLE_PRT("HI_MPI_VENC_DestroyChn vechn[%d] failed with %#x!\n", \
                   VencSnapChn_[VencSnapChn], s32Ret);
        sleep(1);
    }
}

bool HimppMaster::retartVIVPSSVENC(HI_S32 vi_port,HI_BOOL viSignal,HI_BOOL viRestart)
{
    VENC_CHN veChn;
    VPSS_GRP vpssGrp;
    vi_src_chang[vi_port] = true;             //暂停VIGET  VENCPUSH SNAP
    usleep(20000);
    veChn = 2 * vi_port + 1;                  //视频次码流编码通道关闭
    vpssGrp = vi_port;
    this->VencUnbindVpss(veChn,vpssGrp);
    this->vpssStop(vpssGrp);
    this->VencDestroy(veChn);

    veChn = 8 + vi_port;                        //视频抓图编码通道关闭
    vpssGrp = 4 + vi_port;
    this->VencUnbindVpss(veChn,vpssGrp);
    this->vpssStop(vpssGrp);
    this->VencDestroy(veChn);

    veChn = 2 * vi_port;                      //视频主码流编码通道关闭
    this->VencDestroy(veChn);
    if(HI_TRUE ==viRestart) {
        this->StopViModule(vi_port,vi_port);
    }




    if(viParam[vi_port].venc_SAME_INPUT[0]) { //获取主编码和输入源一致的宽高
        viParam[vi_port].stVencSize.u32Width = viParam[vi_port].u32ViWidth;
        viParam[vi_port].stVencSize.u32Height = viParam[vi_port].u32ViHeigth;
        if(viParam[vi_port].vi_crop_enable) {
            viParam[vi_port].stVencSize.u32Width = viParam[vi_port].u32W;
            viParam[vi_port].stVencSize.u32Height = viParam[vi_port].u32H;
        }
    }
    else {                              //获取主编码UMP设置宽高
        if(viParam[vi_port].vi_crop_enable) {
            viParam[vi_port].stVencSize.u32Width = MIN(viParam[vi_port].stVencSize.u32Width, viParam[vi_port].u32W);
            viParam[vi_port].stVencSize.u32Height = MIN(viParam[vi_port].stVencSize.u32Height, viParam[vi_port].u32H);
        }
        else {
            viParam[vi_port].stVencSize.u32Width = MIN(viParam[vi_port].stVencSize.u32Width, viParam[vi_port].u32ViWidth);
            viParam[vi_port].stVencSize.u32Height = MIN(viParam[vi_port].stVencSize.u32Height, viParam[vi_port].u32ViHeigth);
        }
    }

    if(viParam[vi_port].venc_SAME_INPUT[1]) { //获取次编码和输入源一致的宽高
        viParam[vi_port].stMinorSize.u32Width = viParam[vi_port].u32ViWidth;
        viParam[vi_port].stMinorSize.u32Height = viParam[vi_port].u32ViHeigth;
        if(viParam[vi_port].vi_crop_enable) {
            viParam[vi_port].stMinorSize.u32Width = viParam[vi_port].u32W;
            viParam[vi_port].stMinorSize.u32Height = viParam[vi_port].u32H;
        }
    }
    else {                              //获取次编码UMP设置宽高
        if(viParam[vi_port].vi_crop_enable) {
            viParam[vi_port].stMinorSize.u32Width = MIN(viParam[vi_port].stMinorSize.u32Width, viParam[vi_port].u32W);
            viParam[vi_port].stMinorSize.u32Height = MIN(viParam[vi_port].stMinorSize.u32Height, viParam[vi_port].u32H);
        }
        else {
            viParam[vi_port].stMinorSize.u32Width = MIN(viParam[vi_port].stMinorSize.u32Width, viParam[vi_port].u32ViWidth);
            viParam[vi_port].stMinorSize.u32Height = MIN(viParam[vi_port].stMinorSize.u32Height, viParam[vi_port].u32ViHeigth);
        }
    }

    if(HI_TRUE == viRestart) {
        RECT_S vi_rect;
        if(viParam[vi_port].vi_crop_enable) {
            vi_rect.s32X = viParam[vi_port].u32X;
            vi_rect.s32Y = viParam[vi_port].u32Y;
            vi_rect.u32Width = viParam[vi_port].u32W;
            vi_rect.u32Height = viParam[vi_port].u32H;
        }
        else {
            vi_rect.s32X = 0;
            vi_rect.s32Y = 0;
            vi_rect.u32Width = viParam[vi_port].u32ViWidth;
            vi_rect.u32Height = viParam[vi_port].u32ViHeigth;
        }

        this->ViModuleInit(vi_port,SAMPLE_VI_MODE_8_1080P,vi_rect,viParam[vi_port].vi_model);
        if(viSignal == HI_FALSE) {
            while(1) {
                if(this->EnableUserPic(vi_port))
                    break;
                else
                    sleep(1);
            }
        }
    }

    this->VencModuleInit(viParam[vi_port].stVencSize, viParam[vi_port].stMinorSize, viParam[vi_port].stMinorSize, viParam[vi_port].stSnapSize,viParam[vi_port].enType, \
                                      viParam[vi_port].enRcMode, viParam[vi_port].u32Profile, viParam[vi_port].u32Gop, viParam[vi_port].u32BitRate, viParam[vi_port].u32SrcFrmRate, viParam[vi_port].u32DstFrameRate, \
                                      viParam[vi_port].u32MinQp, viParam[vi_port].u32MaxQp, viParam[vi_port].u32MinIQp, viParam[vi_port].u32IQp, viParam[vi_port].u32PQp, viParam[vi_port].u32BQp,  \
                                      viParam[vi_port].u32MinIProp, viParam[vi_port].u32MaxIProp,vi_port,HI_FALSE);
    veChn = vi_port * 2 + 1;
    vpssGrp = vi_port;
    HI35xx_VPSS_Start(vpssGrp,0,viParam[vi_port].stMinorSize,{viParam[vi_port].u32ViWidth,viParam[vi_port].u32ViHeigth});   //次码流编码VPSS启用
    this->VencBindVpss(veChn,vpssGrp);

    veChn = 8 + vi_port;
    vpssGrp = 4 + vi_port;
    HI35xx_COMM_VENC_SnapStart(veChn,&viParam[vi_port].stSnapSize);      //抓图编码通道启用
    HI35xx_VPSS_Start(vpssGrp,0,viParam[vi_port].stSnapSize,{viParam[vi_port].u32ViWidth,viParam[vi_port].u32ViHeigth});
    this->VencBindVpss(veChn,vpssGrp);

    usleep(20000);
    vi_src_chang[vi_port] = true;
    return true;

}


bool HimppMaster::VpssModuleDestroy()
{
    if(HI_SUCCESS != SAMPLE_COMM_VPSS_Stop(VpssGrpCnt_, VpssChnCnt_)){
        SAMPLE_PRT("SAMPLE_COMM_VPSS_Stop failed!\n");
        return false;
    }

    return true;
}

bool HimppMaster::vpssStop(VPSS_GRP vpssGrp_,VPSS_CHN vpssChn_)
{
    HI_S32 s32Ret;
    s32Ret = Hi35xx_VPSS_Destory(vpssGrp_,vpssChn_);
    if (s32Ret != HI_SUCCESS){
        printf("Hi35xx_VPSS_Destory [%d] failed  with %x\n",vpssGrp_, s32Ret);
        return false;
    }
    return true;
}

bool HimppMaster::VoModuleDestroy()
{
    if(HI_SUCCESS != HI35xx_COMM_VO_StopChn(VoLayer_, VoChn_))
        return false;

    if(HI_SUCCESS != SAMPLE_COMM_VO_StopLayer(VoLayer_))
        return false;

    if(HI_SUCCESS != SAMPLE_COMM_VO_StopDev(VoDev_))
        return false;

    if(HI_SUCCESS != SAMPLE_COMM_VO_HdmiStop())
        return false;

    return true;
}

bool HimppMaster::AiModeuleInit(AUDIO_DEV AiDevId,AUDIO_SAMPLE_RATE_E enAiSampleRate)
{
    HI_S32 s32Ret;
    AIO_ATTR_S stAioAttr;
    stAioAttr.enSamplerate   = enAiSampleRate;
    stAioAttr.enBitwidth     = AUDIO_BIT_WIDTH_16;
    stAioAttr.enWorkmode     = AIO_MODE_I2S_SLAVE;
    stAioAttr.enSoundmode    = AUDIO_SOUND_MODE_MONO;
    stAioAttr.u32EXFlag      = 1;
    stAioAttr.u32FrmNum      = 30;
    stAioAttr.u32PtNumPerFrm = SAMPLE_AUDIO_PTNUMPERFRM;
    stAioAttr.u32ChnCnt      = 2;
    stAioAttr.u32ClkChnCnt   = 2;
    stAioAttr.u32ClkSel      = 0;

#ifdef __EDC_VENC__
    if(2 == AiDevId) {
        stAioAttr.enSoundmode = AUDIO_SOUND_MODE_STEREO;
        stAioAttr.enWorkmode = AIO_MODE_I2S_MASTER;
        stAioAttr.u32ClkSel = 1;
    }
#endif

    s32Ret = HI35xx_COMM_AUDIO_StartAi(AiDevId, stAioAttr.u32ChnCnt, &stAioAttr, AUDIO_SAMPLE_RATE_BUTT, HI_FALSE, NULL,0);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_StartAi %d failed  with %x\n",AiDevId, s32Ret);
        return HI_FALSE;
    }

    AI_CHN_PARAM_S stAiParam;

    for(int i = 0; i < 2; i++){
#ifdef __EDC_VENC__
        if((2 == AiDevId) && 1 == i)
            continue;
#endif
        s32Ret = HI_MPI_AI_GetChnParam(AiDevId,i,&stAiParam);
        if (HI_SUCCESS != s32Ret)
        {
            printf("get ai dev:%d chn:%d max depth err:0x%x\n", AiDevId,i,s32Ret);
            return s32Ret;
        }
        stAiParam.u32UsrFrmDepth = 1;
        s32Ret = HI_MPI_AI_SetChnParam(AiDevId, i, &stAiParam);
        if (HI_SUCCESS != s32Ret)
        {
            printf("set ai dev:%d chn:%d max depth err:0x%x\n", AiDevId,i,s32Ret);
            return s32Ret;
        }
    }
    return true;
}

bool HimppMaster::AencModeuleInit(HI_S32 aencNum,AUDIO_SAMPLE_RATE_E enAiSampleRate)
{
    HI_S32 s32Ret;
    AIO_ATTR_S stAioAttr;
    stAioAttr.enSamplerate   = enAiSampleRate;
    stAioAttr.enBitwidth     = AUDIO_BIT_WIDTH_16;
    stAioAttr.enWorkmode     = AIO_MODE_I2S_SLAVE;
    stAioAttr.enSoundmode    = AUDIO_SOUND_MODE_MONO;
    stAioAttr.u32EXFlag      = 1;
    stAioAttr.u32FrmNum      = 30;
    stAioAttr.u32PtNumPerFrm = SAMPLE_AUDIO_PTNUMPERFRM;
    stAioAttr.u32ChnCnt      = 2;
    stAioAttr.u32ClkChnCnt   = 4;
    stAioAttr.u32ClkSel      = 0;

    s32AencChnCnt_ = aencNum;
    s32Ret = SAMPLE_COMM_AUDIO_StartAenc(s32AencChnCnt_, &stAioAttr, enPayloadType_);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_StartAenc failed with %x\n", s32Ret);
        return HI_FALSE;
    }

    return true;
}

#if 0
bool HimppMaster::StartAudioMcast(AUDIO_DEV AiDev,AI_CHN AiChn)
{
    HI_S32 s32Ret,packlen;
    AUDIO_FRAME_S pstFrm;
    AEC_FRAME_S pstAecFrm;
    AI_CHN_PARAM_S pstChnParam;
    HI_S32 s32MilliSec = -1;

    int aframe_number = 0;
    char aframe_data[INFO_HEADER + PACK_LEN];
    memset(aframe_data,0, (INFO_HEADER + PACK_LEN));
    aframe_data[INFO_HEADER_H0] = 'S';
    aframe_data[INFO_HEADER_H1] = 'L';
    //aframe_data[INFO_HEADER_VA] = 'A';
    UDPSocket *MultiSendSocket = new UDPSocket();
    int socket = MultiSendSocket->CreateUDPClient(nullptr,41235, false);
    MultiSendSocket->BindLocalAddr(ETH0);

    int value = (PACK_LEN + INFO_HEADER)*16;
    setsockopt(socket, SOL_SOCKET, SO_SNDBUF,(char *)&value, sizeof(value));

    s32Ret = HI_MPI_AI_GetChnParam(AiDev, AiChn, &pstChnParam);
    if (HI_SUCCESS != s32Ret)
    {
        printf("%s: Get ai chn param failed:0x%x\n", __FUNCTION__,s32Ret);
        return NULL;
    }

    pstChnParam.u32UsrFrmDepth = 2;

    s32Ret = HI_MPI_AI_SetChnParam(AiDev, AiChn, &pstChnParam);
    if (HI_SUCCESS != s32Ret)
    {
        printf("%s: set ai chn param failed\n", __FUNCTION__);
        return NULL;
    }

    while( aumcast_enable[AiDev] ){

        s32Ret = HI_MPI_AI_GetFrame(AiDev, AiChn, &pstFrm, &pstAecFrm, s32MilliSec);
        if (HI_SUCCESS != s32Ret)
        {
            printf("HI_MPI_AI_GetFrame aiDev:%d -- aiChn:%d err:0x%x\n",AiDev,AiChn, s32Ret);
            sleep(1);
            continue;
            //return s32Ret;
        }
        //        else {
        //            printf("gat ai pcm\n");
        //        }
        pstFrm.u64TimeStamp &= 0xFFFFFFFFFFFFFF;
        pstFrm.u64TimeStamp |= (HI_U64)glFrameID << 56;
        //printf("#### len %d, frame %d\n",pstFrm.u32Len,pstFrm.u32Seq);

        if(aframe_number > 255)
            aframe_number = 0;
        aframe_number++;
        packlen = pstFrm.u32Len;


        aframe_data[INFO_HEADER_SN] = aframe_number;
        if(pstFrm.u32Len%PACK_LEN > 0)
            *(HI_U16*)&aframe_data[INFO_HEADER_TP] = pstFrm.u32Len/PACK_LEN + 1;
        else
            *(HI_U16*)&aframe_data[INFO_HEADER_TP] = pstFrm.u32Len/PACK_LEN;

        aframe_data[INFO_HEADER_FD] = pstFrm.u64TimeStamp >> 56;

        *(HI_U16*)&aframe_data[INFO_HEADER_CP] = 0;
        char* bkup_data = (char*)pstFrm.pVirAddr[0];
        uint x=0;
        x++;
        if(x%10 == 0)
            printf("send multicast ip:  %s,port:  %d\n",aumcast_ip[AiDev],aumcast_port[AiDev]);
        while (packlen > 0){
            memcpy(&aframe_data[INFO_HEADER], bkup_data, min(PACK_LEN,packlen));
            bkup_data += min(PACK_LEN,packlen);
            MultiSendSocket->SendTo(
                        aumcast_ip[AiDev],
                        aumcast_port[AiDev],
                        (char*)aframe_data, min(PACK_LEN,packlen) + INFO_HEADER);
            packlen -= min(PACK_LEN,packlen);
            (*(HI_U16*)&aframe_data[INFO_HEADER_CP]) ++;
        }

        s32Ret = HI_MPI_AI_ReleaseFrame(AiDev, AiChn, &pstFrm, &pstAecFrm);
        if (HI_SUCCESS != s32Ret )
        {
            printf("%s: HI_MPI_AI_ReleaseFrame(%d), failed with %#x!\n",\
                   __FUNCTION__, AiChn, s32Ret);
            //return NULL;
        }
    }

    return NULL;
}
#endif

bool HimppMaster::StartAudioEnc(AUDIO_DEV AiDevId)
{

    HI_S32 s32Ret;
    AUDIO_FRAME_S stFrame_lt,stFrame_rt;
    AEC_FRAME_S pstAecFrm;
    byte* g711 = new byte[SAMPLE_AUDIO_PTNUMPERFRM+10];
    HI_U8 *outBytes = new HI_U8[6144];
    HI_U32 numOutBytes = 0, packLen = 0;
    bool getFlag_l = false, getFlag_r = false;

    int auFrameNum = 0;
    char auFramData[INFO_HEADER + PACK_LEN] = {0};
    int value = (PACK_LEN + INFO_HEADER)*16;

    int socket;
    memset(auFramData,0,sizeof(auFramData));
    auFramData[INFO_HEADER_H0] = 'S';
    auFramData[INFO_HEADER_H1] = 'L';
    auFramData[INFO_HEADER_VA] = 'A';




    UDPSocket * MultiSendSocket = new UDPSocket();
    socket = MultiSendSocket->CreateUDPClient(nullptr,aumcast_sv_port[AiDevId], false); //设为阻塞

    MultiSendSocket->BindLocalAddr(ETH0);
    int ttl = 64;
    setsockopt(socket, IPPROTO_IP, IP_MULTICAST_TTL, (char *)&ttl, sizeof(ttl));
    setsockopt(socket, IPPROTO_IP, IP_TTL, (char *)&ttl, sizeof(ttl));
    setsockopt(socket, SOL_SOCKET, SO_SNDBUF,(char *)&value, sizeof(value));



    int Get_ai = -1;
    FILE *audio_file = NULL;
     printf("--------------------------------------AI audio =%d\n",AiDevId);
//     if(0 == AiDevId) {

//         audio_file = fopen("audio.aac","w+");
//     }


    while(pth_ai_push[AiDevId]){
        /* get stream from aenc chn */

        s32Ret = HI_MPI_AI_GetFrame(AiDevId, 0,&stFrame_lt, &pstAecFrm,Get_ai);
        if (HI_SUCCESS != s32Ret )
        {
            getFlag_l = false;
            printf("%s: HI_MPI_AI_GetFrame(%d-0), failed with %#x!\n",\
                   __FUNCTION__, AiDevId, s32Ret);
            continue;
        }
        else {
            getFlag_l = true;
        }

#ifdef __EDC_VENC__
        if(2 != AiDevId) {
            s32Ret = HI_MPI_AI_GetFrame(AiDevId, 1,&stFrame_rt, &pstAecFrm,Get_ai);
            if (HI_SUCCESS != s32Ret )
            {
                getFlag_r = false;
                printf("%s: HI_MPI_AI_GetFrame(%d-1), failed with %#x!\n",\
                       __FUNCTION__, AiDevId, s32Ret);
                s32Ret = HI_MPI_AI_ReleaseFrame(AiDevId,0,&stFrame_lt,&pstAecFrm);
                if (HI_SUCCESS != s32Ret )
                {
                    printf("%s: HI_MPI_AI_ReleaseFrame(%d), failed with %#x!\n",\
                           __FUNCTION__, AiDevId, s32Ret);
                }
                continue;
            }
            else {

                getFlag_r = true;
            }
        }
#else
        s32Ret = HI_MPI_AI_GetFrame(AiDevId, 1,&stFrame_rt, &pstAecFrm,Get_ai);
        if (HI_SUCCESS != s32Ret )
        {
            getFlag_r = false;
            printf("%s: HI_MPI_AI_GetFrame(%d-1), failed with %#x!\n",\
                   __FUNCTION__, AiDevId, s32Ret);
            s32Ret = HI_MPI_AI_ReleaseFrame(AiDevId,0,&stFrame_lt,&pstAecFrm);
            if (HI_SUCCESS != s32Ret )
            {
                printf("%s: HI_MPI_AI_ReleaseFrame(%d), failed with %#x!\n",\
                       __FUNCTION__, AiDevId, s32Ret);
            }
            continue;
        }
        else {

            getFlag_r = true;
        }



#endif
        stFrame_lt.u64TimeStamp &= 0xFFFFFFFFFFFFFF;
        stFrame_lt.u64TimeStamp |= (HI_U64)pts << 56;
        //   stFrame_lt.u64TimeStamp |= (HI_U64)glFrameID << 56;
        HI_U64 u64CurPts;
        HI_MPI_SYS_GetCurPts(&u64CurPts);
        HI_U32 pts_rtsp = u64CurPts/1000;
        memset(outBytes,0,sizeof(outBytes));
#ifdef __EDC_VENC__
        if(2 ==  AiDevId) {
            s32Ret = g_audio_aac[AiDevId]->enc((unsigned char*)stFrame_lt.pVirAddr[0],  \
                    (unsigned char*)stFrame_lt.pVirAddr[0],stFrame_lt.u32Len,outBytes,&numOutBytes);    //编码AAC
        }
        else {
            s32Ret = g_audio_aac[AiDevId]->enc((unsigned char*)stFrame_lt.pVirAddr[0],  \
                    (unsigned char*)stFrame_rt.pVirAddr[0],stFrame_lt.u32Len,outBytes,&numOutBytes);    //编码AAC
        }
#else
        s32Ret = g_audio_aac[AiDevId]->enc((unsigned char*)stFrame_lt.pVirAddr[0],  \
                (unsigned char*)stFrame_rt.pVirAddr[0],stFrame_lt.u32Len,outBytes,&numOutBytes);    //编码AAC
#endif
//        if(0 == AiDevId)
//            fwrite(outBytes,numOutBytes,1,audio_file);

        g711_encode((byte*)stFrame_lt.pVirAddr[0], stFrame_lt.u32Len, g711);                        //编码G771A

        for(int vi_port = 0; vi_port < VI_PORT_NUMBER; vi_port++) {
            if((viParam[vi_port].AI_enable) && (viParam[vi_port].aiDev == AiDevId)) {
                if(viParam[vi_port].AI_type == AUDIO_AAC) {
//                    printf("viport %d,aiDev:%d\n",vi_port,AiDevId);
                    crtsps_pushaudio( g_stream[2*vi_port],(void*)outBytes,numOutBytes,pts_rtsp);
                    crtsps_pushaudio( g_stream[2*vi_port + 1],(void*)outBytes,numOutBytes,pts_rtsp);
                }
                else if(viParam[vi_port].AI_type == AUDIO_G711A) {
                    crtsps_pushaudio( g_stream[2*vi_port],(void*)g711,stFrame_lt.u32Len/2, pts_rtsp);
                    crtsps_pushaudio( g_stream[2*vi_port + 1],(void*)g711,stFrame_lt.u32Len/2, pts_rtsp);
                }

            }
        }
        if(aiParam[AiDevId].Amcast_enable) {
            if(auFrameNum > 255)
                auFrameNum = 0;
            auFrameNum++;       //音频帧号 1-255
            packLen = stFrame_lt.u32Len;

            auFramData[INFO_HEADER_SN] = auFrameNum;
            if(packLen%PACK_LEN > 0)   //一帧音频分包总数
                *(HI_U16*)&auFramData[INFO_HEADER_TP] = packLen/PACK_LEN + 1;
            else
                *(HI_U16*)&auFramData[INFO_HEADER_TP] = packLen/PACK_LEN;

            auFramData[INFO_HEADER_FD] = stFrame_lt.u64TimeStamp >> 56;

            *(HI_U16*)&auFramData[INFO_HEADER_CP] = 0;   //帧当前包序号
            char *au_data = (char*)stFrame_lt.pVirAddr[0];

            while(packLen > 0) {
                memcpy(&auFramData[INFO_HEADER],au_data,MIN(PACK_LEN,packLen));
                au_data += MIN(PACK_LEN,packLen);
                MultiSendSocket->SendTo(aiParam[AiDevId].Amcast_ip,aiParam[AiDevId].Amcast_port,(char*)auFramData,MIN(PACK_LEN,packLen) + INFO_HEADER);
                //   printf("audio %d send mucastIp:%s,port:%d\n",AiDevId,aumcast_ip[AiDevId],aumcast_port[AiDevId]);
                packLen -= MIN(PACK_LEN,packLen);
                (*(HI_U16*)&auFramData[INFO_HEADER_CP]) ++;
            }
        }


        /* finally you must release the stream */
        if(getFlag_l) {
            s32Ret = HI_MPI_AI_ReleaseFrame(AiDevId,0,&stFrame_lt,&pstAecFrm);
            if (HI_SUCCESS != s32Ret )
            {
                printf("%s: HI_MPI_AI_ReleaseFrame(%d), failed with %#x!\n",\
                       __FUNCTION__, AiDevId, s32Ret);
            }
        }

        if(getFlag_r) {
            s32Ret = HI_MPI_AI_ReleaseFrame(AiDevId,1,&stFrame_rt,&pstAecFrm);
            if (HI_SUCCESS != s32Ret )
            {
                printf("%s: HI_MPI_AI_ReleaseFrame(%d), failed with %#x!\n",\
                       __FUNCTION__, AiDevId, s32Ret);
            }
        }


    }
//    if(0 == AiDevId)
//        fclose(audio_file);

    delete MultiSendSocket;

    delete [] g711;
    delete [] outBytes;

    printf("======pth StartAudioEnc exit [%d]======\n",AiDevId);
    return NULL;
}


bool HimppMaster::ModeuleBind()
{
    /*
     * Ai(0) >--Aenc(0)
    */
    HI_S32   s32Ret;
    s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(0, 0, 0);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return false;
    }
    s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(1, 1, 1);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return false;
    }
    /*s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(2, 2, 2);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return false;
    }
    s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(2, 2, 3);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return false;
    }
    s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(2, 2, 4);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return false;
    }
    s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(2, 2, 5);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return false;
    }*/
    return true;
}


bool HimppMaster::ModeuleAiBind(AUDIO_DEV AiDev)
{
    /*
     * Ai(0) >--Aenc(0)
    */
    HI_S32   s32Ret;
    s32Ret = SAMPLE_COMM_AUDIO_AencBindAi(AiDev, 0, AiDev);
    if (s32Ret != HI_SUCCESS){
        COMMON_PRT("SAMPLE_COMM_AUDIO_AencBindAi failed with %x", s32Ret);
        return false;
    }

    return true;
}

bool HimppMaster::AudioModuleDestroy()
{
    HI_S32   s32Ret,i,j;
    for(i = 0; i < AI_PORT_NUMBER; i++){

        s32Ret = HI35xx_COMM_AUDIO_StopAi(i, 2, HI_FALSE, HI_FALSE);
        if (s32Ret != HI_SUCCESS)
        {
            COMMON_PRT("SAMPLE_COMM_AUDIO_StopAi failed with %x", s32Ret);
            return false;
        }

    }
    COMMON_PRT("audio modle destroy ok\n");

    return true;
}
