#ifndef HIMPP_MASTER_H
#define HIMPP_MASTER_H

#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "hi3531d_common/sample_comm.h"
#include "hi35xx_comm_fun.h"
#include "udpsocket/udpsocket.h"
#include "software_config.h"
#include "mytimer.h"
#include "global.h"

#define SNAP_LEN_MAX 1200
#define INITIVE    HI_TRUE
#ifdef __TINY_RTSP_SERVER__
#include <tiny_rtsp_server_i.h>
extern tiny_rtsp_server_i* g_prtspserver;
#else
#include "librtsps_i.h"
#include "aac/aac_encode.h"
extern aacenc *g_audio_aac[AI_PORT_NUMBER];
#endif
extern int ttl;
extern int g_stream[8];
extern int osd_enable[VI_PORT_NUMBER];
extern bool l_startloop[4];
extern bool mult_snap_start[4];
extern bool mult_snap[4];
extern bool audio_running[3];
extern int vi_flag[VI_PORT_NUMBER];         //输入端有无信号,判断osd是否使能
extern int  kerrFrameNum  ;
extern pthread_t input_tid[4];
extern pthread_t venc_tid[4];
extern AENC_CHN Aechn0Chn;
extern AENC_CHN Aechn1Chn;
extern AENC_CHN Aechn2Chn;
extern AENC_CHN Aechn3Chn;

extern AENC_CHN Aechn[4];   //4输入视频使能音频AIDEV；-1为不使能音频，HDMI和SDI音频只能对应其视频接口，3.5mm音频可对应任意视频接口

extern HI_U32 AI_value_35[4];
extern HI_U32 AI_value_HD[4];

extern IVE_MEM_INFO_S  pstMap;
bool swAudioFormat(int port);

class HimppMaster
{
public:
    HimppMaster();
    ~HimppMaster();

    bool InitHimpp(HI_S32 vdecMaxCnt,HI_S32 vpssMaxCnt, HI_S32 vencMaxCnt);

    bool VpssModuleInit(HI_S32 vpssGrp, HI_S32 vpssChn = 0);
    bool ViModuleInit(HI_S32 vi_port,SAMPLE_VI_MODE_E enViMode, RECT_S stCapRect, HI_S32 input_type);
    bool VencModuleInit(SIZE_S stVencSize , SIZE_S stMinor1Size , SIZE_S stMinor2Size, SIZE_S stSnapSize, PAYLOAD_TYPE_E *enType, SAMPLE_RC_E *enRcMode, HI_U32  *u32Profile,  HI_U32 *u32Gop, HI_U32 *u32BitRate, HI_U32 u32SrcFrmRate, HI_U32 *u32DstFrameRate, HI_U32 *u32MinQp, HI_U32 *u32MaxQp, HI_U32 *u32MinIQp, HI_U32 *u32IQp, HI_U32 *u32PQp, HI_U32 *u32BQp, HI_U32 *u32MinIProp, HI_U32 *u32MaxIProp, int viPort, HI_BOOL setFlag);


    bool VdecModuleInit(HI_U32 u32VdCnt);
    bool VoModeuleInit();

    bool BindPreViewSys();
    bool SnapBindPreViewSys(VI_DEV viDev,VI_CHN viChn);

    bool SetUserPic(VI_CHN ViChn);
    bool EnableUserPic(VI_CHN ViChn);
    bool DisableUserPic(VI_CHN ViChn);
    bool StopViModule(VI_DEV ViDev, VI_CHN ViChn);
    bool VdecModuleDestroy(HI_U32 u32VdCnt);
    bool VencModuleDestroy(HI_U32 u32VeChnStart, HI_U32 u32VeChnCnt);
    bool SnapModuleDestroy(HI_U32 VencSnapChn);
    bool VoModuleDestroy();
    bool VpssModuleDestroy();
    bool vpssStop(VPSS_GRP vpssGrp_,VPSS_CHN vpssChn_ = 0);
    bool StartLoop(HI_U32 streamstart, HI_U32 streamend, HI_U32 vi_port);
    bool viGetToVenc(HI_U32 vi_port);
    bool GetVencSnap(HI_U32 SnapChn, UDPSocket *socket, const char* clientip, const HI_U32 clientport, HI_U32 snaplen);
    bool GetVencRateSnap(HI_U32 SnapChn, UDPSocket *socket, HI_U32 tcpflag = HI_FALSE);

    HI_S32 VencSnapInit(HI_S32 veChn,SIZE_S stSize);
    bool VencSnapDestroy(VENC_CHN veChn);
    bool AiModeuleInit(AUDIO_DEV AiDevId, AUDIO_SAMPLE_RATE_E enAiSampleRate);
    bool AencModeuleInit(HI_S32 aencNum,AUDIO_SAMPLE_RATE_E enAiSampleRate);
    bool StartAudioEnc(AUDIO_DEV AiDevId);
    bool ModeuleBind();
    bool ModeuleAiBind(AUDIO_DEV AiDev);
    bool AudioModuleDestroy();
    bool SnapUNBindPreViewSys(VI_DEV viDev,VI_CHN viChn);
//    bool StartAudioMcast(AUDIO_DEV AiDev,AI_CHN AiChn);
    bool SnapUNBindVpss(VENC_CHN snapVenc);
    bool VencSetWH(VENC_CHN veChn,PAYLOAD_TYPE_E type,SIZE_S stVencSize,HI_S32 dstFrameRate = 0);
    bool VencBindVpss(VENC_CHN veChn_, VPSS_GRP vpssGrp_,VPSS_CHN vpssChn_ = 0);
    bool VencUnbindVpss(VENC_CHN veChn_, VPSS_GRP vpssGrp_,VPSS_CHN vpssChn_ = 0);
    bool VencDestroy(VENC_CHN veChn_);
    bool retartVIVPSSVENC(HI_S32 vi_port, HI_BOOL viSignal, HI_BOOL viRestart);

    bool VpssBindVo(VPSS_GRP vpssGrp, VPSS_CHN vpssChn, VO_LAYER VoLayer, VO_CHN VoChn);


public:
    bool resetVenc[4] = {false,false,false,false};    //用于退出VI-》VENC-》RTSP/组播线程
    HI_BOOL snap_running[4] = {HI_TRUE,HI_TRUE,HI_TRUE,HI_TRUE  };


private:
    //VIDEO_NORM_E enNorm_    = VIDEO_ENCODING_MODE_NTSC;
    //SAMPLE_RC_E enRcMode_   = SAMPLE_RC_CBR;
    VDEC_CHN VdChn_[2]={0,1};  //用于解码采集到的视频，并且作90度旋转
    ROTATE_E enRotate_ = ROTATE_NONE;
    HI_S32   VpssChnCnt_ = 1; //vpss的chn固定使用第0号通道，故个数为1
    HI_S32   VpssGrpCnt_ = 4; // = 5;
    VO_LAYER VoLayer_ = SAMPLE_VO_LAYER_VHD0;  // = SAMPLE_VO_LAYER_VHD0;
    VO_DEV   VoDev_ = SAMPLE_VO_DEV_DHD0;    // = SAMPLE_VO_DEV_DHD0;
#ifdef __EDC_VENC__
    VI_DEV   ViDev_[4] = {1,3,5,7};    // = 1;
    VI_CHN   ViChn_[4] = {4,12,20,28};    // = 4;
#else
    VI_DEV   ViDev_[4] = {0,2,4,6};    // = 1;
    VI_CHN   ViChn_[4] = {0,8,16,24};    // = 4;
#endif
    VENC_CHN VencChn_[16]={0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15};  // = 0;
    VENC_CHN VencSnapChn_[4];  // = 1;
    VPSS_GRP snapGrp_[4] = {4,5,6,7};
    VPSS_GRP vencMinGrp_[4] = {0,1,2,3};            //次码流使用VPSS缩放
    VENC_CHN VencMinorChn_[2];
    HI_U32   VdChnCnt_; // = 1;
    HI_U32   VoChn_ = 0;    // = 0;
    HI_S32   VencStreamCnt_;
    HI_S32   VencChnCnt_;
    bool SetNosignal = false;

    AI_CHN   AiChn_ = 0;
    AENC_CHN AeChn_ = 0;
    AUDIO_DEV AiDev_;
    PAYLOAD_TYPE_E  enPayloadType_ = PT_LPCM;
    HI_BOOL         bUserGetMode_ = HI_FALSE;
    HI_S32 s32AiChnCnt_;
    HI_S32 s32AencChnCnt_;

    SIZE_S stMaxSize_ = {3840,2160};
    SIZE_S stVencSize_[4];
    SIZE_S stVenc1Size_[4];
    SIZE_S stVenc2Size_[4];
    SIZE_S stVenc3Size_[4];
    SIZE_S stVoSize_[VI_PORT_NUMBER];
    SIZE_S stViSize_[VI_PORT_NUMBER];
    VI_USERPIC_ATTR_S stUsrPic;
    HI_U32 pts;


    typedef struct snap_data
    {
        HI_U32   u32TotalPack;  //总包数
        HI_U32   u32CurrPack;   //当前包数
        HI_U8    pData[SNAP_LEN_MAX]; //图片信息
    }SNAP_DATA;
    bool SendSnap(HI_U8 *pu8Data, HI_U32 u32TotalLen, HI_U32 u32PackLen, UDPSocket *socket, const char* clientip, const uint clientport, HI_S32 SnapDelay,HI_BOOL macsnap,HI_U32 Snap_chN);

};

#endif // HIMPP_MASTER_H
