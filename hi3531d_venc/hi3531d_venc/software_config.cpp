#include "software_config.h"
char netfilepath_[36];
char filepath_[36];

SoftwareConfig::SoftwareConfig()
{
    if( !MakeDir(DATA_FILE) ){
        sprintf(netfilepath_, "./%s", NET_CONFIG_FILE);
        sprintf(filepath_, "./%s", VENC_CONFIG_FILE);
        }else{
        sprintf(netfilepath_, "%s/%s", DATA_FILE, NET_CONFIG_FILE);
        sprintf(filepath_, "%s/%s", DATA_FILE, VENC_CONFIG_FILE);
        }

    // 初始化 向量类型softwareConfig,定义其长度为kSoftWareConfigIDMax，内容都先置为空
    configvalue_.assign(kSoftWareConfigIDMax, "");
    //network
    configvalue_[kEth0Ip]                   = "0.0.0.0";
    configvalue_[kEth0Mask]                 = "0.0.0.0";
    configvalue_[kEth0Gateway]              = "0.0.0.0";
    configvalue_[kEth0Dns]                  = "0.0.0.0";
    configvalue_[kEth0Mac]                  = "00:00:00:00:00:00";
    //
    configvalue_[kWebPort]                  = "80";
    configvalue_[kUdpSvPort]                = "12345";
    //色彩增强
    configvalue_[kChn0_SW_Color]        = "0";
    configvalue_[kChn1_SW_Color]        = "0";
    configvalue_[kChn2_SW_Color]        = "0";
    configvalue_[kChn3_SW_Color]        = "0";
    //audio
    configvalue_[kAiSampleRate]             = "48000";
    configvalue_[kChn0_AiCoderFormat]            = "0";     //0-AAC 1-G711 2-null
    configvalue_[kChn1_AiCoderFormat]            = "0";     //0-AAC 1-G711 2-null
    configvalue_[kChn2_AiCoderFormat]            = "2";     //0-AAC 1-G711 2-null
    configvalue_[kChn3_AiCoderFormat]            = "2";     //0-AAC 1-G711 2-null
    configvalue_[kAiBitRate]                = "128000";
    configvalue_[kChn0_AiDev]               = "HDMI";      //HDMI0 3.5mm
    configvalue_[kChn1_AiDev]               = "HDMI";      //HDMI1 3.5mm
    configvalue_[kChn2_AiDev]               = "3.5mm";      //3.5mm
    configvalue_[kChn3_AiDev]               = "3.5mm";      //3.5mm
    //vi
    configvalue_[kChn0_VI_W]                = "0";      //接口0主码流
    configvalue_[kChn0_VI_H]                = "0";
    configvalue_[kChn0_SrcFrmRate]          = "0";
    configvalue_[kChn1_VI_W]                = "0";      //接口0主码流
    configvalue_[kChn1_VI_H]                = "0";
    configvalue_[kChn1_SrcFrmRate]          = "0";
    configvalue_[kChn2_VI_W]                = "0";      //接口0主码流
    configvalue_[kChn2_VI_H]                = "0";
    configvalue_[kChn2_SrcFrmRate]          = "0";
    configvalue_[kChn3_VI_W]                = "0";      //接口0主码流
    configvalue_[kChn3_VI_H]                = "0";
    configvalue_[kChn3_SrcFrmRate]          = "0";
    //vi crop
    configvalue_[kChn0_VI_Crop_Enable]      = "0";      //1:vi 裁剪, 0:不裁剪
    configvalue_[kChn0_VI_X]                = "0";
    configvalue_[kChn0_VI_Y]                = "0";
    configvalue_[kChn0_VI_Width]            = "0";
    configvalue_[kChn0_VI_Height]           = "0";
    configvalue_[kChn1_VI_Crop_Enable]      = "0";      //1:vi 裁剪, 0:不裁剪
    configvalue_[kChn1_VI_X]                = "0";
    configvalue_[kChn1_VI_Y]                = "0";
    configvalue_[kChn1_VI_Width]            = "0";
    configvalue_[kChn1_VI_Height]           = "0";
    configvalue_[kChn2_VI_Crop_Enable]      = "0";      //1:vi 裁剪, 0:不裁剪
    configvalue_[kChn2_VI_X]                = "0";
    configvalue_[kChn2_VI_Y]                = "0";
    configvalue_[kChn2_VI_Width]            = "0";
    configvalue_[kChn2_VI_Height]           = "0";
    configvalue_[kChn3_VI_Crop_Enable]      = "0";      //1:vi 裁剪, 0:不裁剪
    configvalue_[kChn3_VI_X]                = "0";
    configvalue_[kChn3_VI_Y]                = "0";
    configvalue_[kChn3_VI_Width]            = "0";
    configvalue_[kChn3_VI_Height]           = "0";
    //multicast
    configvalue_[kChn0_M_McastEnable]       = "0";        // 0:RTSP; 1:multicast
    configvalue_[kChn0_M_McastIp]           = "0.0.0.0";  //组播ip
    configvalue_[kChn0_M_McastPort]         = "0";    //组播端口
    configvalue_[kChn0_M_McastLen]          = "0";   //组播数据大小
    configvalue_[kChn0_M_McastDelay]        = "0";       //组播发送延迟
    configvalue_[kChn0_M_McastNum]          = "1";
    configvalue_[kChn0_S0_McastEnable]      = "0";      // 0:RTSP; 1:multicast
    configvalue_[kChn0_S0_McastIp]          = "0.0.0.0";  //组播ip
    configvalue_[kChn0_S0_McastPort]        = "0";    //组播端口
    configvalue_[kChn0_S0_McastLen]         = "0";   //组播数据大小
    configvalue_[kChn0_S0_McastDelay]       = "0";       //组播发送延迟    
    configvalue_[kChn1_M_McastEnable]       = "0";        // 0:RTSP; 1:multicast
    configvalue_[kChn1_M_McastIp]           = "0.0.0.0";  //组播ip
    configvalue_[kChn1_M_McastPort]         = "0";    //组播端口
    configvalue_[kChn1_M_McastLen]          = "0";   //组播数据大小
    configvalue_[kChn1_M_McastDelay]        = "0";       //组播发送延迟
    configvalue_[kChn1_M_McastNum]          = "1";
    configvalue_[kChn1_S0_McastEnable]      = "0";      // 0:RTSP; 1:multicast
    configvalue_[kChn1_S0_McastIp]          = "0.0.0.0";  //组播ip
    configvalue_[kChn1_S0_McastPort]        = "0";    //组播端口
    configvalue_[kChn1_S0_McastLen]         = "0";   //组播数据大小
    configvalue_[kChn1_S0_McastDelay]       = "0";       //组播发送延迟
    configvalue_[kChn2_M_McastEnable]       = "0";        // 0:RTSP; 1:multicast
    configvalue_[kChn2_M_McastIp]           = "0.0.0.0";  //组播ip
    configvalue_[kChn2_M_McastPort]         = "0";    //组播端口
    configvalue_[kChn2_M_McastLen]          = "0";   //组播数据大小
    configvalue_[kChn2_M_McastDelay]        = "0";       //组播发送延迟
    configvalue_[kChn2_M_McastNum]          = "1";
    configvalue_[kChn2_S0_McastEnable]      = "0";     // 组播使能
    configvalue_[kChn2_S0_McastIp]          = "0";      //组播ip
    configvalue_[kChn2_S0_McastPort]        = "0";      //组播端口
    configvalue_[kChn2_S0_McastLen]         = "0";      //组播数据大小
    configvalue_[kChn2_S0_McastDelay]       = "0";      //组播发送延迟
    configvalue_[kChn3_M_McastEnable]       = "0";        // 0:RTSP; 1:multicast
    configvalue_[kChn3_M_McastIp]           = "0.0.0.0";  //组播ip
    configvalue_[kChn3_M_McastPort]         = "0";    //组播端口
    configvalue_[kChn3_M_McastLen]          = "0";   //组播数据大小
    configvalue_[kChn3_M_McastDelay]        = "0";       //组播发送延迟
    configvalue_[kChn3_M_McastNum]          = "1";
    configvalue_[kChn3_S0_McastEnable]      = "0";      // 0:RTSP; 1:multicast
    configvalue_[kChn3_S0_McastIp]          = "0.0.0.0";  //组播ip
    configvalue_[kChn3_S0_McastPort]        = "0";    //组播端口
    configvalue_[kChn3_S0_McastLen]         = "0";   //组播数据大小
    configvalue_[kChn3_S0_McastDelay]       = "0";       //组播发送延迟
    configvalue_[kHdmi0_Au_McastEnable]     = "0";      //音频HDMI0组播使能
    configvalue_[kHdmi1_Au_McastEnable]     = "0";      //音频HDMI1组播使能
    configvalue_[kAnalog_Au_McastEnable]    = "0";      //音频3.5mm组播使能
    configvalue_[kHdmi0_Au_McastIp]         = "0.0.0.0";
    configvalue_[kHdmi1_Au_McastIp]         = "0.0.0.0";
    configvalue_[kAnalog_Au_McastIp]        = "0.0.0.0";
    configvalue_[kHdmi0_Au_McastPort]       = "0";
    configvalue_[kHdmi1_Au_McastPort]       = "0";
    configvalue_[kAnalog_Au_McastPort]      = "0";
    //venc(main)

    configvalue_[KErrFrameNum]              = "5";
    configvalue_[kChn0_M_VENC_W]            = "1920";      //接口0主码流
    configvalue_[kChn0_M_VENC_H]            = "1080";
    configvalue_[kChn0_M_VENC_SAME_INPUT]   = "1";
    configvalue_[kChn0_M_VENC_Type]         = "96";       // 96:h264; 26:jpeg
    configvalue_[kChn0_M_Gop]               = "10";
    configvalue_[kChn0_M_BitRate]           = "15000";
    configvalue_[kChn0_M_RcMode]            = "0";      /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kChn0_M_Profile]           = "2";      /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kChn0_M_MIN_QP]            = "10";
    configvalue_[kChn0_M_MAX_QP]            = "51";
    configvalue_[kChn0_M_MINI_PROP]         = "1";
    configvalue_[kChn0_M_MAXI_PROP]         = "20";
    configvalue_[kChn0_M_MinI_Qp]           = "10";
    configvalue_[kChn0_M_I_QP]              = "25";
    configvalue_[kChn0_M_P_QP]              = "28";
    configvalue_[kChn0_M_B_QP]              = "21";
    configvalue_[kChn0_M_DstFrmRate]        = "60";
    //venc(minor)
    configvalue_[kChn0_S0_VENC_W]           = "960";    //接口0次码流
    configvalue_[kChn0_S0_VENC_H]           = "540";
    configvalue_[kChn0_S0_VENC_SAME_INPUT]  = "0";
    configvalue_[kChn0_S0_VENC_Type]        = "96";     // 96:h264; 26:jpeg
    configvalue_[kChn0_S0_Gop]              = "10";
    configvalue_[kChn0_S0_BitRate]          = "4000";
    configvalue_[kChn0_S0_RcMode]           = "0";     /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kChn0_S0_Profile]          = "2";     /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kChn0_S0_MIN_QP]           = "10";
    configvalue_[kChn0_S0_MAX_QP]           = "51";
    configvalue_[kChn0_S0_MINI_PROP]        = "1";
    configvalue_[kChn0_S0_MAXI_PROP]        = "20";
    configvalue_[kChn0_S0_MinI_Qp]          = "10";
    configvalue_[kChn0_S0_I_QP]             = "20";
    configvalue_[kChn0_S0_P_QP]             = "23";
    configvalue_[kChn0_S0_B_QP]             = "23";
    configvalue_[kChn0_S0_DstFrmRate]       = "60";
    //venc(main1)
    configvalue_[kChn1_M_VENC_W]            = "1920";      //接口0主码流
    configvalue_[kChn1_M_VENC_H]            = "1080";
    configvalue_[kChn1_M_VENC_SAME_INPUT]   = "1";
    configvalue_[kChn1_M_VENC_Type]         = "96";       // 96:h264; 26:jpeg
    configvalue_[kChn1_M_Gop]               = "10";
    configvalue_[kChn1_M_BitRate]           = "15000";
    configvalue_[kChn1_M_RcMode]            = "0";      /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kChn1_M_Profile]           = "2";      /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kChn1_M_MIN_QP]            = "10";
    configvalue_[kChn1_M_MAX_QP]            = "51";
    configvalue_[kChn1_M_MINI_PROP]         = "1";
    configvalue_[kChn1_M_MAXI_PROP]         = "20";
    configvalue_[kChn1_M_MinI_Qp]           = "10";
    configvalue_[kChn1_M_I_QP]              = "25";
    configvalue_[kChn1_M_P_QP]              = "28";
    configvalue_[kChn1_M_B_QP]              = "21";
    configvalue_[kChn1_M_DstFrmRate]        = "60";
    //venc(minor1)
    configvalue_[kChn1_S0_VENC_W]           = "960";    //接口0次码流
    configvalue_[kChn1_S0_VENC_H]           = "540";
    configvalue_[kChn1_S0_VENC_SAME_INPUT]  = "0";
    configvalue_[kChn1_S0_VENC_Type]        = "96";     // 96:h264; 26:jpeg
    configvalue_[kChn1_S0_Gop]              = "10";
    configvalue_[kChn1_S0_BitRate]          = "4000";
    configvalue_[kChn1_S0_RcMode]           = "0";     /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kChn1_S0_Profile]          = "2";     /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kChn1_S0_MIN_QP]           = "10";
    configvalue_[kChn1_S0_MAX_QP]           = "51";
    configvalue_[kChn1_S0_MINI_PROP]        = "1";
    configvalue_[kChn1_S0_MAXI_PROP]        = "20";
    configvalue_[kChn1_S0_MinI_Qp]          = "10";
    configvalue_[kChn1_S0_I_QP]             = "20";
    configvalue_[kChn1_S0_P_QP]             = "23";
    configvalue_[kChn1_S0_B_QP]             = "23";
    configvalue_[kChn1_S0_DstFrmRate]       = "60";
    //venc(main2)
    configvalue_[kChn2_M_VENC_W]            = "1920";      //接口0主码流
    configvalue_[kChn2_M_VENC_H]            = "1080";
    configvalue_[kChn2_M_VENC_SAME_INPUT]   = "1";
    configvalue_[kChn2_M_VENC_Type]         = "96";       // 96:h264; 26:jpeg
    configvalue_[kChn2_M_Gop]               = "10";
    configvalue_[kChn2_M_BitRate]           = "15000";
    configvalue_[kChn2_M_RcMode]            = "0";      /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kChn2_M_Profile]           = "2";      /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kChn2_M_MIN_QP]            = "10";
    configvalue_[kChn2_M_MAX_QP]            = "51";
    configvalue_[kChn2_M_MINI_PROP]         = "1";
    configvalue_[kChn2_M_MAXI_PROP]         = "20";
    configvalue_[kChn2_M_MinI_Qp]           = "10";
    configvalue_[kChn2_M_I_QP]              = "25";
    configvalue_[kChn2_M_P_QP]              = "28";
    configvalue_[kChn2_M_B_QP]              = "21";
    configvalue_[kChn2_M_DstFrmRate]        = "60";
    //venc(minor2)
    configvalue_[kChn2_S0_VENC_W]           = "960";    //次码流2
    configvalue_[kChn2_S0_VENC_H]           = "540";
    configvalue_[kChn2_S0_VENC_SAME_INPUT]  = "0";
    configvalue_[kChn2_S0_VENC_Type]        = "96";     // 96:h264; 26:jpeg
    configvalue_[kChn2_S0_Gop]              = "10";
    configvalue_[kChn2_S0_BitRate]          = "4000";
    configvalue_[kChn2_S0_RcMode]           = "0";      /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kChn2_S0_Profile]          = "2";      /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kChn2_S0_MIN_QP]           = "10";
    configvalue_[kChn2_S0_MAX_QP]           = "51";
    configvalue_[kChn2_S0_MINI_PROP]        = "1";
    configvalue_[kChn2_S0_MAXI_PROP]        = "20";
    configvalue_[kChn2_S0_MinI_Qp]          = "10";
    configvalue_[kChn2_S0_I_QP]             = "20";
    configvalue_[kChn2_S0_P_QP]             = "23";
    configvalue_[kChn2_S0_B_QP]             = "23";
    configvalue_[kChn2_S0_DstFrmRate]       = "60";
    //venc(main3)
    configvalue_[kChn3_M_VENC_W]            = "1920";      //接口0主码流
    configvalue_[kChn3_M_VENC_H]            = "1080";
    configvalue_[kChn3_M_VENC_SAME_INPUT]   = "1";
    configvalue_[kChn3_M_VENC_Type]         = "96";       // 96:h264; 26:jpeg
    configvalue_[kChn3_M_Gop]               = "10";
    configvalue_[kChn3_M_BitRate]           = "15000";
    configvalue_[kChn3_M_RcMode]            = "0";      /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kChn3_M_Profile]           = "2";      /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kChn3_M_MIN_QP]            = "10";
    configvalue_[kChn3_M_MAX_QP]            = "51";
    configvalue_[kChn3_M_MINI_PROP]         = "1";
    configvalue_[kChn3_M_MAXI_PROP]         = "20";
    configvalue_[kChn3_M_MinI_Qp]           = "10";
    configvalue_[kChn3_M_I_QP]              = "25";
    configvalue_[kChn3_M_P_QP]              = "28";
    configvalue_[kChn3_M_B_QP]              = "21";
    configvalue_[kChn3_M_DstFrmRate]        = "60";
    //venc(minor3)
    configvalue_[kChn3_S0_VENC_W]           = "960";    //接口0次码流
    configvalue_[kChn3_S0_VENC_H]           = "540";
    configvalue_[kChn3_S0_VENC_SAME_INPUT]  = "0";
    configvalue_[kChn3_S0_VENC_Type]        = "96";     // 96:h264; 26:jpeg
    configvalue_[kChn3_S0_Gop]              = "10";
    configvalue_[kChn3_S0_BitRate]          = "4000";
    configvalue_[kChn3_S0_RcMode]           = "0";     /*0: CBR; 1: VBR; 2: FIXQP*/
    configvalue_[kChn3_S0_Profile]          = "2";     /*0: baseline; 1:MP; 2:HP 3:svc-t */
    configvalue_[kChn3_S0_MIN_QP]           = "10";
    configvalue_[kChn3_S0_MAX_QP]           = "51";
    configvalue_[kChn3_S0_MINI_PROP]        = "1";
    configvalue_[kChn3_S0_MAXI_PROP]        = "20";
    configvalue_[kChn3_S0_MinI_Qp]          = "10";
    configvalue_[kChn3_S0_I_QP]             = "20";
    configvalue_[kChn3_S0_P_QP]             = "23";
    configvalue_[kChn3_S0_B_QP]             = "23";
    configvalue_[kChn3_S0_DstFrmRate]       = "60";
    //snap
    configvalue_[kChn0_SnapPort]            = "54321";
    configvalue_[kChn0_SnapLen]             = "1200";
    configvalue_[kChn0_Snap_Delay]          = "1000";
    configvalue_[kChn0_Snap_W]              = "320";    //抓图
    configvalue_[kChn0_Snap_H]              = "180";
    configvalue_[kChn0_Snap_Quality]        = "50";
    configvalue_[kChn0_Snap_McastFreq]      = "20";
    configvalue_[kChn0_Snap_McastEnable]    = "0";      //组播抓图
    configvalue_[kChn0_Snap_McastIp]        = "0.0.0.0";
    configvalue_[kChn0_Snap_McastPort]      = "0";
    configvalue_[kChn0_Snap_McastFreq]      = "20";
    configvalue_[kChn1_SnapPort]            = "54322";
    configvalue_[kChn1_SnapLen]             = "1200";
    configvalue_[kChn1_Snap_Delay]          = "1000";
    configvalue_[kChn1_Snap_W]              = "320";    //抓图
    configvalue_[kChn1_Snap_H]              = "180";
    configvalue_[kChn1_Snap_Quality]        = "50";
    configvalue_[kChn1_Snap_McastEnable]    = "0";      //组播抓图
    configvalue_[kChn1_Snap_McastIp]        = "0.0.0.0";
    configvalue_[kChn1_Snap_McastPort]      = "0";
    configvalue_[kChn1_Snap_McastFreq]      = "20";
    configvalue_[kChn2_SnapPort]            = "54323";
    configvalue_[kChn2_SnapLen]             = "1200";
    configvalue_[kChn2_Snap_Delay]          = "1000";
    configvalue_[kChn2_Snap_W]              = "320";    //抓图
    configvalue_[kChn2_Snap_H]              = "180";
    configvalue_[kChn2_Snap_Quality]        = "50";
    configvalue_[kChn2_Snap_McastEnable]    = "0";      //组播抓图
    configvalue_[kChn2_Snap_McastIp]        = "0.0.0.0";
    configvalue_[kChn2_Snap_McastPort]      = "0";
    configvalue_[kChn2_Snap_McastFreq]      = "20";
    configvalue_[kChn3_SnapPort]            = "54324";
    configvalue_[kChn3_SnapLen]             = "1200";
    configvalue_[kChn3_Snap_Delay]          = "1000";
    configvalue_[kChn3_Snap_W]              = "320";    //抓图
    configvalue_[kChn3_Snap_H]              = "180";
    configvalue_[kChn3_Snap_Quality]        = "50";
    configvalue_[kChn3_Snap_McastEnable]    = "0";      //组播抓图
    configvalue_[kChn3_Snap_McastIp]        = "0.0.0.0";
    configvalue_[kChn3_Snap_McastPort]      = "0";
    configvalue_[kChn3_Snap_McastFreq]      = "20";
    //osd(字符叠加)
    configvalue_[kChn0_Osd_Enable]          = "0";
    configvalue_[kChn0_Osd_Name]            = "HDMI0";
    configvalue_[kChn0_Osd_Color]           = "#EEEE00";
    configvalue_[kChn0_Osd_X]               = "50";
    configvalue_[kChn0_Osd_Y]               = "50";
    configvalue_[kChn0_Osd_Size]            = "60";
    configvalue_[kChn0_Osd_Transparent]     = "1";
    configvalue_[kChn0_Osd_BGColor]         = "0";
    configvalue_[kChn1_Osd_Enable]          = "0";
    configvalue_[kChn1_Osd_Name]            = "HDMI1";
    configvalue_[kChn1_Osd_Color]           = "#DC1437";
    configvalue_[kChn1_Osd_X]               = "50";
    configvalue_[kChn1_Osd_Y]               = "50";
    configvalue_[kChn1_Osd_Size]            = "60";
    configvalue_[kChn1_Osd_Transparent]     = "1";
    configvalue_[kChn1_Osd_BGColor]         = "0";
    configvalue_[kChn2_Osd_Enable]          = "0";
    configvalue_[kChn2_Osd_Name]            = "HDMI2";
    configvalue_[kChn2_Osd_Color]           = "#EEEE00";
    configvalue_[kChn2_Osd_X]               = "50";
    configvalue_[kChn2_Osd_Y]               = "50";
    configvalue_[kChn2_Osd_Size]            = "60";
    configvalue_[kChn2_Osd_Transparent]     = "1";
    configvalue_[kChn2_Osd_BGColor]         = "0";
    configvalue_[kChn3_Osd_Enable]          = "0";
    configvalue_[kChn3_Osd_Name]            = "HDMI3";
    configvalue_[kChn3_Osd_Color]           = "#DC1437";
    configvalue_[kChn3_Osd_X]               = "50";
    configvalue_[kChn3_Osd_Y]               = "50";
    configvalue_[kChn3_Osd_Size]            = "60";
    configvalue_[kChn3_Osd_Transparent]     = "1";
    configvalue_[kChn3_Osd_BGColor]         = "0";
    //tickmaster
    configvalue_[kTickAuto]             = "0";
    configvalue_[kTickModule]           = "slave";
    configvalue_[kTickRate]             = "512";
    configvalue_[kTickOffset]           = "80";
    configvalue_[kTickMDelay]           = "-1"; //主节点给从节点主程序接收的节拍的延迟
    configvalue_[kTickServerIp]         = "-1";
    configvalue_[kTickServerPort]       = "-1";
    configvalue_[kChn0_VI_Module]       = "0";
    configvalue_[kChn1_VI_Module]       = "0";
    configvalue_[kChn2_VI_Module]       = "0";
    configvalue_[kChn3_VI_Module]       = "0";
    configvalue_[kEdid]                 = "0";
    //4k



    snprintf(paramsnamelist[kEth0Ip],                   PARA_NAME_LEN, "%s=", EnumtoStr(kEth0Ip));
    snprintf(paramsnamelist[kEth0Mask],                 PARA_NAME_LEN, "%s=", EnumtoStr(kEth0Mask));
    snprintf(paramsnamelist[kEth0Gateway],              PARA_NAME_LEN, "%s=", EnumtoStr(kEth0Gateway));
    snprintf(paramsnamelist[kEth0Dns],                  PARA_NAME_LEN, "%s=", EnumtoStr(kEth0Dns));
    snprintf(paramsnamelist[kEth0Mac],                  PARA_NAME_LEN, "%s=", EnumtoStr(kEth0Mac));
    snprintf(paramsnamelist[kWebPort],                  PARA_NAME_LEN, "%s=", EnumtoStr(kWebPort));
    snprintf(paramsnamelist[kUdpSvPort],                PARA_NAME_LEN, "%s=", EnumtoStr(kUdpSvPort));
    snprintf(paramsnamelist[kChn0_SW_Color],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_SW_Color));
    snprintf(paramsnamelist[kChn1_SW_Color],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_SW_Color));
    snprintf(paramsnamelist[kChn2_SW_Color],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_SW_Color));
    snprintf(paramsnamelist[kChn3_SW_Color],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_SW_Color));
    snprintf(paramsnamelist[kAiSampleRate],             PARA_NAME_LEN, "%s=", EnumtoStr(kAiSampleRate));
    snprintf(paramsnamelist[kChn0_AiCoderFormat],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_AiCoderFormat));
    snprintf(paramsnamelist[kChn1_AiCoderFormat],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_AiCoderFormat));
    snprintf(paramsnamelist[kChn2_AiCoderFormat],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_AiCoderFormat));
    snprintf(paramsnamelist[kChn3_AiCoderFormat],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_AiCoderFormat));
    snprintf(paramsnamelist[kAiBitRate],                PARA_NAME_LEN, "%s=", EnumtoStr(kAiBitRate));
    snprintf(paramsnamelist[kChn0_AiDev],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_AiDev));
    snprintf(paramsnamelist[kChn1_AiDev],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_AiDev));
    snprintf(paramsnamelist[kChn2_AiDev],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_AiDev));
    snprintf(paramsnamelist[kChn3_AiDev],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_AiDev));
    snprintf(paramsnamelist[kChn0_VI_W],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_VI_W));
    snprintf(paramsnamelist[kChn0_VI_H],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_VI_H));
    snprintf(paramsnamelist[kChn0_SrcFrmRate],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_SrcFrmRate));
    snprintf(paramsnamelist[kChn1_VI_W],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_VI_W));
    snprintf(paramsnamelist[kChn1_VI_H],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_VI_H));
    snprintf(paramsnamelist[kChn1_SrcFrmRate],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_SrcFrmRate));
    snprintf(paramsnamelist[kChn2_VI_W],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_VI_W));
    snprintf(paramsnamelist[kChn2_VI_H],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_VI_H));
    snprintf(paramsnamelist[kChn2_SrcFrmRate],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_SrcFrmRate));
    snprintf(paramsnamelist[kChn3_VI_W],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_VI_W));
    snprintf(paramsnamelist[kChn3_VI_H],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_VI_H));
    snprintf(paramsnamelist[kChn3_SrcFrmRate],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_SrcFrmRate));
    snprintf(paramsnamelist[kChn0_VI_Crop_Enable],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_VI_Crop_Enable));
    snprintf(paramsnamelist[kChn0_VI_X],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_VI_X));
    snprintf(paramsnamelist[kChn0_VI_Y],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_VI_Y));
    snprintf(paramsnamelist[kChn0_VI_Width],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_VI_Width));
    snprintf(paramsnamelist[kChn0_VI_Height],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_VI_Height));
    snprintf(paramsnamelist[kChn1_VI_Crop_Enable],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_VI_Crop_Enable));
    snprintf(paramsnamelist[kChn1_VI_X],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_VI_X));
    snprintf(paramsnamelist[kChn1_VI_Y],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_VI_Y));
    snprintf(paramsnamelist[kChn1_VI_Width],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_VI_Width));
    snprintf(paramsnamelist[kChn1_VI_Height],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_VI_Height));
    snprintf(paramsnamelist[kChn2_VI_Crop_Enable],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_VI_Crop_Enable));
    snprintf(paramsnamelist[kChn2_VI_X],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_VI_X));
    snprintf(paramsnamelist[kChn2_VI_Y],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_VI_Y));
    snprintf(paramsnamelist[kChn2_VI_Width],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_VI_Width));
    snprintf(paramsnamelist[kChn2_VI_Height],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_VI_Height));
    snprintf(paramsnamelist[kChn3_VI_Crop_Enable],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_VI_Crop_Enable));
    snprintf(paramsnamelist[kChn3_VI_X],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_VI_X));
    snprintf(paramsnamelist[kChn3_VI_Y],                PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_VI_Y));
    snprintf(paramsnamelist[kChn3_VI_Width],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_VI_Width));
    snprintf(paramsnamelist[kChn3_VI_Height],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_VI_Height));
    snprintf(paramsnamelist[kChn0_M_McastEnable],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_McastEnable));
    snprintf(paramsnamelist[kChn0_M_McastIp],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_McastIp));
    snprintf(paramsnamelist[kChn0_M_McastPort],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_McastPort));
    snprintf(paramsnamelist[kChn0_M_McastLen],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_McastLen));
    snprintf(paramsnamelist[kChn0_M_McastDelay],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_McastDelay));
    snprintf(paramsnamelist[kChn0_M_McastNum],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_McastNum));
    snprintf(paramsnamelist[kChn0_S0_McastEnable],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_McastEnable));
    snprintf(paramsnamelist[kChn0_S0_McastIp],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_McastIp));
    snprintf(paramsnamelist[kChn0_S0_McastPort],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_McastPort));
    snprintf(paramsnamelist[kChn0_S0_McastLen],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_McastLen));
    snprintf(paramsnamelist[kChn0_S0_McastDelay],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_McastDelay));
    snprintf(paramsnamelist[kChn1_M_McastEnable],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_McastEnable));
    snprintf(paramsnamelist[kChn1_M_McastIp],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_McastIp));
    snprintf(paramsnamelist[kChn1_M_McastPort],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_McastPort));
    snprintf(paramsnamelist[kChn1_M_McastLen],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_McastLen));
    snprintf(paramsnamelist[kChn1_M_McastDelay],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_McastDelay));
    snprintf(paramsnamelist[kChn1_M_McastNum],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_McastNum));
    snprintf(paramsnamelist[kChn1_S0_McastEnable],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_McastEnable));
    snprintf(paramsnamelist[kChn1_S0_McastIp],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_McastIp));
    snprintf(paramsnamelist[kChn1_S0_McastPort],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_McastPort));
    snprintf(paramsnamelist[kChn1_S0_McastLen],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_McastLen));
    snprintf(paramsnamelist[kChn1_S0_McastDelay],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_McastDelay));
    snprintf(paramsnamelist[kChn2_M_McastEnable],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_McastEnable));
    snprintf(paramsnamelist[kChn2_M_McastIp],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_McastIp));
    snprintf(paramsnamelist[kChn2_M_McastPort],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_McastPort));
    snprintf(paramsnamelist[kChn2_M_McastLen],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_McastLen));
    snprintf(paramsnamelist[kChn2_M_McastDelay],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_McastDelay));
    snprintf(paramsnamelist[kChn2_M_McastNum],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_McastNum));
    snprintf(paramsnamelist[kChn2_S0_McastEnable],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_McastEnable));
    snprintf(paramsnamelist[kChn2_S0_McastIp],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_McastIp));
    snprintf(paramsnamelist[kChn2_S0_McastPort],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_McastPort));
    snprintf(paramsnamelist[kChn2_S0_McastLen],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_McastLen));
    snprintf(paramsnamelist[kChn2_S0_McastDelay],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_McastDelay));
    snprintf(paramsnamelist[kChn3_M_McastEnable],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_McastEnable));
    snprintf(paramsnamelist[kChn3_M_McastIp],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_McastIp));
    snprintf(paramsnamelist[kChn3_M_McastPort],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_McastPort));
    snprintf(paramsnamelist[kChn3_M_McastLen],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_McastLen));
    snprintf(paramsnamelist[kChn3_M_McastDelay],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_McastDelay));
    snprintf(paramsnamelist[kChn3_M_McastNum],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_McastNum));
    snprintf(paramsnamelist[kChn3_S0_McastEnable],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_McastEnable));
    snprintf(paramsnamelist[kChn3_S0_McastIp],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_McastIp));
    snprintf(paramsnamelist[kChn3_S0_McastPort],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_McastPort));
    snprintf(paramsnamelist[kChn3_S0_McastLen],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_McastLen));
    snprintf(paramsnamelist[kChn3_S0_McastDelay],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_McastDelay));
    snprintf(paramsnamelist[kHdmi0_Au_McastEnable],     PARA_NAME_LEN, "%s=", EnumtoStr(kHdmi0_Au_McastEnable));
    snprintf(paramsnamelist[kHdmi1_Au_McastEnable],     PARA_NAME_LEN, "%s=", EnumtoStr(kHdmi1_Au_McastEnable));
    snprintf(paramsnamelist[kAnalog_Au_McastEnable],    PARA_NAME_LEN, "%s=", EnumtoStr(kAnalog_Au_McastEnable));
    snprintf(paramsnamelist[kHdmi0_Au_McastIp],         PARA_NAME_LEN, "%s=", EnumtoStr(kHdmi0_Au_McastIp));
    snprintf(paramsnamelist[kHdmi1_Au_McastIp],         PARA_NAME_LEN, "%s=", EnumtoStr(kHdmi1_Au_McastIp));
    snprintf(paramsnamelist[kAnalog_Au_McastIp],        PARA_NAME_LEN, "%s=", EnumtoStr(kAnalog_Au_McastIp));
    snprintf(paramsnamelist[kHdmi0_Au_McastPort],       PARA_NAME_LEN, "%s=", EnumtoStr(kHdmi0_Au_McastPort));
    snprintf(paramsnamelist[kHdmi1_Au_McastPort],       PARA_NAME_LEN, "%s=", EnumtoStr(kHdmi1_Au_McastPort));
    snprintf(paramsnamelist[kAnalog_Au_McastPort],      PARA_NAME_LEN, "%s=", EnumtoStr(kAnalog_Au_McastPort));
    snprintf(paramsnamelist[kChn0_M_VENC_W],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_VENC_W));
    snprintf(paramsnamelist[kChn0_M_VENC_H],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_VENC_H));
    snprintf(paramsnamelist[kChn0_M_VENC_SAME_INPUT],   PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_VENC_SAME_INPUT));
    snprintf(paramsnamelist[kChn0_M_VENC_Type],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_VENC_Type));

       snprintf(paramsnamelist[KErrFrameNum],               PARA_NAME_LEN, "%s=", EnumtoStr(KErrFrameNum));
    snprintf(paramsnamelist[kChn0_M_Gop],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_Gop));
    snprintf(paramsnamelist[kChn0_M_BitRate],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_BitRate));
    snprintf(paramsnamelist[kChn0_M_RcMode],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_RcMode));
    snprintf(paramsnamelist[kChn0_M_Profile],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_Profile));
    snprintf(paramsnamelist[kChn0_M_MIN_QP],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_MIN_QP));
    snprintf(paramsnamelist[kChn0_M_MAX_QP],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_MAX_QP));
    snprintf(paramsnamelist[kChn0_M_MINI_PROP],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_MINI_PROP));
    snprintf(paramsnamelist[kChn0_M_MAXI_PROP],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_MAXI_PROP));
    snprintf(paramsnamelist[kChn0_M_MinI_Qp],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_MinI_Qp));
    snprintf(paramsnamelist[kChn0_M_I_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_I_QP));
    snprintf(paramsnamelist[kChn0_M_P_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_P_QP));
    snprintf(paramsnamelist[kChn0_M_B_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_B_QP));
    snprintf(paramsnamelist[kChn0_M_DstFrmRate],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_M_DstFrmRate));
    snprintf(paramsnamelist[kChn0_S0_VENC_W],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_VENC_W));
    snprintf(paramsnamelist[kChn0_S0_VENC_H],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_VENC_H));
    snprintf(paramsnamelist[kChn0_S0_VENC_SAME_INPUT],  PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_VENC_SAME_INPUT));
    snprintf(paramsnamelist[kChn0_S0_VENC_Type],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_VENC_Type));
    snprintf(paramsnamelist[kChn0_S0_Gop],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_Gop));
    snprintf(paramsnamelist[kChn0_S0_BitRate],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_BitRate));
    snprintf(paramsnamelist[kChn0_S0_RcMode],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_RcMode));
    snprintf(paramsnamelist[kChn0_S0_Profile],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_Profile));
    snprintf(paramsnamelist[kChn0_S0_MIN_QP],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_MIN_QP));
    snprintf(paramsnamelist[kChn0_S0_MAX_QP],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_MAX_QP));
    snprintf(paramsnamelist[kChn0_S0_MINI_PROP],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_MINI_PROP));
    snprintf(paramsnamelist[kChn0_S0_MAXI_PROP],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_MAXI_PROP));
    snprintf(paramsnamelist[kChn0_S0_MinI_Qp],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_MinI_Qp));
    snprintf(paramsnamelist[kChn0_S0_I_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_I_QP));
    snprintf(paramsnamelist[kChn0_S0_P_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_P_QP));
    snprintf(paramsnamelist[kChn0_S0_B_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_B_QP));
    snprintf(paramsnamelist[kChn0_S0_DstFrmRate],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_S0_DstFrmRate));
    snprintf(paramsnamelist[kChn1_M_VENC_W],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_VENC_W));
    snprintf(paramsnamelist[kChn1_M_VENC_H],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_VENC_H));
    snprintf(paramsnamelist[kChn1_M_VENC_SAME_INPUT],   PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_VENC_SAME_INPUT));
    snprintf(paramsnamelist[kChn1_M_VENC_Type],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_VENC_Type));
    snprintf(paramsnamelist[kChn1_M_Gop],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_Gop));
    snprintf(paramsnamelist[kChn1_M_BitRate],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_BitRate));
    snprintf(paramsnamelist[kChn1_M_RcMode],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_RcMode));
    snprintf(paramsnamelist[kChn1_M_Profile],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_Profile));
    snprintf(paramsnamelist[kChn1_M_MIN_QP],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_MIN_QP));
    snprintf(paramsnamelist[kChn1_M_MAX_QP],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_MAX_QP));
    snprintf(paramsnamelist[kChn1_M_MINI_PROP],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_MINI_PROP));
    snprintf(paramsnamelist[kChn1_M_MAXI_PROP],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_MAXI_PROP));
    snprintf(paramsnamelist[kChn1_M_MinI_Qp],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_MinI_Qp));
    snprintf(paramsnamelist[kChn1_M_I_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_I_QP));
    snprintf(paramsnamelist[kChn1_M_P_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_P_QP));
    snprintf(paramsnamelist[kChn1_M_B_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_B_QP));
    snprintf(paramsnamelist[kChn1_M_DstFrmRate],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_M_DstFrmRate));
    snprintf(paramsnamelist[kChn1_S0_VENC_W],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_VENC_W));
    snprintf(paramsnamelist[kChn1_S0_VENC_H],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_VENC_H));
    snprintf(paramsnamelist[kChn1_S0_VENC_SAME_INPUT],  PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_VENC_SAME_INPUT));
    snprintf(paramsnamelist[kChn1_S0_VENC_Type],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_VENC_Type));
    snprintf(paramsnamelist[kChn1_S0_Gop],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_Gop));
    snprintf(paramsnamelist[kChn1_S0_BitRate],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_BitRate));
    snprintf(paramsnamelist[kChn1_S0_RcMode],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_RcMode));
    snprintf(paramsnamelist[kChn1_S0_Profile],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_Profile));
    snprintf(paramsnamelist[kChn1_S0_MIN_QP],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_MIN_QP));
    snprintf(paramsnamelist[kChn1_S0_MAX_QP],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_MAX_QP));
    snprintf(paramsnamelist[kChn1_S0_MINI_PROP],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_MINI_PROP));
    snprintf(paramsnamelist[kChn1_S0_MAXI_PROP],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_MAXI_PROP));
    snprintf(paramsnamelist[kChn1_S0_MinI_Qp],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_MinI_Qp));
    snprintf(paramsnamelist[kChn1_S0_I_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_I_QP));
    snprintf(paramsnamelist[kChn1_S0_P_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_P_QP));
    snprintf(paramsnamelist[kChn1_S0_B_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_B_QP));
    snprintf(paramsnamelist[kChn1_S0_DstFrmRate],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_S0_DstFrmRate));
    snprintf(paramsnamelist[kChn2_M_VENC_W],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_VENC_W));
    snprintf(paramsnamelist[kChn2_M_VENC_H],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_VENC_H));
    snprintf(paramsnamelist[kChn2_M_VENC_SAME_INPUT],   PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_VENC_SAME_INPUT));
    snprintf(paramsnamelist[kChn2_M_VENC_Type],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_VENC_Type));
    snprintf(paramsnamelist[kChn2_M_Gop],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_Gop));
    snprintf(paramsnamelist[kChn2_M_BitRate],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_BitRate));
    snprintf(paramsnamelist[kChn2_M_RcMode],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_RcMode));
    snprintf(paramsnamelist[kChn2_M_Profile],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_Profile));
    snprintf(paramsnamelist[kChn2_M_MIN_QP],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_MIN_QP));
    snprintf(paramsnamelist[kChn2_M_MAX_QP],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_MAX_QP));
    snprintf(paramsnamelist[kChn2_M_MINI_PROP],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_MINI_PROP));
    snprintf(paramsnamelist[kChn2_M_MAXI_PROP],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_MAXI_PROP));
    snprintf(paramsnamelist[kChn2_M_MinI_Qp],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_MinI_Qp));
    snprintf(paramsnamelist[kChn2_M_I_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_I_QP));
    snprintf(paramsnamelist[kChn2_M_P_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_P_QP));
    snprintf(paramsnamelist[kChn2_M_B_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_B_QP));
    snprintf(paramsnamelist[kChn2_M_DstFrmRate],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_M_DstFrmRate));
    snprintf(paramsnamelist[kChn2_S0_VENC_W],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_VENC_W));
    snprintf(paramsnamelist[kChn2_S0_VENC_H],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_VENC_H));
    snprintf(paramsnamelist[kChn2_S0_VENC_SAME_INPUT],  PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_VENC_SAME_INPUT));
    snprintf(paramsnamelist[kChn2_S0_VENC_Type],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_VENC_Type));
    snprintf(paramsnamelist[kChn2_S0_Gop],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_Gop));
    snprintf(paramsnamelist[kChn2_S0_BitRate],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_BitRate));
    snprintf(paramsnamelist[kChn2_S0_RcMode],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_RcMode));
    snprintf(paramsnamelist[kChn2_S0_Profile],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_Profile));
    snprintf(paramsnamelist[kChn2_S0_MIN_QP],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_MIN_QP));
    snprintf(paramsnamelist[kChn2_S0_MAX_QP],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_MAX_QP));
    snprintf(paramsnamelist[kChn2_S0_MINI_PROP],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_MINI_PROP));
    snprintf(paramsnamelist[kChn2_S0_MAXI_PROP],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_MAXI_PROP));
    snprintf(paramsnamelist[kChn2_S0_MinI_Qp],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_MinI_Qp));
    snprintf(paramsnamelist[kChn2_S0_I_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_I_QP));
    snprintf(paramsnamelist[kChn2_S0_P_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_P_QP));
    snprintf(paramsnamelist[kChn2_S0_B_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_B_QP));
    snprintf(paramsnamelist[kChn2_S0_DstFrmRate],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_S0_DstFrmRate));
    snprintf(paramsnamelist[kChn3_M_VENC_W],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_VENC_W));
    snprintf(paramsnamelist[kChn3_M_VENC_H],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_VENC_H));
    snprintf(paramsnamelist[kChn3_M_VENC_SAME_INPUT],   PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_VENC_SAME_INPUT));
    snprintf(paramsnamelist[kChn3_M_VENC_Type],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_VENC_Type));
    snprintf(paramsnamelist[kChn3_M_Gop],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_Gop));
    snprintf(paramsnamelist[kChn3_M_BitRate],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_BitRate));
    snprintf(paramsnamelist[kChn3_M_RcMode],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_RcMode));
    snprintf(paramsnamelist[kChn3_M_Profile],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_Profile));
    snprintf(paramsnamelist[kChn3_M_MIN_QP],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_MIN_QP));
    snprintf(paramsnamelist[kChn3_M_MAX_QP],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_MAX_QP));
    snprintf(paramsnamelist[kChn3_M_MINI_PROP],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_MINI_PROP));
    snprintf(paramsnamelist[kChn3_M_MAXI_PROP],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_MAXI_PROP));
    snprintf(paramsnamelist[kChn3_M_MinI_Qp],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_MinI_Qp));
    snprintf(paramsnamelist[kChn3_M_I_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_I_QP));
    snprintf(paramsnamelist[kChn3_M_P_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_P_QP));
    snprintf(paramsnamelist[kChn3_M_B_QP],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_B_QP));
    snprintf(paramsnamelist[kChn3_M_DstFrmRate],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_M_DstFrmRate));
    snprintf(paramsnamelist[kChn3_S0_VENC_W],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_VENC_W));
    snprintf(paramsnamelist[kChn3_S0_VENC_H],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_VENC_H));
    snprintf(paramsnamelist[kChn3_S0_VENC_SAME_INPUT],  PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_VENC_SAME_INPUT));
    snprintf(paramsnamelist[kChn3_S0_VENC_Type],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_VENC_Type));
    snprintf(paramsnamelist[kChn3_S0_Gop],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_Gop));
    snprintf(paramsnamelist[kChn3_S0_BitRate],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_BitRate));
    snprintf(paramsnamelist[kChn3_S0_RcMode],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_RcMode));
    snprintf(paramsnamelist[kChn3_S0_Profile],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_Profile));
    snprintf(paramsnamelist[kChn3_S0_MIN_QP],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_MIN_QP));
    snprintf(paramsnamelist[kChn3_S0_MAX_QP],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_MAX_QP));
    snprintf(paramsnamelist[kChn3_S0_MINI_PROP],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_MINI_PROP));
    snprintf(paramsnamelist[kChn3_S0_MAXI_PROP],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_MAXI_PROP));
    snprintf(paramsnamelist[kChn3_S0_MinI_Qp],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_MinI_Qp));
    snprintf(paramsnamelist[kChn3_S0_I_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_I_QP));
    snprintf(paramsnamelist[kChn3_S0_P_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_P_QP));
    snprintf(paramsnamelist[kChn3_S0_B_QP],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_B_QP));
    snprintf(paramsnamelist[kChn3_S0_DstFrmRate],       PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_S0_DstFrmRate));
    snprintf(paramsnamelist[kChn0_SnapPort],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_SnapPort));
    snprintf(paramsnamelist[kChn0_SnapLen],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_SnapLen));
    snprintf(paramsnamelist[kChn0_Snap_Delay],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Snap_Delay));
    snprintf(paramsnamelist[kChn0_Snap_W],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Snap_W));
    snprintf(paramsnamelist[kChn0_Snap_H],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Snap_H));
    snprintf(paramsnamelist[kChn0_Snap_Quality],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Snap_Quality));
    snprintf(paramsnamelist[kChn0_Snap_McastEnable],    PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Snap_McastEnable));
    snprintf(paramsnamelist[kChn0_Snap_McastIp],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Snap_McastIp));
    snprintf(paramsnamelist[kChn0_Snap_McastPort],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Snap_McastPort));
    snprintf(paramsnamelist[kChn0_Snap_McastFreq],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Snap_McastFreq));
    snprintf(paramsnamelist[kChn1_SnapPort],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_SnapPort));
    snprintf(paramsnamelist[kChn1_SnapLen],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_SnapLen));
    snprintf(paramsnamelist[kChn1_Snap_Delay],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Snap_Delay));
    snprintf(paramsnamelist[kChn1_Snap_W],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Snap_W));
    snprintf(paramsnamelist[kChn1_Snap_H],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Snap_H));
    snprintf(paramsnamelist[kChn1_Snap_Quality],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Snap_Quality));
    snprintf(paramsnamelist[kChn1_Snap_McastEnable],    PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Snap_McastEnable));
    snprintf(paramsnamelist[kChn1_Snap_McastIp],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Snap_McastIp));
    snprintf(paramsnamelist[kChn1_Snap_McastPort],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Snap_McastPort));
    snprintf(paramsnamelist[kChn1_Snap_McastFreq],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Snap_McastFreq));
    snprintf(paramsnamelist[kChn2_SnapPort],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_SnapPort));
    snprintf(paramsnamelist[kChn2_SnapLen],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_SnapLen));
    snprintf(paramsnamelist[kChn2_Snap_Delay],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Snap_Delay));
    snprintf(paramsnamelist[kChn2_Snap_W],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Snap_W));
    snprintf(paramsnamelist[kChn2_Snap_H],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Snap_H));
    snprintf(paramsnamelist[kChn2_Snap_Quality],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Snap_Quality));
    snprintf(paramsnamelist[kChn2_Snap_McastEnable],    PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Snap_McastEnable));
    snprintf(paramsnamelist[kChn2_Snap_McastIp],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Snap_McastIp));
    snprintf(paramsnamelist[kChn2_Snap_McastPort],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Snap_McastPort));
    snprintf(paramsnamelist[kChn2_Snap_McastFreq],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Snap_McastFreq));
    snprintf(paramsnamelist[kChn3_SnapPort],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_SnapPort));
    snprintf(paramsnamelist[kChn3_SnapLen],             PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_SnapLen));
    snprintf(paramsnamelist[kChn3_Snap_Delay],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Snap_Delay));
    snprintf(paramsnamelist[kChn3_Snap_W],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Snap_W));
    snprintf(paramsnamelist[kChn3_Snap_H],              PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Snap_H));
    snprintf(paramsnamelist[kChn3_Snap_Quality],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Snap_Quality));
    snprintf(paramsnamelist[kChn3_Snap_McastEnable],    PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Snap_McastEnable));
    snprintf(paramsnamelist[kChn3_Snap_McastIp],        PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Snap_McastIp));
    snprintf(paramsnamelist[kChn3_Snap_McastPort],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Snap_McastPort));
    snprintf(paramsnamelist[kChn3_Snap_McastFreq],      PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Snap_McastFreq));
    snprintf(paramsnamelist[kChn0_Osd_Enable],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Osd_Enable));
    snprintf(paramsnamelist[kChn0_Osd_Name],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Osd_Name));
    snprintf(paramsnamelist[kChn0_Osd_Color],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Osd_Color));
    snprintf(paramsnamelist[kChn0_Osd_X],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Osd_X));
    snprintf(paramsnamelist[kChn0_Osd_Y],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Osd_Y));
    snprintf(paramsnamelist[kChn0_Osd_Size],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Osd_Size));
    snprintf(paramsnamelist[kChn0_Osd_Transparent],     PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Osd_Transparent));
    snprintf(paramsnamelist[kChn0_Osd_BGColor],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_Osd_BGColor));
    snprintf(paramsnamelist[kChn1_Osd_Enable],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Osd_Enable));
    snprintf(paramsnamelist[kChn1_Osd_Name],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Osd_Name));
    snprintf(paramsnamelist[kChn1_Osd_Color],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Osd_Color));
    snprintf(paramsnamelist[kChn1_Osd_X],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Osd_X));
    snprintf(paramsnamelist[kChn1_Osd_Y],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Osd_Y));
    snprintf(paramsnamelist[kChn1_Osd_Size],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Osd_Size));
    snprintf(paramsnamelist[kChn1_Osd_Transparent],     PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Osd_Transparent));
    snprintf(paramsnamelist[kChn1_Osd_BGColor],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_Osd_BGColor));
    snprintf(paramsnamelist[kChn2_Osd_Enable],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Osd_Enable));
    snprintf(paramsnamelist[kChn2_Osd_Name],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Osd_Name));
    snprintf(paramsnamelist[kChn2_Osd_Color],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Osd_Color));
    snprintf(paramsnamelist[kChn2_Osd_X],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Osd_X));
    snprintf(paramsnamelist[kChn2_Osd_Y],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Osd_Y));
    snprintf(paramsnamelist[kChn2_Osd_Size],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Osd_Size));
    snprintf(paramsnamelist[kChn2_Osd_Transparent],     PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Osd_Transparent));
    snprintf(paramsnamelist[kChn2_Osd_BGColor],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_Osd_BGColor));
    snprintf(paramsnamelist[kChn3_Osd_Enable],          PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Osd_Enable));
    snprintf(paramsnamelist[kChn3_Osd_Name],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Osd_Name));
    snprintf(paramsnamelist[kChn3_Osd_Color],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Osd_Color));
    snprintf(paramsnamelist[kChn3_Osd_X],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Osd_X));
    snprintf(paramsnamelist[kChn3_Osd_Y],               PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Osd_Y));
    snprintf(paramsnamelist[kChn3_Osd_Size],            PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Osd_Size));
    snprintf(paramsnamelist[kChn3_Osd_Transparent],     PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Osd_Transparent));
    snprintf(paramsnamelist[kChn3_Osd_BGColor],         PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_Osd_BGColor));
    snprintf(paramsnamelist[kTickAuto],                 PARA_NAME_LEN, "%s=", EnumtoStr(kTickAuto));
    snprintf(paramsnamelist[kTickModule],               PARA_NAME_LEN, "%s=", EnumtoStr(kTickModule));
    snprintf(paramsnamelist[kTickRate],                 PARA_NAME_LEN, "%s=", EnumtoStr(kTickRate));
    snprintf(paramsnamelist[kTickOffset],               PARA_NAME_LEN, "%s=", EnumtoStr(kTickOffset));
    snprintf(paramsnamelist[kTickMDelay],               PARA_NAME_LEN, "%s=", EnumtoStr(kTickMDelay));
    snprintf(paramsnamelist[kTickServerIp],             PARA_NAME_LEN, "%s=", EnumtoStr(kTickServerIp));
    snprintf(paramsnamelist[kTickServerPort],           PARA_NAME_LEN, "%s=", EnumtoStr(kTickServerPort));
    snprintf(paramsnamelist[kChn0_VI_Module],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn0_VI_Module));
    snprintf(paramsnamelist[kChn1_VI_Module],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn1_VI_Module));
    snprintf(paramsnamelist[kChn2_VI_Module],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn2_VI_Module));
    snprintf(paramsnamelist[kChn3_VI_Module],           PARA_NAME_LEN, "%s=", EnumtoStr(kChn3_VI_Module));
    snprintf(paramsnamelist[kEdid],                     PARA_NAME_LEN, "%s=", EnumtoStr(kEdid));

}

SoftwareConfig::~SoftwareConfig()
{
    COMMON_PRT("SoftwareConfig Normal Exit!\n");
}

bool SoftwareConfig::ReadConfig()
{
    COMMON_PRT("read_config:%s and %s\n", netfilepath_, filepath_);

    read_ini rnet( netfilepath_ );
    for(uint i = 0; i <= kEth0Mac; i++){
        rnet.find_value( paramsnamelist[i],   configvalue_[i] );
    }

    read_ini ri( filepath_ );
    for(uint i = kEth0Mac+1; i < kSoftWareConfigIDMax; i++){
        ri.find_value( paramsnamelist[i],   configvalue_[i] );
    }

    return true;
}

bool SoftwareConfig::SaveNetConfig()
{
    FILE *f = NULL;
    f = fopen( netfilepath_, "wb" );
    if(f) {
        for(uint i = 0; i <= kEth0Mac; i++){
            fprintf(f, "%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
        }
        fflush(f);
        fclose(f);
        f = NULL;
        COMMON_PRT("save net config succeed");
        return true;
    }

    COMMON_PRT("save net config failed");
    return false;

}

bool SoftwareConfig::SaveConfig()
{
    FILE *f = NULL;
    f = fopen( filepath_, "wb" );
    if(f)
    {
        for(uint i = kEth0Mac+1; i < kSoftWareConfigIDMax; i++){
            fprintf(f, "%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
        }
        fflush(f);
        fclose(f);
        f = NULL;
        COMMON_PRT("save config succeed");
        return true;

    }

    COMMON_PRT("save config failed");
    return false;
}

void SoftwareConfig::PrintConfig()
{
    for(uint i = 0; i < kSoftWareConfigIDMax; i++){
        printf("%s%s\n", paramsnamelist[i], configvalue_[i].c_str());
    }
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, string value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = value;
    else
        return false;

    return true;
}

bool SoftwareConfig::SetConfig(const SoftWareConfigID kId, int value)
{
    if(kId < kSoftWareConfigIDMax)
        configvalue_[kId] = int2str(value);
    else
        return false;

    return true;
}

string SoftwareConfig::GetConfig(const SoftWareConfigID kId)
{
    if(kId < kSoftWareConfigIDMax)
        return configvalue_[kId];
    else
        return "error";
}

bool SoftwareConfig::LoadConfigToGlobal(bool bPrintThem)
{
    if(bPrintThem){

    }

    return true;
}

void SoftwareConfig::do_set_net(const char *kIp, const char *kMask, const char *kGateway, const char *kDns, const char* kEthName )
{
    if( 0 == strcmp( kIp, "0.0.0.0" ) ) {
        printf("[%s] ip set failed\n", kEthName);
        return;
    }

    char cmd[1024]={0};
    // set ip and netmask
    memset( cmd, 0, 1024 );
    sprintf( cmd, "ifconfig %s %s netmask %s", kEthName, kIp, kMask );
    system ( cmd );
    //log_i( "%s", cmd );
    // set dns
    memset( cmd, 0, 1024 );
    sprintf( cmd, "nameserver %s", kDns );
    FILE * f = fopen( DNS_NAME, "wb");
    if( f )
    {
        fwrite( cmd, strlen( (const char*)(cmd) ), 1, f );
        fclose( f );
    }

    if( 0 == strcmp( kGateway, "0.0.0.0" ) ) {
        printf("[%s] Gateway set failed\n", kEthName);
        return;
    }
    // del gateway
    sprintf( cmd, "route del default gw %s dev %s", "0.0.0.0", kEthName);
    system( cmd );
    //log_i( "%s", cmd );
    // set gateway
    memset ( cmd, 0, 1024 );
    sprintf( cmd, "route add default gw %s dev %s", kGateway, kEthName);
    system ( cmd );
    //log_i( "%s", cmd );
}

void SoftwareConfig::do_set_mac(const char* kMac, const char* kEthName )
{    
    if( 0 == strcmp( kMac, "00.00.00.00.00.00" ) ) {
     SetConfig(kEth0Mac,"00:00:00:00:00:00");
     SaveConfig();
        return;
    }

    if( 0 == strcmp( kMac, "00:00:00:00:00:00" ) ) {
        printf("[%s] mac set failed\n", kEthName);
        return;
    }

    char buffer[1024];
    sprintf(buffer, "ifconfig %s down", kEthName);
    system(buffer);
    //printf( "%s", buffer );

    sprintf(buffer, "ifconfig %s hw ether %s", kEthName, kMac );
    printf("%s", buffer);
    system(buffer);
    printf( "%s", buffer );

    sprintf(buffer, "ifconfig %s up", kEthName );
    system(buffer);
    printf( "%s", buffer );
}


void SoftwareConfig::SetNetwork()
{
    do_set_mac( configvalue_[kEth0Mac].c_str(), ETH0_NAME );
    do_set_net( configvalue_[kEth0Ip].c_str(), configvalue_[kEth0Mask].c_str(), configvalue_[kEth0Gateway].c_str(), configvalue_[kEth0Dns].c_str(), ETH0_NAME );
}
