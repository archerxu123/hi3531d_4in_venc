#融合4HDMI  4MIX输入，3.5mm音频测试完成，4MIX-HDMI音频未测试
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

##指定目标文件(obj)的存放目录
OBJECTS_DIR += ../obj
TARGET = hi3531d_edc_venc

#头文件包含路径

#INCLUDEPATH += /home/hwyin/work/xjh_4in/hi3531d_4in_venc-master/hi3531d_venc/common \
#               /home/hwyin/work/xjh_4in/hi3531d_4in_venc-master/hi3531d_venc/hi3531d_venc/hi3531d_common
INCLUDEPATH  +=/home/jhxu/work/gitclone/hi3531d_edc_venc/hi3531d_4in_venc/hi3531d_venc/common
                /home/jhxu/work/gitclone/hi3531d_edc_venc/hi3531d_4in_venc/hi3531d_venc/hi3531d_venc/hi3531d_common



INCLUDEPATH += /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/include \
               /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/extdrv/nvp6134_ex \
               /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/extdrv/tlv320aic31 \
               /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/extdrv/tp2823 \
               /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/extdrv/tp2827 \
               /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/extdrv/tp2853c \
               /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv400-freetype-2.4.4/include/freetype2


#引入的lib文件的路径  -L：引入路径
LIBS += /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libmpi.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libhdmi.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libive.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libjpeg6b.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libjpeg.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libtde.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libVoiceEngine.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libupvqe.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libdnvqe.a \
#        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libaacdec.a \
#        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv500-hi3531d-sdk-v1.0.4.0/lib/libaacenc.a
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv400-freetype-2.4.4/lib/libfreetype.a \
        /home/pubilc-tool-lib/Hisilicon/lib/arm-hisiv300-aacenc/libaacenc.a
LIBS += ./libcrtsps.so
LIBS += -lpthread -lm -ldl -lrt \
        -lstdc++

#不检测定义后未使用的参数错误等
QMAKE_CXXFLAGS += -Wno-unused-parameter \
                  -Wno-unused-but-set-variable \
                  -Wno-unused-but-set-parameter \
                  -Wno-narrowing \
                  -Wno-literal-suffix \
                  -std=c++11
QMAKE_CFLAGS += -Wno-unused-parameter \
                -Wno-unused-but-set-variable \
                -Wno-unused-but-set-parameter
#定义编译选项
DEFINES += __LINUX__
DEFINES += __DEBUG__

SOURCES += \
    himpp_master.cpp \
    main.cpp \
    ../common/common.cpp \
    software_config.cpp \
    udpsocket/udpsocket.cpp \
    hi35xx_comm_fun.cpp \
    http_server.cpp \
    mongoose/mongoose.c \
    cJSON.c \
    hi3531d_common/loadbmp.c \
    hi3531d_common/sample_comm_ivs.c \
    hi3531d_common/sample_comm_sys.c \
    hi3531d_common/sample_comm_vda.c \
    hi3531d_common/sample_comm_vdec.c \
    hi3531d_common/sample_comm_venc.c \
    hi3531d_common/sample_comm_vi.c \
    hi3531d_common/sample_comm_vo.c \
    hi3531d_common/sample_comm_vpss.c \
    audio_g711.cpp \
    hi3531d_common/sample_comm_audio.c \
    commandmap.cpp \
    ../common/uuid/gen_uuid.c

HEADERS += \
    himpp_master.h \
    ../common/singleton.h \
    ../common/readini.h \
    ../common/common.h \
    software_config.h \
    global.h \
    mytimer.h \
    udpsocket/networkinfo.h \
    udpsocket/udpsocket.h \
    tiny_rtsp_server_i.h \
    hi35xx_comm_fun.h \
    mongoose/mongoose.h \
    cJSON.h \
    http_handler.h \
    http_server.h \
    hi3531d_common/loadbmp.h \
    hi3531d_common/sample_comm.h \
    hi3531d_common/sample_comm_ivs.h \
    ../common/version.h \
    audio_g711.h \
    librtsps_i.h \
    aac/aac_encode.h \
    aac/aacenc.h \
    commandmap.h \
    ../common/version.h \
    ../common/uuid/uuid.h \
    ../common/uuid/uuidd.h \
    ../common/uuid/uuidP.h \
    ../common/uuid/uuid_types.h \
    cnFreeType.h \
    bmptoyuv.h


DEFINES += __EDC_VENC__
