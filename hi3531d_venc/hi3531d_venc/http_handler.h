#ifndef HTTP_HANDLER_H
#define HTTP_HANDLER_H

#include <iostream>
#include <common.h>

#include "http_server.h"
#include "software_config.h"
#include "global.h"

#define CODE_200 "200 OK"
#define CODE_202 "202 Accepted"
#define CODE_400 "400 Bad Request"
#define CODE_401 "401 Unauthorized"
#define CODE_402 "402 Payment Required"
#define CODE_403 "403 Forbidden"
#define CODE_404 "404 Not Found"
#define CODE_423 "423 Locked"
#define CODE_500 "500 Internal Server Error"
#define CODE_501 "501 Not Implemented"
#define CODE_503 "503 Service Unavailable"
#define CODE_505 "505 HTTP Version Not Supported"

using namespace std;
typedef SoftwareConfig Config; //把Class类SoftwareConfig的名称缩写以下

// 解url编码实现
char* urldecode(char* encd, char* decd)
{
    uint j,i;
    char *cd = (char*)encd;
    char p[2];
    j = 0;

    for( i = 0; i < strlen(cd); i++ ){
        memset( p, '\0', sizeof(p) );
        if( cd[i] != '%' ){
            decd[j++] = cd[i];
            continue;
        }

        p[0] = cd[++i];
        p[1] = cd[++i];

        p[0] = p[0] - 48 - ((p[0] >= 'A') ? 7 : 0) - ((p[0] >= 'a') ? 32 : 0);
        p[1] = p[1] - 48 - ((p[1] >= 'A') ? 7 : 0) - ((p[1] >= 'a') ? 32 : 0);
        decd[j++] = (p[0] * 16 + p[1]);

    }
    decd[j] = '\0';

    return decd;
}

/*
 * 功能： 在serialctrl程序与netserver程序之间传递一些东西(somthing)
 * 输入： const char *senddata：需要发送的数据
 *       char *reply：收到的回复数据
 *       int id = c_netmsgid：进程通信 消息队列id
 *       int type = c_netmsgtype：进程通信 消息队列type
 * 返回： bool
 * 日期： 2018.07.09
 * 作者： zh.sun
 */
bool transmit_sth(const char *senddata, char *reply, int id = c_netmsgid, int type = c_netmsgtype)
{
    /* 发送给serialctrl程序 */
    msg_st msgdata;
    sprintf(msgdata.mtext.msg, "%s", senddata);
    msgdata.mtype = type;
    if(msgsnd(id, (void*)&msgdata,
              strlen(msgdata.mtext.msg), 0) == -1){
        perror("msgsnd failed");

        return false;
    }

    uint time_ms   = 1000;
    uint max_count = 800;
    if(strstr(senddata, "updata:")){
        time_ms     = 1000000; // 1s
        max_count   = 60*15; // 15min
    }
    /* 接收由serialctrl程序发来的内容 */
    uint count = 0;
    memset(&msgdata, '\0', sizeof(struct msg_st));
    while( 1 ){
        // 阻塞读取。若设非阻塞，最后一个参数设为IPC_NOWAIT
        if(msgrcv(id, (void*)&msgdata,
                  BUFF_SIZE, type*2+1, IPC_NOWAIT) == -1){
            count++;
            if(count >= max_count) //total:500*1ms
                return false;
            usleep(time_ms); //1ms
            continue;
        }
        else
            break;
    }

    //    COMMON_PRT("recv serial reply: %s\n", msgdata.mtext);
    sprintf(reply, "%s", msgdata.mtext);

    return true;
}

bool ResetHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    reply_callback(c, CODE_200, "start reboot");
    system("reboot");

    return true;
}

bool UpdataHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    if(query_string.find("matrix.tar") != string::npos){
        COMMON_PRT("start reboot\n");
        reply_callback(c, CODE_200, "start reboot");
        system("reboot");
        return true;
    }

    char data[64] = "updata:";
    if((query_string.find(".mot") != string::npos) ||
       (query_string.find(".bin") != string::npos)){

        uint pos = 0;
        if( (pos = query_string.find("&")) )
            strncat(data, query_string.c_str(), pos);
        else
            strcat(data, query_string.c_str());
    }
    else if((query_string.find(".png") != string::npos)/* ||
                    (query_string.find(".jpg") != string::npos) ||
                    (query_string.find(".jpeg") != string::npos)*/){

        uint pos = 0;
        if( (pos = query_string.find("&")) )
            query_string = query_string.substr(0, pos);

        char cmd[256] = "";
        sprintf(cmd, "cp %s /home/matrix/Matrix_Web/imgs/logo", query_string.c_str());
        system(cmd);
        sprintf(cmd, "mv %s /home/matrix/Matrix_Web/imgs/logo.png", query_string.c_str());
        system(cmd);
        reply_callback(c, CODE_200, "succeed");
        return true;
    }
    else{
        reply_callback(c, CODE_400, "error");
        return false;
    }

    g_isupdata = true; //升级过程中，禁止UDP指令的转发

    char reply[BUFF_SIZE] = "";
    if( transmit_sth(data, reply) ) {
        /* 将单片机的回复内容原文，直接回复给http客户端 */
        reply_callback(c, CODE_200, reply);
        COMMON_PRT("echo data: %s\n", reply);
    }
    else
        reply_callback(c, CODE_500, "failed");

    g_isupdata = false;

    return true;
}

bool TransmitHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    char data[100] = "";
    if(!query_string.empty()){
        struct mg_str http_req;
        http_req.p   = query_string.c_str();
        http_req.len = query_string.length();
        mg_get_http_var(&http_req, "data", data, sizeof(data));
    }
    else
        reply_callback(c, CODE_400, "failed");

    char reply[BUFF_SIZE] = "";
    if( transmit_sth(data, reply) ) {
        /* 将单片机的回复内容原文，直接回复给http客户端 */
        reply_callback(c, CODE_200, reply);
        COMMON_PRT("echo data: %s\n", reply);
    }
    else
        reply_callback(c, CODE_500, "failed");

    return true;
}

bool SetosdHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    int  len = 0;
    char data[512] = "\0";
    if(!query_string.empty()){
        struct mg_str http_req;
        http_req.p   = query_string.c_str();
        http_req.len = query_string.length();

        auto http_server = Singleton<HttpServer>::getInstance();
        // 添加分组
        len = mg_get_http_var(&http_req, "addgroup", data, sizeof(data));
        if(len > 0){
            cJSON *obj = cJSON_CreateObject();
            cJSON_AddStringToObject(obj, "name", data); //添加键值对
            cJSON_AddArrayToObject(obj, "list");
            cJSON_AddItemToArray(http_server->planlistroot_, obj); //添加节点
        }

        //删除分组
        memset(data, '\0', sizeof(data));
        len = mg_get_http_var(&http_req, "removegroup", data, sizeof(data));
        if(len > 0){
            int i = 0;
            cJSON *p_child;
            cJSON_ArrayForEach(p_child, http_server->planlistroot_){
                if(p_child && cJSON_Object  == p_child->type){
                    if(!strcmp(p_child->child->string, "name") &&
                       !strcmp(p_child->child->valuestring, data)){
                        cJSON_DeleteItemFromArray(http_server->planlistroot_, i); //删除节点
                        break;
                    }
                    i++;
                }
            }
        }

        //添加场景
        memset(data, '\0', sizeof(data));
        len = mg_get_http_var(&http_req, "group", data, sizeof(data));
        if(len > 0){
            cJSON *p_child;
            cJSON_ArrayForEach(p_child, http_server->planlistroot_){
                if(p_child && cJSON_Object  == p_child->type){
                    if(!strcmp(p_child->child->string, "name") &&
                       !strcmp(p_child->child->valuestring, data)){

                        len = mg_get_http_var(&http_req, "additem", data, sizeof(data));
                        if(len > 0)
                            cJSON_AddItemToArray(p_child->child->next,
                                                 cJSON_CreateString(data));
                        else{
                            COMMON_PRT("can't find <additem>\n");
                            reply_callback(c, CODE_400, "failed");
                            return false;
                        }

                        len = mg_get_http_var(&http_req, "id", data, sizeof(data));
                        if(len > 0)
                            cJSON_AddItemToArray(p_child->child->next,
                                                 cJSON_CreateNumber(str2int(data)));
                        else{
                            COMMON_PRT("can't find <id>\n");
                            reply_callback(c, CODE_400, "failed");
                            return false;
                        }
                    }
                }
                else{
                    COMMON_PRT("planlist format error\n");
                    reply_callback(c, CODE_400, "failed");
                    return false;
                }
            }
        }

        //删除场景
        memset(data, '\0', sizeof(data));
        len = mg_get_http_var(&http_req, "removeitem", data, sizeof(data));
        if(len > 0){
            len = mg_get_http_var(&http_req, "id", data, sizeof(data));
            if(len > 0){
                cJSON *p_child;
                cJSON_ArrayForEach(p_child, http_server->planlistroot_){
                    if(p_child && cJSON_Object  == p_child->type){
                        cJSON *list = p_child->child->next;
                        if(!strcmp(list->string, "list") &&
                           list->type == cJSON_Array){
                            int i = 1;
                            cJSON *listchild;
                            cJSON_ArrayForEach(listchild, list){
                                if(listchild && cJSON_IsNumber(listchild)){
                                    if(listchild->valueint == str2int(data)){
                                        cJSON_DeleteItemFromArray(list, i);
                                        cJSON_DeleteItemFromArray(list, i-1);
                                        break;
                                    }
                                    i += 2;
                                }
                            }
                        }
                    }
                    else{
                        COMMON_PRT("planlist format error\n");
                        reply_callback(c, CODE_400, "failed");
                        return false;
                    }
                }
            }
            else{
                COMMON_PRT("can't find <id>\n");
                reply_callback(c, CODE_400, "failed");
                return false;
            }
        }

        printf("%s\n", cJSON_Print(http_server->planlistroot_));

        // 保存到场景配置文件 planlist.json 中
        char filename[20] = "planlist.json";
        FILE *f = fopen( filename, "wb" );
        if(f){
            stringstream ss;
            ss << cJSON_PrintUnformatted(http_server->planlistroot_);
            fwrite(ss.str().c_str(), 1, ss.str().size(), f);
            fclose(f);
            COMMON_PRT("save %s succeed\n", filename);
        }
        else{
            COMMON_PRT("save %s failed\n", filename);
            reply_callback(c, CODE_500, "failed");
            return false;
        }
    }
    else
        reply_callback(c, CODE_400, "failed");

    reply_callback(c, CODE_200, "succeed");

    return true;
}

bool SetnetHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    int  len = 0;
    char data[100] = "";
    if(!query_string.empty()){
        struct mg_str http_req;
        http_req.p   = query_string.c_str();
        http_req.len = query_string.length();

        auto thisini = Singleton<SoftwareConfig>::getInstance();
        len = mg_get_http_var(&http_req, "ip", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kEth0Ip, data);
        }

        len = mg_get_http_var(&http_req, "netmask", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kEth0Mask, data);
        }

        len = mg_get_http_var(&http_req, "gateway", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kEth0Gateway, data);
        }

        len = mg_get_http_var(&http_req, "dns0", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kEth0Dns, data);
        }

        len = mg_get_http_var(&http_req, "mac", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kEth0Mac, data);
        }

        len = mg_get_http_var(&http_req, "http_port", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kWebPort, data);
        }

        /*len = mg_get_http_var(&http_req, "dns1", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kEth0Mask, data);
        }

        len = mg_get_http_var(&http_req, "rtsp_port", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kEth0Gateway, data);
        }

        len = mg_get_http_var(&http_req, "dhcp_enable", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(SoftwareConfig::kWebPort, data);
        }*/


        thisini->SaveConfig();
        thisini->SaveConfig();
    }
    else
        reply_callback(c, CODE_400, "failed");

    reply_callback(c, CODE_200, "succeed");

    return true;
}

bool SetadvHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    int  len = 0;
    char data[100] = "";
    if(!query_string.empty()){
        struct mg_str http_req;
        http_req.p   = query_string.c_str();
        http_req.len = query_string.length();

        auto thisini = Singleton<Config>::getInstance();
        len = mg_get_http_var(&http_req, "ai_sprate", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(Config::kAiSampleRate, data);
            thisini->SaveConfig();
        }

        /*len = mg_get_http_var(&http_req, "aicodformat", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(Config::kAiCoderFormat, data);
            thisini->SaveConfig();
        }

        len = mg_get_http_var(&http_req, "aibitrate", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(Config::kAiBitRate, data);
            thisini->SaveConfig();
        }*/

        //thisini->SaveConfig();
    }
    else
        reply_callback(c, CODE_400, "failed");

    reply_callback(c, CODE_200, "succeed");

    return true;
}


bool SetserialHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    int  len = 0;
    char data[100] = "";
    if(!query_string.empty()){
        struct mg_str http_req;
        http_req.p   = query_string.c_str();
        http_req.len = query_string.length();

        stringstream ss;
        ss << "[serial]" << "\n";
        read_ini ri( "./serialctrl.ini" );

        len = mg_get_http_var(&http_req, "com_port", data, sizeof(data));
        if(len > 0){
            ss << "com_port="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "com_port=", value );
            ss << "com_port="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "buad_rate", data, sizeof(data));
        if(len > 0){
            ss << "buad_rate="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "buad_rate=", value );
            ss << "buad_rate="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "data_bits", data, sizeof(data));
        if(len > 0){
            ss << "data_bits="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "data_bits=", value );
            ss << "data_bits="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "parity", data, sizeof(data));
        if(len > 0){
            ss << "parity="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "parity=", value );
            ss << "parity="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "stop_bits", data, sizeof(data));
        if(len > 0){
            ss << "stop_bits="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "stop_bits=", value );
            ss << "stop_bits="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "vtime", data, sizeof(data));
        if(len > 0){
            ss << "vtime="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "vtime=", value );
            ss << "vtime="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "vmin", data, sizeof(data));
        if(len > 0){
            ss << "vmin="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "vmin=", value );
            ss << "vmin="  << value << "\n";
        }

        FILE * f = fopen( "./serialctrl.ini", "wb" );
        if(f){
            fwrite(ss.str().c_str(), 1, ss.str().size(), f);
            fclose(f);
            COMMON_PRT("save %s succeed\n", "./serialctrl.ini");
        }
        else{
            COMMON_PRT("save %s failed\n", "./serialctrl.ini");
            reply_callback(c, CODE_400, "failed");
            return false;
        }
    }
    else
        reply_callback(c, CODE_400, "failed");

    reply_callback(c, CODE_200, "succeed");

    return true;
}

bool SetpreviewHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    int  len = 0;
    int input, output;
    char data[100] = "";
    if(!query_string.empty()){
        struct mg_str http_req;
        http_req.p   = query_string.c_str();
        http_req.len = query_string.length();

        stringstream ss;
        ss << "[hi3531d venc]" << "\n";
        read_ini ri( "./hi3531d_venc.ini" );
        string value;
        ri.find_value( "vi_w=", value );
        ss << "vi_w="  << value << "\n";
        ri.find_value( "vi_h=", value );
        ss << "vi_h="  << value << "\n";

        /*http_ts_uri=%2F0.ts&http_flv_uri=%2F0.flv&rtsp_uri=%2F0;
        rtmp_publish_uri=rtmp%3A%2F%2F192.168.1.50%2Flive%2F0&rtmp_enable=0&http_ts_enable=0;
        http_flv_enable=0&rtsp_enable=1&http_hls_uri=%2F0.hls&http_hls_enable=0&multicast_es=0&mjpeg_enable=0&*/

        len = mg_get_http_var(&http_req, "input", data, sizeof(data));
        if(len > 0){
            input = data[0];
        }

        len = mg_get_http_var(&http_req, "output", data, sizeof(data));
        if(len > 0){
            output = data[0];
        }

        printf("input %d,output %d\n",input,output);

        if((input == 48)&&(output == 48)){  //主码流
            len = mg_get_http_var(&http_req, "venc_framerate", data, sizeof(data));
            if(len > 0){
                ss << "venc_dstframerate="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "venc_dstframerate=", value );
                ss << "venc_dstframerate="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_gop", data, sizeof(data));
            if(len > 0){
                ss << "gop="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "gop=", value );
                ss << "gop="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_width", data, sizeof(data));
            if(len > 0){
                ss << "venc_w="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "venc_w=", value );
                ss << "venc_w="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_height", data, sizeof(data));
            if(len > 0){
                ss << "venc_h="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "venc_h=", value );
                ss << "venc_h="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_width_height_same_as_input", data, sizeof(data));
            if(len > 0){
                ss << "venc_same_as_input="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "venc_same_as_input=", value );
                ss << "venc_same_as_input="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_rc_mode", data, sizeof(data));
            if(len > 0){
                ss << "rcmode="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "rcmode=", value );
                ss << "rcmode="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_bitrate", data, sizeof(data));
            if(len > 0){
                ss << "bit_rate="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "bit_rate=", value );
                ss << "bit_rate="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_profile", data, sizeof(data));
            if(len > 0){
                ss << "profile="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "profile=", value );
                ss << "profile="  << value << "\n";
            }

            /*len = mg_get_http_var(&http_req, "snap_len", data, sizeof(data));
        if(len > 0){
            ss << "snap_len="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_len=", value );
            ss << "snap_len="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "snap_w", data, sizeof(data));
        if(len > 0){
            ss << "snap_w="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_w=", value );
            ss << "snap_w="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "snap_h", data, sizeof(data));
        if(len > 0){
            ss << "snap_h="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_h=", value );
            ss << "snap_h="  << value << "\n";
        }*/

            len = mg_get_http_var(&http_req, "multicast_ip", data, sizeof(data));
            if(len > 0){
                ss << "mcast_ip="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "mcast_ip=", value );
                ss << "mcast_ip="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_port", data, sizeof(data));
            if(len > 0){
                ss << "mcast_port="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "mcast_port=", value );
                ss << "mcast_port="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_enable", data, sizeof(data));
            if(len > 0){
                ss << "mcast_enable="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "mcast_enable=", value );
                ss << "mcast_enable="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_size", data, sizeof(data));
            if(len > 0){
                ss << "mcast_len="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "mcast_len=", value );
                ss << "mcast_len="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_sleep", data, sizeof(data));
            if(len > 0){
                ss << "mcast_delay="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "mcast_delay=", value );
                ss << "mcast_delay="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "mjpeg_port", data, sizeof(data));
            if(len > 0){
                ss << "snap_port="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "snap_port=", value );
                ss << "snap_port="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_type", data, sizeof(data));
            if(len > 0){
                ss << "venc_type="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "venc_type=", value );
                ss << "venc_type="  << value << "\n";
            }
        }else if((input == 48)&&(output == 49)){  //次码流1
            len = mg_get_http_var(&http_req, "venc_framerate", data, sizeof(data));
            if(len > 0){
                ss << "minor1_venc_dstframerate="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_venc_dstframerate=", value );
                ss << "minor1_venc_dstframerate="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_gop", data, sizeof(data));
            if(len > 0){
                ss << "minor1_gop="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_gop=", value );
                ss << "minor1_gop="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_width", data, sizeof(data));
            if(len > 0){
                ss << "minor1_w="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_w=", value );
                ss << "minor1_w="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_height", data, sizeof(data));
            if(len > 0){
                ss << "minor1_h="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_h=", value );
                ss << "minor1_h="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_rc_mode", data, sizeof(data));
            if(len > 0){
                ss << "minor1_rcmode="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_rcmode=", value );
                ss << "minor1_rcmode="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_bitrate", data, sizeof(data));
            if(len > 0){
                ss << "minor1_bit_rate="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_bit_rate=", value );
                ss << "minor1_bit_rate="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_profile", data, sizeof(data));
            if(len > 0){
                ss << "minor1_profile="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_profile=", value );
                ss << "minor1_profile="  << value << "\n";
            }

            /*len = mg_get_http_var(&http_req, "snap_len", data, sizeof(data));
        if(len > 0){
            ss << "snap_len="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_len=", value );
            ss << "snap_len="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "snap_w", data, sizeof(data));
        if(len > 0){
            ss << "snap_w="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_w=", value );
            ss << "snap_w="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "snap_h", data, sizeof(data));
        if(len > 0){
            ss << "snap_h="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_h=", value );
            ss << "snap_h="  << value << "\n";
        }*/

            len = mg_get_http_var(&http_req, "multicast_ip", data, sizeof(data));
            if(len > 0){
                ss << "minor1_mcast_ip="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_mcast_ip=", value );
                ss << "minor1_mcast_ip="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_port", data, sizeof(data));
            if(len > 0){
                ss << "minor1_mcast_port="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_mcast_port=", value );
                ss << "minor1_mcast_port="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_enable", data, sizeof(data));
            if(len > 0){
                ss << "minor1_mcast_enable="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_mcast_enable=", value );
                ss << "minor1_mcast_enable="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_size", data, sizeof(data));
            if(len > 0){
                ss << "minor1_mcast_len="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_mcast_len=", value );
                ss << "minor1_mcast_len="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_sleep", data, sizeof(data));
            if(len > 0){
                ss << "minor1_mcast_delay="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_mcast_delay=", value );
                ss << "minor1_mcast_delay="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "mjpeg_port", data, sizeof(data));
            if(len > 0){
                ss << "minor1_snap_port="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_snap_port=", value );
                ss << "minor1_snap_port="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_type", data, sizeof(data));
            if(len > 0){
                ss << "minor1_venc_type="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor1_venc_type=", value );
                ss << "minor1_venc_type="  << value << "\n";
            }
        }else if((input == 48)&&(output == 50)){  //次码流2
            len = mg_get_http_var(&http_req, "venc_framerate", data, sizeof(data));
            if(len > 0){
                ss << "minor2_venc_dstframerate="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_venc_dstframerate=", value );
                ss << "minor2_venc_dstframerate="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_gop", data, sizeof(data));
            if(len > 0){
                ss << "minor2_gop="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_gop=", value );
                ss << "minor2_gop="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_width", data, sizeof(data));
            if(len > 0){
                ss << "minor2_w="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_w=", value );
                ss << "minor2_w="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_height", data, sizeof(data));
            if(len > 0){
                ss << "minor2_h="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_h=", value );
                ss << "minor2_h="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_rc_mode", data, sizeof(data));
            if(len > 0){
                ss << "minor2_rcmode="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_rcmode=", value );
                ss << "minor2_rcmode="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_bitrate", data, sizeof(data));
            if(len > 0){
                ss << "minor2_bit_rate="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_bit_rate=", value );
                ss << "minor2_bit_rate="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_profile", data, sizeof(data));
            if(len > 0){
                ss << "minor2_profile="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_profile=", value );
                ss << "minor2_profile="  << value << "\n";
            }

            /*len = mg_get_http_var(&http_req, "snap_len", data, sizeof(data));
        if(len > 0){
            ss << "snap_len="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_len=", value );
            ss << "snap_len="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "snap_w", data, sizeof(data));
        if(len > 0){
            ss << "snap_w="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_w=", value );
            ss << "snap_w="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "snap_h", data, sizeof(data));
        if(len > 0){
            ss << "snap_h="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_h=", value );
            ss << "snap_h="  << value << "\n";
        }*/

            len = mg_get_http_var(&http_req, "multicast_ip", data, sizeof(data));
            if(len > 0){
                ss << "minor2_mcast_ip="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_mcast_ip=", value );
                ss << "minor2_mcast_ip="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_port", data, sizeof(data));
            if(len > 0){
                ss << "minor2_mcast_port="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_mcast_port=", value );
                ss << "minor2_mcast_port="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_enable", data, sizeof(data));
            if(len > 0){
                ss << "minor2_mcast_enable="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_mcast_enable=", value );
                ss << "minor2_mcast_enable="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_size", data, sizeof(data));
            if(len > 0){
                ss << "minor2_mcast_len="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_mcast_len=", value );
                ss << "minor2_mcast_len="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_sleep", data, sizeof(data));
            if(len > 0){
                ss << "minor2_mcast_delay="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_mcast_delay=", value );
                ss << "minor2_mcast_delay="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "mjpeg_port", data, sizeof(data));
            if(len > 0){
                ss << "minor2_snap_port="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_snap_port=", value );
                ss << "minor2_snap_port="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_type", data, sizeof(data));
            if(len > 0){
                ss << "minor2_venc_type="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor2_venc_type=", value );
                ss << "minor2_venc_type="  << value << "\n";
            }
        }else if((input == 48)&&(output == 51)){  //次码流3
            len = mg_get_http_var(&http_req, "venc_framerate", data, sizeof(data));
            if(len > 0){
                ss << "minor3_venc_dstframerate="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_venc_dstframerate=", value );
                ss << "minor3_venc_dstframerate="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_gop", data, sizeof(data));
            if(len > 0){
                ss << "minor3_gop="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_gop=", value );
                ss << "minor3_gop="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_width", data, sizeof(data));
            if(len > 0){
                ss << "minor3_w="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_w=", value );
                ss << "minor3_w="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_height", data, sizeof(data));
            if(len > 0){
                ss << "minor3_h="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_h=", value );
                ss << "minor3_h="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_rc_mode", data, sizeof(data));
            if(len > 0){
                ss << "minor3_rcmode="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_rcmode=", value );
                ss << "minor3_rcmode="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_bitrate", data, sizeof(data));
            if(len > 0){
                ss << "minor3_bit_rate="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_bit_rate=", value );
                ss << "minor3_bit_rate="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_profile", data, sizeof(data));
            if(len > 0){
                ss << "minor3_profile="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_profile=", value );
                ss << "minor3_profile="  << value << "\n";
            }

            /*len = mg_get_http_var(&http_req, "snap_len", data, sizeof(data));
        if(len > 0){
            ss << "snap_len="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_len=", value );
            ss << "snap_len="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "snap_w", data, sizeof(data));
        if(len > 0){
            ss << "snap_w="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_w=", value );
            ss << "snap_w="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "snap_h", data, sizeof(data));
        if(len > 0){
            ss << "snap_h="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "snap_h=", value );
            ss << "snap_h="  << value << "\n";
        }*/

            len = mg_get_http_var(&http_req, "multicast_ip", data, sizeof(data));
            if(len > 0){
                ss << "minor3_mcast_ip="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_mcast_ip=", value );
                ss << "minor3_mcast_ip="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_port", data, sizeof(data));
            if(len > 0){
                ss << "minor3_mcast_port="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_mcast_port=", value );
                ss << "minor3_mcast_port="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_enable", data, sizeof(data));
            if(len > 0){
                ss << "minor3_mcast_enable="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_mcast_enable=", value );
                ss << "minor3_mcast_enable="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_size", data, sizeof(data));
            if(len > 0){
                ss << "minor3_mcast_len="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_mcast_len=", value );
                ss << "minor3_mcast_len="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "multicast_sleep", data, sizeof(data));
            if(len > 0){
                ss << "minor3_mcast_delay="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_mcast_delay=", value );
                ss << "minor3_mcast_delay="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "mjpeg_port", data, sizeof(data));
            if(len > 0){
                ss << "minor3_snap_port="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_snap_port=", value );
                ss << "minor3_snap_port="  << value << "\n";
            }

            len = mg_get_http_var(&http_req, "venc_type", data, sizeof(data));
            if(len > 0){
                ss << "minor3_venc_type="  << data << "\n";
            }
            else{
                string value;
                ri.find_value( "minor3_venc_type=", value );
                ss << "minor3_venc_type="  << value << "\n";
            }
        }

        auto thisini = Singleton<Config>::getInstance();
        len = mg_get_http_var(&http_req, "aenc_samplerate", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(Config::kAiSampleRate, data);
            thisini->SaveConfig();
        }

        /*len = mg_get_http_var(&http_req, "aicodformat", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(Config::kAiCoderFormat, data);
            thisini->SaveConfig();
        }

        len = mg_get_http_var(&http_req, "aibitrate", data, sizeof(data));
        if(len > 0){
            thisini->SetConfig(Config::kAiBitRate, data);
            thisini->SaveConfig();
        }*/

        /*len = mg_get_http_var(&http_req, "min_qp", data, sizeof(data));
        if(len > 0){
            ss << "min_qp="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "min_qp=", value );
            ss << "min_qp="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "max_qp", data, sizeof(data));
        if(len > 0){
            ss << "max_qp="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "max_qp=", value );
            ss << "max_qp="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "i_qp", data, sizeof(data));
        if(len > 0){
            ss << "i_qp="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "i_qp=", value );
            ss << "i_qp="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "p_qp", data, sizeof(data));
        if(len > 0){
            ss << "p_qp="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "p_qp=", value );
            ss << "p_qp="  << value << "\n";
        }*/

        len = mg_get_http_var(&http_req, "minor1_venc_type", data, sizeof(data));
        if(len > 0){
            ss << "minor1_venc_type="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_venc_type=", value );
            ss << "minor1_venc_type="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "minor1_gop", data, sizeof(data));
        if(len > 0){
            ss << "minor1_gop="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_gop=", value );
            ss << "minor1_gop="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "minor1_bit_rate", data, sizeof(data));
        if(len > 0){
            ss << "minor1_bit_rate="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_bit_rate=", value );
            ss << "minor1_bit_rate="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "minor1_rcmode", data, sizeof(data));
        if(len > 0){
            ss << "minor1_rcmode="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_rcmode=", value );
            ss << "minor1_rcmode="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "minor1_profile", data, sizeof(data));
        if(len > 0){
            ss << "minor1_profile="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_profile=", value );
            ss << "minor1_profile="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "minor1_min_qp", data, sizeof(data));
        if(len > 0){
            ss << "minor1_min_qp="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_min_qp=", value );
            ss << "minor1_min_qp="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "minor1_max_qp", data, sizeof(data));
        if(len > 0){
            ss << "minor1_max_qp="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_max_qp=", value );
            ss << "minor1_max_qp="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "minor1_i_qp", data, sizeof(data));
        if(len > 0){
            ss << "minor1_i_qp="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_i_qp=", value );
            ss << "minor1_i_qp="  << value << "\n";
        }

        len = mg_get_http_var(&http_req, "minor1_p_qp", data, sizeof(data));
        if(len > 0){
            ss << "minor1_p_qp="  << data << "\n";
        }
        else{
            string value;
            ri.find_value( "minor1_p_qp=", value );
            ss << "minor1_p_qp="  << value << "\n";
        }

        FILE * f = fopen( "./hi3531d_venc.ini", "wb" );
        if(f){
            fwrite(ss.str().c_str(), 1, ss.str().size(), f);
            fclose(f);
            COMMON_PRT("save %s succeed\n", "./hi3531d_venc.ini");
        }
        else{
            COMMON_PRT("save %s failed\n", "./hi3531d_venc.ini");
            reply_callback(c, CODE_400, "failed");
            return false;
        }
    }
    else
        reply_callback(c, CODE_400, "failed");

    reply_callback(c, CODE_200, "succeed");

    return true;
}

bool GetperplanHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    auto http_server = Singleton<HttpServer>::getInstance();

    reply_callback(c, CODE_200, cJSON_PrintUnformatted(http_server->planlistroot_));

    return true;
}

bool GetnetsettingHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());
    
    auto thisini = Singleton<Config>::getInstance();
    cJSON *root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "ip", string(thisini->GetConfig(SoftwareConfig::kEth0Ip)).c_str());
    cJSON_AddStringToObject(root, "mask", string(thisini->GetConfig(SoftwareConfig::kEth0Mask)).c_str());
    cJSON_AddStringToObject(root, "gateway", string(thisini->GetConfig(SoftwareConfig::kEth0Gateway)).c_str());
    cJSON_AddStringToObject(root, "web_port", string(thisini->GetConfig(SoftwareConfig::kWebPort)).c_str());

    reply_callback(c, CODE_200, cJSON_PrintUnformatted(root));
    //printf("net is %s\n",cJSON_PrintUnformatted(root));

    /*TiXmlPrinter printer;
    string xmlstr;

    // 创建XML的文档对象
    TiXmlDocument *xmlDocument = new TiXmlDocument();
    TiXmlDeclaration * decl = new TiXmlDeclaration("1.0", "UTF-8", "");
    xmlDocument->LinkEndChild(decl);
    // 根元素
    TiXmlElement *rootElement = new TiXmlElement("sys");
    // 连接到文档对象，作为根元素
    xmlDocument->LinkEndChild(rootElement);

    // 创建name元素和id元素
    TiXmlElement *ipElement = new TiXmlElement("ip");
    TiXmlElement *maskElement = new TiXmlElement("netmask");
    // 连接
    rootElement->LinkEndChild(ipElement);
    rootElement->LinkEndChild(maskElement);
    // 设置name元素的值
    TiXmlText *ipValue = new TiXmlText(string(thisini->GetConfig(SoftwareConfig::kEth0Ip)).c_str());
    ipElement->LinkEndChild(ipValue);
    TiXmlText *maskValue = new TiXmlText(string(thisini->GetConfig(SoftwareConfig::kEth0Mask)).c_str());
    maskElement->LinkEndChild(maskValue);
    xmlDocument->Accept(&printer);
    xmlstr = printer.CStr();
    printf("&&&&&&&&&&sys %s\n",printer.CStr());
    //return xmlstr;

    /*TiXmlDocument *root = CreateXMLobject();
    cJSON_AddStringToObject(root, "ip", string(thisini->GetConfig(SoftwareConfig::kEth0Ip)).c_str());
    cJSON_AddStringToObject(root, "mask", string(thisini->GetConfig(SoftwareConfig::kEth0Mask)).c_str());*/

    return true;
}

/*bool GetaudiosettingHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    auto thisini = Singleton<Config>::getInstance();
    cJSON *root = cJSON_CreateObject();
    cJSON_AddStringToObject(root, "ai_sprate", string(thisini->GetConfig(Config::kAiSampleRate)).c_str());
    cJSON_AddStringToObject(root, "aicodformat", string(thisini->GetConfig(Config::kAiCoderFormat)).c_str());
    cJSON_AddStringToObject(root, "aibitrate", string(thisini->GetConfig(Config::kAiBitRate)).c_str());

    reply_callback(c, CODE_200, cJSON_PrintUnformatted(root));

    return true;
}*/

/*
bool GetserialsettingHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    read_ini ri( "./serialctrl.ini" );
    string value;
    cJSON *root = cJSON_CreateObject();
    ri.find_value( "com_port=",     value );
    cJSON_AddStringToObject(root, "com_port", value.c_str());
    ri.find_value( "buad_rate=",    value );
    cJSON_AddStringToObject(root, "buad_rate", value.c_str());
    ri.find_value( "data_bits=",    value );
    cJSON_AddStringToObject(root, "data_bits", value.c_str());
    ri.find_value( "parity=",       value );
    cJSON_AddStringToObject(root, "parity", value.c_str());
    ri.find_value( "stop_bits=",    value );
    cJSON_AddStringToObject(root, "stop_bits", value.c_str());
    ri.find_value( "vtime=",        value );
    cJSON_AddStringToObject(root, "vtime", value.c_str());
    ri.find_value( "vmin=",         value );
    cJSON_AddStringToObject(root, "vmin", value.c_str());

    reply_callback(c, CODE_200, cJSON_PrintUnformatted(root));

    return true;
}

bool GetpreviewsettingHandler(string body, string query_string, mg_connection *c, OnRspCallback reply_callback)
{
    if(!body.empty())
        COMMON_PRT("body: %s\n", body.c_str());
    if(!query_string.empty())
        COMMON_PRT("query_string: %s\n", query_string.c_str());

    read_ini ri( "./hi3531d_venc.ini" );
    string value;
    cJSON *root = cJSON_CreateObject();
    ri.find_value( "vi_w=",         value );
    cJSON_AddStringToObject(root, "vi_w", value.c_str());
    ri.find_value( "vi_h=",         value );
    cJSON_AddStringToObject(root, "vi_h", value.c_str());
    ri.find_value( "venc_type=",    value );
    cJSON_AddStringToObject(root, "venc_type", value.c_str());
    ri.find_value( "gop=",          value );
    cJSON_AddStringToObject(root, "gop", value.c_str());
    ri.find_value( "bit_rate=",     value );
    cJSON_AddStringToObject(root, "bit_rate", value.c_str());
    ri.find_value( "rcmode=",       value );
    cJSON_AddStringToObject(root, "rcmode", value.c_str());
    ri.find_value( "profile=",      value );
    cJSON_AddStringToObject(root, "profile", value.c_str());
    ri.find_value( "min_qp=",       value );
    cJSON_AddStringToObject(root, "min_qp", value.c_str());
    ri.find_value( "max_qp=",       value );
    cJSON_AddStringToObject(root, "max_qp", value.c_str());
    ri.find_value( "i_qp=",         value );
    cJSON_AddStringToObject(root, "i_qp", value.c_str());
    ri.find_value( "p_qp=",         value );
    cJSON_AddStringToObject(root, "p_qp", value.c_str());
    ri.find_value( "venc_framerate=",   value );
    cJSON_AddStringToObject(root, "venc_framerate", value.c_str());
    ri.find_value( "ref_fb_dvi=",   value );
    cJSON_AddStringToObject(root, "ref_fb_dvi", value.c_str());
    ri.find_value( "dvi1_2=",       value );
    cJSON_AddStringToObject(root, "dvi1_2", value.c_str());
    ri.find_value( "snap_len=",     value );
    cJSON_AddStringToObject(root, "snap_len", value.c_str());

    reply_callback(c, CODE_200, cJSON_PrintUnformatted(root));

    return true;
}*/

#endif // HTTP_HANDLER_H
