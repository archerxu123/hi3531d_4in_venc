#ifndef SINGLETON_H
#define SINGLETON_H

#include <memory>  /* shared_ptr */
#include <iostream>
//#include <boost/noncopyable.hpp> /* 貌似可以不用这个noncopyable类 */

template<typename T>
class Singleton //: boost::noncopyable /* noncopyable类主要作用是把构造函数和析构函数设置成protected权限，这样子类可以调用，外面的类不能调用 */
{
public:
    static std::shared_ptr<T> getInstance()
    {
        pthread_once(&ponce_, &Singleton::init);
        return value_;
    }

private:
    Singleton();
    ~Singleton();

    static void init()
    {
        value_ = std::make_shared<T>(); //value_ = new T();
    }

private:
    static pthread_once_t ponce_;
    static std::shared_ptr<T> value_;
};

template<typename T>
pthread_once_t Singleton<T>::ponce_ = PTHREAD_ONCE_INIT;

template<typename T>
std::shared_ptr<T> Singleton<T>::value_ = nullptr;

#endif // SINGLETON_H
